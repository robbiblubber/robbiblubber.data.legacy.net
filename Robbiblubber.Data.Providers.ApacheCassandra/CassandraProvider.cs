﻿using System;
using System.Collections.Generic;
using System.Data;

using Cassandra.Data;

using Robbiblubber.Data.DDL;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Providers.ApacheCassandra
{
    /// <summary>This class implements the Cassandra database provider.</summary>
    public class CassandraProvider: ProviderBase, IProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public CassandraProvider(): base()
        {
            _Parser = new CassandraParser();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Provider configuration data.</param>
        public CassandraProvider(string data): base(data)
        {
            _Parser = new CassandraParser();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a provider for a connection string.</summary>
        /// <param name="connectionString">Connection string.</param>
        /// <returns>SQLite provider object.</returns>
        public static CassandraProvider ForConnectionsString(string connectionString)
        {
            Ddp ddp = new Ddp();
            ddp.Set("enhanced", connectionString);

            return new CassandraProvider(ddp.ToString());
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the Cassandra contact points.</summary>
        public string[] ContactPoints { get; set; }


        /// <summary>Gets or sets the default keyspace.</summary>
        public string DefaultKeyspace { get; set; } = null;


        /// <summary>Gets or sets the port.</summary>
        public int Port { get; set; } = -1;


        /// <summary>Gets or sets the user name.</summary>
        public string UserName { get; set; } = null;


        /// <summary>Gets or sets the user password.</summary>
        public string Password { get; set; } = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLProvider                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the provider configuration data.</summary>
        public override string Data
        {
            get
            {
                Ddp ddp = new Ddp();

                ddp.Set("provider", ProviderName);
                ddp.Set("contactpoints", ContactPoints);
                ddp.Set("keyspace", DefaultKeyspace);
                ddp.Set("port", Port);
                ddp.Set("user", UserName);
                ddp.Set("password", Password);
                ddp.Set("enhanced", EnhancedString);

                return ddp.ToString();
            }
            set
            {
                Ddp ddp = new Ddp(value);

                ContactPoints = ddp.Get<string[]>("contactpoints");
                DefaultKeyspace = ddp.Get<string>("keyspace", null);
                Port = ddp.Get<int>("port", -1);
                UserName = ddp.Get<string>("user", null);
                Password = ddp.Get<string>("password", null);
                EnhancedString = ddp.Get<string>("enhanced");
            }
        }


        /// <summary>Connects to the database.</summary>
        public override void Connect()
        {
            if(string.IsNullOrWhiteSpace(EnhancedString))
            {
                string cstr = "Contact Points=" + ContactPoints.Chain();

                if(!string.IsNullOrWhiteSpace(DefaultKeyspace)) { cstr += ";Default Keyspace=" + DefaultKeyspace; }
                if(Port > 0) { cstr += ";Port=" + Port.ToString(); }
                if(!string.IsNullOrWhiteSpace(UserName)) { cstr += ";Username=" + UserName; }
                if(!string.IsNullOrWhiteSpace(Password)) { cstr += ";Password=" + Password; }

                _Connection = new CqlConnection(cstr);
            }
            else
            {
                _Connection = new CqlConnection(EnhancedString);
            }

            _Connection.Open();
        }


        /// <summary>Returns the next sequence value of the given database sequence.</summary>
        /// <param name="sequence">Sequence name.</param>
        /// <returns>Next value.</returns>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support sequences.</exception>
        public override long NextValue(string sequence)
        {
            throw new NotSupportedException();
        }


        /// <summary>Returns the number of fields in a data reader.</summary>
        /// <param name="re">Data reader.</param>
        /// <returns>Number of fields.</returns>
        public override int CountFields(IDataReader re)
        {
            int rval = 0;

            try
            {
                while(true)
                {
                    re.GetName(rval);
                    rval++;
                }
            }
            catch(Exception) {}

            return rval;
        }



        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        /// <remarks>CHECK constraints will not be retrieved.</remarks>
        public override IDDLTable GetTable(string tableName)
        {
            /*
            DDLTable rval = new DDL(Parser).AddTable(tableName);

            IDbCommand cmd = null;
            IDataReader re = null;

            cmd = Connection.CreateCommand("DESCRIBE TABLE " + tableName);
            re = cmd.ExecuteReader();

            List<string> tmp = new List<string>();
            while(re.Read())
            {
                rval.AddColumn(re.GetString(1), ((CassandraParser) Parser).ToSQLDataType(re.GetString(2)), re.GetInt32(3) == 0, string.IsNullOrWhiteSpace(re.GetString(4)) ? null : re.GetString(4));
                if(re.GetInt32(5) != 0) { tmp.Add(re.GetString(1)); }
            }
            Disposal.Dispose(re, cmd);

            if(tmp.Count > 0) { rval.AddPrimaryKey(tmp.ToArray()); }

            cmd = Connection.CreateCommand("PRAGMA Foreign_Key_List(" + tableName + ")");
            re = cmd.ExecuteReader();

            while(re.Read())
            {
                rval.AddForeignKey(re.GetString(3), re.GetString(2), re.GetString(4), re.GetString(5).ToUpper() == "CASCADE");
            }
            Disposal.Dispose(re, cmd);

            cmd = Connection.CreateCommand("PRAGMA Index_List(" + tableName + ")");
            re = cmd.ExecuteReader();

            List<Tuple<string, bool>> ixs = new List<Tuple<string, bool>>();
            while(re.Read())
            {
                if(re.GetString(3).ToUpper() != "PK") { ixs.Add(new Tuple<string, bool>(re.GetString(1), re.GetInt32(2) != 0)); }
            }
            Disposal.Dispose(re, cmd);

            foreach(Tuple<string, bool> i in ixs)
            {
                cmd = Connection.CreateCommand("PRAGMA Index_List(" + tableName + ")");
                re = cmd.ExecuteReader();

                tmp = new List<string>();
                while(re.Read()) { tmp.Add(re.GetString(2)); }
                Disposal.Dispose(re, cmd);

                if(i.Item2)
                {
                    rval.AddNamedUniqueIndex(i.Item1, tmp.ToArray());
                }
                else { rval.AddNamedIndex(i.Item1, tmp.ToArray()); }
            }

            return rval;
            */

            // TODO: get Cassandra DDL
            return null;
        }


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        public override string[] GetTablesNames(string filter)
        {
            /*
            List<string> rval = new List<string>();

            if(string.IsNullOrWhiteSpace(filter)) { filter = "*"; }
            filter = filter.Replace("*", "%").Replace("?", "_");

            IDbCommand cmd = CreateCommand("SELECT NAME FROM SQLITE_MASTER WHERE TYPE = 'table' AND NAME LIKE :f ORDER BY NAME");
            cmd.AddParameter(":f", filter);
            IDataReader re = cmd.ExecuteReader();

            while(re.Read()) { rval.Add(re.GetString(0)); }
            Disposal.Recycle(re, cmd);

            return rval.ToArray();
            */

            // TODO: Get Cassandra table names
            return null;
        }
    }
}
