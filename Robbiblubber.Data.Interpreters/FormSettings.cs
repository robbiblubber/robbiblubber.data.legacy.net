﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class implements the settings window.</summary>
    internal partial class FormSettings: Form, ISettingsWindow
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Controls.</summary>
        private Dictionary<string, Control> _Controls = new Dictionary<string, Control>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        internal FormSettings()
        {
            InitializeComponent();

            DialogResult = DialogResult.Cancel;

            this.Localize();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal methods                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a settings page.</summary>
        /// <param name="page"></param>
        internal void AddPage(Settings.SettingsPage page)
        {
            _TabSettings.TabPages.Add(page.Key, page.Title);

            Control c = (Control) Activator.CreateInstance(page.Control);
            _TabSettings.TabPages[page.Key].Controls.Add(c);
            c.Dock = DockStyle.Fill;

            _Controls.Add(page.Key, c);

            this.Localize();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ISettingsWindow                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a control by key.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Control.</returns>
        public Control GetControl(string key)
        {
            return _Controls[key];
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Button "OK" click.</summary>
        private void _ButtonOK_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Button "Cancel" click.</summary>
        private void _ButtonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
