﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.PostgreSQL
{
    /// <summary>This class represents a PostgreSQL view.</summary>
    public class PgView: PgTable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        internal PgView(PgDatabase database, PgTablesFolder folder, string name): base(database, folder, name)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] PgTable                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.view; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "pg::view"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            _Children.Add(new PgColumnsFolder(Database, this));
            _Children.Add(new PgRulesFolder(Database, this));
            _Children.Add(new PgTriggersFolder(Database, this));
        }
        
        
        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                IDbCommand cmd = null;
                string rval = null;

                try
                {
                    cmd = Database.Provider.CreateCommand("SELECT VIEW_DEFINITION FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_SCHEMA = '" + Folder.Schema.Name + "' AND TABLE_NAME = '" + Name + "'");
                    rval = ((string) cmd.ExecuteScalar());
                }
                catch(Exception) {}

                Disposal.Dispose(cmd);

                return ((rval == null) ? rval : ("CREATE VIEW " + Name + " AS\r\n" + rval.TrimEnd(' ', ';') + ";"));
            }
        }


        /// <summary>Gets the qualified name for this object.</summary>
        public override string QualifiedName
        {
            get { return Folder.Schema.Name + "." + Name; }
        }
    }
}
