﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.SQLite
{
    /// <summary>This class represents a SQLite tables folder.</summary>
    public class LiteTablesFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        internal LiteTablesFolder(LiteDatabase database)
        {
            _Parent = Database = database;
            _Name = "sqlfwx::tree.folder.tables".Localize("Tables");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public LiteDatabase Database { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "lite::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("SELECT NAME FROM SQLITE_MASTER WHERE TYPE = 'table' ORDER BY NAME");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new LiteTable(Database, this, re.GetString(0)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0010b", ex); }

            Disposal.Dispose(re, cmd);

            _Children.Add(new LiteTable(Database, this, "SQLITE_MASTER"));
            _Children.Add(new LiteTable(Database, this, "SQLITE_TEMP_MASTER"));
        }
    }
}
