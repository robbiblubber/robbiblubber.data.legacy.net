﻿using Robbiblubber.Data.Interpreters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra index.</summary>
    public class CasIndex: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Table name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="kind">Index kind.</param>
        /// <param name="options">Index options.</param>
        internal CasIndex(CasIndexesFolder folder, string name, string kind, IDictionary<string, string> options)
        {
            _Parent = Folder = folder;
            _Name = name;

            Custom = (kind.ToLower() == "custom");
            OptionMap = "";

            foreach(KeyValuePair<string, string> i in options)
            {
                if(i.Key == "class_name")
                {
                    ClassName = i.Value;
                }
                else if(i.Key == "target")
                {
                    Target = i.Value.Replace("keys(", "KEYS(");
                }
                else
                {
                    if(OptionMap.Length > 0) { OptionMap += ",\r\n"; }
                    OptionMap += ("   '" + i.Key + "': '" + i.Value + "'");
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent table.</summary>
        public CasTable Table
        {
            get { return Folder.Table; }
        }


        /// <summary>Gets the parent folder.</summary>
        public CasIndexesFolder Folder { get; protected set; }


        /// <summary>Gets the keyspace.</summary>
        public CasKeyspace Keyspace
        {
            get { return Folder.Table.Keyspace; }
        }


        /// <summary>Gets the index target column(s).</summary>
        public string Target { get; protected set; }


        /// <summary>Gets if the index is a custom index.</summary>
        public bool Custom { get; protected set; }


        /// <summary>Gets the index class name.</summary>
        public string ClassName { get; protected set; }


        /// <summary>Gets the index option map.</summary>
        public string OptionMap { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return (Custom ? Resources.index_cust : Resources.index); }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return (Custom ? "cas::index_cust" : "cas::index") ; }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get 
            {
                string rval = "CREATE ";
                if(Custom) { rval += "CUSTOM "; }
                rval += ("INDEX " + Name + "\r\nON " + Keyspace.Name + "." + Table.Name + " (" + Target + ")");

                if(Custom)
                {
                    rval += ("\r\nUSING '" + ClassName + "'");

                    if(OptionMap.Length > 0)
                    {
                        rval += ("\r\nWITH OPTIONS =\r\n{\r\n" + OptionMap + "\r\n}");
                    }
                }
                rval += ";";

                return rval;
            }
        }
    }
}
