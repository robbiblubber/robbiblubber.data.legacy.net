﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server view.</summary>
    public class MsView: MsTable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        internal MsView(MsDatabase database, MsTablesFolder folder, string name): base(database, folder, name)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] PgTable                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.view; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mssql::view"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            _Children.Add(new MsColumnsFolder(Database, this));
            _Children.Add(new MsTriggersFolder(Database, this));
        }
        
        
        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                IDbCommand cmd = null;
                string rval = "";

                try
                {
                    cmd = Database.Provider.CreateCommand();
                    cmd.CommandText = "SELECT VIEW_DEFINITION FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_SCHEMA = '" + Folder.Schema.Name + "' AND TABLE_NAME = '" + Name + "'";

                    rval = ((string) cmd.ExecuteScalar());
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX01224", ex); }

                Disposal.Dispose(cmd);

                return rval;
            }
        }
    }
}
