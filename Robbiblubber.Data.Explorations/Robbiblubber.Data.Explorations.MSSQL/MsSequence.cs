﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server sequence.</summary>
    public class MsSequnece: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sequence name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        internal MsSequnece(MsDatabase database, MsSequencesFolder folder, string name)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public MsSequencesFolder Folder { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                return Resources.sequence;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                return "mssql::sequence";
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                IDbCommand cmd = null;
                IDataReader re = null;
                string rval = "";

                try
                {
                    cmd = Database.Provider.CreateCommand();
                    cmd.CommandText = "SELECT INCREMENT, MINIMUM_VALUE, MAXIMUM_VALUE, START_VALUE, CYCLE_OPTION FROM INFORMATION_SCHEMA.SEQUENCES WHERE SEQUENCE_SCHEMA = '" + Folder.Schema.Name + "' AND SEQUENCE_NAME = '" + Name + "'";
                    re = cmd.ExecuteReader();

                    rval = "CREATE SEQUENCE " + Folder.Schema.Name + "." + Name;
                    if(re.Read())
                    {
                        rval += ("\r\n   INCREMENT " + re.GetInt64(0).ToString());
                        rval += ("\r\n   MINVALUE " + re.GetInt64(1).ToString());
                        rval += ("\r\n   MAXVALUE " + re.GetInt64(2).ToString());
                        rval += ("\r\n   START " + re.GetInt64(3).ToString());

                        if(re.GetBoolean(4)) { rval += "\r\n   CYCLE"; }
                    }
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX01215", ex); }

                Disposal.Dispose(re, cmd);

                return rval + ";";
            }
        }


        /// <summary>Gets the qualified name for this object.</summary>
        public override string QualifiedName
        {
            get { return Folder.Schema.Name + "." + Name; }
        }
    }
}
