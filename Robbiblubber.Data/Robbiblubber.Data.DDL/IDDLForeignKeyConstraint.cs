﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>Classes representing a DDL foreign key constraint implement this interface.</summary>
    public interface IDDLForeignKeyConstraint: IDDLConstraint
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the constraint column name.</summary>
        string ColumnName { get; }


        /// <summary>Gets the referenced table name.</summary>
        string ReferencedTableName { get; }


        /// <summary>Gets the referenced column name.</summary>
        string ReferencedColumnName { get; }


        /// <summary>Gets the delete cascade option for this constraint.</summary>
        bool CascadeDelete { get; }
    }
}
