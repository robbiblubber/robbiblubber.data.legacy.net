﻿using System;
using System.Drawing;

using Robbiblubber.Util;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>Provioder managers implement this interface.</summary>
    public interface IProviderManager
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider name.</summary>
        string ProviderName { get; }


        /// <summary>Gets the manager display name.</summary>
        string DisplayName { get; }


        /// <summary>Gets the provider control.</summary>

        IProviderControl Control { get; }


        /// <summary>Gets the provider icon.</summary>
        Image ProviderIcon { get; }


        /// <summary>Gets the connction icon.</summary>
        Image ConnectionIcon { get; }


        /// <summary>Gets if the provider is valid.</summary>
        bool Valid { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Spawns a new provider.</summary>
        /// <param name="id">ID.</param>
        /// <param name="parent">Parent folder.</param>
        /// <returns>Provider.</returns>
        ProviderItem Spawn(string id, Folder parent);


        /// <summary>Spawns a saved provider.</summary>
        /// <param name="sec">Configuration data.</param>
        /// <param name="parent">Parent folder.</param>
        /// <returns>Provider.</returns>
        ProviderItem Spawn(DdpElement sec, Folder parent);
    }
}
