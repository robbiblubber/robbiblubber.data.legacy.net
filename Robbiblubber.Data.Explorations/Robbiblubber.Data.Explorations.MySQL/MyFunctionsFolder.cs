﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MySQL
{
    /// <summary>This class represents a MySQL functions folder.</summary>
    public class MyFunctionsFolder: MyProceduresFolder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="host">Host.</param>
        /// <param name="database">Parent database.</param>
        internal MyFunctionsFolder(MyHost host, MyDatabase database): base(host, database)
        {
            _Name = "Functions";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] MyProceduresFolder                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Host.Provider.CreateCommand();
                cmd.CommandText = "SELECT NAME FROM MYSQL.PROC WHERE TYPE = 'FUNCTION' AND DB = '" + Database.Name + "' ORDER BY NAME";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new MyFunction(Host, this, re.GetString(0)));
                }
            }
            catch(Exception) {}

            Disposal.Dispose(re, cmd);
        }
    }
}
