﻿using Robbiblubber.Data.SQL;
using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>This class represents a DDL column.</summary>
    public class DDLColumn: IDDLColumn
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        protected DDLColumn()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="table">Table.</param>
        /// <param name="columnName">Column name.</param>
        /// <param name="dataType">Data type.</param>
        /// <param name="notNull">Not null option.</param>
        /// <param name="defaultValue">Default value.</param>
        protected internal DDLColumn(IDDLTable table, string columnName, SQLDataType dataType, bool notNull = false, string defaultValue = null)
        {
            TableName = table.TableName;
            ColumnName = columnName;
            DataType = dataType;
            NotNull = notNull;
            DefaultValue = defaultValue;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnName">Column name.</param>
        /// <param name="dataType">Data type.</param>
        /// <param name="notNull">Not null option.</param>
        /// <param name="defaultValue">Default value.</param>
        protected internal DDLColumn(string tableName, string columnName, SQLDataType dataType, bool notNull = false, string defaultValue = null)
        {
            TableName = tableName;
            ColumnName = columnName;
            DataType = dataType;
            NotNull = notNull;
            DefaultValue = defaultValue;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDDLColumn                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the table name.</summary>
        public virtual string TableName 
        {
            get; protected set;
        }


        /// <summary>Gets the column name.</summary>
        public virtual string ColumnName { get; protected set; }


        /// <summary>Gets the column data type.</summary>
        public virtual SQLDataType DataType { get; protected set; }


        /// <summary>Gets if the column is not null.</summary>
        public virtual bool NotNull { get; protected set; }


        /// <summary>Gets a string representation of the default value.</summary>
        public virtual string DefaultValue { get; protected set; }
    }
}
