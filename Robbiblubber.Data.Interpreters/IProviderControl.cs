﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>Provider controls implement this interface.</summary>
    public interface IProviderControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the database provider.</summary>
        ProviderItem Provider { get; set; }


        /// <summary>Gets or sets the tree node.</summary>
        TreeNode Node { get; set; }
    }
}
