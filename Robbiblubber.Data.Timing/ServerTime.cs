﻿using System;
using System.Data;

using Robbiblubber.Data.Providers;



namespace Robbiblubber.Data.Timing
{
    /// <summary>This class represents the server time.</summary>
    public class ServerTime
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Provider.</summary>
        protected IProvider _Provider;

        /// <summary>Initialization flag.</summary>
        protected bool _IsInit = false;

        /// <summary>Server offset.</summary>
        protected TimeSpan _Offset;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider">Provider.</param>
        public ServerTime(IProvider provider)
        {
            _Provider = provider;
        }


        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets or sets a global server time instance.</summary>
        public static ServerTime Instance
        {
            get; set;
        }


        /// <summary>Gets the current server time.</summary>
        public static DateTime Now
        {
            get { return Instance.GetServerTime(); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the server time offset.</summary>
        public TimeSpan Offset
        {
            get
            {
                if(!_IsInit) 
                {
                    _IsInit = true;
                    IDbCommand cmd = _Provider.CreateCommand("SELECT " + _Provider.Parser.CurrentTimestampFunction + " " + _Provider.Parser.FromDual);
                    IDataReader re = cmd.ExecuteReader();

                    re.Read();
                    _Offset = (DateTime.Now - re.GetDateTime(0));
                    re.Close();
                    re.Dispose();
                    cmd.Dispose();
                }

                return _Offset;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the server time for a given time.</summary>
        /// <param name="time">Client time.</param>
        /// <returns>Server time.</returns>
        public DateTime GetServerTime(DateTime time)
        {
            return (time - Offset);
        }


        /// <summary>Returns the current server time.</summary>
        /// <returns>Server time.</returns>
        public DateTime GetServerTime()
        {
            return GetServerTime(DateTime.Now);
        }


        /// <summary>Returns the local time for a given server time.</summary>
        /// <param name="serverTime">Server time.</param>
        /// <returns>Local time.</returns>
        public DateTime GetLocalTime(DateTime serverTime)
        {
            return (serverTime + Offset);
        }


        /// <summary>Returns the current local time.</summary>
        /// <returns>Local time.</returns>
        public DateTime GetLocalTime()
        {
            return DateTime.Now;
        }
    }
}
