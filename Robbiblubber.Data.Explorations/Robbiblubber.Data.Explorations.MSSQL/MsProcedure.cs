﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server procedure.</summary>
    public class MsProcedure: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sequence name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="type">Procedure type.</param>
        internal MsProcedure(MsDatabase database, MsProceduresFolder folder, string name, string type)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
            Type = type;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public MsProceduresFolder Folder { get; private set; }


        /// <summary>Gets the procedure type code.</summary>
        public string Type { get; set; }


        /// <summary>Gets if the procedure is a function.</summary>
        public bool IsFunction
        {
            get { return ((Type == "FN") || (Type == "IF") || (Type == "TF")); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                switch(Type)
                {
                    case "PC":
                    case "AF":
                    case "FS":
                    case "FT":
                        return Resources.procedure_clr;
                    case "FN":
                        return Resources.function;
                    case "IF":
                        return Resources.function_if;
                    case "TF":
                        return Resources.function_tf;
                    case "RF":
                        return Resources.procedure_rf;
                    case "X":
                        return Resources.procedure_x;
                }

                return Resources.procedure;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                switch(Type)
                {
                    case "PC":
                    case "AF":
                    case "FS":
                    case "FT":
                        return "mssql::procedure/clr";
                    case "FN":
                        return "mssql::function";
                    case "IF":
                        return "mssql::function/if";
                    case "TF":
                        return "mssql::function/tf";
                    case "RF":
                        return "mssql::procedure/rf";
                    case "X":
                        return "mssql::procedure/x";
                }

                return "mssql::procedure";
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                IDbCommand cmd = null;
                IDataReader re = null;
                string rval = "";

                try
                {
                    cmd = Database.Provider.CreateCommand();
                    cmd.CommandText = "sp_helptext '" + QualifiedName + "'";
                    re = cmd.ExecuteReader();
                    
                    while(re.Read())
                    {
                        if(re.GetString(0).Trim().ToLower().StartsWith("--sp_helptext")) continue;
                        if(string.IsNullOrWhiteSpace(re.GetString(0))) continue;
                        rval += re.GetString(0);
                    }
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX01211", ex); }

                Disposal.Dispose(re, cmd);

                return rval.Trim().TrimEnd(';').TrimEnd() + ";";
            }
        }


        /// <summary>Gets the qualified name for this object.</summary>
        public override string QualifiedName
        {
            get { return Folder.Schema.Name + "." + Name; }
        }
    }
}
