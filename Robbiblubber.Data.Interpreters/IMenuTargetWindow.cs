﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This interface defines a menu taget window</summary>
    public interface IMenuTargetWindow
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // events                                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Occurs when the menu is opening.</summary>
        event MenuTargetEventHandler MenuOpening;


        /// <summary>Occurs when the menu is opened.</summary>
        event MenuTargetEventHandler MenuOpened;


        /// <summary>Occurs when the menu is closing.</summary>
        event MenuTargetEventHandler MenuClosing;


        /// <summary>Occurs when the menu is closed.</summary>
        event MenuTargetEventHandler MenuClosed;


        /// <summary>Occors when a menu item is clicked.</summary>
        event MenuTargetEventHandler MenuSelected;


        /// <summary>Occurs when a key down event is raised by the tree view.</summary>
        event MenuTargetKeyEventHandler TreeKeyDown;


        /// <summary>Occurs when a key up event is raised by the tree view.</summary>
        event MenuTargetKeyEventHandler TreeKeyUp;


        /// <summary>Occurs when a double click event is raised by the tree view.</summary>
        event MenuTargetEventHandler TreeDoubleClick;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the drop down menu items associated with this provider.</summary>
        IList<ToolStripItem> DropDownMenus { get; }


        /// <summary>Gets the database menu items associated with this provider.</summary>
        IList<ToolStripItem> DatabaseMenus { get; }


        /// <summary>Gets the target window.</summary>
        ITargetWindow TargetWindow { get; }
    }
}
