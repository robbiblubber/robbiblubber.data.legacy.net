﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using Npgsql;

using Robbiblubber.Data.DDL;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Providers.PostgreSQL
{
    /// <summary>This class implements the PostgreSQL database provider.</summary>
    public class PostgreProvider: ProviderBase, IProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public PostgreProvider(): base()
        {
            _Parser = new PostgreParser();
            Port = 5432;
        }



        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Provider configuration data.</param>
        public PostgreProvider(string data): base(data)
        {
            _Parser = new PostgreParser();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the server.</summary>
        public string Server { get; set; }


        /// <summary>Gets or sets the port.</summary>
        public int Port { get; set; }


        /// <summary>Gets or sets the database name.</summary>
        public string Database { get; set; }


        /// <summary>Gets or sets the user ID.</summary>
        public string UserID { get; set; }


        /// <summary>Gets or sets the password.</summary>
        [PasswordPropertyTextAttribute]
        public string Password { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the table DDL for a number of tables.</summary>
        /// <param name="schemaName">Schama name.</param>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table DDL.</returns>
        public virtual IDDLTable[] GetTables(string schemaName, string filter)
        {
            List<IDDLTable> rval = new List<IDDLTable>();
            foreach(string i in GetTablesNames(schemaName, filter)) { rval.Add(GetTable(i)); }

            return rval.ToArray();
        }


        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="schemaName">Schema name.</param>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        public virtual IDDLTable GetTable(string schemaName, string tableName)
        {
            return GetTable(schemaName + "." + tableName);
        }


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="schemaName">Schema name.</param>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        public virtual string[] GetTablesNames(string schemaName, string filter)
        {
            if(filter == null) { filter = "%"; }
            filter = filter.Replace("*", "%").Replace("?", "_");

            IDbCommand cmd;
            List<string> rval = new List<string>();

            if(schemaName == null)
            {
                cmd = CreateCommand("SELECT CURRENT_SCHEMA");
                schemaName = (string) cmd.ExecuteScalar();
                Disposal.Dispose(cmd);
            }

            cmd = CreateCommand("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = :s AND TABLE_NAME LIKE :f ORDER BY TABLE_NAME");
            cmd.AddParameter(":s", schemaName);
            cmd.AddParameter(":f", filter);
            IDataReader re = cmd.ExecuteReader();

            while(re.Read())
            {
                rval.Add(schemaName + "." + re.GetString(0));
            }
            Disposal.Dispose(re, cmd);

            return rval.ToArray();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLProvider                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the provider configuration data.</summary>
        public override string Data
        {
            get
            {
                Ddp ddp = new Ddp();

                ddp["provider"].Value = ProviderName;
                ddp["server"].Value = Server;
                ddp["port"].Value = Port;
                ddp["database"].Value = Database;
                ddp["userid"].Value = UserID;
                ddp["password"].Value = Password;
                ddp["enhanced"].Value = EnhancedString;

                return ddp.ToString();
            }
            set
            {
                Ddp ddp = new Ddp(value);

                Server = ddp["server"];
                Port = ddp["port"];
                Database = ddp["database"];
                UserID = ddp["userid"];
                Password = ddp["password"];
                EnhancedString = ddp["enhanced"];
            }
        }


        /// <summary>Connects to the database.</summary>
        public override void Connect()
        {
            _Connection = new NpgsqlConnection("Server=" + Server + ";Port=" + Port.ToString() + ";Database=" + Database + ";User ID=" + UserID + ";Password=" + Password + ";");
            _Connection.Open();
        }


        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        public override IDDLTable GetTable(string tableName)
        {
            string[] tab = tableName.Split('.');

            DDLTable rval = new DDLBuilder(Parser).AddTable(tableName);

            IDbCommand cmd = CreateCommand("SELECT COLUMN_NAME, IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, COLUMN_DEFAULT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = :s AND TABLE_NAME = :t ORDER BY ORDINAL_POSITION");
            cmd.AddParameter(":s", tab[0]);
            cmd.AddParameter(":t", tab[1]);
            IDataReader re = cmd.ExecuteReader();

            while(re.Read())
            {
                string ty = re.GetString(2);
                if(!re.IsDBNull(3)) { ty = "(" + re.GetInt32(3).ToString() + ")"; }
                rval.AddColumn(re.GetString(0), ((PostgreParser) Parser).ToSQLDataType(ty), re.GetString(1).ToUpper() == "NO", re.GetString(4));
            }
            Disposal.Dispose(re, cmd);

            List<string> tmp = new List<string>();
            cmd = CreateCommand("SELECT U.COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS C, INFORMATION_SCHEMA.KEY_COLUMN_USAGE U WHERE C.TABLE_NAME = U.TABLE_NAME AND C.TABLE_SCHEMA = U.TABLE_SCHEMA AND C.TABLE_SCHEMA = :s AND C.TABLE_NAME = :t AND C.CONSTRAINT_TYPE = 'PRIMARY KEY' AND U.POSITION_IN_UNIQUE_CONSTRAINT IS NULL");
            cmd.AddParameter(":s", tab[0]);
            cmd.AddParameter(":t", tab[1]);
            re = cmd.ExecuteReader();

            while(re.Read())
            {
                tmp.Add(re.GetString(0));
            }
            Disposal.Dispose(re, cmd);
            if(tmp.Count > 0) rval.AddPrimaryKey(tmp.ToArray());

            cmd = CreateCommand("SELECT X.CONSTRAINT_NAME, U.COLUMN_NAME, X.TABLE_SCHEMA, X.TABLE_NAME, X.COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS C, INFORMATION_SCHEMA.KEY_COLUMN_USAGE U, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE X " +
                                "WHERE C.CONSTRAINT_NAME = U.CONSTRAINT_NAME AND X.CONSTRAINT_NAME = C.CONSTRAINT_NAME AND C.CONSTRAINT_TYPE = 'FOREIGN KEY' " +
                                "AND C.TABLE_SCHEMA = :s AND C.TABLE_NAME = :t");
            cmd.AddParameter(":s", tab[0]);
            cmd.AddParameter(":t", tab[1]);
            re = cmd.ExecuteReader();

            while(re.Read())
            {
                rval.AddNamedForeignKey(re.GetString(0), re.GetString(1), re.GetString(2) + "." + re.GetString(3), re.GetString(4));
            }
            Disposal.Dispose(re, cmd);

            cmd = CreateCommand("SELECT INDEXNAME, Lower(INDEXDEF) FROM PG_CATALOG.PG_INDEXES WHERE SCHEMANAME = :s AND TABLENAME = :t ORDER BY INDEXNAME");
            cmd.AddParameter(":s", tab[0]);
            cmd.AddParameter(":t", tab[1]);
            re = cmd.ExecuteReader();

            List<Tuple<string, bool>> idx = new List<Tuple<string, bool>>();
            while(re.Read())
            {
                idx.Add(new Tuple<string, bool>(re.GetString(0), re.GetString(1).Contains(" unique ")));
            }
            Disposal.Dispose(re, cmd);

            foreach(Tuple<string, bool> i in idx)
            {
                cmd = CreateCommand("SELECT A.ATTNAME FROM PG_CATALOG.PG_CLASS T, PG_CATALOG.PG_CLASS I, PG_CATALOG.PG_INDEX X, PG_CATALOG.PG_NAMESPACE N, PG_CATALOG.PG_ATTRIBUTE A " +
                                    "WHERE T.OID = X.INDRELID AND I.OID = X.INDEXRELID AND N.OID = T.RELNAMESPACE AND A.ATTRELID = T.OID AND A.ATTNUM = ANY(X.INDKEY) " +
                                    "AND T.RELKIND = 'r' AND N.NSPNAME = :s AND T.RELNAME = :t AND I.RELNAME = :i ORDER BY I.RELNAME");
                cmd.AddParameter(":s", tab[0]);
                cmd.AddParameter(":t", tab[1]);
                cmd.AddParameter(":i", i.Item1);
                re = cmd.ExecuteReader();

                List<string> cols = new List<string>();
                while(re.Read())
                {
                    cols.Add(re.GetString(0));
                }
                Disposal.Dispose(re, cmd);

                if(i.Item2) { rval.AddNamedUniqueIndex(i.Item1, cols.ToArray()); } else { rval.AddNamedIndex(i.Item1, cols.ToArray()); }
            }

            return rval;
        }


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        public override string[] GetTablesNames(string filter)
        {
            string[] tab = (filter.Contains(".") ? filter.Split('.') : (new string[] { null, filter }));
            return GetTablesNames(tab[0], tab[1]);
        }


        /// <summary>Gets the table DDL for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table DDL.</returns>
        public override IDDLTable[] GetTables(string filter)
        {
            string[] tab = (filter.Contains(".") ? filter.Split('.') : (new string[] { null, filter }));
            return GetTables(tab[0], tab[1]);
        }
    }
}
