﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Util.Localization;

namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class provides a table editor.</summary>
    public partial class FormDataEditor: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Provider.</summary>
        protected ProviderItem _Provider;

        /// <summary>Table name.</summary>
        protected string _Table;

        /// <summary>Data table.</summary>
        protected DataTable _Data = null;

        /// <summary>Key fields.</summary>
        protected string[] _Keys;

        /// <summary>Updating cell.</summary>
        protected Tuple<int, int, object> _UpdatingCell = null;

        /// <summary>New row flag.</summary>
        protected bool _NewRow = false;

        /// <summary>Delete stack.</summary>
        protected List<IDbCommand> _DeleteStack = new List<IDbCommand>();

        /// <summary>Grid editing control.</summary>
        protected TextBoxBase _InternalCurrentlyEditingCell = null;

        /// <summary>Grid layout restored flag.</summary>
        protected bool _LayoutRestored = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider">Provider.</param>
        /// <param name="table">Table name.</param>
        /// <param name="initialFilter">Initial filter.</param>
        /// <param name="keys">Primary keys.</param>
        public FormDataEditor(ProviderItem provider, string table, string initialFilter, IEnumerable<string> keys)
        {
            InitializeComponent();

            Text = (table + " - Data Editor");
            Icon = Icon.FromHandle(Resources.edit_table.GetHicon());

            _Provider = provider;
            _Table = table;
            _Keys = keys.ToArray();

            Ddp ddp = Ddp.Open(PathOp.SystemUserConfigurationPath + @"\robbiblubber.org\SQL\tables.filter");
            _TextFilter.Text = ddp.Get<string>(provider.ID + "/" + table, initialFilter);

            this.RestoreLayout();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets if the editor will use transactions.</summary>
        public bool WithTransaction
        {
            get; set;
        } = true;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Shows the form as a modal dialog.</summary>
        /// <param name="loadData">Load data.</param>
        public virtual void ShowDialog(bool loadData)
        {
            if(loadData) _MenuRefresh_Click(null, null);
            ShowDialog();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected properties                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the current edit control.</summary>
        protected TextBoxBase _CurrentlyEditingCell
        {
            get
            {
                if(_InternalCurrentlyEditingCell != null)
                {
                    try
                    {
                        if(_InternalCurrentlyEditingCell.IsDisposed)
                        {
                            _InternalCurrentlyEditingCell = null;
                        }
                        else { _InternalCurrentlyEditingCell.GetType(); }
                    }
                    catch(Exception) { _InternalCurrentlyEditingCell = null; }
                }

                return _InternalCurrentlyEditingCell;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the value of a cell before update.</summary>
        /// <param name="row">Row.</param>
        /// <param name="col">Column.</param>
        /// <returns>Value.</returns>
        protected object _OldValueOf(int row, int col)
        {
            if(_UpdatingCell != null)
            {
                if((_UpdatingCell.Item1 == row) && (_UpdatingCell.Item2 == col)) { return _UpdatingCell.Item3; }
            }

            return _GridTable.Rows[row].Cells[col].Value;
        }


        /// <summary>Gets the current value of a cell before update.</summary>
        /// <param name="row">Row.</param>
        /// <param name="col">Column.</param>
        /// <returns>Value.</returns>
        protected object _CurrentValueOf(int row, int col)
        {
            return _GridTable.Rows[row].Cells[col].Value;
        }


        /// <summary>Gets the column index for a key.</summary>
        /// <param name="i">Key index.</param>
        /// <returns>Column index.</returns>
        protected int _GetKeyColumn(int i)
        {
            return _GridTable.Columns[_Keys[i]].Index;
        }



        /// <summary>Adds a where clause to a command.</summary>
        /// <param name="cmd">Command object.</param>
        /// <param name="row">Row index.</param>
        protected void _AddWhere(IDbCommand cmd, int row)
        {
            cmd.CommandText += " WHERE ";

            for(int i = 0; i < _Keys.Length; i++)
            {
                if(i > 0) { cmd.CommandText += " AND "; }

                if(_OldValueOf(row, _GetKeyColumn(i)) == null)
                {
                    cmd.CommandText += (_Keys[i] + " " + _Provider.Parser.EqualNullOperator + " NULL");
                }
                else
                {
                    cmd.CommandText += (_Keys[i] + " = " + _Provider.Parser.ToBindVariableName(_Keys[i]));
                    cmd.AddParameter(_Provider.Parser.ToBindVariableName(_Keys[i]), _OldValueOf(row, _GetKeyColumn(i)));
                }
            }
        }


        /// <summary>Adds a where clause to a command.</summary>
        /// <param name="cmd">Command object.</param>
        /// <param name="row">Row.</param>
        protected void _AddWhere(IDbCommand cmd, DataGridViewRow row)
        {
            cmd.CommandText += " WHERE ";

            for(int i = 0; i < _Keys.Length; i++)
            {
                if(i > 0) { cmd.CommandText += " AND "; }

                if(row.Cells[_GetKeyColumn(i)].Value == null)
                {
                    cmd.CommandText += (_Keys[i] + " " + _Provider.Parser.EqualNullOperator + " NULL");
                }
                else
                {
                    cmd.CommandText += (_Keys[i] + " = " + _Provider.Parser.ToBindVariableName(_Keys[i]));
                    cmd.AddParameter(_Provider.Parser.ToBindVariableName(_Keys[i]), row.Cells[_GetKeyColumn(i)].Value);
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Menu "Refresh" click.</summary>
        private void _MenuRefresh_Click(object sender, EventArgs e)
        {
            string query = "SELECT * FROM " + _Table;
            if(!string.IsNullOrWhiteSpace(_TextFilter.Text)) { query += (" " + _TextFilter.Text.Trim()); }

            try
            {
                _Data = _Provider.CreateDataTable(query);
                _GridTable.DataSource = _Data;

                if(!_LayoutRestored) { _GridTable.RestoreLayout(_Table); }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Table Acces Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>End cell edit.</summary>
        private void _GridTable_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if(_NewRow) return;

            object newValue = _GridTable.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
            if(Equals(newValue, _UpdatingCell.Item3)) return;

            IDbCommand cmd = _Provider.CreateCommand("UPDATE " + _Table + " SET " + _GridTable.Columns[e.ColumnIndex].Name + " = ");
            if(newValue == null)
            {
                cmd.CommandText += "NULL";
            }
            else if((newValue.ToString().Trim() == "") && (_Data.Columns[_GridTable.Columns[e.ColumnIndex].Name].DataType != typeof(string)))
            {
                cmd.CommandText += "NULL";
            }
            else
            {
                cmd.CommandText += _Provider.Parser.ToBindVariableName("new_" + _GridTable.Columns[e.ColumnIndex].Name);
                cmd.AddParameter("new_" + _GridTable.Columns[e.ColumnIndex].Name, _GridTable.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
            }
            _AddWhere(cmd, e.RowIndex);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex) 
            {
                MessageBox.Show(ex.Message, "Update Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _GridTable.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = _UpdatingCell.Item3;
            }
            cmd.Dispose();

            _UpdatingCell = null;
        }


        /// <summary>Begin cell edit.</summary>
        private void _GridTable_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            _UpdatingCell = new Tuple<int, int, object>(e.RowIndex, e.ColumnIndex, _GridTable.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
        }


        /// <summary>Data error handler.</summary>
        private void _GridTable_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("The value is not valid for this column.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            e.Cancel = true;
        }


        /// <summary>Row added.</summary>
        private void _GridTable_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            _NewRow = true;
        }

        private void _GridTable_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            if(!_NewRow) return;

            IDbCommand cmd = _Provider.CreateCommand("INSERT INTO " + _Table + " (");
            string vals = "";
            for(int i = 0; i < _GridTable.Columns.Count; i++)
            {
                if(i > 0) { cmd.CommandText += ", "; vals += ", "; }
                cmd.CommandText += _GridTable.Columns[i].Name;
                
                object v = _GridTable.Rows[e.RowIndex].Cells[i].Value;
                if(v == null)
                {
                    vals += "NULL";
                }
                else if((v.ToString().Trim() == "") && (_Data.Columns[_GridTable.Columns[i].Name].DataType != typeof(string)))
                {
                    vals += "NULL";
                }
                else
                {
                    vals += _Provider.Parser.ToBindVariableName(_GridTable.Columns[i].Name);
                    cmd.AddParameter(_Provider.Parser.ToBindVariableName(_GridTable.Columns[i].Name), v);
                }
            }
            cmd.CommandText += (") VALUES (" + vals + ")");

            try
            {
                cmd.ExecuteNonQuery();
                _NewRow = false;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Insert Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _GridTable.Rows[e.RowIndex].Cells[e.ColumnIndex].Selected = true;
            }
        }


        /// <summary>Row deleting.</summary>
        private void _GridTable_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if(e.Row.IsNewRow) return;

            _TimerTime.Enabled = false;
            IDbCommand cmd = _Provider.CreateCommand("DELETE FROM " + _Table);
            _AddWhere(cmd, e.Row);

            _DeleteStack.Add(cmd);
            _TimerTime.Enabled = true;
        }


        /// <summary>Delete timer tick.</summary>
        private void _TimerTime_Tick(object sender, EventArgs e)
        {
            _TimerTime.Enabled = false;

            IDbCommand[] del = _DeleteStack.ToArray();
            _DeleteStack.Clear();

            if(MessageBox.Show("Do you want to delete $(0)?".Replace("$(0)", (del.Length == 1) ? "this row" : "these rows"), "Delete Rows", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                IDbTransaction t = (WithTransaction ? _Provider.Connection.BeginTransaction() : null);

                try
                {
                    foreach(IDbCommand i in del)
                    {
                        i.Transaction = t;
                        i.ExecuteNonQuery();
                    }

                    if(t != null) { t.Commit(); }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Delete Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if(t != null) { t.Rollback(); }

                    _MenuRefresh_Click(null, null);
                }
            }
            else { _MenuRefresh_Click(null, null); }
        }


        /// <summary>Menu "Close" click.</summary>
        private void _MenuClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Grid editing control showing.</summary>
        private void _GridTable_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if(e.Control is TextBoxBase) { _InternalCurrentlyEditingCell = (TextBoxBase) e.Control; }
        }


        /// <summary>Menu "Cut" click.</summary>
        private void _MenuCut_Click(object sender, EventArgs e)
        {
            if(_CurrentlyEditingCell != null) { _CurrentlyEditingCell.Cut(); }
        }


        /// <summary>Menu "Undo" click.</summary>
        private void _MenuUndo_Click(object sender, EventArgs e)
        {
            if((_CurrentlyEditingCell != null) && (_CurrentlyEditingCell.CanUndo)) { _CurrentlyEditingCell.Undo(); }
        }


        /// <summary>Menu "Copy" click.</summary>
        private void _MenuCopy_Click(object sender, EventArgs e)
        {
            if(_CurrentlyEditingCell != null) { _CurrentlyEditingCell.Copy(); }
        }


        /// <summary>Menu "Paste" click.</summary>
        private void _MenuPaste_Click(object sender, EventArgs e)
        {
            if(_CurrentlyEditingCell != null) { _CurrentlyEditingCell.Paste(); }
        }


        /// <summary>Menu "Find" click.</summary>
        private void _MenuFind_Click(object sender, EventArgs e)
        {
            _FindSearch.Show();
            _FindSearch.BringToFront();
        }


        /// <summary>Find clicked.</summary>
        private void _FindSearch_Search(object sender, EventArgs e)
        {
            _GridTable.Find(FindControl.CurrentSearchText);
        }


        /// <summary>Menu "Find Next" clicked.</summary>
        private void _MenuFindNext_Click(object sender, EventArgs e)
        {
            try
            {
                if(string.IsNullOrEmpty(_FindSearch.SearchText))
                {
                    _MenuFind_Click(sender, e);
                }
                else
                {
                    _GridTable.Find(FindControl.CurrentSearchText);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLW00137", ex); }
        }


        /// <summary>Form closing.</summary>
        private void FormDataEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            Ddp ddp = Ddp.Open(PathOp.SystemUserConfigurationPath + @"\robbiblubber.org\SQL\tables.filter");
            ddp.Set(_Provider.ID + "/" + _Table, _TextFilter.Text);
            ddp.Save();

            this.SaveLayout();
            _GridTable.SaveLayout(_Table);
        }
    }
}
