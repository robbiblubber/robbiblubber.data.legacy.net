﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle columns folder.</summary>
    public class OraClusterColumnsFolder: OraColumnsFolder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="cluster">Parent cluster.</param>
        internal OraClusterColumnsFolder(OraDatabase database, OraCluster cluster): base(database, null)
        {
            _Parent = Cluster = cluster;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent cluster.</summary>
        public OraCluster Cluster { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("SELECT CLU_COLUMN_NAME FROM SYS.DBA_CLU_COLUMNS WHERE OWNER = '" + Cluster.Folder.Schema.Name + "' AND CLUSTER_NAME = '" + Cluster.Name + "' ORDER BY CLU_COLUMN_NAME");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new OraColumn(Database, this, re.GetString(0), "", false, false, false));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX02406", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
