﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>Database item base class.</summary>
    public abstract class DbItem: IAutoCompletionKeyword
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Children.</summary>
        protected List<DbItem> _Children = null;


        /// <summary>Parent object.</summary>
        protected DbItem _Parent = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public abstract string Name { get; }
        

        /// <summary>Gets the item image.</summary>
        public abstract Image Icon { get; }


        /// <summary>Gets the icon key.</summary>
        public abstract string IconKey { get; }
        

        /// <summary>Gets if the object has children.</summary>
        public virtual bool HasChildren
        {
            get { return true; }
        }

        /// <summary>Gets the item children.</summary>
        public virtual IEnumerable<DbItem> Children
        {
            get
            {
                if(_Children == null) _Load();
                return _Children;
            }
        }

        
        /// <summary>Gets the object display text.</summary>
        public virtual string DisplayText
        {
            get { return ""; }
        }


        /// <summary>Gets if the object defines a query.</summary>
        public virtual bool HasQuery
        {
            get { return false; }
        }


        /// <summary>Gets the query for this object.</summary>
        public virtual string Query
        {
            get { return null; }
        }


        /// <summary>Gets the qualified name for this object.</summary>
        public virtual string QualifiedName
        {
            get { return Name; }
        }


        /// <summary>Gets the parent object.</summary>
        public virtual DbItem Parent
        {
            get { return _Parent; }
        }


        /// <summary>Gets if the object is a folder.</summary>
        public virtual bool IsFolder
        {
            get { return GetType().Name.EndsWith("Folder"); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads child objects.</summary>
        protected virtual void _Load()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes the item.</summary>
        public virtual void Refresh()
        {
            _Children = null;
        }
        

        /// <summary>Gets an item.</summary>
        /// <param name="name">Full qualified name.</param>
        /// <returns>Item.</returns>
        public DbItem GetItem(string name)
        {
            string n = null;
            DbItem rval = null;

            if(name.Contains('.'))
            {
                n = name.Substring(name.IndexOf('.') + 1).ToLower();
                name = name.Substring(0, name.IndexOf('.')).ToLower();
            }
            else { name = name.ToLower(); }

            if(HasChildren)
            {
                foreach(DbItem i in Children)
                {
                    if(i.IsFolder)
                    {
                        if((rval = i.GetItem(name)) != null) break;
                    }
                    else
                    {
                        if(i.Name.ToLower() == name)
                        {
                            rval = i;
                            break;
                        }
                    }
                }
            }

            if((rval != null) && (n != null))
            {
                rval = rval.GetItem(n);
            }

            return rval;
        }


        /// <summary>Returns if the current object is ancestor of an object.</summary>
        /// <param name="item">Item.</param>
        /// <param name="includeSelf">Include the item itself.</param>
        /// <returns>Returns TRUE if the object is an ancestor.</returns>
        public bool AncestorOf(DbItem item, bool includeSelf)
        {
            DbItem par = (includeSelf ? item : item.Parent);

            while(par != null)
            {
                if(par == this) return true;
                par = par.Parent;
            }

            return false;
        }


        /// <summary>Gets all ancestors of this item.</summary>
        /// <returns>Array of ancestors.</returns>
        public DbItem[] GetAncestors()
        {
            List<DbItem> rval = new List<DbItem>();
            DbItem par = Parent;

            while(par != null)
            {
                rval.Add(par);
                par = par.Parent;
            }

            return rval.ToArray();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IAutoCompletionKeyword                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the key word.</summary>
        string IAutoCompletionKeyword.Word
        {
            get { return Name; }
        }


        /// <summary>Gets the image key.</summary>
        string IAutoCompletionKeyword.ImageKey
        {
            get { return IconKey; }
        }


        /// <summary>Gets if the keyword should be italic.</summary>
        bool IAutoCompletionKeyword.Italic
        {
            get { return false; }
        }


        /// <summary>Gets if the keyword should be bold.</summary>
        bool IAutoCompletionKeyword.Bold
        {
            get { return false; }
        }
    }
}
