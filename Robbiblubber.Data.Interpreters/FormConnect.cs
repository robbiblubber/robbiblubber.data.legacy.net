﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class provides the direct connect window.</summary>
    public partial class FormConnect: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormConnect()
        {
            InitializeComponent();

            Icon = Icon.FromHandle(new Bitmap(Resources.connect_database).GetHicon());

            foreach(IProviderManager i in ProviderManager.Managers)
            {
                _IlistConnect.Images.Add(i.ProviderName, i.ProviderIcon);
            }

            foreach(IProviderManager i in ProviderManager.Managers)
            {
                _ComboProvider.Items.Add(new RichComboItem(i.DisplayName, i.ProviderName, i));
            }

            try
            {
                _ComboProvider.SelectedIndex = 0;
            }
            catch(Exception)
            {
                MessageBox.Show("sqlfwx::udiag.noproviders.text".Localize("There are no database providers available."), "sqlfwx::udiag.noproviders.caption".Localize("Connect"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Close();
            }

            this.Localize();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the selected provider.</summary>
        public ProviderItem Provider { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Selected provider changed.</summary>
        private void _ComboProvider_SelectedIndexChanged(object sender, EventArgs e)
        {
            IProviderManager p = (IProviderManager) ((RichComboItem) _ComboProvider.SelectedItem).Tag;
            if(!_PanelSettings.Controls.Contains((Control) p.Control))
            {
                _PanelSettings.Controls.Add((Control) p.Control);
                ((Control) p.Control).Dock = DockStyle.Fill;
            }

            Provider = p.Control.Provider = p.Spawn((string) null, null);
            ((Control) p.Control).BringToFront();

            _PanelSettings.Visible = true;
        }
    }
}
