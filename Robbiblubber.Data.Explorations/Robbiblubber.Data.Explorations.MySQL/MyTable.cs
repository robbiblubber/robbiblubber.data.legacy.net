﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MySQL
{
    /// <summary>This class represents a MySQL table.</summary>
    public class MyTable: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Table name.</summary>
        protected string _Name;


        /// <summary>Definition.</summary>
        protected string _Definition = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="host">Host.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        internal MyTable(MyHost host, MyTablesFolder folder, string name)
        {
            Host = host;
            _Parent = Folder = folder;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the host instance.</summary>
        public MyHost Host { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public MyTablesFolder Folder { get; private set; }

        
        /// <summary>Gets the colums folder.</summary>
        public MyColumnsFolder Columns
        {
            get
            {
                foreach(DbItem i in Children)
                {
                    if(i is MyColumnsFolder) { return ((MyColumnsFolder) i); }
                }

                return null;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.table; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "my::table"; }
        }

        
        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            
            _Children.Add(new MyColumnsFolder(Host, this));
            _Children.Add(new MyIndexesFolder(Host, this));
            _Children.Add(new MyTriggersFolder(Host, this));
        }
        

        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = "DDL not available.";

                try
                {
                    IDbCommand cmd = Host.Provider.CreateCommand();
                    cmd.CommandText = "SHOW CREATE TABLE " + Folder.Database.Name + "." + Name; ;
                    IDataReader re = cmd.ExecuteReader();

                    if(re.Read())
                    {
                        rval = re.GetString(1);
                    }
                    Disposal.Dispose(re, cmd);
                }
                catch(Exception) {}

                return rval;
            }
        }


        /// <summary>Gets if the object defines a query.</summary>
        public override bool HasQuery
        {
            get { return true; }
        }


        /// <summary>Gets the query for this object.</summary>
        public override string Query
        {
            get
            {
                string f = "";
                bool first = true;
                
                foreach(MyColumn i in Columns.Children)
                {
                    if(first)
                    {
                        first = false;
                    }
                    else { f += ", "; }

                    f += i.Name;
                }
                return "SELECT " + f + " FROM " + QualifiedName;
            }
        }


        /// <summary>Gets the qualified name for this object.</summary>
        public override string QualifiedName
        {
            get { return Folder.Database.Name + "." + Name; }
        }
    }
}
