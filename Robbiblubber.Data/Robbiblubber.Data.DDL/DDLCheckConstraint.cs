﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>This class represents a DDL check constraint.</summary>
    public class DDLCheckConstraint: DDLConstraint, IDDLCheckConstraint, IDDLConstraint
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        protected DDLCheckConstraint(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="table">Table.</param>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="condition">Check condition.</param>
        protected internal DDLCheckConstraint(IDDLTable table, string constraintName, string condition): base(table, constraintName)
        {
            Condition = condition;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="table">Table.</param>
        /// <param name="condition">Check condition.</param>
        protected internal DDLCheckConstraint(IDDLTable table, string condition): base(table, null)
        {
            Condition = condition;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="condition">Check condition.</param>
        protected internal DDLCheckConstraint(string tableName, string constraintName, string condition): base(tableName, constraintName)
        {
            Condition = condition;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="condition">Check condition.</param>
        protected internal DDLCheckConstraint(string tableName, string condition): base(tableName, null)
        {
            Condition = condition;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDDLCheckConstraint                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the constraint condition.</summary>
        public virtual string Condition { get; protected set; }
    }
}
