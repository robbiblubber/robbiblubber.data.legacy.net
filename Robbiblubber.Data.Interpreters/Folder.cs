﻿using System;
using System.Collections.Generic;
using System.Linq;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class represents a folder.</summary>
    public sealed class Folder: ISortable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Root folder.</summary>
        private static Folder _Root = null;
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parent">Parent folder.</param>
        public Folder(Folder parent)
        {
            Folders = new List<Folder>();
            Providers = new List<ProviderItem>();

            if((parent == null) || (parent.Items.Count() == 0))
            {
                SortIndex = 0;
            }
            else
            {
                SortIndex = (parent.Items.Max(m => m.SortIndex) + 1);
            }

            ID = StringOp.Unique(24);
            Parent = parent;

            if(Parent == null)
            {
                _Root = this;
            }
            else
            {
                Parent.Folders.Add(this);
            }
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="cfg">Configuration file.</param>
        /// <param name="sec">Configuration data.</param>
        /// <param name="import">Import flag.</param>
        public Folder(Ddp cfg, DdpElement sec, bool import = false)
        {
            Folders = new List<Folder>();
            Providers = new List<ProviderItem>();

            ID = sec.Name.Substring(8);
            Name = sec.Get<string>("name");
            SortIndex = sec.Get<int>("sort");

            if(import)
            {
                Parent = null;
            }
            else if(string.IsNullOrWhiteSpace(sec.Get<string>("parent")))
            {
                Parent = null;
                _Root = this;
            }
            else
            {
                Parent = FolderByID(sec.Get<string>("parent"));
                Parent.Folders.Add(this);
            }

            foreach(DdpElement i in cfg.Sections.Where(m => ((m.Name.StartsWith("folder::")) && (m.Get<string>("parent") == ID))))
            {
                new Folder(cfg, i);
            }

            foreach(DdpElement i in cfg.Sections.Where(m => ((m.Name.StartsWith("provider::")) && (m.Get<string>("parent") == ID))))
            {
                try
                {
                    ProviderManager.GetManager(Ddp.Create(i.Get<string>("data")).Get<string>("provider")).Spawn(i, this);
                }
                catch(Exception ex)
                {
                    DebugOp.Dump("SQLFWX04104", ex);
                    new UnknownProviderItem(i, this);
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the folder with a given ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Folder.</returns>
        public static Folder FolderByID(string id)
        {
            return FolderByID(null, id);
        }


        /// <summary>Returns the folder with a given ID.</summary>
        /// <param name="parent">Parent folder.</param>
        /// <param name="id">ID.</param>
        /// <returns>Folder.</returns>
        public static Folder FolderByID(Folder parent, string id)
        {
            if(parent == null) { parent = _Root; }

            if(parent.ID == id) { return parent; }

            Folder rval;
            foreach(Folder i in parent.Folders)
            {
                if((rval = FolderByID(i, id)) != null) { return rval; }
            }

            return null;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the folders in this folder.</summary>
        public List<Folder> Folders { get; private set; }


        /// <summary>Gets the providers in this folder.</summary>
        public List<ProviderItem> Providers { get; private set; }


        /// <summary>Gets all items.</summary>
        public IEnumerable<ISortable> Items
        {
            get { return ((IEnumerable<ISortable>) Folders).Union(Providers).OrderBy(n => n.SortIndex); }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves this folder.</summary>
        /// <param name="cfg">Configuration file.</param>
        public void Save(Ddp cfg)
        {
            DdpElement sec = cfg["folder::" + ID];

            sec.Set("parent", (Parent == null) ? "NULL" : Parent.ID);
            sec.Set("name", Name);
            sec.Set("sort", SortIndex);

            foreach(Folder i in Folders) { i.Save(cfg); }
            foreach(ProviderItem i in Providers)
            {
                i.Save(cfg["provider::" + i.ID]);
            }
        }


        /// <summary>Moves an item into the folder.</summary>
        /// <param name="item">Item.</param>
        public void MoveHere(ISortable item)
        {
            if(Items.Count() == 0)
            {
                item.SortIndex = 0;
            }
            else
            {
                item.SortIndex = Items.Max(m => m.SortIndex) + 1;
            }

            if(item is Folder)
            {
                if(item.Parent != null) { item.Parent.Folders.Remove((Folder) item); }
                Folders.Add((Folder) item);
            }
            else
            {
                if(item.Parent != null) { item.Parent.Providers.Remove((ProviderItem) item); }
                Providers.Add((ProviderItem) item);
            }
            item.Parent = this;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal methods                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns if a child can move up.</summary>
        /// <param name="child">Child item.</param>
        /// <returns>Returns TRUE if the child can move up.</returns>
        internal bool __CanMoveUp(ISortable child)
        {
            foreach(ISortable i in Items) { if(i.SortIndex < child.SortIndex) return true; }
            return false;
        }


        /// <summary>Returns if a child can move down.</summary>
        /// <param name="child">Child item.</param>
        /// <returns>Returns TRUE if the child can move down.</returns>
        internal bool __CanMoveDown(ISortable child)
        {
            foreach(ISortable i in Items) { if(i.SortIndex > child.SortIndex) return true; }
            return false;
        }


        /// <summary>Moves a child up.</summary>
        /// <param name="child">Child item.</param>
        internal void __MoveUp(ISortable child)
        {
            ISortable prev = null;

            foreach(ISortable i in Items)
            {
                if(i.SortIndex >= child.SortIndex)
                {
                    int v = prev.SortIndex;
                    prev.SortIndex = child.SortIndex;
                    child.SortIndex = v;

                    return;
                }

                prev = i;
            }
        }


        /// <summary>Moves a child down.</summary>
        /// <param name="child">Child item.</param>
        internal void __MoveDown(ISortable child)
        {
            foreach(ISortable i in Items)
            {
                if(i.SortIndex > child.SortIndex)
                {
                    int v = i.SortIndex;
                    i.SortIndex = child.SortIndex;
                    child.SortIndex = v;

                    return;
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ISortable                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the folder ID.</summary>
        public string ID { get; set; }


        /// <summary>Gets or sets the folder name.</summary>
        public string Name { get; set; }


        /// <summary>Gets or sets the parent folder.</summary>
        public Folder Parent { get; set; }


        /// <summary>Gets or sets the sort index.</summary>
        public int SortIndex { get; set; }


        /// <summary>Gets if the object can move up.</summary>
        public bool CanMoveUp
        {
            get
            {
                if(Parent == null) { return false; }
                return Parent.__CanMoveUp(this);
            }
        }


        /// <summary>Gets if the object can move down.</summary>
        public bool CanMoveDown
        {
            get
            {
                if(Parent == null) { return false; }
                return Parent.__CanMoveDown(this);
            }
        }


        /// <summary>Moves the item up.</summary>
        public void MoveUp()
        {
            if(Parent != null) { Parent.__MoveUp(this); }
        }


        /// <summary>Moves the item down.</summary>
        public void MoveDown()
        {
            if(Parent != null) { Parent.__MoveDown(this); }
        }


        /// <summary>Exports the item.</summary>
        /// <param name="cfg">Configuration file.</param>
        public void Export(Ddp cfg)
        {
            Save(cfg);
        }
    }
}
