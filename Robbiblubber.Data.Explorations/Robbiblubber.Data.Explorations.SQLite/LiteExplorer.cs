﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Controls;
using Robbiblubber.Util;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.SQLite
{
    /// <summary>This class represents the SQLite database structure.</summary>
    public class LiteExplorer: IExporer, IDisposable, IAutoCompletionSource
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Provider.</summary>
        protected ProviderItem _Provider = null;

        /// <summary>Default keywords.</summary>
        protected List<IAutoCompletionKeyword> _DefaultKeyWords;

        /// <summary>Returns if the default keywords are currently available for auto completion.</summary>
        protected bool _DefaultKeywordsGiven = false;

        /// <summary>SQLite keywords.</summary>
        protected SQLKeywords _KeyWords = new SQLKeywords(PathOp.ApplicationDirectory + @"\sqlite.keywords");

        /// <summary>Database explorer root.</summary>
        protected LiteDatabase _DatabaseExplorer = null;

        /// <summary>Tree node menu.</summary>
        protected IMenuTargetWindow _TreeNodeMenu;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider"></param>
        public LiteExplorer(ProviderItem provider)
        {
            Items = new DbItem[] { _DatabaseExplorer = new LiteDatabase(_Provider = provider) };
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes the default keywords.</summary>
        protected internal void __RefreshDefaultKeywords()
        {
            _DefaultKeyWords = new List<IAutoCompletionKeyword>();

            _DefaultKeyWords.AddRange(_KeyWords);

            foreach(DbItem i in _DatabaseExplorer.Children)
            {
                foreach(DbItem j in i.Children)
                {
                    _DefaultKeyWords.Add(j);
                }
            }

            foreach(string i in _GetColumnNames())
            {
                _DefaultKeyWords.Add(new SQLKeywords.KeyWord(i, "lite::column"));
            }
            _DefaultKeywordsGiven = false;
        }


        /// <summary>Gets a list of unique column names.</summary>
        /// <returns>String list.</returns>
        protected List<string> _GetColumnNames()
        {
            List<string> rval = new List<string>();

            foreach(DbItem i in _DatabaseExplorer.Children)
            {
                foreach(DbItem j in i.Children)
                {
                    foreach(DbItem k in j.Children)
                    {
                        if(k is LiteColumnsFolder)
                        {
                            foreach(DbItem l in k.Children)
                            {
                                if(!rval.Contains(l.Name)) { rval.Add(l.Name); }
                            }
                        }
                    }
                }
            }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbExplorer                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the items root for this explorer.</summary>
        public DbItem[] Items
        {
            get; protected set;
        }


        /// <summary>Initializes auto completion.</summary>
        /// <param name="a">AutoCompletion instance.</param>
        public void InitAutoCompletion(AutoCompletion a)
        {
            a.AddImage("lite::column", Resources.column);
            a.AddImage("lite::table", Resources.table);
            a.AddImage("lite::view", Resources.view);

            __RefreshDefaultKeywords();
        }


        /// <summary>Initializes custom menus.</summary>
        /// <param name="menu">Menu provider.</param>
        public void InitMenus(IMenuTargetWindow menu)
        {
            _TreeNodeMenu = menu;

            menu.DatabaseMenus.AddMenu("sqlfwx::menu.editdata".Localize("&Edit Data..."), Resources.edit_table, "edit", Keys.F10);
            menu.DropDownMenus.AddMenu("sqlfwx::menu.editdata".Localize("Edit Data..."), Resources.edit_table, "edit");
            menu.DatabaseMenus.AddSeparator("blank0");
            menu.DropDownMenus.AddSeparator("blank0");

            menu.DatabaseMenus.AddMenu("sqlfwx::menu.compact".Localize("&Compact Database"), Resources.vacuum, "compact");
            menu.DropDownMenus.AddMenu("sqlfwx::menu.compact".Localize("Compact Database"), Resources.vacuum, "compact");

            menu.DatabaseMenus.AddMenu("&Analyze", Resources.robot, "analyze");
            menu.DropDownMenus.AddMenu("Analyze", Resources.robot, "analyze");

            menu.DatabaseMenus.AddMenu("Drop", Resources.remove, "drop", Keys.None, "Del");
            menu.DropDownMenus.AddMenu("Drop", Resources.remove, "drop");
            menu.DatabaseMenus.AddSeparator("blank1");
            menu.DropDownMenus.AddSeparator("blank1");

            menu.MenuOpening +=_MenuOpening;
            menu.MenuSelected += _MenuSelected;
            menu.TreeKeyDown += _TreeKeyDown;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Menu opening.</summary>
        private void _MenuOpening(object sender, MenuTargetEventArgs e)
        {
            IList<ToolStripItem> list = ((IList<ToolStripItem>) sender);
            foreach(ToolStripItem i in list) { i.Visible = false; i.Enabled = true; }

            if(e.Item is LiteDatabase)
            {
                list.GetItem("analyze").Text = "sqlfwx::menu.analdb".Localize("&Analyze Database");
                list.GetItem("blank1").Visible = list.GetItem("compact").Visible = list.GetItem("analyze").Visible = true;
            }
            else if(e.Item is LiteView)
            {
                list.GetItem("drop").Text = "sqlfwx::menu.dropview".Localize("&Drop View...");
                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
            else if(e.Item is LiteTable)
            {
                list.GetItem("analyze").Text = "sqlfwx::menu.analtab".Localize("&Analyze Table");
                list.GetItem("drop").Text = "sqlfwx::menu.droptab".Localize("&Drop Table...");

                list.GetItem("blank1").Visible =list.GetItem("analyze").Visible = list.GetItem("drop").Visible = true;

                if((e.Item.Name.ToLower() == "sqlite_master") ||
                   (e.Item.Name.ToLower() == "sqlite_temp_master") ||
                   (e.Item.Name.ToLower() == "sqlite_schema") ||
                   (e.Item.Name.ToLower() == "sqlite_temp_schema") ||
                   (e.Item.Name.ToLower() == "sqlite_sequence")) { list.GetItem("drop").Enabled = false; }


                if(((((LiteTable) e.Item).PrimaryKeys) != null) && ((((LiteTable) e.Item).PrimaryKeys).Length > 0))
                {
                    list.GetItem("edit").Visible = list.GetItem("blank0").Visible = true;
                }
            }
            else if(e.Item is LiteIndex)
            {
                list.GetItem("analyze").Text = "sqlfwx::menu.analidx".Localize("&Analyze Index");
                list.GetItem("drop").Text = "sqlfwx::menu.dropidx".Localize("&Drop Index...");

                list.GetItem("blank1").Visible = list.GetItem("analyze").Visible = list.GetItem("drop").Visible = true;
            }
            else if(e.Item is LiteTrigger)
            {
                list.GetItem("drop").Text = "sqlfwx::menu.droptrig".Localize("&Drop Trigger...");

                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
        }


        /// <summary>Menu selected.</summary>
        private void _MenuSelected(object sender, MenuTargetEventArgs e)
        {
            switch((string) ((ToolStripItem) sender).Tag)
            {
                case "compact":
                    _TreeNodeMenu.TargetWindow.ShowWait();
                    try
                    {
                        IDbCommand cmd = _Provider.CreateCommand("VACUUM");
                        cmd.ExecuteNonQuery();
                    }
                    catch(Exception) { }
                    _TreeNodeMenu.TargetWindow.HideWait();
                    break;
                case "edit":
                    FormDataEditor t = new FormDataEditor(_Provider, e.Item.Name, "LIMIT 20", ((LiteTable) e.Item).PrimaryKeys);
                    t.ShowDialog(true);
                    break;
                case "analyze":
                    _TreeNodeMenu.TargetWindow.ShowWait();
                    try
                    {
                        IDbCommand cmd = _Provider.CreateCommand((e.Item is LiteDatabase) ? "ANALYZE" : ("ANALYZE " + e.Item.Name));
                        cmd.ExecuteNonQuery();
                    }
                    catch(Exception) { }

                    _TreeNodeMenu.TargetWindow.HideWait();
                    break;
                case "drop":
                    string dcpt = null, dtxt = null;
                    string stmt = null;
                    if(e.Item is LiteView)
                    {
                        dcpt = "sqlfwx::udiag.dropview.caption".Localize("Drop View"); dtxt = "sqlfwx::udiag.dropview".Localize("Do you want to drop the view \"$(0)\"?").Replace("$(0)", e.Item.Name);
                        stmt = "DROP VIEW " + e.Item.Name;
                    }
                    else if(e.Item is LiteTable)
                    {
                        dcpt = "sqlfwx::udiag.droptab.caption".Localize("Drop Table"); dtxt = "sqlfwx::udiag.droptab".Localize("Do you want to drop the table \"$(0)\"?").Replace("$(0)", e.Item.Name);
                        stmt = "DROP TABLE " + e.Item.Name;
                    }
                    else if(e.Item is LiteIndex)
                    {
                        dcpt = "sqlfwx::udiag.dropidx.caption".Localize("Drop Index"); dtxt = "sqlfwx::udiag.dropidx".Localize("Do you want to drop the index \"$(0)\"?").Replace("$(0)", e.Item.Name);
                        stmt = "DROP INDEX " + e.Item.Name;
                    }
                    else if(e.Item is LiteTrigger)
                    {
                        dcpt = "sqlfwx::udiag.droptrg.caption".Localize("Drop Trigger"); dtxt = "sqlfwx::udiag.droptrg".Localize("Do you want to drop the trigger \"$(0)\"?").Replace("$(0)", e.Item.Name);
                        stmt = "DROP TRIGGER " + e.Item.Name;
                    }

                    if(MessageBox.Show(dtxt, dcpt, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes) { return; }

                    _TreeNodeMenu.TargetWindow.ShowWait();
                    try
                    {
                        IDbCommand cmd = _Provider.CreateCommand(stmt);
                        cmd.ExecuteNonQuery();
                    }
                    catch(Exception ex) { MessageBox.Show(ex.Message, "sqlfwx::udiag.error".Localize("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error); }

                    _TreeNodeMenu.TargetWindow.HideWait();
                    _TreeNodeMenu.TargetWindow.RefreshTree();
                    break;
            }
        }


        /// <summary>Tree key down.</summary>
        private void _TreeKeyDown(object sender, MenuTargetKeyEventArgs e)
        {
            if(e.Item == null) return;
            if((e.KeyArgs.KeyCode != Keys.Delete) || (e.KeyArgs.Modifiers != Keys.None)) return;

            if((e.Item is LiteTable) || (e.Item is LiteView) || (e.Item is LiteIndex) || (e.Item is LiteTrigger))
            {
                _MenuSelected(_TreeNodeMenu.DropDownMenus.GetItem("drop"), e);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IAutoCompletionSource                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Updates the words for a given preceding text.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Word list.</returns>
        public IEnumerable<IAutoCompletionKeyword> UpdateWords(string pretext, char key)
        {
            _DefaultKeywordsGiven = false;
            return _DefaultKeyWords;
        }


        /// <summary>Returns if the words need to be updated.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Returns TRUE if an update is required, otherwise returns FALSE.</returns>
        public bool UpdateRequired(string pretext, char key)
        {
            return (!_DefaultKeywordsGiven);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDisposable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Disposes the object.</summary>
        public void Dispose()
        { }
    }
}
