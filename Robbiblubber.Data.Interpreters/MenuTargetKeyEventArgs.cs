﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class implements event arguments for IMenuTargetWindow key events.</summary>
    public class MenuTargetKeyEventArgs: MenuTargetEventArgs
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="item">Affected item.</param>
        /// <param name="node">Tree node.</param>
        /// <param name="keyArgs">Parent key event arguments.</param>
        public MenuTargetKeyEventArgs(DbItem item, TreeNode node, KeyEventArgs keyArgs): base(item, node)
        {
            KeyArgs = keyArgs;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent key event arguments.</summary>
        public KeyEventArgs KeyArgs { get; }
    }
}
