﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSAccess
{
    /// <summary>This class represents an Access view.</summary>
    public class AccessView: AccessTable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        internal AccessView(AccessDatabase database, AccessTablesFolder folder, string name): base(database, folder, name)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] PgTable                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.msacc_view; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mdb::view"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            _Children.Add(new AccessColumnsFolder(Database, this));
        }
        
        
        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                return "CREATE VIEW " + Name + " AS\r\n" + Database.AccessApplication.CurrentDb().QueryDefs[Name].SQL;
            }
        }
    }
}
