﻿namespace Robbiblubber.Data.Interpreters.History
{
    partial class FormHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHistory));
            this._DtpFrom = new System.Windows.Forms.DateTimePicker();
            this._LabelFrom = new System.Windows.Forms.Label();
            this._LabelTo = new System.Windows.Forms.Label();
            this._DtpTo = new System.Windows.Forms.DateTimePicker();
            this._ComboConnection = new Robbiblubber.Util.Controls.RichComboBox();
            this._IlistConnections = new System.Windows.Forms.ImageList();
            this._LableConnection = new System.Windows.Forms.Label();
            this._TextSearch = new System.Windows.Forms.TextBox();
            this._LabelSearch = new System.Windows.Forms.Label();
            this._ButtonApply = new System.Windows.Forms.Button();
            this._TextSQL = new System.Windows.Forms.TextBox();
            this._ButtonApplyText = new System.Windows.Forms.Button();
            this._ButtonCopyText = new System.Windows.Forms.Button();
            this._ButtonPrevious = new System.Windows.Forms.Button();
            this._ButtonNext = new System.Windows.Forms.Button();
            this._ButtonDelete = new System.Windows.Forms.Button();
            this._LabelPosition = new System.Windows.Forms.Label();
            this._ButtonClose = new System.Windows.Forms.Button();
            this._ButtonFirst = new System.Windows.Forms.Button();
            this._ButtonLast = new System.Windows.Forms.Button();
            this._ButtonFavorite = new System.Windows.Forms.Button();
            this._ToolTip = new System.Windows.Forms.ToolTip();
            this.SuspendLayout();
            // 
            // _DtpFrom
            // 
            this._DtpFrom.Checked = false;
            this._DtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this._DtpFrom.Location = new System.Drawing.Point(31, 45);
            this._DtpFrom.MaxDate = new System.DateTime(2036, 12, 31, 0, 0, 0, 0);
            this._DtpFrom.MinDate = new System.DateTime(2016, 1, 1, 0, 0, 0, 0);
            this._DtpFrom.Name = "_DtpFrom";
            this._DtpFrom.ShowCheckBox = true;
            this._DtpFrom.Size = new System.Drawing.Size(141, 25);
            this._DtpFrom.TabIndex = 0;
            // 
            // _LabelFrom
            // 
            this._LabelFrom.AutoSize = true;
            this._LabelFrom.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelFrom.Location = new System.Drawing.Point(28, 29);
            this._LabelFrom.Name = "_LabelFrom";
            this._LabelFrom.Size = new System.Drawing.Size(36, 13);
            this._LabelFrom.TabIndex = 0;
            this._LabelFrom.Tag = "sqlfwx.udiag.bhistory.from";
            this._LabelFrom.Text = "&From:";
            // 
            // _LabelTo
            // 
            this._LabelTo.AutoSize = true;
            this._LabelTo.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelTo.Location = new System.Drawing.Point(197, 29);
            this._LabelTo.Name = "_LabelTo";
            this._LabelTo.Size = new System.Drawing.Size(22, 13);
            this._LabelTo.TabIndex = 1;
            this._LabelTo.Tag = "sqlfwx.udiag.bhistory.to";
            this._LabelTo.Text = "&To:";
            // 
            // _DtpTo
            // 
            this._DtpTo.Checked = false;
            this._DtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this._DtpTo.Location = new System.Drawing.Point(200, 45);
            this._DtpTo.MaxDate = new System.DateTime(2036, 12, 31, 0, 0, 0, 0);
            this._DtpTo.MinDate = new System.DateTime(2016, 1, 1, 0, 0, 0, 0);
            this._DtpTo.Name = "_DtpTo";
            this._DtpTo.ShowCheckBox = true;
            this._DtpTo.Size = new System.Drawing.Size(141, 25);
            this._DtpTo.TabIndex = 1;
            // 
            // _ComboConnection
            // 
            this._ComboConnection.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this._ComboConnection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._ComboConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ComboConnection.ImageList = this._IlistConnections;
            this._ComboConnection.Location = new System.Drawing.Point(385, 43);
            this._ComboConnection.Name = "_ComboConnection";
            this._ComboConnection.Size = new System.Drawing.Size(349, 26);
            this._ComboConnection.TabIndex = 2;
            // 
            // _IlistConnections
            // 
            this._IlistConnections.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_IlistConnections.ImageStream")));
            this._IlistConnections.TransparentColor = System.Drawing.Color.Transparent;
            this._IlistConnections.Images.SetKeyName(0, "*");
            this._IlistConnections.Images.SetKeyName(1, "?");
            this._IlistConnections.Images.SetKeyName(2, "??");
            // 
            // _LableConnection
            // 
            this._LableConnection.AutoSize = true;
            this._LableConnection.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LableConnection.Location = new System.Drawing.Point(382, 27);
            this._LableConnection.Name = "_LableConnection";
            this._LableConnection.Size = new System.Drawing.Size(58, 13);
            this._LableConnection.TabIndex = 2;
            this._LableConnection.Tag = "sqlfwx.udiag.bhistory.database";
            this._LableConnection.Text = "&Database:";
            // 
            // _TextSearch
            // 
            this._TextSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextSearch.Location = new System.Drawing.Point(31, 98);
            this._TextSearch.Name = "_TextSearch";
            this._TextSearch.Size = new System.Drawing.Size(703, 25);
            this._TextSearch.TabIndex = 3;
            // 
            // _LabelSearch
            // 
            this._LabelSearch.AutoSize = true;
            this._LabelSearch.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelSearch.Location = new System.Drawing.Point(28, 82);
            this._LabelSearch.Name = "_LabelSearch";
            this._LabelSearch.Size = new System.Drawing.Size(102, 13);
            this._LabelSearch.TabIndex = 3;
            this._LabelSearch.Tag = "sqlfwx::udiag.bhistory.searchex";
            this._LabelSearch.Text = "&Search expression:";
            // 
            // _ButtonApply
            // 
            this._ButtonApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonApply.Location = new System.Drawing.Point(31, 144);
            this._ButtonApply.Name = "_ButtonApply";
            this._ButtonApply.Size = new System.Drawing.Size(147, 29);
            this._ButtonApply.TabIndex = 4;
            this._ButtonApply.Tag = "sqlfwx::common.button.apply";
            this._ButtonApply.Text = "&Apply";
            this._ButtonApply.UseVisualStyleBackColor = true;
            this._ButtonApply.Click += new System.EventHandler(this._ButtonApply_Click);
            // 
            // _TextSQL
            // 
            this._TextSQL.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this._TextSQL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextSQL.Location = new System.Drawing.Point(31, 194);
            this._TextSQL.Multiline = true;
            this._TextSQL.Name = "_TextSQL";
            this._TextSQL.ReadOnly = true;
            this._TextSQL.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._TextSQL.Size = new System.Drawing.Size(703, 308);
            this._TextSQL.TabIndex = 14;
            this._TextSQL.WordWrap = false;
            // 
            // _ButtonApplyText
            // 
            this._ButtonApplyText.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonApplyText.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonApplyText.Image")));
            this._ButtonApplyText.Location = new System.Drawing.Point(512, 158);
            this._ButtonApplyText.Name = "_ButtonApplyText";
            this._ButtonApplyText.Size = new System.Drawing.Size(30, 30);
            this._ButtonApplyText.TabIndex = 8;
            this._ButtonApplyText.Tag = "||sqlfwx::udiag.bhistory.apply";
            this._ToolTip.SetToolTip(this._ButtonApplyText, "Apply to SQL window");
            this._ButtonApplyText.UseVisualStyleBackColor = true;
            this._ButtonApplyText.Click += new System.EventHandler(this._ButtonApplyText_Click);
            // 
            // _ButtonCopyText
            // 
            this._ButtonCopyText.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCopyText.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonCopyText.Image")));
            this._ButtonCopyText.Location = new System.Drawing.Point(546, 158);
            this._ButtonCopyText.Name = "_ButtonCopyText";
            this._ButtonCopyText.Size = new System.Drawing.Size(30, 30);
            this._ButtonCopyText.TabIndex = 9;
            this._ButtonCopyText.Tag = "||sqlfwx::udiag.bhistory.copy";
            this._ToolTip.SetToolTip(this._ButtonCopyText, "Copy to clipboard");
            this._ButtonCopyText.UseVisualStyleBackColor = true;
            this._ButtonCopyText.Click += new System.EventHandler(this._ButtonCopyText_Click);
            // 
            // _ButtonPrevious
            // 
            this._ButtonPrevious.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonPrevious.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonPrevious.Image")));
            this._ButtonPrevious.Location = new System.Drawing.Point(618, 158);
            this._ButtonPrevious.Name = "_ButtonPrevious";
            this._ButtonPrevious.Size = new System.Drawing.Size(30, 30);
            this._ButtonPrevious.TabIndex = 11;
            this._ButtonPrevious.Tag = "||sqlfwx::udiag.bhistory.prev";
            this._ToolTip.SetToolTip(this._ButtonPrevious, "Move to previous");
            this._ButtonPrevious.UseVisualStyleBackColor = true;
            this._ButtonPrevious.Click += new System.EventHandler(this._ButtonPrevious_Click);
            // 
            // _ButtonNext
            // 
            this._ButtonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonNext.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonNext.Image")));
            this._ButtonNext.Location = new System.Drawing.Point(652, 158);
            this._ButtonNext.Name = "_ButtonNext";
            this._ButtonNext.Size = new System.Drawing.Size(30, 30);
            this._ButtonNext.TabIndex = 12;
            this._ButtonNext.Tag = "||sqlfwx::udiag.bhistory.next";
            this._ToolTip.SetToolTip(this._ButtonNext, "Move to next");
            this._ButtonNext.UseVisualStyleBackColor = true;
            this._ButtonNext.Click += new System.EventHandler(this._ButtonNext_Click);
            // 
            // _ButtonDelete
            // 
            this._ButtonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonDelete.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonDelete.Image")));
            this._ButtonDelete.Location = new System.Drawing.Point(475, 158);
            this._ButtonDelete.Name = "_ButtonDelete";
            this._ButtonDelete.Size = new System.Drawing.Size(30, 30);
            this._ButtonDelete.TabIndex = 7;
            this._ButtonDelete.Tag = "||sqlfwx::udiag.bhistory.delete";
            this._ToolTip.SetToolTip(this._ButtonDelete, "Delete from history");
            this._ButtonDelete.UseVisualStyleBackColor = true;
            this._ButtonDelete.Click += new System.EventHandler(this._ButtonDelete_Click);
            // 
            // _LabelPosition
            // 
            this._LabelPosition.Location = new System.Drawing.Point(477, 510);
            this._LabelPosition.Name = "_LabelPosition";
            this._LabelPosition.Size = new System.Drawing.Size(237, 25);
            this._LabelPosition.TabIndex = 15;
            this._LabelPosition.Text = "0/0";
            this._LabelPosition.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _ButtonClose
            // 
            this._ButtonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonClose.Location = new System.Drawing.Point(184, 144);
            this._ButtonClose.Name = "_ButtonClose";
            this._ButtonClose.Size = new System.Drawing.Size(147, 29);
            this._ButtonClose.TabIndex = 5;
            this._ButtonClose.TabStop = false;
            this._ButtonClose.Tag = "sqlfwx::common.button.close";
            this._ButtonClose.Text = "&Close";
            this._ButtonClose.UseVisualStyleBackColor = true;
            this._ButtonClose.Click += new System.EventHandler(this._ButtonClose_Click);
            // 
            // _ButtonFirst
            // 
            this._ButtonFirst.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonFirst.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonFirst.Image")));
            this._ButtonFirst.Location = new System.Drawing.Point(584, 158);
            this._ButtonFirst.Name = "_ButtonFirst";
            this._ButtonFirst.Size = new System.Drawing.Size(30, 30);
            this._ButtonFirst.TabIndex = 10;
            this._ButtonFirst.Tag = "||sqlfwx::udiag.bhistory.first";
            this._ToolTip.SetToolTip(this._ButtonFirst, "Move to first");
            this._ButtonFirst.UseVisualStyleBackColor = true;
            this._ButtonFirst.Click += new System.EventHandler(this._ButtonFirst_Click);
            // 
            // _ButtonLast
            // 
            this._ButtonLast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonLast.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonLast.Image")));
            this._ButtonLast.Location = new System.Drawing.Point(686, 158);
            this._ButtonLast.Name = "_ButtonLast";
            this._ButtonLast.Size = new System.Drawing.Size(30, 30);
            this._ButtonLast.TabIndex = 13;
            this._ButtonLast.Tag = "||sqlfwx::udiag.bhistory.last";
            this._ToolTip.SetToolTip(this._ButtonLast, "Move to last");
            this._ButtonLast.UseVisualStyleBackColor = true;
            this._ButtonLast.Click += new System.EventHandler(this._ButtonLast_Click);
            // 
            // _ButtonFavorite
            // 
            this._ButtonFavorite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonFavorite.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonFavorite.Image")));
            this._ButtonFavorite.Location = new System.Drawing.Point(437, 158);
            this._ButtonFavorite.Name = "_ButtonFavorite";
            this._ButtonFavorite.Size = new System.Drawing.Size(30, 30);
            this._ButtonFavorite.TabIndex = 6;
            this._ButtonFavorite.Tag = "||sqlfwx::udiag.bhistory.addfav";
            this._ToolTip.SetToolTip(this._ButtonFavorite, "Add to favorite SQL");
            this._ButtonFavorite.UseVisualStyleBackColor = true;
            this._ButtonFavorite.Click += new System.EventHandler(this._ButtonFavorite_Click);
            // 
            // FormHistory
            // 
            this.AcceptButton = this._ButtonApply;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonClose;
            this.ClientSize = new System.Drawing.Size(762, 541);
            this.Controls.Add(this._ButtonFavorite);
            this.Controls.Add(this._ButtonLast);
            this.Controls.Add(this._ButtonFirst);
            this.Controls.Add(this._ButtonClose);
            this.Controls.Add(this._LabelPosition);
            this.Controls.Add(this._ButtonDelete);
            this.Controls.Add(this._ButtonNext);
            this.Controls.Add(this._ButtonPrevious);
            this.Controls.Add(this._ButtonCopyText);
            this.Controls.Add(this._ButtonApplyText);
            this.Controls.Add(this._TextSQL);
            this.Controls.Add(this._ButtonApply);
            this.Controls.Add(this._LabelSearch);
            this.Controls.Add(this._TextSearch);
            this.Controls.Add(this._LableConnection);
            this.Controls.Add(this._ComboConnection);
            this.Controls.Add(this._LabelTo);
            this.Controls.Add(this._DtpTo);
            this.Controls.Add(this._LabelFrom);
            this.Controls.Add(this._DtpFrom);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormHistory";
            this.Tag = "";
            this.Text = "Browse History";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker _DtpFrom;
        private System.Windows.Forms.Label _LabelFrom;
        private System.Windows.Forms.Label _LabelTo;
        private System.Windows.Forms.DateTimePicker _DtpTo;
        private Robbiblubber.Util.Controls.RichComboBox _ComboConnection;
        private System.Windows.Forms.Label _LableConnection;
        private System.Windows.Forms.TextBox _TextSearch;
        private System.Windows.Forms.Label _LabelSearch;
        private System.Windows.Forms.Button _ButtonApply;
        private System.Windows.Forms.TextBox _TextSQL;
        private System.Windows.Forms.Button _ButtonApplyText;
        private System.Windows.Forms.Button _ButtonCopyText;
        private System.Windows.Forms.Button _ButtonPrevious;
        private System.Windows.Forms.Button _ButtonNext;
        private System.Windows.Forms.ImageList _IlistConnections;
        private System.Windows.Forms.Button _ButtonDelete;
        private System.Windows.Forms.Label _LabelPosition;
        private System.Windows.Forms.Button _ButtonClose;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.Button _ButtonFirst;
        private System.Windows.Forms.Button _ButtonLast;
        private System.Windows.Forms.Button _ButtonFavorite;
    }
}

