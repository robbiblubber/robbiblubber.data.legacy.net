﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.PostgreSQL
{
    /// <summary>This class represents a PostgreSQL indexes folder.</summary>
    public class PgIndexesFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="table">Parent table.</param>
        internal PgIndexesFolder(PgDatabase database, PgTable table)
        {
            Database = database;
            _Parent = Table = table;
            _Name = "sqlfwx::tree.folder.indexes".Localize("Indexes");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public PgDatabase Database { get; private set; }


        /// <summary>Gets the parent table.</summary>
        public PgTable Table { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "pg::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT INDEXNAME, Lower(INDEXDEF) FROM PG_CATALOG.PG_INDEXES WHERE SCHEMANAME = '" + Table.Folder.Schema.Name + "' AND TABLENAME = '" + Table.Name + "' ORDER BY INDEXNAME";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new PgIndex(Database, this, re.GetString(0), re.GetString(1).Contains(" unique ")));
                }
            }
            catch(Exception) {}

            Disposal.Dispose(re, cmd);
        }
    }
}
