﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using Robbiblubber.Util.Debug;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class provides SQL export providers.</summary>
    public static class SQLExport
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private static List<ISQLExport> _Modules = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // oublic static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a list of available SQL export provider modules.</summary>
        public static ISQLExport[] Modules
        {
            get
            {
                if(_Modules == null)
                {
                    _Modules = new List<ISQLExport>();
                    _Load();
                }

                return _Modules.OrderBy(m => m.SortIndex).ToArray();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads database providers.</summary>
        /// <param name="f">Directory or file name.</param>
        private static void _Load(string f = null)
        {
            if(f == null) { f = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location); }

            if(Directory.Exists(f))
            {
                foreach(string i in Directory.GetDirectories(f)) { _Load(i); }
                foreach(string i in Directory.GetFiles(f)) { _Load(i); }

                return;
            }

            Assembly a;

            try
            {
                a = Assembly.LoadFile(f);
            }
            catch(Exception ex)
            {
                DebugOp.Dump("SQLFWX0410a", ex);
                return;
            }

            try
            {
                foreach(Type i in a.GetTypes())
                {
                    if(i.GetInterfaces().Contains(typeof(ISQLExport)))
                    {
                        if(i.IsInterface || i.IsAbstract) continue;

                        try
                        {
                            ISQLExport m = (ISQLExport) Activator.CreateInstance(i);

                            _Modules.Add(m);
                        }
                        catch(Exception ex) { DebugOp.Dump("SQLFWX0410b", ex); }
                    }
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0410c", ex); }
        }
    }
}
