﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Data.SQL
{
    /// <summary>SQL Statement classes implement this interface.</summary>
    public interface ISQLStatement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the statement source.</summary>
        string Source { get; set; }


        /// <summary>Gets if the statement is DDL.</summary>
        bool IsDDL { get; }


        /// <summary>Gets if the statement is commit.</summary>
        bool IsCommit { get; }


        /// <summary>Gets if the statement is rollback.</summary>
        bool IsRollback { get; }


        /// <summary>Gets if the statement is a SELECT query.</summary>
        bool IsSelect { get; }


        /// <summary>Gets if the statement is an INSERT statement.</summary>
        bool IsInsert { get; }


        /// <summary>Gets if the statement is an UPDATE statement.</summary>
        bool IsUpdate { get; }


        /// <summary>Gets if the statement is a DELETE statement.</summary>
        bool IsDelete { get; }


        /// <summary>Gets if the statement is an INSERT statement.</summary>
        bool IsDML { get; }
    }
}
