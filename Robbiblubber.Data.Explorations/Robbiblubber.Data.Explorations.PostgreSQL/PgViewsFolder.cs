﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.PostgreSQL
{
    /// <summary>This class represents a PostgreSQL views folder.</summary>
    public class PgViewsFolder: PgTablesFolder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="schema">Parent schema.</param>
        internal PgViewsFolder(PgDatabase database, PgSchema schema): base(database, schema)
        {
            _Name = "sqlfwx::tree.folder.views".Localize("Views");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] PgTablesFolder                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_SCHEMA = '" + Schema.Name + "' ORDER BY TABLE_NAME";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new PgView(Database, this, re.GetString(0)));
                }
            }
            catch(Exception) {}

            Disposal.Dispose(re, cmd);
        }
    }
}
