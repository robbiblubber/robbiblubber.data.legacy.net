﻿using System;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Interpreters.MySQL
{
    /// <summary>This class implements the MySQL database manager.</summary>
    public sealed class MySQLManager: ProviderManager, IProviderManager
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Control.</summary>
        private IProviderControl _Control = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLProviderManager                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider name.</summary>
        public override string ProviderName
        {
            get { return "Robbiblubber.Data.Providers.MySQL.MySQLProvider"; }
        }


        /// <summary>Gets the manager display name.</summary>
        public override string DisplayName
        {
            get { return "MySQL"; }
        }


        /// <summary>Gets the provider control.</summary>

        public override IProviderControl Control
        {
            get
            {
                if(_Control == null) { _Control = new MySQLProviderControl(); }

                return _Control;
            }
        }


        /// <summary>Gets the provider icon.</summary>
        public override Image ProviderIcon
        {
            get { return Resources.mysql; }
        }


        /// <summary>Gets the connction icon.</summary>
        public override Image ConnectionIcon
        {
            get { return Resources.mysqldb; }
        }
    }
}