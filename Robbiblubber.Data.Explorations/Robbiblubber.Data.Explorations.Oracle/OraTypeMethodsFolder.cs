﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle type methods folder.</summary>
    public class OraTypeMethodsFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="type">Parent type.</param>
        internal OraTypeMethodsFolder(OraDatabase database, OraType type)
        {
            Database = database;
            _Parent = Type = type;
            _Name = "Methods";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public OraDatabase Database { get; private set; }


        /// <summary>Gets the parent type.</summary>
        public OraType Type { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "ora::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("SELECT DISTINCT METHOD_NAME FROM (SELECT METHOD_NAME , METHOD_NO FROM SYS.ALL_TYPE_METHODS WHERE OWNER = '" + Type.Folder.Schema.Name + "' AND TYPE_NAME = '" + Type.Name + "' ORDER BY METHOD_NO)");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new OraTypeMethod(Database, this, re.GetString(0)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0243b", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
