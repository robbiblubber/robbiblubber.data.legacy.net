﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSAccess
{
    /// <summary>This class represents an Access table.</summary>
    public class AccessTable: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Table name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        internal AccessTable(AccessDatabase database, AccessTablesFolder folder, string name)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public AccessDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public AccessTablesFolder Folder { get; private set; }

        
        /// <summary>Gets the colums folder.</summary>
        public AccessColumnsFolder Columns
        {
            get
            {
                foreach(DbItem i in Children)
                {
                    if(i is AccessColumnsFolder) { return ((AccessColumnsFolder) i); }
                }

                return null;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.msacc_table; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mdb::table"; }
        }

        
        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            _Children.Add(new AccessColumnsFolder(Database, this));
            _Children.Add(new AccessIndexesFolder(Database, this));
        }
        

        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = "CREATE TABLE " + Name + "\r\n(";
                
                int ln = Columns.Children.Max(m => m.Name.Length) + 3;
                int lt = Columns.Children.Max(m => ((AccessColumn) m).DataType.Length) + 3;
                bool first = true;

                foreach(AccessColumn i in Columns.Children)
                {
                    if(first)
                    {
                        first = false;
                    }
                    else
                    {
                        rval += ",";
                    }

                    rval += ("\r\n   " + i.Name.PadRight(ln) + i.DataType.ToUpper().PadRight(lt) + (i.IsNotNull ? "NOT NULL   " : "           "));
                    if(i.IsPrimaryKey) { rval += "PRIMARY KEY   "; }
                    if(i.IsForeignKey) { rval += "REFERENCES " + i.ReferencedTable + "(" + i.ReferencedColumn + ")"; }

                    rval = rval.Trim();
                }

                rval += "\r\n)";
                
                return rval;
            }
        }


        /// <summary>Gets if the object defines a query.</summary>
        public override bool HasQuery
        {
            get { return true; }
        }


        /// <summary>Gets the query for this object.</summary>
        public override string Query
        {
            get
            {
                string f = "";
                bool first = true;
                
                foreach(AccessColumn i in Columns.Children)
                {
                    if(first)
                    {
                        first = false;
                    }
                    else { f += ", "; }

                    f += i.Name;
                }
                return "SELECT " + f + " FROM " + QualifiedName;
            }
        }


        /// <summary>Gets the qualified name for this object.</summary>
        public override string QualifiedName
        {
            get { return Name; }
        }
    }
}
