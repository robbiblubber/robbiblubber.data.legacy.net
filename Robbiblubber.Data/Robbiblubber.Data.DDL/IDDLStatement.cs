﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>DDL statements imlement this interface.</summary>
    public interface IDDLStatement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets a statements list for this DDL.</summary>
        string[] Text { get; }


        /// <summary>Gets a list of child DDL statements.</summary>
        IDDLStatement[] ChildStatements { get; }
    }
}
