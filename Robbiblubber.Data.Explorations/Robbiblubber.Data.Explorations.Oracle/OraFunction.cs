﻿using System;
using System.Drawing;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle function.</summary>
    public class OraFunction: OraProcedure
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        internal OraFunction(OraDatabase database, DbItem folder, string name): base(database, folder, name)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] OraProcedure                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                return Resources.function;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                return "ora::function";
            }
        }
    }
}
