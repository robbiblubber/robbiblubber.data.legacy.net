﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Microsoft.Office.Interop.Access.Dao;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSAccess
{
    /// <summary>This class represents an Access index.</summary>
    public class AccessIndex: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Index name.</summary>
        private string _Name;


        /// <summary>Columns.</summary>
        private List<string> _Columns = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="unique">Unique flag.</param>
        internal AccessIndex(AccessDatabase database, AccessIndexesFolder folder, string name, bool unique)
        {
            Database = database;
            _Parent = Folder = folder;
            IsUnique = unique;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public AccessDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public AccessIndexesFolder Folder { get; private set; }


        /// <summary>Gets if the index is unique.</summary>
        public bool IsUnique { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the column names.</summary>
        /// <returns>List of column names.</returns>
        private List<string> _GetColumns()
        {
            if(_Columns == null)
            {
                _Columns = new List<string>();

                foreach(Field i in (IEnumerable<Field>) Database.AccessApplication.CurrentDb().TableDefs[Folder.Table.Name].Indexes[Name].Fields)
                {
                    _Columns.Add(i.Name);
                }
            }

            return _Columns;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                if(IsUnique) { return Resources.msacc_index_unique; }

                return Resources.msacc_index;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                if(IsUnique) { return "my::index/unique"; }

                return "my::index";
            }
        }
        
        
        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = "CREATE ";

                if(IsUnique) { rval += "UNIQUE "; }
                rval += ("INDEX " + Name + " ON " + Folder.Table.Name + " (");

                foreach(string i in _GetColumns())
                {
                    rval += (i + ", ");
                }

                return (rval.TrimEnd(' ', ',') + ")");
            }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            foreach(AccessColumn i in Folder.Table.Columns.Children)
            {
                if(_GetColumns().Contains(i.Name))
                {
                    _Children.Add(i);
                }
            }
        }
    }
}
