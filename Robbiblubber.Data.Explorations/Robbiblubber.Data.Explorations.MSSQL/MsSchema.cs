﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server schema.</summary>
    public class MsSchema: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Schema name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="name">Name.</param>
        internal MsSchema(MsDatabase database, string name)
        {
            _Parent = Database = database;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a rule with a given name.</summary>
        /// <param name="name">Name.</param>
        /// <returns>Rule.</returns>
        public MsRule GetRule(string name)
        {
            foreach(DbItem i in Children)
            {
                if(i is MsRulesFolder)
                {
                    foreach(MsRule j in i.Children)
                    {
                        if(j.Name == name) { return j; }
                    }

                    return null;
                }
            }

            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.schema; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mssql::schema"; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return "CREATE SCHEMA " + Name + ";"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            _Children.Add(new MsTablesFolder(Database, this));
            _Children.Add(new MsViewsFolder(Database, this));
            _Children.Add(new MsTypesFolder(Database, this));
            _Children.Add(new MsProceduresFolder(Database, this));
            _Children.Add(new MsFunctionsFolder(Database, this));
            _Children.Add(new MsSynonymsFolder(Database, this));
            _Children.Add(new MsRulesFolder(Database, this));
            _Children.Add(new MsSequencesFolder(Database, this));
        }
    }
}
