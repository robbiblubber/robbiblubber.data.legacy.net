﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.SQLite
{
    /// <summary>This class represents a SQLite table.</summary>
    public class LiteTable: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Table name.</summary>
        protected string _Name;
        
        /// <summary>Definition.</summary>
        protected string _Definition = null;

        /// <summary>Primary keys.</summary>
        protected string[] _PrimaryKeys = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Folder.</param>
        /// <param name="name">Name.</param>
        internal LiteTable(LiteDatabase database, LiteTablesFolder folder, string name)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent folder.</summary>
        public LiteTablesFolder Folder { get; protected set; }


        /// <summary>Gets the database instance.</summary>
        public LiteDatabase Database { get; protected set; }

        
        /// <summary>Gets the colums folder.</summary>
        public LiteColumnsFolder Columns
        {
            get
            {
                foreach(DbItem i in Children)
                {
                    if(i is LiteColumnsFolder) { return ((LiteColumnsFolder) i); }
                }

                return null;
            }
        }


        /// <summary>Gets the primary keys for the table.</summary>
        public string[] PrimaryKeys
        {
            get
            {
                if(_PrimaryKeys == null)
                {
                    List<string> pks = new List<string>();

                    foreach(LiteColumn i in Columns.Children) 
                    { 
                        if(i.IsPrimaryKey) { pks.Add(i.Name); }
                    }
                    _PrimaryKeys = pks.ToArray();
                }

                return _PrimaryKeys;
            }
        }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.table; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "lite::table"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            _Children.Add(new LiteColumnsFolder(Database, this));
            _Children.Add(new LiteIndexesFolder(Database, this));
            _Children.Add(new LiteTriggerFolder(Database, this));
        }
        

        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                IDbCommand cmd = null;
                string rval = "";

                try
                {
                    cmd = Database.Provider.CreateCommand("SELECT SQL FROM SQLITE_MASTER WHERE TYPE = 'table' AND NAME = '" + Name + "'");
                    rval = (string) cmd.ExecuteScalar();
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX0010a", ex); }

                Disposal.Dispose(cmd);

                return (string.IsNullOrWhiteSpace(rval) ? null : (rval.Trim(' ', ';') + ";"));
            }
        }
    }
}
