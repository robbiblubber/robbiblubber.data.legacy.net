﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>Database structure search extension class.</summary>
    public static class DbSearch
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Finds the next item by search text.</summary>
        /// <param name="provider">Provider.</param>
        /// <param name="start">Start item.</param>
        /// <param name="text">Search text.</param>
        /// <returns></returns>
        public static DbItem FindNext(this ProviderItem provider, DbItem start, string text)
        {
            DbItem rval;
            bool hit = false;

            text = text.ToLower();

            if((rval = _FindNext(provider.Items, text, start, ref hit)) != null) { return rval; }

            hit = true;
            return _FindNext(provider.Items, text, start, ref hit);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Finds the next item.</summary>
        /// <param name="items">Item list.</param>
        /// <param name="text">Search text.</param>
        /// <param name="start">Start item.</param>>
        /// <param name="hit">Hit flag.</param>
        /// <returns></returns>
        private static DbItem _FindNext(IEnumerable<DbItem> items, string text, DbItem start, ref bool hit)
        {
            DbItem rval;
            foreach(DbItem i in items)
            {
                if(hit && i.Name.ToLower().Contains(text)) { return i; }

                if(i == start)
                {
                    if(hit) { return null; }
                    hit = true;
                }

                if(i.HasChildren)
                {
                    if((rval = _FindNext(i.Children, text, start, ref hit)) != null) { return rval; }
                }
            }

            return null;
        }
    }
}
