﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra Keyspace.</summary>
    public class CasKeyspace: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Table name.</summary>
        protected string _Name;


        /// <summary>Definition.</summary>
        protected string _Definition = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="name">Name.</param>
        /// <param name="durable">Durable writes flag.</param>
        /// <param name="replication">Replication options.</param>
        internal CasKeyspace(CasDatabase database, string name, bool durable, IDictionary<string, string> replication)
        {
            Database = database;
            _Name = name;
            _Definition = "CREATE KEYSPACE " + name + "\r\nWITH REPLICATION =\r\n{\r\n";

            bool first = true;
            foreach(KeyValuePair<string, string> i in replication)
            {
                if(first)
                {
                    first = false;
                }
                else { _Definition += ",\r\n"; }

                _Definition += ("'" + i.Key + "' : " + (i.Value.IsNumeric() ? i.Value : "'" + i.Value + "'"));
            }
            _Definition += ("\r\n}\r\nAND DURABLE_WRITES = " + (durable ? "true" : "false") + ";");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public CasDatabase Database { get; protected set; }

        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.tablespace; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "cas::kspace"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            
            _Children.Add(new CasTablesFolder(this));
            _Children.Add(new CasViewsFolder(this));
            _Children.Add(new CasTypesFolder(this));
            _Children.Add(new CasFunctionsFolder(this));
            _Children.Add(new CasAggregatesFolder(this));
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return _Definition; }
        }
    }
}
