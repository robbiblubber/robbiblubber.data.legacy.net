﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Util.Coding;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>Connection manager window.</summary>
    public partial class FormDatabases: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private constants                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Configuration file.</summary>
        private static readonly string _CONFIG_FILE = PathOp.SystemUserConfigurationPath + @"\robbiblubber.org\SQL\db.connections";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Root folder.</summary>
        private Folder _Root = null;

        /// <summary>Source text.</summary>
        private string _Source = "";
        
        /// <summary>Last provider.</summary>
        private string _LastProvider = "";
        
        /// <summary>Determines if combo box is currently updating.</summary>
        private bool _ComboUpdating = false;
        
        /// <summary>Currently dragged node.</summary>
        private TreeNode _DragNode = null;

        /// <summary>Drop target node.</summary>
        private TreeNode _DropTarget = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormDatabases()
        {
            InitializeComponent();

            foreach(IProviderManager i in ProviderManager.Managers)
            {
                _IlistProviders.Images.Add(i.ProviderName, i.ProviderIcon);
            }

            foreach(IProviderManager i in ProviderManager.Managers)
            {
                _ComboProvider.Items.Add(new RichComboItem(i.DisplayName, i.ProviderName, i));
            }
            _UpdateCombo(_LastProvider);

            Icon = Icon.FromHandle(new Bitmap(Resources.database_manager).GetHicon());


            Ddp cfg = Ddp.Open(_CONFIG_FILE, GZip.BASE64);

            if(!File.Exists(_CONFIG_FILE))
            {
                _Root = new Folder(null);
                _Root.Name = "sqlfwx::udiag.databases.root".Localize("Databases");

                _Root.Save(cfg);
                cfg.Save();
            }
            else
            {
                _Root = new Folder(cfg, cfg.Sections.Where(m => (m.Get<string>("parent") == "NULL")).First());
                _LastProvider = cfg.Get<string>("general/last");
            }
            _Source = cfg.Text;

            foreach(IProviderManager i in ProviderManager.Managers)
            {
                _IlistConnections.Images.Add(i.ProviderName, i.ConnectionIcon);
            }
            UnknownManager unk = new UnknownManager();
            _IlistConnections.Images.Add(unk.ProviderName, unk.ConnectionIcon);


            _Draw(_Root, _TreeConnections.Nodes);
            _TreeConnections_AfterSelect(_TreeConnections, null);

            this.Localize();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads a database provider.</summary>
        /// <param name="name">Provider name.</param>
        /// <returns>Database provider.</returns>
        public static ProviderItem LoadProvider(string name)
        {
            Ddp cfg = Ddp.Open(_CONFIG_FILE, GZip.BASE64);

            foreach(DdpElement i in cfg.Sections.Where(m => m.Name.StartsWith("provider::")))
            {
                if(i.Get<string>("name").ToLower().Trim() == name)
                {
                    return ProviderManager.GetManager(i.Get<string>("class")).Spawn(i, null);
                }
            }

            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the selected provider.</summary>
        public ProviderItem Provider
        {
            get
            {
                if(_TreeConnections.SelectedNode.Tag is ProviderItem)
                {
                    return (ProviderItem) _TreeConnections.SelectedNode.Tag;
                }

                return null;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a folder.</summary>
        public void AddFolder()
        {
            Folder f = new Folder((Folder) _TreeConnections.SelectedNode.Tag);
            f.Name = "sqlfwx::udiag.databases.newfolder".Localize("New folder");
            _TreeConnections.SelectedNode = _Draw(f, _TreeConnections.SelectedNode.Nodes);
        }


        /// <summary>Adds a connection.</summary>
        public void AddConnection()
        {
            if(ProviderManager.Managers.Length == 0)
            {
                MessageBox.Show("sqlfwx::udiag.noproviders.text".Localize("There are no database providers available."), "sqlfwx::udiag.noproviders.caption".Localize("Connect"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            ProviderItem p = ProviderManager.Managers.First().Spawn((string) null, (Folder) _TreeConnections.SelectedNode.Tag);
            p.Name = "sqlfwx::udiag.databases.newconnection".Localize("New connection");
            _TreeConnections.SelectedNode = _Draw(p, _TreeConnections.SelectedNode.Nodes);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sets the combobox selection.</summary>
        /// <param name="provider">Provider class.</param>
        private void _UpdateCombo(string provider = null)
        {
            int n = -1;
            _ComboUpdating = true;
            
            if(provider == null) { provider = ((ProviderItem) _TreeConnections.SelectedNode.Tag).Manager.ProviderName; }
            for(int i = 0; i < _ComboProvider.Items.Count; i++)
            {
                if(((IProviderManager) ((RichComboItem) _ComboProvider.Items[i]).Tag).ProviderName == provider)
                {
                    n = i;
                    break;
                }
            }

            try
            {
                if(n < 0)
                {
                    _ComboProvider.SelectedIndex = 0;
                    _ComboProvider.Visible = false;
                }
                else
                {
                    _ComboProvider.SelectedIndex = n;
                    _ComboProvider.Visible = true;
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX04106", ex); }

            _ComboUpdating = false;
        }


        /// <summary>Updates the panel.</summary>
        private void _UpdatePanel()
        {
            if(!_PanelSettings.Controls.Contains((Control) ((ProviderItem) _TreeConnections.SelectedNode.Tag).Manager.Control))
            {
                _PanelSettings.Controls.Add((Control) ((ProviderItem) _TreeConnections.SelectedNode.Tag).Manager.Control);
                ((Control) ((ProviderItem) _TreeConnections.SelectedNode.Tag).Manager.Control).Dock = DockStyle.Fill;
            }

            ((Control) ((ProviderItem) _TreeConnections.SelectedNode.Tag).Manager.Control).BringToFront();

            ((ProviderItem) _TreeConnections.SelectedNode.Tag).Manager.Control.Node = _TreeConnections.SelectedNode;
            ((ProviderItem) _TreeConnections.SelectedNode.Tag).Manager.Control.Provider = (ProviderItem) _TreeConnections.SelectedNode.Tag;

            _ButtonConnect.Enabled = _ButtonOK.Enabled = (!(((Control) ((ProviderItem) _TreeConnections.SelectedNode.Tag).Manager.Control) is UnknownProviderControl));
        }


        /// <summary>Draws a tree node.</summary>
        /// <param name="item">Item.</param>
        /// <param name="nodes">Parent tree node collection.</param>
        /// <returns>Node.</returns>
        private TreeNode _Draw(ISortable item, TreeNodeCollection nodes)
        {
            TreeNode n = new TreeNode(item.Name);
            n.Tag = item;
            
            if(item is Folder)
            {
                n.ImageKey = n.SelectedImageKey = ((item.Parent == null) ? "databases" : "folder");

                foreach(ISortable i in ((Folder) item).Items) { _Draw(i, n.Nodes); }
            }
            else
            {
                n.ImageKey = n.SelectedImageKey = ((ProviderItem) item).Manager.ProviderName;
            }

            nodes.Add(n);

            if(item.Parent == null) { n.Expand(); }

            return n;
        }


        /// <summary>Redraws the part of the tree that contains a given node.</summary>
        /// <param name="node">Node.</param>
        private void _Redraw(TreeNode node = null)
        {
            if(node == null) { node = _TreeConnections.SelectedNode; }

            _Redraw(node.Parent, (ISortable) node.Tag);
        }


        /// <summary>Redraws children of a node.</summary>
        /// <param name="node">Node.</param>
        /// <param name="item">Item.</param>
        private void _Redraw(TreeNode node, ISortable item)
        {
            List<ISortable> open = new List<ISortable>();

            _ListOpenNodes(open, node);

            node.Nodes.Clear();
            TreeNode n, m = null;

            foreach(ISortable i in ((Folder) node.Tag).Items)
            {
                n = _Draw(i, node.Nodes);
                if(i == item) { m = n; }
                if(open.Contains(i)) n.Expand();
            }

            if(m != null) { _TreeConnections.SelectedNode = m; }
        }


        /// <summary>Creates a list of open tree node tags.</summary>
        /// <param name="tags">List.</param>
        /// <param name="node">Start node.</param>
        private void _ListOpenNodes(List<ISortable> tags, TreeNode node)
        {
            if(node.IsExpanded) { tags.Add((ISortable) node.Tag); }

            foreach(TreeNode i in node.Nodes) { _ListOpenNodes(tags, i); }
        }


        /// <summary>Saves the configuration file.</summary>
        private void _SaveConfig()
        {
            Ddp cfg = new Ddp();

            cfg.Set("general/last", _LastProvider);
            ((Folder) _TreeConnections.Nodes[0].Tag).Save(cfg);
            cfg.Save(_CONFIG_FILE, GZip.BASE64);
        }


        /// <summary>Returns if a drag operation is allowed.</summary>
        /// <returns>Returns TRUE if the operation is allowed.</returns>
        private bool _DragAllowed()
        {
            if(_DropTarget == null) return false;
            if(_DropTarget.Tag is ProviderItem) return false;
            return (_DragNode.Tag != _Root);
        }


        /// <summary>Updates item IDs.</summary>
        /// <param name="item">Item.</param>
        private void _UpdateIDs(ISortable item)
        {
            item.ID = StringOp.Unique(24);

            if(item is Folder)
            {
                foreach(ISortable i in ((Folder) item).Providers) { i.ID = StringOp.Unique(24); }
                foreach(Folder i in ((Folder) item).Folders) { _UpdateIDs(i); }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>After select.</summary>
        private void _TreeConnections_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if(_TreeConnections.SelectedNode == null)
            {
                _ButtonConnect.Enabled = false;
                return;
            }

            if(_TreeConnections.SelectedNode.Tag is Folder)
            {
                _ButtonConnect.Enabled = false;

                _LabelIcon.Visible = _LabelClass.Visible = true;
                _ComboProvider.Visible = false;
                _PanelSettings.Visible = true;

                if(!_PanelSettings.Controls.Contains(FolderControl.Instance))
                {
                    _PanelSettings.Controls.Add(FolderControl.Instance);
                    FolderControl.Instance.Dock = DockStyle.Fill;
                }
                FolderControl.Instance.BringToFront();

                FolderControl.Instance.Node = _TreeConnections.SelectedNode;

                if(((Folder) _TreeConnections.SelectedNode.Tag).Parent == null)
                {
                    _LabelIcon.Image = _IlistConnections.Images["databases"];
                    _LabelClass.Text = "sqlgwx::udiag.databases.class.databases".Localize("Databases");
                }
                else
                {
                    _LabelIcon.Image = _IlistConnections.Images["folder"];
                    _LabelClass.Text = "sqlgwx::udiag.databases.class.folder".Localize("Folder");
                }
            }
            else
            {
                _ButtonConnect.Enabled = (!(_TreeConnections.SelectedNode.Tag is UnknownProviderItem));

                _ComboProvider.Visible = true;
                _LabelIcon.Visible = _LabelClass.Visible = false;
                _PanelSettings.Visible = true;

                _UpdateCombo();
                _UpdatePanel();
            }
        }


        /// <summary>Menu "Add folder" click.</summary>
        private void _CmenuAddFolder_Click(object sender, EventArgs e)
        {
            AddFolder();
        }


        /// <summary>Tree mouse down.</summary>
        private void _TreeConnections_MouseDown(object sender, MouseEventArgs e)
        {
            _TreeConnections.SelectedNode = _TreeConnections.HitTest(e.Location).Node;
        }


        /// <summary>Menu "Add connection" click.</summary>
        private void _CmenuAddConnection_Click(object sender, EventArgs e)
        {
            AddConnection();
        }


        /// <summary>Provider selection changed.</summary>
        private void _ComboProvider_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(_TreeConnections.SelectedNode == null) return;
            if(_ComboUpdating) return;

            try
            {
                ProviderItem oldv = (ProviderItem) _TreeConnections.SelectedNode.Tag;
                ProviderItem newv = ((IProviderManager) ((RichComboItem) _ComboProvider.SelectedItem).Tag).Spawn(oldv.ID, oldv.Parent);
                newv.Parent.Providers.Remove(oldv);
                newv.Name = oldv.Name;
                newv.SortIndex = oldv.SortIndex;

                _LastProvider = newv.Manager.ProviderName;
                _TreeConnections.SelectedNode.Tag = newv;
                _TreeConnections.SelectedNode.ImageKey = _TreeConnections.SelectedNode.SelectedImageKey = newv.Manager.ProviderName;
                _UpdatePanel();
            }
            catch(Exception) {}
        }


        /// <summary>Button "Connect" click.</summary>
        private void _ButtonConnect_Click(object sender, EventArgs e)
        {
            _SaveConfig();
            DialogResult = DialogResult.OK;
            Close();
        }


        /// <summary>Button "OK" click.</summary>
        private void _ButtonOK_Click(object sender, EventArgs e)
        {
            _SaveConfig();
            Close();
        }


        /// <summary>Menu "Delete" click.</summary>
        private void _CmenuDelete_Click(object sender, EventArgs e)
        {
            string mtext = "sqlfwx::udiag.databases.confirmdelete.text".Localize("Do you really want to delete $(a) $(b)?").Replace("$(a)", (_TreeConnections.SelectedNode.Tag is Folder) ? "folder" : "connction");
                   mtext = mtext.Replace("$(b)", "\"" + _TreeConnections.SelectedNode.Text + "\"");

            if(MessageBox.Show(mtext, "sqlfwx::udiag.database.confirmdelete.caption".Localize("Delete"), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if(_TreeConnections.SelectedNode.Tag is Folder)
                {
                    ((Folder) _TreeConnections.SelectedNode.Tag).Parent.Folders.Remove((Folder) _TreeConnections.SelectedNode.Tag);
                }
                else
                {
                    ((ProviderItem) _TreeConnections.SelectedNode.Tag).Parent.Providers.Remove((ProviderItem) _TreeConnections.SelectedNode.Tag);
                }

                _TreeConnections.SelectedNode.Remove();
            }
        }
        

        /// <summary>Context menu opening.</summary>
        private void _CmenuConnections_Opening(object sender, CancelEventArgs e)
        {
            if(_TreeConnections.SelectedNode == null)
            {
                _CmenuAddFolder.Visible = _CmenuAddConnection.Visible = _CmenuConnect.Visible = _CmenuMove.Visible = _CmenuDelete.Visible = false;
                return;
            }

            _CmenuAddFolder.Visible = _CmenuAddConnection.Visible = (_TreeConnections.SelectedNode.Tag is Folder);
            _CmenuConnect.Visible = (!(_TreeConnections.SelectedNode.Tag is Folder));
            _CmenuMove.Visible = _CmenuDelete.Visible = (_TreeConnections.SelectedNode.Tag != _Root);
        }


        /// <summary>Menu "Move up" click.</summary>
        private void _CmenuMoveUp_Click(object sender, EventArgs e)
        {
            ((ISortable) _TreeConnections.SelectedNode.Tag).MoveUp();
            _Redraw();
        }


        /// <summary>Menu "Move down" click.</summary>
        private void _CmenuMoveDown_Click(object sender, EventArgs e)
        {
            ((ISortable) _TreeConnections.SelectedNode.Tag).MoveDown();
            _Redraw();
        }


        /// <summary>Menu "Move" opening.</summary>
        private void _CmenuMove_DropDownOpening(object sender, EventArgs e)
        {
            _CmenuMoveUp.Enabled = ((ISortable) _TreeConnections.SelectedNode.Tag).CanMoveUp;
            _CmenuMoveDown.Enabled = ((ISortable) _TreeConnections.SelectedNode.Tag).CanMoveDown;
        }


        /// <summary>Tree view key down.</summary>
        private void _TreeConnections_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Delete)
            {
                if(_TreeConnections.SelectedNode.Tag != _Root) { _CmenuDelete_Click(null, null); }
                return;
            }

            if(e.Control)
            {
                if(e.KeyCode == Keys.Up)
                {
                    if(((ISortable) _TreeConnections.SelectedNode.Tag).CanMoveUp) { _CmenuMoveUp_Click(null, null); }
                }
                else if(e.KeyCode == Keys.Down)
                {
                    if(((ISortable) _TreeConnections.SelectedNode.Tag).CanMoveDown) { _CmenuMoveDown_Click(null, null); }
                }
            }
        }


        /// <summary>Tree view mouse move.</summary>
        private void _TreeConnections_MouseMove(object sender, MouseEventArgs e)
        {
            if((e.Button == MouseButtons.None) || (_TreeConnections.SelectedNode == null)) return;

            if(_TreeConnections.SelectedNode.Tag != _Root)
            {
                _DragNode = _TreeConnections.SelectedNode;
                _TreeConnections.DoDragDrop("d12e142://local", DragDropEffects.Move);
            }
            else
            {
                _TreeConnections.DoDragDrop("", DragDropEffects.None);
            }
        }


        /// <summary>Tree view drag over.</summary>
        private void _TreeConnections_DragOver(object sender, DragEventArgs e)
        {
            if(!e.Data.GetDataPresent(typeof(string)) || ((((string) e.Data.GetData(typeof(string)) != "d12e142://local"))))
            {
                _DropTarget = _DragNode = null;
                return;
            }

            _DropTarget = _TreeConnections.GetNodeAt(_TreeConnections.PointToClient(new Point(e.X, e.Y)));

            if(!_DragAllowed())
            {
                e.Effect = DragDropEffects.None;
            }
            else
            {
                e.Effect = DragDropEffects.Move;
            }
        }


        /// <summary>Tree view drag drop.</summary>
        private void _TreeConnections_DragDrop(object sender, DragEventArgs e)
        {
            _DropTarget = _TreeConnections.GetNodeAt(_TreeConnections.PointToClient(new Point(e.X, e.Y)));

            if(!_DragAllowed()) return;

            try
            {
                ((Folder) _DropTarget.Tag).MoveHere((ISortable) _DragNode.Tag);
                
                _DragNode.Remove();
                _Redraw(_DropTarget, (ISortable) _DragNode.Tag);
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX04107", ex); }
        }


        /// <summary>Menu "Export" click.</summary>
        private void _CmenuExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog f = new SaveFileDialog();
            f.Filter = "sqlfwx::udiag.filter.d12e142".Localize("Connection Export Files") + " (*.d12e142)|*.d12e142|" + "sqlfwx::udiag.filter.all".Localize("All Files") + "|*.*";

            if(f.ShowDialog() != DialogResult.OK) return;

            Ddp cfg = new Ddp();
            cfg.Set("export/version", Assembly.GetCallingAssembly().GetName().Version.ToString());
            cfg.Set("export/root", ((_TreeConnections.SelectedNode.Tag is Folder) ? "folder::" : "provider::") + ((ISortable) _TreeConnections.SelectedNode.Tag).ID);

            ((ISortable) _TreeConnections.SelectedNode.Tag).Export(cfg);

            cfg.Save(f.FileName, GZip.BASE64);
        }


        /// <summary>Menu "Import" click.</summary>
        private void _CmenuImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            f.Filter = "sqlfwx::udiag.filter.d12e142".Localize("Connection Export Files") + " (*.d12e142)|*.d12e142|" + "sqlfwx::udiag.filter.all".Localize("All Files") + "|*.*";

            if(f.ShowDialog() != DialogResult.OK) return;

            try
            {
                Ddp cfg = Ddp.Open(f.FileName, GZip.BASE64);

                string rootid = cfg.Get<string>("export/root");

                if(rootid.StartsWith("folder::"))
                {
                    Folder root = new Folder(cfg, cfg[rootid], true);

                    _UpdateIDs(root);
                    ((Folder) _TreeConnections.SelectedNode.Tag).MoveHere(root);
                }
                else
                {
                    ProviderItem p = ProviderManager.GetManager(cfg.Get<string>(rootid, "class")).Spawn(cfg[rootid], (Folder) _TreeConnections.SelectedNode.Tag);
                    p.ID = StringOp.Unique(24);
                }

                _Redraw(_TreeConnections.SelectedNode, (ISortable) _TreeConnections.SelectedNode.Tag);
            }
            catch(Exception ex)
            {
                DebugOp.Dump("SQLFWX04109", ex);
                MessageBox.Show("sqlfwx::udiag.importfailed.text".Localize("File could not be imported."), "sqlfwx::udiag.importfailed.text".Localize("Import"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>Menu "Advanced" opening.</summary>
        private void _CmenuAdvanced_DropDownOpening(object sender, EventArgs e)
        {
            _CmenuImport.Visible = ((_TreeConnections.SelectedNode == null) || (_TreeConnections.SelectedNode.Tag is Folder));
        }
    }
}
