﻿namespace Robbiblubber.Data.Interpreters.MSSQL
{
    partial class MSSQLProviderControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._TextServer = new System.Windows.Forms.TextBox();
            this._ButtonTest = new System.Windows.Forms.Button();
            this._LabelServer = new System.Windows.Forms.Label();
            this._LabelName = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._LabelDatabase = new System.Windows.Forms.Label();
            this._TextDatabase = new System.Windows.Forms.TextBox();
            this._LabelUserID = new System.Windows.Forms.Label();
            this._TextUserID = new System.Windows.Forms.TextBox();
            this._LabelPassword = new System.Windows.Forms.Label();
            this._TextPassword = new System.Windows.Forms.TextBox();
            this._CheckTrusted = new System.Windows.Forms.CheckBox();
            this._LinkEnhaced = new Robbiblubber.Util.Controls.LinkControl();
            this.SuspendLayout();
            // 
            // _TextServer
            // 
            this._TextServer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextServer.Location = new System.Drawing.Point(30, 85);
            this._TextServer.Name = "_TextServer";
            this._TextServer.Size = new System.Drawing.Size(403, 25);
            this._TextServer.TabIndex = 1;
            this._TextServer.TextChanged += new System.EventHandler(this._TextServer_TextChanged);
            // 
            // _ButtonTest
            // 
            this._ButtonTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonTest.Location = new System.Drawing.Point(286, 284);
            this._ButtonTest.Name = "_ButtonTest";
            this._ButtonTest.Size = new System.Drawing.Size(147, 29);
            this._ButtonTest.TabIndex = 7;
            this._ButtonTest.Tag = "sqlfwx::common.button.test";
            this._ButtonTest.Text = "&Test";
            this._ButtonTest.UseVisualStyleBackColor = true;
            this._ButtonTest.Click += new System.EventHandler(this._ButtonTest_Click);
            // 
            // _LabelServer
            // 
            this._LabelServer.AutoSize = true;
            this._LabelServer.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelServer.Location = new System.Drawing.Point(27, 69);
            this._LabelServer.Name = "_LabelServer";
            this._LabelServer.Size = new System.Drawing.Size(41, 13);
            this._LabelServer.TabIndex = 1;
            this._LabelServer.Tag = "sqlfwx::udiag.connect.server";
            this._LabelServer.Text = "&Server:";
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.Location = new System.Drawing.Point(27, 19);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Tag = "sqlfwx::udiag.connect.name";
            this._LabelName.Text = "&Name:";
            // 
            // _TextName
            // 
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(30, 35);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(403, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._TextName_TextChanged);
            // 
            // _LabelDatabase
            // 
            this._LabelDatabase.AutoSize = true;
            this._LabelDatabase.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDatabase.Location = new System.Drawing.Point(27, 117);
            this._LabelDatabase.Name = "_LabelDatabase";
            this._LabelDatabase.Size = new System.Drawing.Size(58, 13);
            this._LabelDatabase.TabIndex = 2;
            this._LabelDatabase.Tag = "sqlfwx::udiag.connect.db";
            this._LabelDatabase.Text = "&Database:";
            // 
            // _TextDatabase
            // 
            this._TextDatabase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDatabase.Location = new System.Drawing.Point(30, 133);
            this._TextDatabase.Name = "_TextDatabase";
            this._TextDatabase.Size = new System.Drawing.Size(403, 25);
            this._TextDatabase.TabIndex = 2;
            this._TextDatabase.TextChanged += new System.EventHandler(this._TextDatabase_TextChanged);
            // 
            // _LabelUserID
            // 
            this._LabelUserID.AutoSize = true;
            this._LabelUserID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelUserID.Location = new System.Drawing.Point(27, 201);
            this._LabelUserID.Name = "_LabelUserID";
            this._LabelUserID.Size = new System.Drawing.Size(47, 13);
            this._LabelUserID.TabIndex = 4;
            this._LabelUserID.Tag = "sqlfwx::udiag.connect.uid";
            this._LabelUserID.Text = "&User ID:";
            // 
            // _TextUserID
            // 
            this._TextUserID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextUserID.Location = new System.Drawing.Point(30, 217);
            this._TextUserID.Name = "_TextUserID";
            this._TextUserID.Size = new System.Drawing.Size(199, 25);
            this._TextUserID.TabIndex = 4;
            this._TextUserID.TextChanged += new System.EventHandler(this._TextUserID_TextChanged);
            // 
            // _LabelPassword
            // 
            this._LabelPassword.AutoSize = true;
            this._LabelPassword.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelPassword.Location = new System.Drawing.Point(231, 201);
            this._LabelPassword.Name = "_LabelPassword";
            this._LabelPassword.Size = new System.Drawing.Size(59, 13);
            this._LabelPassword.TabIndex = 5;
            this._LabelPassword.Tag = "sqlfwx::udiag.connect.pwd";
            this._LabelPassword.Text = "&Password:";
            // 
            // _TextPassword
            // 
            this._TextPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextPassword.Location = new System.Drawing.Point(234, 217);
            this._TextPassword.Name = "_TextPassword";
            this._TextPassword.PasswordChar = '*';
            this._TextPassword.Size = new System.Drawing.Size(199, 25);
            this._TextPassword.TabIndex = 5;
            this._TextPassword.UseSystemPasswordChar = true;
            this._TextPassword.TextChanged += new System.EventHandler(this._TextPassword_TextChanged);
            // 
            // _CheckTrusted
            // 
            this._CheckTrusted.AutoSize = true;
            this._CheckTrusted.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._CheckTrusted.Location = new System.Drawing.Point(30, 174);
            this._CheckTrusted.Name = "_CheckTrusted";
            this._CheckTrusted.Size = new System.Drawing.Size(126, 17);
            this._CheckTrusted.TabIndex = 3;
            this._CheckTrusted.Tag = "sqlfwx::udiag.connect.trusted";
            this._CheckTrusted.Text = "&Trusted Connection";
            this._CheckTrusted.UseVisualStyleBackColor = true;
            this._CheckTrusted.CheckedChanged += new System.EventHandler(this._CheckTrusted_CheckedChanged);
            // 
            // _LinkEnhaced
            // 
            this._LinkEnhaced.AutoSize = true;
            this._LinkEnhaced.Cursor = System.Windows.Forms.Cursors.Hand;
            this._LinkEnhaced.DisabledColor = System.Drawing.Color.Gray;
            this._LinkEnhaced.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LinkEnhaced.LinkText = "Enhanced...";
            this._LinkEnhaced.Location = new System.Drawing.Point(30, 287);
            this._LinkEnhaced.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LinkEnhaced.Name = "_LinkEnhaced";
            this._LinkEnhaced.Size = new System.Drawing.Size(72, 21);
            this._LinkEnhaced.TabIndex = 6;
            this._LinkEnhaced.TabStop = false;
            this._LinkEnhaced.Tag = "sqlfwx::common.link.enhanced";
            this._LinkEnhaced.Underline = false;
            this._LinkEnhaced.Click += new System.EventHandler(this._LinkEnhaced_Click);
            // 
            // MSSQLProviderControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._LinkEnhaced);
            this.Controls.Add(this._CheckTrusted);
            this.Controls.Add(this._LabelPassword);
            this.Controls.Add(this._TextPassword);
            this.Controls.Add(this._LabelUserID);
            this.Controls.Add(this._TextUserID);
            this.Controls.Add(this._LabelDatabase);
            this.Controls.Add(this._TextDatabase);
            this.Controls.Add(this._LabelName);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelServer);
            this.Controls.Add(this._ButtonTest);
            this.Controls.Add(this._TextServer);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MSSQLProviderControl";
            this.Size = new System.Drawing.Size(463, 338);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _TextServer;
        private System.Windows.Forms.Button _ButtonTest;
        private System.Windows.Forms.Label _LabelServer;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.Label _LabelDatabase;
        private System.Windows.Forms.TextBox _TextDatabase;
        private System.Windows.Forms.Label _LabelUserID;
        private System.Windows.Forms.TextBox _TextUserID;
        private System.Windows.Forms.Label _LabelPassword;
        private System.Windows.Forms.TextBox _TextPassword;
        private System.Windows.Forms.CheckBox _CheckTrusted;
        private Robbiblubber.Util.Controls.LinkControl _LinkEnhaced;
    }
}
