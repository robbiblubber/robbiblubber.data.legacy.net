﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;

using Robbiblubber.Data.DDL;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Providers.MSSQL
{
    /// <summary>This class implements the SQL Server database provider.</summary>
    public class MSSQLProvider: ProviderBase, IProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MSSQLProvider(): base()
        {
            _Parser = new MSSQLParser();
        }
        

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Provider configuration data.</param>
        public MSSQLProvider(string data): base(data)
        {
            _Parser = new MSSQLParser();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the server.</summary>
        public string Server { get; set; }


        /// <summary>Gets or sets the database name.</summary>
        public string Database { get; set; }


        /// <summary>Gets or sets if the connection is trusted.</summary>
        public bool Trusted { get; set; }


        /// <summary>Gets or sets the user ID.</summary>
        public string UserID { get; set; }


        /// <summary>Gets or sets the password.</summary>
        [PasswordPropertyTextAttribute]
        public string Password { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLProvider                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the provider configuration data.</summary>
        public override string Data
        {
            get
            {
                Ddp ddp = new Ddp();

                ddp["provider"].Value = ProviderName;
                ddp["server"].Value = Server;
                ddp["database"].Value = Database;
                ddp["trusted"].Value = Trusted;
                ddp["userid"].Value = UserID;
                ddp["password"].Value = Password;
                ddp["enhanced"].Value = EnhancedString;

                return ddp.ToString();
            }
            set
            {
                Ddp ddp = new Ddp(value);

                Server = ddp["server"];
                Database = ddp["database"];
                Trusted = ddp["trusted"];
                UserID = ddp["userid"];
                Password = ddp["password"];
                EnhancedString = ddp["enhanced"];
            }
        }


        /// <summary>Connects to the database.</summary>
        public override void Connect()
        {
            if(EnhancedString.Trim() == "")
            {
                if(Trusted)
                {
                    _Connection = new SqlConnection("server=" + Server + ";Database=" + Database + ";Trusted_Connection=True;");
                }
                else
                {
                    _Connection = new SqlConnection("server=" + Server + ";Database=" + Database + ";User ID=" + UserID + ";Password=" + Password + ";");
                }
            }
            else
            {
                _Connection = new SqlConnection(EnhancedString);
            }
            _Connection.Open();
        }



        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        /// <remarks>Currently does not read default values and check constraints.</remarks>
        public override IDDLTable GetTable(string tableName)
        {
            string[] tab = tableName.Split('.');
            IDbCommand cmd = CreateCommand();
            cmd.CommandText = "SELECT C.NAME, C.IS_NULLABLE, Y.NAME, C.MAX_LENGTH FROM sys.ALL_COLUMNS C, sys.TABLES T, sys.SCHEMAS S, sys.TYPES Y WHERE S.NAME = '" + tab[0] + "' AND T.NAME = '" + tab[1] + "' AND S.SCHEMA_ID = T.SCHEMA_ID AND T.OBJECT_ID = C.OBJECT_ID AND Y.SYSTEM_TYPE_ID = C.SYSTEM_TYPE_ID AND Y.USER_TYPE_ID = C.USER_TYPE_ID ORDER BY C.COLUMN_ID";
            IDataReader re = cmd.ExecuteReader();

            IDDLTable rval = new DDLBuilder(Parser).AddTable(tableName);
            while(re.Read())
            {
                rval.AddColumn(re.GetString(0), ((MSSQLParser) Parser).ToSQLDataType(re.GetString(2)), re.GetInt32(1) != 0);
                // TODO: get default values.
            }
            Disposal.Dispose(re, cmd);

            List<string> tmp = new List<string>();

            cmd = CreateCommand();
            cmd.CommandText = "SELECT U.COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS C, INFORMATION_SCHEMA.KEY_COLUMN_USAGE U WHERE C.CONSTRAINT_TYPE = 'PRIMARY KEY' AND C.CONSTRAINT_NAME = U.CONSTRAINT_NAME AND U.TABLE_SCHEMA = '" + tab[0] + "' AND U.TABLE_NAME = '" + tab[1] + "'";
            re = cmd.ExecuteReader();

            while(re.Read())
            {
                tmp.Add(re.GetString(0));
            }
            Disposal.Dispose(re, cmd);
            if(tmp.Count > 0) { rval.AddPrimaryKey(tmp.ToArray()); }

            foreach(IDDLColumn i in rval.Columns)
            {
                cmd = CreateCommand();
                cmd.CommandText = "SELECT X.TABLE_SCHEMA, X.TABLE_NAME, X.COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS C, INFORMATION_SCHEMA.KEY_COLUMN_USAGE U, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE X " +
                                    "WHERE C.CONSTRAINT_NAME = U.CONSTRAINT_NAME AND X.CONSTRAINT_NAME = C.CONSTRAINT_NAME AND C.CONSTRAINT_TYPE = 'FOREIGN KEY' " +
                                    "AND C.TABLE_SCHEMA = '" + tab[0] + "' AND C.TABLE_NAME = '" + tab[1] + "' AND U.COLUMN_NAME = '" + i.ColumnName + "';";
                re = cmd.ExecuteReader();

                if(re.Read())
                {
                    rval.AddForeignKey(i.ColumnName, re.GetString(0) + "." + re.GetString(1), re.GetString(2));
                    // TODO: check cascades.
                }
                Disposal.Dispose(re, cmd);
            }

            // TODO: get check constraints.

            cmd = CreateCommand();
            cmd.CommandText = "SELECT I.NAME, I.IS_UNIQUE FROM sys.INDEXES I, sys.TABLES T, sys.SCHEMAS S WHERE I.OBJECT_ID = T.OBJECT_ID AND T.SCHEMA_ID = S.SCHEMA_ID AND S.NAME = '" + tab[0] + "' AND T.NAME = '" + tab[1] + "' AND I.NAME IS NOT NULL ORDER BY I.NAME";
            re = cmd.ExecuteReader();

            List<Tuple<string, bool>> idx = new List<Tuple<string, bool>>();
            while(re.Read())
            {
                idx.Add(new Tuple<string, bool>(re.GetString(0), re.GetBoolean(1)));
            }
            Disposal.Dispose(re, cmd);

            foreach(Tuple<string, bool> i in idx)
            {
                cmd = CreateCommand();
                cmd.CommandText = "SELECT C.NAME FROM sys.COLUMNS C, sys.TABLES T, sys.SCHEMAS S, sys.INDEXES I, sys.INDEX_COLUMNS X " +
                                  "WHERE C.OBJECT_ID = T.OBJECT_ID AND I.OBJECT_ID = T.OBJECT_ID AND X.OBJECT_ID = T.OBJECT_ID AND X.INDEX_ID = I.INDEX_ID AND T.SCHEMA_ID = S.SCHEMA_ID " +
                                  "AND X.COLUMN_ID = C.COLUMN_ID AND S.NAME = '" + tab[0] + "' AND T.NAME = '" + tab[1] + "' AND I.NAME = '" + i.Item1 + "' ORDER BY X.KEY_ORDINAL";
                re = cmd.ExecuteReader();

                tmp = new List<string>();
                while(re.Read())
                {
                    tmp.Add(re.GetString(0));
                }
                Disposal.Dispose(re, cmd);

                if(i.Item2) { rval.AddNamedUniqueIndex(i.Item1, tmp.ToArray()); } else { rval.AddNamedIndex(i.Item1, tmp.ToArray()); }
            }

            return rval;
        }


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        public override string[] GetTablesNames(string filter)
        {
            if(string.IsNullOrWhiteSpace(filter)) { filter = "%"; }
            filter = filter.Replace("*", "%").Replace("?", "_");

            List<string> rval = new List<string>();

            IDbCommand cmd = Connection.CreateCommand();
            cmd.CommandText = "SELECT T.NAME, S.NAME FROM sys.TABLES T, sys.SCHEMAS S WHERE (Upper(S.NAME) LIKE '" + filter.ToUpper() + "' OR Upper(T.NAME) LIKE  '" + filter.ToUpper() + " AND S.SCHEMA_ID = T.SCHEMA_ID ORDER BY T.NAME";
            IDataReader re = cmd.ExecuteReader();

            while(re.Read())
            {
                rval.Add(re.GetString(1) + "." + re.GetString(0));
            }
            Disposal.Dispose(re, cmd);

            return rval.ToArray();
        }
    }
}
