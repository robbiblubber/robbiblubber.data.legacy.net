﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server table.</summary>
    public class MsTable: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Table name.</summary>
        protected string _Name;
        
        /// <summary>Definition.</summary>
        protected string _Definition = null;

        /// <summary>Primary keys.</summary>
        protected string[] _PrimaryKeys = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        internal MsTable(MsDatabase database, MsTablesFolder folder, string name)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public MsTablesFolder Folder { get; private set; }

        
        /// <summary>Gets the colums folder.</summary>
        public MsColumnsFolder Columns
        {
            get
            {
                foreach(DbItem i in Children)
                {
                    if(i is MsColumnsFolder) { return ((MsColumnsFolder) i); }
                }

                return null;
            }
        }


        /// <summary>Gets the primary keys for the table.</summary>
        public string[] PrimaryKeys
        {
            get
            {
                if(_PrimaryKeys == null)
                {
                    List<string> pks = new List<string>();

                    foreach(MsColumn i in Columns.Children)
                    {
                        if(i.IsPrimaryKey) { pks.Add(i.Name); }
                    }
                    _PrimaryKeys = pks.ToArray();
                }

                return _PrimaryKeys;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.table; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mssql::table"; }
        }

        
        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            
            _Children.Add(new MsColumnsFolder(Database, this));
            _Children.Add(new MsIndexesFolder(Database, this));
            _Children.Add(new MsTriggersFolder(Database, this));
        }
        

        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = "CREATE TABLE " + QualifiedName + "\r\n(";
                
                int ln = Columns.Children.Max(m => m.QualifiedName.Length) + 3;
                int lt = Columns.Children.Max(m => ((MsColumn) m).DataType.Length) + 3;
                bool first = true;

                foreach(MsColumn i in Columns.Children)
                {
                    if(first)
                    {
                        first = false;
                    }
                    else
                    {
                        rval += ",";
                    }

                    rval += ("\r\n   " + i.QualifiedName.PadRight(ln) + i.DataType.ToUpper().PadRight(lt) + (i.IsNotNull ? "NOT NULL   " : "           "));
                    if(i.IsPrimaryKey) { rval += "PRIMARY KEY   "; }
                    if(i.IsForeignKey) { rval += "REFERENCES " + i.ReferencedTable + " (" + i.ReferencedColumn + ")"; }

                    rval = rval.Trim();
                }

                rval += "\r\n);";
                
                return rval;
            }
        }


        /// <summary>Gets if the object defines a query.</summary>
        public override bool HasQuery
        {
            get { return true; }
        }


        /// <summary>Gets the query for this object.</summary>
        public override string Query
        {
            get
            {
                string f = "";
                bool first = true;
                
                foreach(MsColumn i in Columns.Children)
                {
                    if(first)
                    {
                        first = false;
                    }
                    else { f += ", "; }

                    f += i.QualifiedName;
                }
                return "SELECT " + f + " FROM " + QualifiedName;
            }
        }


        /// <summary>Gets the qualified name for this object.</summary>
        public override string QualifiedName
        {
            get { return Folder.Schema.Name + "." + Name; }
        }
    }
}
