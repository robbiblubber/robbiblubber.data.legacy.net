﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra function.</summary>
    public class CasFunction: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="lang">Language.</param>
        /// <param name="onNull">Called on null.</param>
        /// <param name="argnames">Argument names.</param>
        /// <param name="argtypes">Argument types.</param>
        /// <param name="rtype">Return type.</param>
        /// <param name="body">Function body.</param>
        internal CasFunction(CasFunctionsFolder folder, string name, string lang, bool onNull, IList<string> argnames, IList<string> argtypes, string rtype, string body)
        {
            _Parent = Folder = folder;
            FullName = SimpleName = name;

            FullName += "(";
            for(int i = 0; i < argtypes.Count; i++)
            {
                if(i > 0) { FullName += ", "; }
                FullName += argtypes[i].ToUpper();
            }
            FullName += ")";

            switch(lang.ToLower())
            {
                case "java":       Language = "Java"; break;
                case "javascript": Language = "JavaScript"; break;
                case "jruby":      Language = "JRuby"; break;
                case "jpython":    Language = "JPython"; break;
                case "Scala":      Language = "Scala"; break;
                default:           Language = lang; break;
            }
            
            CalledOnNull = onNull;
            ArgumentNames = argnames;
            ArgumentTypes = argtypes;
            ReturnType = rtype;
            Body = body;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent folder.</summary>
        public CasFunctionsFolder Folder { get; protected set; }


        /// <summary>Gets the keyspace.</summary>
        public CasKeyspace Keyspace { get { return Folder.Keyspace; } }


        /// <summary>Gets the function simple name.</summary>
        public string SimpleName { get; protected set; }


        /// <summary>Gets the function full name.</summary>
        public string FullName { get; protected set; }


        /// <summary>Gets the function language.</summary>
        public string Language { get; protected set; }


        /// <summary>Gets if the function is called on null.</summary>
        public bool CalledOnNull { get; protected set; }


        /// <summary>Gets the function argument names.</summary>
        public IList<string> ArgumentNames { get; protected set; }


        /// <summary>Gets the function argument types.</summary>
        public IList<string> ArgumentTypes { get; protected set; }


        /// <summary>Gets the function return type.</summary>
        public string ReturnType { get; protected set; }


        /// <summary>Gets the function body.</summary>
        public string Body { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return FullName; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get 
            {
                switch(Language.ToLower())
                {
                    case "java":       return Resources.function_java;
                    case "javascript": return Resources.function_js;
                    case "jruby":      return Resources.function_ruby;
                    case "jpython":    return Resources.function_python;
                    case "Scala":      return Resources.function_scala;
                }

                return Resources.function;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                switch(Language.ToLower())
                {
                    case "java":       return "cas::jfunc";
                    case "javascript": return "cas::jsfunc";
                    case "jruby":      return "cas::rfunc";
                    case "jpython":    return "cas::rfunc";
                    case "Scala":      return "cas::sfunc";
                }

                return "cas::func";
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = ("CREATE FUNCTION " + Keyspace.Name + "." + SimpleName + "(");

                for(int i = 0; i < ArgumentNames.Count; i++)
                {
                    if(i > 0) { rval += ", "; }
                    rval += (ArgumentNames[i] + " " + ArgumentTypes[i].ToUpper());
                }

                rval += (CalledOnNull ? ")\r\nCALLED ON NULL INPUT" : ")\r\nRETURNS NULL ON NULL INPUT");
                rval += ("\r\nRETURNS " + ReturnType.ToUpper());
                rval += ("\r\nLANGUAGE " + Language);
                rval += ("\r\nAS '\r\n" + Body.Replace("'", "''") + "\r\n';");

                return rval;
            }
        }
    }
}
