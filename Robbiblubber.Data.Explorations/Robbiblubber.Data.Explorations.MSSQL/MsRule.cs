﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server rule.</summary>
    public class MsRule: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Rule name.</summary>
        private string _Name;


        /// <summary>Object ID.</summary>
        private int _ID;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="id">ID.</param>
        internal MsRule(MsDatabase database, MsRulesFolder folder, string name, int id)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
            _ID = id;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public MsRulesFolder Folder { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.rule; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mssql::rule"; }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                IDbCommand cmd = null;
                IDataReader re = null;
                string rval = "";

                try
                {
                    cmd = Database.Provider.CreateCommand();
                    cmd.CommandText = "sp_helptext '" + QualifiedName + "'";
                    re = cmd.ExecuteReader();

                    while(re.Read())
                    {
                        if(re.GetString(0).Trim().ToLower().StartsWith("--sp_helptext")) continue;
                        if(string.IsNullOrWhiteSpace(re.GetString(0))) continue;
                        rval += re.GetString(0);
                    }
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX01214", ex); }

                Disposal.Dispose(re, cmd);

                return rval.Trim().TrimEnd(';').TrimEnd() + ";";
            }
        }


        /// <summary>Gets the qualified name for this object.</summary>
        public override string QualifiedName
        {
            get { return Folder.Schema.Name + "." + Name; }
        }
    }
}
