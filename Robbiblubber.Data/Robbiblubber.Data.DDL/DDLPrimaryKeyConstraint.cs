﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>This class represents a primary key constraint.</summary>
    public class DDLPrimaryKeyConstraint: DDLConstraint, IDDLPrimaryKeyConstraint, IDDLConstraint
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        protected DDLPrimaryKeyConstraint(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="table">Table.</param>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="columnNames">Column names.</param>
        protected internal DDLPrimaryKeyConstraint(IDDLTable table, string constraintName, params string[] columnNames): base(table, constraintName)
        {
            ColumnNames = columnNames;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="table">Table.</param>
        /// <param name="columnNames">Column names.</param>
        protected internal DDLPrimaryKeyConstraint(IDDLTable table, params string[] columnNames) : base(table, null)
        {
            ColumnNames = columnNames;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="columnNames">Column names.</param>
        protected internal DDLPrimaryKeyConstraint(string tableName, string constraintName, params string[] columnNames): base(tableName, constraintName)
        {
            ColumnNames = columnNames;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDDLPrimaryKeyConstraint                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the primary key column names.</summary>
        public virtual string[] ColumnNames { get; protected set; }
    }
}
