﻿using System;
using System.Collections.Generic;
using System.Reflection;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Providers.ApacheCassandra
{
    /// <summary>This class provides a Cassandra-specific implementation of the SQLParser class.</summary>
    public class CassandraParser: Parser, IParser
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the function used to add milliseconds to a timestamp.</summary>
        public static string AddToTimestampFunc
        {
            get; set;
        } = "sysp.addt";


        /// <summary>Gets or sets the function used to get the current timestamp.</summary>
        public static string DualTable
        {
            get; set;
        } = "sysp.DUAL";


        /// <summary>Gets or sets the function used to convert a string to lower case.</summary>
        public static string LowerFunc
        {
            get; set;
        } = "sysp.lower";


        /// <summary>Gets or sets the function used to convert a string to upper case.</summary>
        public static string UpperFunc
        {
            get; set;
        } = "sysp.upper";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a SQL data type for a database data type expression.</summary>
        /// <param name="dbDataType">Database data type string.</param>
        /// <returns>SQL data type.</returns>
        public SQLDataType ToSQLDataType(string dbDataType)
        {
            switch(dbDataType.ToUpper())
            {
                case "ASCII": return SQLDataType.TEXT;
                case "BIGINT": return SQLDataType.INT64;
                case "BLOB": return SQLDataType.BINARY;
                case "BOOLEAN": return SQLDataType.BOOLEAN;
                case "COUNTER": return SQLDataType.INT32;
                case "DECIMAL": return SQLDataType.DECIMAL;
                case "DOUBLE": return SQLDataType.DOUBLE;
                case "FLOAT": return SQLDataType.FLOAT;
                case "INET": return SQLDataType.STRING;
                case "INT": return SQLDataType.INT32;
                case "TEXT": return SQLDataType.TEXT;
                case "TIMESTAMP": return SQLDataType.TIMESTAMP;
                case "TIMEUUID": return SQLDataType.STRING;
                case "UUID": return SQLDataType.STRING;
                case "VARCHAR": return SQLDataType.TEXT;
                case "VARINT": return SQLDataType.INT64;
            }

            return SQLDataType.TEXT;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLParser                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parses a source string.</summary>
        /// <param name="source">Source string.</param>
        /// <returns>Parsed SQL statements.</returns>
        public override ISQLStatement[] Parse(string source)
        {
            List<string> strings = _Parse(source);

            ISQLStatement[] rval = new ISQLStatement[strings.Count];

            for(int i = 0; i < strings.Count; i++)
            {
                rval[i] = new CassandraStatement(strings[i]);
            }

            return rval;
        }


        /// <summary>Adds a time interval to a database date/time expression.</summary>
        /// <param name="value">Database expression that represents a date/time value.</param>
        /// <param name="interval">Time interval to add.</param>
        /// <returns>Database timestamp operation string.</returns>
        public override string AddTimeInterval(string value, TimeSpan interval)
        {
            return AddToTimestampFunc + "(" + value + ", " + Convert.ToInt64(interval.TotalMilliseconds).ToString() + ")";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLParser                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a well-formatted query function call for the current timestamp.</summary>
        /// <returns>Timestamp query string.</returns>
        public override string CurrentTimestampFunction
        {
            get { return "toTimestamp(now())"; }
        }


        /// <summary>Gets the database concatination operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string ConcatOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the database table as operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string AsOperator
        {
            get { return " AS "; }
        }


        /// <summary>Gets the add operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string AddOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the subtraction operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string SubtractOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the multiplication operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string MultiplyOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the division operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string DivideOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the modulo operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string ModuloOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the negation operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string NegateOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the binary or operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string BinaryOrOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the binary and operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string BinaryAndOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the exclusive or operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string ExOrOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the not equal operator.</summary>
        /// <remarks>This operator is documented but will not work.</remarks>
        public override string NotEqualOperator
        {
            get { return "!="; }
        }


        /// <summary>Gets the specific FROM part for dual selects.</summary>
        public override string FromDual
        {
            get { return "FROM " + DualTable; }
        }


        /// <summary>Returns a well-formatted query function call for next value from the given database sequence.</summary>
        /// <param name="sequence">Sequence Name.</param>
        /// <returns>Sequence query string.</returns>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support sequences.</exception>
        public override string SequenceNextValue(string sequence)
        {
            throw new NotSupportedException();
        }


        /// <summary>Returns the database lowercase value for a given expression.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Lowercase expression.</returns>
        public override string Lowercase(string expression)
        {
            return LowerFunc + "(" + expression + ")";
        }


        /// <summary>Returns the database lowercase value for a given expression.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Lowercase expression.</returns>
        public override string Uppercase(string expression)
        {
            return UpperFunc + "(" + expression + ")";
        }


        /// <summary>Returns a database data type name for a data type.</summary>
        /// <param name="dataType">Data type.</param>
        /// <returns>Data type name.</returns>
        public override string ToDataTypeName(SQLDataType dataType)
        {
            switch(dataType.ShortName)
            {
                case "s":
                case "sd":
                case "sx": return ((dataType.Length == 0) ? "TEXT" : "VARCHAR");
                case "t": return "TIMESTAMP";
                case "b": return "BOOLEAN";
                case "ib": return "INTEGER";
                case "i":
                    if(dataType.Length == 8) return "TINYINT";
                    if(dataType.Length == 16) return "SMALLINT";
                    if(dataType.Length == 64) return "BIGINT";
                    return "INTEGER";
                case "ff": return "FLOAT";
                case "fd": return "DOUBLE";
                case "fx": return "DECIMAL";
            }

            return "<ERROR>";
        }


        /// <summary>Returns a string representation of an object.</summary>
        /// <param name="obj">Object</param>
        /// <returns>String.</returns>
        /// <param name="style">Data format.</param>
        /// <param name="multiLine">Multi-line flag.</param>
        /// <param name="width">Line width.</param>
        public override string ToString(object obj, SQLComplexDataTypeFormatStyle style, bool multiLine, int width)
        {
            if((obj != null) && (obj.GetType().FullName.StartsWith("CqlSharp.UserDefined")))
            {
                SortedDictionary<string, object> rval = new SortedDictionary<string, object>();
                foreach(PropertyInfo i in obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
                {
                    rval.Add(i.Name.ToLower(), i.GetValue(obj));
                }

                return ToString(rval, style, multiLine, width);
            }

            return base.ToString(obj, style, multiLine, width);
        }
    }
}
