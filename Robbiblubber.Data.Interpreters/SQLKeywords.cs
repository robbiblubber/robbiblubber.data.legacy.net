﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Debug;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class implements a list of SQL keywords.</summary>
    public class SQLKeywords: IEnumerable<SQLKeywords.KeyWord>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Instance.</summary>
        private static SQLKeywords _Instance = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Elements.</summary>
        private Dictionary<string, KeyWord> _Elements = new Dictionary<string, KeyWord>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SQLKeywords()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="fileName">File name.</param>
        /// <param name="imageKey">Image key.</param>
        /// <param name="italic">Italic.</param>
        /// <param name="bold">Bold.</param>
        public SQLKeywords(string fileName, string imageKey = null, bool italic = true, bool bold = false)
        {
            Load(fileName);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the singleton instance.</summary>
        public static SQLKeywords Instance
        {
            get
            {
                if(_Instance == null)
                {
                    _Instance = new SQLKeywords();
                    _Instance.Load();
                }

                return _Instance;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds keywords to the list.</summary>
        /// <param name="keywords">Keywords.</param>
        public void AddKeywords(params string[] keywords)
        {
            AddKeywords(null, true, false, keywords);
        }


        /// <summary>Adds keywords to the list.</summary>
        /// <param name="keywords">Keywords.</param>
        /// <param name="imageKey">Image key.</param>
        /// <param name="italic">Italic.</param>
        /// <param name="bold">Bold.</param>
        public void AddKeywords(string imageKey, bool italic, bool bold, params string[] keywords)
        {
            foreach(string i in keywords)
            {
                if(string.IsNullOrWhiteSpace(i)) continue;
                if(!_Elements.ContainsKey(i)) { _Elements.Add(i, new KeyWord(i, imageKey, italic, bold)); }
            }
        }


        /// <summary>Removes keywords from the list.</summary>
        /// <param name="keywords">Keywords.</param>
        public void RemoveKeywords(params string[] keywords)
        {
            foreach(string i in keywords)
            {
                if(_Elements.ContainsKey(i)) { _Elements.Remove(i); }
            }
        }


        /// <summary>Loads keywords from file.</summary>
        /// <param name="fileName">File name.</param>
        /// <param name="imageKey">Image key.</param>
        /// <param name="italic">Italic.</param>
        /// <param name="bold">Bold.</param>
        public void Load(string fileName = null, string imageKey = null, bool italic = true, bool bold = false)
        {
            if(fileName == null) { fileName = PathOp.ApplicationDirectory + @"\sql.keywords"; }

            if(File.Exists(fileName))
            {
                _Elements = new Dictionary<string, KeyWord>();

                try
                {
                    AddKeywords(imageKey, italic, bold, File.ReadAllText(fileName).Replace('\r', '\n').Split(',', ';', '\n', ' '));
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX04131", ex); }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this list.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Elements.Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<KeyWord>                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this list.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<KeyWord> IEnumerable<KeyWord>.GetEnumerator()
        {
            return _Elements.Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] Keyword                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This class represents a keywoed.</summary>
        public class KeyWord: IAutoCompletionKeyword
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="word">Word.</param>
            /// <param name="imageKey">Image key.</param>
            /// <param name="italic">Italic flag.</param>
            /// <param name="bold">Bold flag.</param>
            public KeyWord(string word, string imageKey = null, bool italic = false, bool bold = false)
            {
                Word = word;
                ImageKey = imageKey;
                Italic = italic;
                Bold = bold;
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // [interface] IAutoCompletionKeyword                                                                           //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets the key word.</summary>
            public string Word { get; set; }


            /// <summary>Gets the image key.</summary>
            public string ImageKey { get; set; }


            /// <summary>Gets if the keyword should be italic.</summary>
            public bool Italic { get; set; }


            /// <summary>Gets if the keyword should be bold.</summary>
            public bool Bold { get; set; }
        }
    }
}
