﻿using System;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Providers.ApacheCassandra
{
    /// <summary>This class provides a Cassandra-specific implementation of the SQLStatement class.</summary>
    public class CassandraStatement: SQLStatement, ISQLStatement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // cosntructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public CassandraStatement(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="source">Source.</param>
        public CassandraStatement(string source): base(source)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLStatement                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the statement is a SELECT query.</summary>
        public override bool IsSelect
        {
            get { return (_LowerSource.StartsWith("select")); }
        }
    }
}
