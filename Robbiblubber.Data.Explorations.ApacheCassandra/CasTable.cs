﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra table.</summary>
    public class CasTable: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Table name.</summary>
        protected string _Name;


        /// <summary>Definition.</summary>
        protected string _Definition = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="folder">Folder.</param>
        /// <param name="name">Name.</param>
        internal CasTable(CasTablesFolder folder, string name)
        {
            _Parent = Folder = folder;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent folder.</summary>
        public CasTablesFolder Folder { get; protected set; }


        /// <summary>Gets the keyspace.</summary>
        public CasKeyspace Keyspace 
        { 
            get { return Folder.Keyspace; }
        }


        /// <summary>Gets the colums folder.</summary>
        public CasColumnsFolder Columns
        {
            get
            {
                foreach(DbItem i in Children)
                {
                    if(i is CasColumnsFolder) { return ((CasColumnsFolder) i); }
                }

                return null;
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.table; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "cas::table"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            _Children.Add(new CasColumnsFolder(this));
            _Children.Add(new CasIndexesFolder(this));
            _Children.Add(new CasTriggersFolder(this));
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = ("CREATE TABLE " + Keyspace.Name + "." + Name + "\r\n(\r\n");
                
                int l = Columns.Children.Max(m => m.Name.Length) + 3;
                int pks = Columns.Children.Where(m => ((CasColumn) m).PartitionKey).Count();
                bool first = true;
                foreach(CasColumn i in Columns.Children)
                {
                    if(first)
                    {
                        first = false;
                    }
                    else { rval += ",\r\n"; }

                    rval += ("   " + i.Name.PadRight(l, ' ') + i.DataType);
                }
                
                if(pks == 1)
                {
                    rval += (",\r\n   PRIMARY KEY (" + Columns.Children.Where(m => ((CasColumn) m).PartitionKey).Single().Name);
                }
                else if(pks > 1)
                {
                    rval += (",\r\n   PRIMARY KEY ((");

                    first = true;
                    foreach(CasColumn i in Columns.Children.Where(m => ((CasColumn) m).PartitionKey))
                    {
                        if(first)
                        {
                            first = false;
                        }
                        else { rval += ", "; }

                        rval += i.Name;
                    }
                    rval += ")";
                }

                if(pks > 0)
                {
                    foreach(CasColumn i in Columns.Children.Where(m => ((CasColumn) m).Clustering))
                    {
                        rval += (", " + i.Name);
                    }
                    rval += ")\r\n";
                }

                rval += ");";

                return rval;
            }
        }
    }
}
