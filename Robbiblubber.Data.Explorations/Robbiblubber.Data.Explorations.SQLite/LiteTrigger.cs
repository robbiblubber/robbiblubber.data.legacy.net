﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.SQLite
{
    /// <summary>This class represents a SQLite trigger.</summary>
    public class LiteTrigger: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Trigger name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        internal LiteTrigger(LiteDatabase database, LiteTriggerFolder folder, string name)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public LiteDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public LiteTriggerFolder Folder { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }

                
        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.trigger; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "lite::trigger"; }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                IDbCommand cmd = null;
                string rval = "";

                try
                {
                    cmd = Database.Provider.CreateCommand("SELECT SQL FROM SQLITE_MASTER WHERE TYPE = 'trigger' AND TBL_NAME = '" + Folder.Table.Name + "' AND NAME = '" + Name + "'");
                    rval = (string) cmd.ExecuteScalar();
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX0010c", ex); }

                Disposal.Dispose(cmd);

                return (rval.Trim(' ', ';') + ";");
            }
        }
    }
}
