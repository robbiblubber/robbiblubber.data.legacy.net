﻿namespace Robbiblubber.Data.Interpreters
{
    partial class GeneralSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GeneralSettingsControl));
            this._LabelTransactions = new System.Windows.Forms.Label();
            this._LabelTransactionsIcon = new System.Windows.Forms.Label();
            this._PanelTransactions = new System.Windows.Forms.Panel();
            this._OptionTransactionsDisabled = new System.Windows.Forms.RadioButton();
            this._OptionTransactionsEnabled = new System.Windows.Forms.RadioButton();
            this._OptionTransactionsDefault = new System.Windows.Forms.RadioButton();
            this._CheckUseTransactions = new System.Windows.Forms.CheckBox();
            this._LabelHistory = new System.Windows.Forms.Label();
            this._LabelHistoryIcon = new System.Windows.Forms.Label();
            this._PanelHistory = new System.Windows.Forms.Panel();
            this._OptionHistoryNeverPurge = new System.Windows.Forms.RadioButton();
            this._OptionHistoryAlwaysPurge = new System.Windows.Forms.RadioButton();
            this._OptionHistoryPurgeConnection = new System.Windows.Forms.RadioButton();
            this._CheckCreateHistory = new System.Windows.Forms.CheckBox();
            this._PanelTransactions.SuspendLayout();
            this._PanelHistory.SuspendLayout();
            this.SuspendLayout();
            // 
            // _LabelTransactions
            // 
            this._LabelTransactions.AutoSize = true;
            this._LabelTransactions.Location = new System.Drawing.Point(44, 178);
            this._LabelTransactions.Name = "_LabelTransactions";
            this._LabelTransactions.Size = new System.Drawing.Size(80, 17);
            this._LabelTransactions.TabIndex = 4;
            this._LabelTransactions.Tag = "sqlfwx::udiag.genset.transactions";
            this._LabelTransactions.Text = "&Transactions";
            // 
            // _LabelTransactionsIcon
            // 
            this._LabelTransactionsIcon.Image = ((System.Drawing.Image)(resources.GetObject("_LabelTransactionsIcon.Image")));
            this._LabelTransactionsIcon.Location = new System.Drawing.Point(21, 177);
            this._LabelTransactionsIcon.Name = "_LabelTransactionsIcon";
            this._LabelTransactionsIcon.Size = new System.Drawing.Size(24, 21);
            this._LabelTransactionsIcon.TabIndex = 4;
            // 
            // _PanelTransactions
            // 
            this._PanelTransactions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelTransactions.Controls.Add(this._OptionTransactionsDisabled);
            this._PanelTransactions.Controls.Add(this._OptionTransactionsEnabled);
            this._PanelTransactions.Controls.Add(this._OptionTransactionsDefault);
            this._PanelTransactions.Controls.Add(this._CheckUseTransactions);
            this._PanelTransactions.Location = new System.Drawing.Point(24, 201);
            this._PanelTransactions.Name = "_PanelTransactions";
            this._PanelTransactions.Size = new System.Drawing.Size(651, 108);
            this._PanelTransactions.TabIndex = 4;
            // 
            // _OptionTransactionsDisabled
            // 
            this._OptionTransactionsDisabled.AutoSize = true;
            this._OptionTransactionsDisabled.Location = new System.Drawing.Point(246, 57);
            this._OptionTransactionsDisabled.Name = "_OptionTransactionsDisabled";
            this._OptionTransactionsDisabled.Size = new System.Drawing.Size(239, 21);
            this._OptionTransactionsDisabled.TabIndex = 7;
            this._OptionTransactionsDisabled.TabStop = true;
            this._OptionTransactionsDisabled.Tag = "sqlfwx::udiag.genset.disabletr";
            this._OptionTransactionsDisabled.Text = " D&isable transactions for this session";
            this._OptionTransactionsDisabled.UseVisualStyleBackColor = true;
            // 
            // _OptionTransactionsEnabled
            // 
            this._OptionTransactionsEnabled.AutoSize = true;
            this._OptionTransactionsEnabled.Location = new System.Drawing.Point(246, 36);
            this._OptionTransactionsEnabled.Name = "_OptionTransactionsEnabled";
            this._OptionTransactionsEnabled.Size = new System.Drawing.Size(235, 21);
            this._OptionTransactionsEnabled.TabIndex = 6;
            this._OptionTransactionsEnabled.TabStop = true;
            this._OptionTransactionsEnabled.Tag = "sqlfwx::udiag.genset.enabletr";
            this._OptionTransactionsEnabled.Text = " &Enable transactions for this session";
            this._OptionTransactionsEnabled.UseVisualStyleBackColor = true;
            // 
            // _OptionTransactionsDefault
            // 
            this._OptionTransactionsDefault.AutoSize = true;
            this._OptionTransactionsDefault.Location = new System.Drawing.Point(246, 15);
            this._OptionTransactionsDefault.Name = "_OptionTransactionsDefault";
            this._OptionTransactionsDefault.Size = new System.Drawing.Size(219, 21);
            this._OptionTransactionsDefault.TabIndex = 5;
            this._OptionTransactionsDefault.TabStop = true;
            this._OptionTransactionsDefault.Tag = "sqlfwx::udiag.genset.default";
            this._OptionTransactionsDefault.Text = " Use default transaction &behavior";
            this._OptionTransactionsDefault.UseVisualStyleBackColor = true;
            // 
            // _CheckUseTransactions
            // 
            this._CheckUseTransactions.AutoSize = true;
            this._CheckUseTransactions.Location = new System.Drawing.Point(22, 15);
            this._CheckUseTransactions.Name = "_CheckUseTransactions";
            this._CheckUseTransactions.Size = new System.Drawing.Size(129, 21);
            this._CheckUseTransactions.TabIndex = 4;
            this._CheckUseTransactions.Tag = "sqlfwx::udiag.genset.usetr";
            this._CheckUseTransactions.Text = " &Use Transactions";
            this._CheckUseTransactions.UseVisualStyleBackColor = true;
            // 
            // _LabelHistory
            // 
            this._LabelHistory.AutoSize = true;
            this._LabelHistory.Location = new System.Drawing.Point(44, 27);
            this._LabelHistory.Name = "_LabelHistory";
            this._LabelHistory.Size = new System.Drawing.Size(49, 17);
            this._LabelHistory.TabIndex = 0;
            this._LabelHistory.Tag = "sqlfwx::udiag.genset.history";
            this._LabelHistory.Text = "&History";
            // 
            // _LabelHistoryIcon
            // 
            this._LabelHistoryIcon.Image = ((System.Drawing.Image)(resources.GetObject("_LabelHistoryIcon.Image")));
            this._LabelHistoryIcon.Location = new System.Drawing.Point(21, 26);
            this._LabelHistoryIcon.Name = "_LabelHistoryIcon";
            this._LabelHistoryIcon.Size = new System.Drawing.Size(24, 21);
            this._LabelHistoryIcon.TabIndex = 0;
            // 
            // _PanelHistory
            // 
            this._PanelHistory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelHistory.Controls.Add(this._OptionHistoryNeverPurge);
            this._PanelHistory.Controls.Add(this._OptionHistoryAlwaysPurge);
            this._PanelHistory.Controls.Add(this._OptionHistoryPurgeConnection);
            this._PanelHistory.Controls.Add(this._CheckCreateHistory);
            this._PanelHistory.Location = new System.Drawing.Point(24, 50);
            this._PanelHistory.Name = "_PanelHistory";
            this._PanelHistory.Size = new System.Drawing.Size(651, 108);
            this._PanelHistory.TabIndex = 0;
            // 
            // _OptionHistoryNeverPurge
            // 
            this._OptionHistoryNeverPurge.AutoSize = true;
            this._OptionHistoryNeverPurge.Location = new System.Drawing.Point(246, 57);
            this._OptionHistoryNeverPurge.Name = "_OptionHistoryNeverPurge";
            this._OptionHistoryNeverPurge.Size = new System.Drawing.Size(161, 21);
            this._OptionHistoryNeverPurge.TabIndex = 3;
            this._OptionHistoryNeverPurge.TabStop = true;
            this._OptionHistoryNeverPurge.Tag = "sqlfwx::udiag.genset.dupl";
            this._OptionHistoryNeverPurge.Text = " Allow duplicate entries";
            this._OptionHistoryNeverPurge.UseVisualStyleBackColor = true;
            // 
            // _OptionHistoryAlwaysPurge
            // 
            this._OptionHistoryAlwaysPurge.AutoSize = true;
            this._OptionHistoryAlwaysPurge.Location = new System.Drawing.Point(246, 36);
            this._OptionHistoryAlwaysPurge.Name = "_OptionHistoryAlwaysPurge";
            this._OptionHistoryAlwaysPurge.Size = new System.Drawing.Size(262, 21);
            this._OptionHistoryAlwaysPurge.TabIndex = 2;
            this._OptionHistoryAlwaysPurge.TabStop = true;
            this._OptionHistoryAlwaysPurge.Tag = "sqlfwx::udiag.genset.prevall";
            this._OptionHistoryAlwaysPurge.Text = " Prevent duplicate entries (for all history)";
            this._OptionHistoryAlwaysPurge.UseVisualStyleBackColor = true;
            // 
            // _OptionHistoryPurgeConnection
            // 
            this._OptionHistoryPurgeConnection.AutoSize = true;
            this._OptionHistoryPurgeConnection.Location = new System.Drawing.Point(246, 15);
            this._OptionHistoryPurgeConnection.Name = "_OptionHistoryPurgeConnection";
            this._OptionHistoryPurgeConnection.Size = new System.Drawing.Size(297, 21);
            this._OptionHistoryPurgeConnection.TabIndex = 1;
            this._OptionHistoryPurgeConnection.TabStop = true;
            this._OptionHistoryPurgeConnection.Tag = "sqlfwx::udiag.genset.prevcn";
            this._OptionHistoryPurgeConnection.Text = " Prevent duplicate entries (on connection level)";
            this._OptionHistoryPurgeConnection.UseVisualStyleBackColor = true;
            // 
            // _CheckCreateHistory
            // 
            this._CheckCreateHistory.AutoSize = true;
            this._CheckCreateHistory.Location = new System.Drawing.Point(22, 15);
            this._CheckCreateHistory.Name = "_CheckCreateHistory";
            this._CheckCreateHistory.Size = new System.Drawing.Size(141, 21);
            this._CheckCreateHistory.TabIndex = 0;
            this._CheckCreateHistory.Tag = "sqlfwx::udiag.genset.createhistory";
            this._CheckCreateHistory.Text = " &Create SQL History";
            this._CheckCreateHistory.UseVisualStyleBackColor = true;
            // 
            // GeneralSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._LabelTransactions);
            this.Controls.Add(this._LabelTransactionsIcon);
            this.Controls.Add(this._PanelTransactions);
            this.Controls.Add(this._LabelHistory);
            this.Controls.Add(this._LabelHistoryIcon);
            this.Controls.Add(this._PanelHistory);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "GeneralSettingsControl";
            this.Size = new System.Drawing.Size(697, 349);
            this._PanelTransactions.ResumeLayout(false);
            this._PanelTransactions.PerformLayout();
            this._PanelHistory.ResumeLayout(false);
            this._PanelHistory.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelTransactions;
        private System.Windows.Forms.Label _LabelTransactionsIcon;
        private System.Windows.Forms.Panel _PanelTransactions;
        private System.Windows.Forms.RadioButton _OptionTransactionsDisabled;
        private System.Windows.Forms.RadioButton _OptionTransactionsEnabled;
        private System.Windows.Forms.RadioButton _OptionTransactionsDefault;
        private System.Windows.Forms.CheckBox _CheckUseTransactions;
        private System.Windows.Forms.Label _LabelHistory;
        private System.Windows.Forms.Label _LabelHistoryIcon;
        private System.Windows.Forms.Panel _PanelHistory;
        private System.Windows.Forms.RadioButton _OptionHistoryNeverPurge;
        private System.Windows.Forms.RadioButton _OptionHistoryAlwaysPurge;
        private System.Windows.Forms.RadioButton _OptionHistoryPurgeConnection;
        private System.Windows.Forms.CheckBox _CheckCreateHistory;
    }
}
