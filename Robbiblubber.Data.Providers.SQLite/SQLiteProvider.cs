﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;

using Robbiblubber.Data.DDL;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Providers.SQLite
{
    /// <summary>This class implements the SQLite database provider.</summary>
    public class SQLiteProvider: ProviderBase, IProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SQLiteProvider(): base()
        {
            _Parser = new SQLiteParser();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Provider configuration data.</param>
        public SQLiteProvider(string data): base(data)
        {
            _Parser = new SQLiteParser();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a provider for a connection string.</summary>
        /// <param name="connectionString">Connection string.</param>
        /// <returns>SQLite provider object.</returns>
        public static SQLiteProvider ForConnectionsString(string connectionString)
        {
            Ddp ddp = new Ddp();
            ddp.Set("enhanced", connectionString);

            return new SQLiteProvider(ddp.ToString());
        }


        /// <summary>Creates a provider for a file.</summary>
        /// <param name="fileName">File name.</param>
        /// <returns>SQLite provider object.</returns>
        public static SQLiteProvider ForFile(string fileName)
        {
            Ddp ddp = new Ddp();
            ddp.Set("file", fileName);

            return new SQLiteProvider(ddp.ToString());
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the SQLite file name.</summary>
        public string FileName { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLProvider                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets or sets the provider configuration data.</summary>
        public override string Data
        {
            get
            {
                Ddp ddp = new Ddp ();

                ddp["provider"].Value = ProviderName;
                ddp["file"].Value = FileName;
                ddp["enhanced"].Value = EnhancedString;

                return ddp.ToString();
            }
            set
            {
                Ddp ddp = new Ddp(value);

                FileName = ddp["file"];
                EnhancedString = ddp["enhanced"];
            }
        }


        /// <summary>Connects to the database.</summary>
        public override void Connect()
        {
            if(string.IsNullOrWhiteSpace(EnhancedString))
            {
                _Connection = new SQLiteConnection("Data Source=" + FileName + ";Version=3;");
            }
            else
            {
                _Connection = new SQLiteConnection(EnhancedString);
            }

            _Connection.Open();
        }


        /// <summary>Gets specific provider data.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Data.</returns>
        public override object GetData(string key)
        {
            if(key == "file")
            {
                if(!string.IsNullOrWhiteSpace(EnhancedString))
                {
                    try
                    {
                        return Ddp.Create(EnhancedString).Get<string>("Data Source");
                    }
                    catch(Exception) { return "SQLite"; }
                }
                return FileName;
            }

            return base.GetData(key);
        }


        /// <summary>Returns the next sequence value of the given database sequence.</summary>
        /// <param name="sequence">Sequence name.</param>
        /// <returns>Next value.</returns>
        public override long NextValue(string sequence)
        {
            string lck = StringOp.Unique();
            long rval = -1;
            int bkcond = 0;
            IDbCommand cmd;

            while(true)
            {
                cmd = Connection.CreateCommand("UPDATE SEQUENCES SET JLOCK = " + Parser.ToBindVariableName("lck") + " WHERE SEQUENCE_NAME = " + Parser.ToBindVariableName("seq"));
                cmd.AddParameter(Parser.ToBindVariableName("lck"), lck);
                cmd.AddParameter(Parser.ToBindVariableName("seq"), sequence);
                cmd.ExecuteNonQuery();
                cmd.Dispose();

                cmd = Connection.CreateCommand("SELECT VALUE FROM SEQUENCES WHERE SEQUENCE_NAME = " + Parser.ToBindVariableName("seq") + " AND JLOCK = " + Parser.ToBindVariableName("lck"));
                cmd.AddParameter(Parser.ToBindVariableName("seq"), sequence);
                cmd.AddParameter(Parser.ToBindVariableName("lck"), lck);

                IDataReader re = cmd.ExecuteReader();
                if(re.Read())
                {
                    rval = Convert.ToInt64(re.GetValue(0));
                }
                re.Close();

                Disposal.Recycle(re, cmd);

                if(rval > -1)
                {
                    cmd = Connection.CreateCommand("UPDATE SEQUENCES SET VALUE = VALUE + 1, JLOCK = NULL WHERE SEQUENCE_NAME = " + Parser.ToBindVariableName("seq"));
                    cmd.AddParameter(Parser.ToBindVariableName("seq"), sequence);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();

                    return rval;
                }

                if(bkcond++ > 2000) { throw new InvalidOperationException("Sequence \"" + sequence + "\" did could not be retrieved in time."); }
            }
        }


        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        /// <remarks>CHECK constraints will not be retrieved.</remarks>
        public override IDDLTable GetTable(string tableName)
        {
            DDLTable rval = new DDLBuilder(Parser).AddTable(tableName);

            IDbCommand cmd = null;
            IDataReader re = null;

            cmd = Connection.CreateCommand("PRAGMA TABLE_INFO(" + tableName + ")");
            re = cmd.ExecuteReader();

            List<string> tmp = new List<string>();
            while(re.Read())
            {
                rval.AddColumn(re.GetString(1), ((SQLiteParser) Parser).ToSQLDataType(re.GetString(2)), re.GetInt32(3) == 0, string.IsNullOrWhiteSpace(re.GetString(4)) ? null : re.GetString(4));
                if(re.GetInt32(5) != 0) { tmp.Add(re.GetString(1)); }
            }
            Disposal.Dispose(re, cmd);

            if(tmp.Count > 0) { rval.AddPrimaryKey(tmp.ToArray()); }

            cmd = Connection.CreateCommand("PRAGMA Foreign_Key_List(" + tableName + ")");
            re = cmd.ExecuteReader();

            while(re.Read())
            {
                rval.AddForeignKey(re.GetString(3), re.GetString(2), re.GetString(4), re.GetString(5).ToUpper() == "CASCADE");
            }
            Disposal.Dispose(re, cmd);

            cmd = Connection.CreateCommand("PRAGMA Index_List(" + tableName + ")");
            re = cmd.ExecuteReader();

            List<Tuple<string, bool>> ixs = new List<Tuple<string, bool>>();
            while(re.Read())
            {
                if(re.GetString(3).ToUpper() != "PK") { ixs.Add(new Tuple<string, bool>(re.GetString(1), re.GetInt32(2) != 0)); }
            }
            Disposal.Dispose(re, cmd);

            foreach(Tuple<string, bool> i in ixs)
            {
                cmd = Connection.CreateCommand("PRAGMA Index_List(" + tableName + ")");
                re = cmd.ExecuteReader();

                tmp = new List<string>();
                while(re.Read()) { tmp.Add(re.GetString(2)); }
                Disposal.Dispose(re, cmd);

                if(i.Item2)
                {
                    rval.AddNamedUniqueIndex(i.Item1, tmp.ToArray());
                }
                else { rval.AddNamedIndex(i.Item1, tmp.ToArray()); }
            }

            return rval;
        }


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        public override string[] GetTablesNames(string filter)
        {
            List<string> rval = new List<string>();

            if(string.IsNullOrWhiteSpace(filter)) { filter = "*"; }
            filter = filter.Replace("*", "%").Replace("?", "_");

            IDbCommand cmd = CreateCommand("SELECT NAME FROM SQLITE_MASTER WHERE TYPE = 'table' AND NAME LIKE :f ORDER BY NAME");
            cmd.AddParameter(":f", filter);
            IDataReader re = cmd.ExecuteReader();

            while(re.Read()) { rval.Add(re.GetString(0)); }
            Disposal.Recycle(re, cmd);

            return rval.ToArray();
        }
    }
}
