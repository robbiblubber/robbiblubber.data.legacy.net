﻿using System;
using System.Data.OleDb;
using System.Runtime.Versioning;

using Robbiblubber.Data.DDL;
using Robbiblubber.Util;




namespace Robbiblubber.Data.Providers.MSAccess
{
    /// <summary>This class implements the MS Access database provider.</summary>
    [SupportedOSPlatform("windows")]
    public class MSAccessProvider: ProviderBase, IProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MSAccessProvider(): base()
        {}



        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Provider configuration data.</param>
        public MSAccessProvider(string data): base(data)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the SQLite file name.</summary>
        public string FileName { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLProvider                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        /// <summary>Gets or sets the provider configuration data.</summary>
        public override string Data
        {
            get
            {
                Ddp ddp = new Ddp();

                ddp["provider"].Value = ProviderName;
                ddp["file"].Value = FileName;
                ddp["enhanced"].Value = EnhancedString;

                return ddp.ToString();
            }
            set
            {
                Ddp ddp = new Ddp(value);

                FileName = ddp["file"];
                EnhancedString = ddp["enhanced"];
            }
        }


        /// <summary>Connects to the database.</summary>
        public override void Connect()
        {
            _Connection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";");
            _Connection.Open();
        }


        /// <summary>Disconnects from the database.</summary>
        public override void Disconnect()
        {
            base.Disconnect();
        }


        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        public override IDDLTable GetTable(string tableName)
        {
            throw new NotSupportedException();
        }


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        public override string[] GetTablesNames(string filter)
        {
            throw new NotSupportedException();
        }
    }
}
