﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Data.Interpreters;

using MongoDB.Driver;
using MongoDB.Bson;



namespace Robbiblubber.Data.Explorations.MongoDB
{
    /// <summary>This class represents a MongoDB collections folder.</summary>
    public class MgoIndexesFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;

        /// <summary>Parent collection name.</summary>
        protected string _Collection = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="collection">Collection.</param>
        internal MgoIndexesFolder(MgoCollection collection)
        {
            _Parent = collection.Database;
            _Collection = collection.Name;
            _Name = "Indexes";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MgoDatabase Database
        {
            get { return (MgoDatabase) _Parent; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mgo::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IAsyncCursor<BsonDocument> cur = Database.Cluster.Provider.GetClient().GetDatabase(Database.Name).GetCollection<BsonDocument>(_Collection).Indexes.List();

            while(cur.MoveNext())
            {
                foreach(BsonDocument i in cur.Current)
                {
                    _Children.Add(new MgoIndex(this, i));
                }
            }
            cur.Dispose();
        }
    }
}
