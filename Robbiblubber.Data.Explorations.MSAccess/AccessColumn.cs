﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Microsoft.Office.Interop.Access.Dao;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSAccess
{
    /// <summary>This class represents an Access column.</summary>
    public class AccessColumn: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Column name.</summary>
        private string _Name;


        /// <summary>Referenced table.</summary>
        private string _ReferencedTable = null;


        /// <summary>Referenced column.</summary>
        private string _ReferencedColumn = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="dataType">Data type.</param>
        /// <param name="pk">Primary key.</param>
        /// <param name="fk">Foreign key.</param>
        /// <param name="notNull">Not null.</param>
        internal AccessColumn(AccessDatabase database, AccessColumnsFolder folder, string name, string dataType, bool pk, bool fk, bool notNull)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
            DataType = dataType;
            IsPrimaryKey = pk;
            IsForeignKey = fk;
            IsNotNull = notNull;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public AccessDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public AccessColumnsFolder Folder { get; private set; }


        /// <summary>Gets the data type.</summary>
        public string DataType { get; private set; }


        /// <summary>Gets if the field is a primary key.</summary>
        public bool IsPrimaryKey { get; private set; }


        /// <summary>Gets if the field is a foreign key.</summary>
        public bool IsForeignKey { get; private set; }


        /// <summary>Gets if the field is not nullable.</summary>
        public bool IsNotNull { get; private set; }


        /// <summary>Gets the referenced table.</summary>
        public string ReferencedTable
        {
            get
            {
                _Read();
                return _ReferencedTable;
            }
        }


        /// <summary>Gets the referenced column.</summary>
        public string ReferencedColumn
        {
            get
            {
                _Read();
                return _ReferencedColumn;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Reads referenced table and column.</summary>
        private void _Read()
        {
            foreach(Relation i in Database.AccessApplication.CurrentDb().Relations)
            {
                if(i.Table == Folder.Table.Name)
                {
                    foreach(Microsoft.Office.Interop.Access.Dao.Field j in i.Fields)
                    {
                        if(j.Name == Name)
                        {
                            _ReferencedTable = i.ForeignTable;
                            _ReferencedColumn = j.ForeignName;
                        }
                    }
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                //if(Folder.Table is AccessView) { return Resources.column_view; }

                if(IsPrimaryKey) { return Resources.msacc_column_pk; }
                if(IsForeignKey) { return Resources.msacc_column_fk; }
                if(IsNotNull) { return Resources.msacc_column_nn; }

                return Resources.msacc_column;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                //if(Folder.Table is AccessView) { return "mdb::column/view"; }

                if(IsPrimaryKey) { return "mdb::column/pk"; }
                if(IsForeignKey) { return "mdb::column/fk"; }
                if(IsNotNull) { return "mdb::column/nn"; }

                return "mdb::column";
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = Name + "   " + DataType.ToUpper();
                if(IsNotNull)    { rval += "   NOT NULL"; }
                if(IsPrimaryKey) { rval += "   PRIMARY KEY"; }
                if(IsForeignKey) { rval += "   REFERENCES " + ReferencedTable + "(" + ReferencedColumn + ")"; }
                rval += ";";

                return rval;
            }
        }


        /// <summary>Gets if the object defines a query.</summary>
        public override bool HasQuery
        {
            get { return true; }
        }


        /// <summary>Gets the query for this object.</summary>
        public override string Query
        {
            get { return "SELECT " + Name + " FROM " + Folder.Table.Name; }
        }
    }
}
