﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle viewss folder.</summary>
    public class OraViewsFolder: OraTablesFolder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="schema">Parent schema.</param>
        internal OraViewsFolder(OraDatabase database, OraSchema schema): base(database, schema)
        {
            _Name = "Views";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] OraTablesFolder                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "ora::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("SELECT VIEW_NAME FROM SYS.ALL_VIEWS WHERE OWNER = '" + Schema.Name + "' ORDER BY VIEW_NAME");
                re = cmd.ExecuteReader();

                _Children = new List<DbItem>();
                while(re.Read())
                {
                    _Children.Add(new OraView(Database, this, re.GetString(0)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX02442", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
