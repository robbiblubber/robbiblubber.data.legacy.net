﻿using System;

using Robbiblubber.Data.Providers;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Providers.PostgreSQL
{
    /// <summary>This class provides a PostgreSQL-specific implementation of the SQLParser class.</summary>
    public class PostgreParser: Parser, IParser
    {
        /// <summary>Returns a SQL data type for a database data type expression.</summary>
        /// <param name="dbDataType">Database data type string.</param>
        /// <returns>SQL data type.</returns>
        public SQLDataType ToSQLDataType(string dbDataType)
        {
            switch(dbDataType.ToUpper())
            {
                case "BOOLEAN": case "BOOL": return SQLDataType.BOOLEAN;
                case "TEXT": return SQLDataType.TEXT;
                case "SMALLINT": return SQLDataType.INT16;
                case "INT": case "INTEGER": case "SERIAL": return SQLDataType.INT32;
                case "FLOAT": case "REAL": return SQLDataType.FLOAT;
            }

            if(dbDataType.ToUpper().StartsWith("FLOAT"))
            {
                return SQLDataType.DOUBLE;
            }

            if(dbDataType.ToUpper().StartsWith("NUMERIC"))
            {
                return SQLDataType.DECIMAL;
            }

            if(dbDataType.ToUpper().StartsWith("TIMESTAMP"))
            {
                return SQLDataType.TIMESTAMP;
            }

            if(dbDataType.ToUpper().StartsWith("VARC") || dbDataType.ToUpper().StartsWith("CHAR"))
            {
                string l = dbDataType.ToUpper().Trim("ABCDEFGHIJKLMNOPQRSTUVWXYZ()".ToCharArray());
                if(string.IsNullOrWhiteSpace(l)) { return SQLDataType.STRING; }

                return SQLDataType.String(int.Parse(l));
            }

            return SQLDataType.TEXT;
        }


        /// <summary>Gets a well-formatted query function call for last auto increment value.</summary>
        public override string LastValue
        {
            get { return "LastVal()"; }
        }
    }
}
