﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;



namespace Robbiblubber.Data.Explorations
{
    /// <summary>This class provides extensions for working with IMenuTargetWindow menus.</summary>
    internal static class __MenuListExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Adds a menu item.</summary>
        /// <param name="list">List.</param>
        /// <param name="text">Menu text.</param>
        /// <param name="img">Manu image.</param>
        /// <param name="tag">Tag.</param>
        /// <param name="shortcut">Shortcut keys.</param>
        /// <param name="shortcutText">Shortcut text.</param>
        public static void AddMenu(this IList<ToolStripItem> list, string text, Image img, object tag, Keys shortcut = Keys.None, string shortcutText = null)
        {
            ToolStripMenuItem m = new ToolStripMenuItem(text, img);
            m.Tag = tag;

            if(shortcut != Keys.None) { m.ShortcutKeys = shortcut; }
            if(shortcutText != null) { m.ShortcutKeyDisplayString = shortcutText; }

            list.Add(m);
        }


        /// <summary>Adds a separator to the list.</summary>
        /// <param name="list">List.</param>
        /// <param name="tag">Tag.</param>
        public static void AddSeparator(this IList<ToolStripItem> list, object tag)
        {
            ToolStripSeparator m = new ToolStripSeparator();
            m.Tag = tag;
            list.Add(m);
        }


        /// <summary>Gets a menu by its tag.</summary>
        /// <param name="list">List.</param>
        /// <param name="tag">Tag.</param>
        /// <returns>Item.</returns>
        public static ToolStripItem GetItem(this IList<ToolStripItem> list, object tag)
        {
            foreach(ToolStripItem i in list)
            {
                if(i.Tag == tag) { return i; }
            }

            return null;
        }
    }
}
