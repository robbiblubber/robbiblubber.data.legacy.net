﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>This class provides a base implementation of the IDDLConstraint interface.</summary>
    public abstract class DDLConstraint: IDDLConstraint
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        protected DDLConstraint()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="table">Table.</param>
        /// <param name="constraintName">Constraint name.</param>
        protected DDLConstraint(IDDLTable table, string constraintName = null)
        {
            TableName = table.TableName;
            ConstraintName = constraintName;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="constraintName">Constraint name.</param>
        protected DDLConstraint(string tableName, string constraintName = null)
        {
            TableName = tableName;
            ConstraintName = constraintName;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDDLConstraint                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the constraint name.</summary>
        public virtual string ConstraintName { get; protected set; }


        /// <summary>Gets the table name.</summary>
        public virtual string TableName { get; protected set; }
    }
}
