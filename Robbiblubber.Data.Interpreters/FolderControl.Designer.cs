﻿namespace Robbiblubber.Data.Interpreters
{
    partial class FolderControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._LabelName = new System.Windows.Forms.Label();
            this._ButtonAddFolder = new System.Windows.Forms.Button();
            this._TextName = new System.Windows.Forms.TextBox();
            this._ButtonAddConnection = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.Location = new System.Drawing.Point(27, 19);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Tag = "sqlfwx::udiag.folder.name";
            this._LabelName.Text = "&Name:";
            // 
            // _ButtonAddFolder
            // 
            this._ButtonAddFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonAddFolder.Location = new System.Drawing.Point(286, 249);
            this._ButtonAddFolder.Name = "_ButtonAddFolder";
            this._ButtonAddFolder.Size = new System.Drawing.Size(147, 29);
            this._ButtonAddFolder.TabIndex = 1;
            this._ButtonAddFolder.Tag = "sqlfwx::udiag.folder.button.addfolder";
            this._ButtonAddFolder.Text = "Add &Folder";
            this._ButtonAddFolder.UseVisualStyleBackColor = true;
            this._ButtonAddFolder.Click += new System.EventHandler(this._ButtonAddFolder_Click);
            // 
            // _TextName
            // 
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(30, 35);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(403, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._TextName_TextChanged);
            // 
            // _ButtonAddConnection
            // 
            this._ButtonAddConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonAddConnection.Location = new System.Drawing.Point(286, 284);
            this._ButtonAddConnection.Name = "_ButtonAddConnection";
            this._ButtonAddConnection.Size = new System.Drawing.Size(147, 29);
            this._ButtonAddConnection.TabIndex = 1;
            this._ButtonAddConnection.Tag = "sqlfwx::udiag.folder.button.addconnection";
            this._ButtonAddConnection.Text = "&Add Connection";
            this._ButtonAddConnection.UseVisualStyleBackColor = true;
            this._ButtonAddConnection.Click += new System.EventHandler(this._ButtonAddConnection_Click);
            // 
            // FolderControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._ButtonAddConnection);
            this.Controls.Add(this._LabelName);
            this.Controls.Add(this._ButtonAddFolder);
            this.Controls.Add(this._TextName);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FolderControl";
            this.Size = new System.Drawing.Size(463, 338);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.Button _ButtonAddFolder;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.Button _ButtonAddConnection;
    }
}
