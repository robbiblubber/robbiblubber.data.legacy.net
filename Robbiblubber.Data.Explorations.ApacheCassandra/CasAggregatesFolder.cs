﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra aggregates folder.</summary>
    public class CasAggregatesFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="keyspace">Keyspace.</param>
        internal CasAggregatesFolder(CasKeyspace keyspace)
        {
            _Parent = keyspace;
            _Name = "Aggregates";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the keyspace instance.</summary>
        public CasKeyspace Keyspace 
        { 
            get { return (CasKeyspace) _Parent; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "cas::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Keyspace.Database.Provider.CreateCommand("SELECT AGGREGATE_NAME, ARGUMENT_TYPES, RETURN_TYPE, STATE_FUNC, STATE_TYPE, INITCOND, FINAL_FUNC FROM System_Schema.AGGREGATES WHERE KEYSPACE_NAME = :k");
                cmd.AddParameter(":k", Keyspace.Name);

                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new CasAggregate(this, re.GetString(0), (IList<string>) re.GetValue(1), re.GetString(2), re.GetString(3), re.GetString(4), re.GetString(5), re.GetString(6)));
                }

                _Children.Sort((a, b) => a.Name.CompareTo(b.Name));
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0010b", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
