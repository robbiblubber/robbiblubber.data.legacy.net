﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server views folder.</summary>
    public class MsViewsFolder: MsTablesFolder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="schema">Parent schema.</param>
        internal MsViewsFolder(MsDatabase database, MsSchema schema): base(database, schema)
        {
            _Name = "Views";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] PgTablesFolder                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            IDbCommand cmd = null;
            IDataReader re = null;

            _Children = new List<DbItem>();

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT V.NAME FROM sys.ALL_VIEWS V, sys.SCHEMAS S WHERE S.NAME = '" + Schema.Name + "' AND S.SCHEMA_ID = V.SCHEMA_ID ORDER BY V.NAME";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new MsView(Database, this, re.GetString(0)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX01225", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
