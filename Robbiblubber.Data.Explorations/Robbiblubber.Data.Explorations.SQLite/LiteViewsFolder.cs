﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.SQLite
{
    /// <summary>This class represents a SQLite tables folder.</summary>
    public class LiteViewsFolder: LiteTablesFolder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        internal LiteViewsFolder(LiteDatabase database): base(database)
        {
            _Name = "sqlfwx::tree.folder.views".Localize("Views");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] LiteTablesFolder                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("SELECT NAME FROM SQLITE_MASTER WHERE TYPE = 'view' ORDER BY NAME");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new LiteView(Database, this, re.GetString(0)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX00111", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
