﻿using System;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>Database exploration providers implement this interface.</summary>
    public interface IExploration
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the explorer for a provider.</summary>
        /// <param name="provider">Provider.</param>
        /// <returns>Explorer.</returns>
        IExporer this[ProviderItem provider] { get; }
    }
}
