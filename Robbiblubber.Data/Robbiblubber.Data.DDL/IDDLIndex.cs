﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>Classes representing DDL indexes implement this interface.</summary>
    public interface IDDLIndex: IDDLStatement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the index name.</summary>
        string IndexName { get; }


        /// <summary>Gets the index table name.</summary>
        string TableName { get; }


        /// <summary>Gets the index column names.</summary>
        string[] ColumnNames { get; }


        /// <summary>Gets if the index is unique.</summary>
        bool Unique { get; }
    }
}
