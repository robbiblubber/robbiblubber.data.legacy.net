﻿using System;
using System.Data;

using Robbiblubber.Data.DDL;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Providers
{
    /// <summary>Database providers implement this interface.</summary>
    public interface IProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider name.</summary>
        string ProviderName { get; }


        /// <summary>Gets or sets the provider configuration data.</summary>
        string Data { get; set; }


        /// <summary>Gets or sets the provider configuration data key.</summary>
        string DataKey { get; set; }


        /// <summary>Gets or sets an enhanced connection string.</summary>
        string EnhancedString { get; set; }


        /// <summary>Gets the current connection.</summary>
        IDbConnection Connection { get; }


        /// <summary>Gets the SQL parser for this provider.</summary>
        IParser Parser { get; }


        /// <summary>Gets or sets the current transaction.</summary>
        IDbTransaction Transaction { get; set; }


        /// <summary>Returns the current database time.</summary>
        /// <returns>Current database time.</returns>
        DateTime CurrentTimestamp { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Connects to the database.</summary>
        void Connect();


        /// <summary>Disconnects from the database.</summary>
        void Disconnect();


        /// <summary>Tests the connection.</summary>
        void Test();


        /// <summary>Creates a data adapter.</summary>
        /// <param name="sql">SQL statement.</param>
        /// <returns>Data adapter.</returns>
        IDbDataAdapter CreateAdapter(string sql);


        /// <summary>Creates a database command.</summary>
        /// <param name="sql">SQL statement.</param>
        /// <returns>Command object.</returns>
        IDbCommand CreateCommand(string sql = null);


        /// <summary>Creates a database command.</summary>
        /// <param name="query">SQL query.</param>
        /// <returns>Command object.</returns>
        IDbCommand CreateCommand(ISQLQuery query);


        /// <summary>Returns the number of fields in a data reader.</summary>
        /// <param name="re">Data reader.</param>
        /// <returns>Number of fields.</returns>
        int CountFields(IDataReader re);


        /// <summary>Returns a data table representing the data reader.</summary>
        /// <param name="re">Reader.</param>
        /// <returns>Data table.</returns>
        DataTable GetDataTable(IDataReader re);


        /// <summary>Returns a data table of string values representing the data reader.</summary>
        /// <param name="re">Reader.</param>
        /// <returns>Data table.</returns>
        DataTable GetStringDataTable(IDataReader re);


        /// <summary>Gets specific provider data.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Data.</returns>
        object GetData(string key);


        /// <summary>Returns the next sequence value of the given database sequence.</summary>
        /// <param name="sequence">Sequence name.</param>
        /// <returns>Next value.</returns>
        long NextValue(string sequence);


        /// <summary>Gets the last auto increment value.</summary>
        /// <returns>Last value.</returns>
        long LastAutoValue();


        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        IDDLTable GetTable(string tableName);


        /// <summary>Gets the table DDL for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table DDL.</returns>
        IDDLTable[] GetTables(string filter);


        /// <summary>Gets the table DDL for all tables.</summary>
        /// <returns>Table DDL.</returns>
        IDDLTable[] GetTables();


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        string[] GetTablesNames(string filter);


        /// <summary>Gets the table names for all tables.</summary>
        /// <returns>Table names.</returns>
        string[] GetTablesNames();


        /// <summary>Returns a string representation of this instance.</summary>
        /// <param name="key">Determines if the data key will be returned.</param>
        /// <returns>String.</returns>
        string ToString(bool key);
    }
}
