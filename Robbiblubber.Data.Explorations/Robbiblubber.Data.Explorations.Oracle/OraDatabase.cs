﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle database.</summary>
    public class OraDatabase: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Database name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider">Provider.</param>
        internal OraDatabase(ProviderItem provider)
        {
            Provider = provider;

            IDbCommand cmd = null;

            try
            {
                cmd = Provider.CreateCommand("SELECT Sys_Context('USERENV', 'INSTANCE_NAME') FROM SYS.DUAL");
                _Name = (string) cmd.ExecuteScalar();
            }
            catch(Exception ex)
            {
                DebugOp.Dump("SQLFWX02410", ex);
                _Name = provider.Name;
            }

            Disposal.Dispose(cmd);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider.</summary>
        public ProviderItem Provider { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.oracledb; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "ora::database"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Provider.CreateCommand("SELECT DISTINCT OWNER FROM SYS.ALL_OBJECTS ORDER BY OWNER");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new OraSchema(this, re.GetString(0)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX02411", ex); }

            Disposal.Dispose(re, cmd);

            _Children.Add(new OraUsersFolder(this));
            _Children.Add(new OraRolesFolder(this));
            _Children.Add(new OraContextesFolder(this));
            _Children.Add(new OraDirectoriesFolder(this));
            _Children.Add(new OraProfilesFolder(this));
            _Children.Add(new OraTablespacesFolder(this));
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return "CREATE DATABASE " + Name + ";"; }
        }


        /// <summary>Refreshes the item.</summary>
        public override void Refresh()
        {
            base.Refresh();
            ((OraExplorer) Exploration.Instance[Provider]).__RefreshDefaultKeywords();
        }
    }
}
