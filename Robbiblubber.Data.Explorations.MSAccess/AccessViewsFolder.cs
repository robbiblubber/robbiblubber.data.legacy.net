﻿using System;
using System.Collections.Generic;

using Microsoft.Office.Interop.Access.Dao;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSAccess
{
    /// <summary>This class represents an Access views folder.</summary>
    public class AccessViewsFolder: AccessTablesFolder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        internal AccessViewsFolder(AccessDatabase database): base(database)
        {
            _Name = "Views";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] PgTablesFolder                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            
            foreach(QueryDef i in Database.AccessApplication.CurrentDb().QueryDefs)
            {
                _Children.Add(new AccessView(Database, this, i.Name));
            }
        }
    }
}
