﻿using Robbiblubber.Data.SQL;
using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>Classes representing DDL columns implement this interface.</summary>
    public interface IDDLColumn
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the table name.</summary>
        string TableName { get; }


        /// <summary>Gets the column name.</summary>
        string ColumnName { get; }


        /// <summary>Gets the column data type.</summary>
        SQLDataType DataType { get; }


        /// <summary>Gets if the column is not null.</summary>
        bool NotNull { get; }


        /// <summary>Gets a string representation of the default value.</summary>
        string DefaultValue { get; }
    }
}
