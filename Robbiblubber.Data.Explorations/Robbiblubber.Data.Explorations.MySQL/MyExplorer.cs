﻿using System;
using System.Data;
using System.Collections.Generic;

using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Controls;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Explorations.MySQL
{
    /// <summary>This class represents the MS SQL Server database structure.</summary>
    public class MyExplorer: IExporer, IDisposable, IAutoCompletionSource
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Provider.</summary>
        protected ProviderItem _Provider = null;


        /// <summary>Default keywords.</summary>
        protected List<IAutoCompletionKeyword> _DefaultKeyWords;


        /// <summary>Column keywords.</summary>
        protected List<IAutoCompletionKeyword> _ColumnKeywords;


        /// <summary>Returns if the default keywords are currently available for auto completion.</summary>
        protected bool _DefaultKeywordsGiven = false;


        /// <summary>MySQL Host.</summary>
        protected MyHost _HostExplorer = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider"></param>
        public MyExplorer(ProviderItem provider)
        {
            Items = new DbItem[] { _HostExplorer = new MyHost(_Provider = provider) };
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes the default keywords.</summary>
        protected internal void __RefreshDefaultKeywords()
        {
            _DefaultKeyWords = new List<IAutoCompletionKeyword>();
            _ColumnKeywords = new List<IAutoCompletionKeyword>();

            _DefaultKeyWords.AddRange(SQLKeywords.Instance);

            IDbCommand cmd = _Provider.CreateCommand();
            cmd.CommandText = "SELECT DISTINCT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS";
            IDataReader re = cmd.ExecuteReader();
            while(re.Read())
            {
                _DefaultKeyWords.Add(new SQLKeywords.KeyWord(re.GetString(0), "my::column"));
                _ColumnKeywords.Add(new SQLKeywords.KeyWord(re.GetString(0), "my::column"));
            }
            Disposal.Dispose(re, cmd);

            cmd = _Provider.CreateCommand();
            cmd.CommandText = "SELECT DATABASE()";
            string db = (string) cmd.ExecuteScalar();
            Disposal.Dispose(cmd);

            DbItem m = _HostExplorer.GetItem(db);

            if((m != null) && (m.Children != null))
            {
                foreach(DbItem i in m.Children)
                {
                    if(i.IsFolder)
                    {
                        foreach(DbItem j in i.Children)
                        {
                            if(_IncludeInAutoComplete(j)) { _DefaultKeyWords.Add(j); }
                        }
                    }
                    else if(_IncludeInAutoComplete(i)) { _DefaultKeyWords.Add(i); }
                }
            }

            foreach(DbItem i in _HostExplorer.Children)
            {
                if(i is MyDatabase) { _DefaultKeyWords.Add(i); }
            }

            _DefaultKeywordsGiven = false;
        }


        /// <summary>Determines if an object is included in auto compeltion.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if item is include, otherwise returns FALSE.</returns>
        protected bool _IncludeInAutoComplete(DbItem item)
        {
            if(item is MyTable) { return true; }
            if(item is MyView) { return true; }
            if(item is MyProcedure) { return true; }
            if(item is MyFunction) { return true; }
            if(item is MyDatabase) { return true; }

            return false;
        }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbExplorer                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the items root for this explorer.</summary>
        public DbItem[] Items
        {
            get; protected set;
        }


        /// <summary>Initializes auto completion.</summary>
        /// <param name="a">AutoCompletion instance.</param>
        public void InitAutoCompletion(AutoCompletion a)
        {
            a.AddImage("my::column", Resources.column);
            a.AddImage("my::procedure", Resources.procedure);
            a.AddImage("my::function", Resources.function);
            a.AddImage("my::database", Resources.database);
            a.AddImage("my::connected", Resources.dblink);
            a.AddImage("my::table", Resources.table);
            a.AddImage("my::view", Resources.view);

            __RefreshDefaultKeywords();
        }


        /// <summary>Initializes custom menus.</summary>
        /// <param name="menu">Menu provider.</param>
        public void InitMenus(IMenuTargetWindow menu)
        {
            // TODO: implement menu handling.
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IAutoCompletionSource                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Updates the words for a given preceding text.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Word list.</returns>
        public IEnumerable<IAutoCompletionKeyword> UpdateWords(string pretext, char key)
        {
            if((key == '.') || ((key == (char) 27)))
            {
                if(pretext.EndsWith(".")) { pretext = pretext.TrimEnd('.'); }

                List<IAutoCompletionKeyword> rval = new List<IAutoCompletionKeyword>();

                DbItem m = _HostExplorer.GetItem(pretext.LastWord());

                if((m != null) && (m.Children != null))
                {
                    foreach(DbItem i in m.Children)
                    {
                        if(i.IsFolder)
                        {
                            foreach(DbItem j in i.Children)
                            {
                                if(_IncludeInAutoComplete(j)) { rval.Add(j); }
                            }
                        }
                        else if(_IncludeInAutoComplete(i)) { rval.Add(i); }
                    }
                }
                else
                {
                    return _ColumnKeywords;
                }

                _DefaultKeywordsGiven = false;
                return rval;
            }
            else
            {
                _DefaultKeywordsGiven = true;
                return _DefaultKeyWords;
            }
        }


        /// <summary>Returns if the words need to be updated.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Returns TRUE if an update is required, otherwise returns FALSE.</returns>
        public bool UpdateRequired(string pretext, char key)
        {
            if(!_DefaultKeywordsGiven) return true;

            if((key == (char) 27) && pretext.EndsWith(".")) return true;
            return (key == '.');
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDisposable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Disposes the object.</summary>
        public void Dispose()
        {}
    }
}
