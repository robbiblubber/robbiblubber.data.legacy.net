﻿using System;
using System.Collections.Generic;

using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Controls;
using Robbiblubber.Data.Explorations.MSSQL;
using Robbiblubber.Data.Explorations.Oracle;
using Robbiblubber.Data.Explorations.MySQL;
using Robbiblubber.Data.Explorations.SQLite;
using Robbiblubber.Data.Explorations.PostgreSQL;



namespace Robbiblubber.Data.Explorations.ODBC
{
    /// <summary>This class represents the MS SQL Server database structure.</summary>
    public class OdbcExplorer: IExporer, IDisposable, IAutoCompletionSource
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Provider.</summary>
        protected ProviderItem _Provider = null;

        /// <summary>Default keywords.</summary>
        protected IExporer _Explorer = null;

        /// <summary>Default items.</summary>
        protected DbItem[] _Items = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider"></param>
        public OdbcExplorer(ProviderItem provider)
        {
            _Provider = provider;

            if(((string) provider.GetData("driver")).ToLower().Contains("sqlsvr"))
            {
                _Explorer = new MsExplorer(provider);
            }
            else if(((string) provider.GetData("driver")).ToLower().Contains("sqora"))
            {
                _Explorer = new OraExplorer(provider);
            }
            else if(((string) provider.GetData("driver")).ToLower().Contains("myodbc"))
            {
                _Explorer = new MyExplorer(provider);
            }
            else if(((string) provider.GetData("driver")).ToLower().Contains("sqlite"))
            {
                _Explorer = new LiteExplorer(provider);
            }
            else if(((string) provider.GetData("driver")).ToLower().Contains("pgsqlodbc"))
            {
                _Explorer = new PgExplorer(provider);
            }
            else
            {
                _Items = new DbItem[] { new OdbcDatabase(provider) };
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbExplorer                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the items root for this explorer.</summary>
        public DbItem[] Items
        {
            get
            {
                if(_Explorer == null) { return _Items; }
                return _Explorer.Items;
            }
        }


        /// <summary>Initializes auto completion.</summary>
        /// <param name="a">AutoCompletion instance.</param>
        public void InitAutoCompletion(AutoCompletion a)
        {
            if(_Explorer != null) { _Explorer.InitAutoCompletion(a); }
        }


        /// <summary>Initializes custom menus.</summary>
        /// <param name="menu">Menu provider.</param>
        public void InitMenus(IMenuTargetWindow menu)
        {
            // TODO: implement menu handling.
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IAutoCompletionSource                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Updates the words for a given preceding text.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Word list.</returns>
        public IEnumerable<IAutoCompletionKeyword> UpdateWords(string pretext, char key)
        {
            if(_Explorer != null) { return _Explorer.UpdateWords(pretext, key); }

            return new IAutoCompletionKeyword[0];
        }


        /// <summary>Returns if the words need to be updated.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Returns TRUE if an update is required, otherwise returns FALSE.</returns>
        public bool UpdateRequired(string pretext, char key)
        {
            if(_Explorer != null) { return _Explorer.UpdateRequired(pretext, key); }

            return false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDisposable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Disposes the object.</summary>
        public void Dispose()
        {}
    }
}
