﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;



[assembly: AssemblyCompany("robbiblubber.org")]
[assembly: AssemblyProduct("robbiblubber.org Data Libraries")]
[assembly: AssemblyCopyright("© 2021 robbiblubber.org")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("3.0.4")]
[assembly: AssemblyFileVersion("3.0.4")]
