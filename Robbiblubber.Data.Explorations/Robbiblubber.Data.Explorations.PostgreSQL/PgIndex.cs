﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.PostgreSQL
{
    /// <summary>This class represents a PostgreSQL index.</summary>
    public class PgIndex: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Index name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="unique">Unique flag.</param>
        internal PgIndex(PgDatabase database, PgIndexesFolder folder, string name, bool unique)
        {
            Database = database;
            _Parent = Folder = folder;
            IsUnique = unique;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public PgDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public PgIndexesFolder Folder { get; private set; }


        /// <summary>Gets if the index is unique.</summary>
        public bool IsUnique { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                if(IsUnique) { return Resources.index_unique; }

                return Resources.index;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                if(IsUnique) { return "pg::index/unique"; }

                return "pg::index";
            }
        }
        
        
        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                IDbCommand cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT INDEXDEF FROM PG_CATALOG.PG_INDEXES WHERE SCHEMANAME = '" + Folder.Table.Folder.Schema.Name + "' AND TABLENAME = '" + Folder.Table.Name + "' AND INDEXNAME = '" + Name + "'";

                string rval = ((string) cmd.ExecuteScalar());
                Disposal.Dispose(cmd);

                return rval.TrimEnd(' ', ';') + ";";
            }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            List<string> cols = new List<string>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT A.ATTNAME FROM PG_CATALOG.PG_CLASS T, PG_CATALOG.PG_CLASS I, PG_CATALOG.PG_INDEX X, PG_CATALOG.PG_NAMESPACE N, PG_CATALOG.PG_ATTRIBUTE A " +
                                  "WHERE T.OID = X.INDRELID AND I.OID = X.INDEXRELID AND N.OID = T.RELNAMESPACE AND A.ATTRELID = T.OID AND A.ATTNUM = ANY(X.INDKEY) " +
                                  "AND T.RELKIND = 'r' AND N.NSPNAME = '" + Folder.Table.Folder.Schema.Name + "' AND T.RELNAME = '" + Folder.Table.Name + "' AND I.RELNAME = '" + Name + "' ORDER BY I.RELNAME";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    cols.Add(re.GetString(0));
                }
            }
            catch(Exception) {}

            Disposal.Dispose(re, cmd);

            _Children = new List<DbItem>();
            foreach(PgColumn i in Folder.Table.Columns.Children)
            {
                if(cols.Contains(i.Name))
                {
                    _Children.Add(i);
                }
            }
        }


        /// <summary>Gets the qualified name for this object.</summary>
        public override string QualifiedName
        {
            get { return Folder.Table.Folder.Schema.Name + "." + Name; }
        }
    }
}
