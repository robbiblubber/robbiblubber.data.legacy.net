﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server database.</summary>
    public class MsDatabase: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Database name.</summary>
        private string _Name;
                
        /// <summary>Determines if the instance is being initialized.</summary>
        private bool _Initial = true;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider">Provider.</param>
        internal MsDatabase(ProviderItem provider)
        {
            Provider = provider;
            Refresh();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider.</summary>
        public ProviderItem Provider { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a schema with a given name.</summary>
        /// <param name="name">Name.</param>
        /// <returns>Schema.</returns>
        public MsSchema GetSchema(string name)
        {
            foreach(DbItem i in Children)
            {
                if((i is MsSchema) && (i.Name == name)) { return ((MsSchema) i); }
            }

            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.sqlserverdb; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mssql::database"; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return "CREATE DATABASE " + Name + ";"; }
        }


        /// <summary>Refreshes the item.</summary>
        public override void Refresh()
        {
            IDbCommand cmd = null;

            try
            {
                cmd = Provider.CreateCommand();
                cmd.CommandText = "SELECT DB_Name()";
                _Name = (string) cmd.ExecuteScalar();
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX01207", ex); }

            Disposal.Dispose(cmd);

            _Children = null;

            if(!_Initial) { ((MsExplorer) Exploration.Instance[Provider]).__RefreshDefaultKeywords(); }
            _Initial = false;
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Provider.CreateCommand();
                cmd.CommandText = "SELECT NAME FROM sys.SCHEMAS ORDER BY NAME";
                re = cmd.ExecuteReader();

                _Children = new List<DbItem>();
                while(re.Read())
                {
                    _Children.Add(new MsSchema(this, re.GetString(0)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX01208", ex); }

            Disposal.Dispose(re, cmd);

            _Children.Add(new MsRolesFolder(this));
            _Children.Add(new MsUsersFolder(this));
            _Children.Add(new MsGroupsFolder(this));
        }


        /// <summary>Gets the qualified name for this object.</summary>
        public override string QualifiedName
        {
            get { return Name; }
        }
    }
}
