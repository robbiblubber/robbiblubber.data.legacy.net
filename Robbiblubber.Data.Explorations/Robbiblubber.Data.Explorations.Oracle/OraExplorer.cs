﻿using System;
using System.Data;
using System.Collections.Generic;

using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Controls;
using Robbiblubber.Util;
using Robbiblubber.Util.Debug;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents the MS SQL Server database structure.</summary>
    public class OraExplorer: IExporer, IDisposable, IAutoCompletionSource
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Provider.</summary>
        protected ProviderItem _Provider = null;

        /// <summary>Default keywords.</summary>
        protected List<IAutoCompletionKeyword> _DefaultKeyWords;
        
        /// <summary>Returns if the default keywords are currently available for auto completion.</summary>
        protected bool _DefaultKeywordsGiven = false;
        
        /// <summary>Oracle keywords.</summary>
        protected SQLKeywords _Keywords = new SQLKeywords(PathOp.ApplicationDirectory + @"\oracle.keywords");
        
        /// <summary>Column keywords.</summary>
        protected List<IAutoCompletionKeyword> _ColumnKeywords;
        
        /// <summary>Oracle standard functions.</summary>
        protected List<DbItem> _Functions = null;
        
        /// <summary>Database explorer.</summary>
        protected OraDatabase _DatabaseExplorer = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider"></param>
        public OraExplorer(ProviderItem provider)
        {
            Items = new DbItem[] { _DatabaseExplorer = new OraDatabase(_Provider = provider) };
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes the default keywords.</summary>
        protected internal void __RefreshDefaultKeywords()
        {
            _DefaultKeyWords = new List<IAutoCompletionKeyword>();
            _ColumnKeywords = new List<IAutoCompletionKeyword>();

            _DefaultKeyWords.AddRange(_Keywords);

            if(_Functions == null)
            {
                _Functions = new List<DbItem>();
                DbItem std = _DatabaseExplorer.GetItem("SYS.STANDARD");

                if(std != null)
                {
                    foreach(DbItem i in std.Children)
                    {
                        _Functions.Add(i);
                    }
                }
            }
            _DefaultKeyWords.AddRange(_Functions);

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = _Provider.CreateCommand("SELECT DISTINCT COLUMN_NAME FROM SYS.ALL_TAB_COLUMNS");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _DefaultKeyWords.Add(new SQLKeywords.KeyWord(re.GetString(0), "ora::column"));
                    _ColumnKeywords.Add(new SQLKeywords.KeyWord(re.GetString(0), "ora::column"));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX02400", ex); }

            Disposal.Dispose(re, cmd);

            try
            {
                cmd = _Provider.CreateCommand();
                cmd.CommandText = "SELECT SYNONYM_NAME FROM SYS.ALL_SYNONYMS WHERE OWNER = 'PUBLIC' AND SYNONYM_NAME NOT LIKE '%/%'";

                re = cmd.ExecuteReader();
                while(re.Read())
                {
                    _DefaultKeyWords.Add(new SQLKeywords.KeyWord(re.GetString(0), "ora::synonym"));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX02401", ex); }

            Disposal.Dispose(re, cmd);

            try
            {
                cmd = _Provider.CreateCommand("SELECT DISTINCT DB_LINK FROM SYS.ALL_DB_LINKS WHERE OWNER = 'PUBLIC'");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _DefaultKeyWords.Add(new SQLKeywords.KeyWord(re.GetString(0), "ora::dblink"));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX02402", ex); }

            Disposal.Dispose(re, cmd);

            string user ="SYS";
            try
            {
                cmd = _Provider.CreateCommand();
                cmd.CommandText = "SELECT USER FROM SYS.DUAL";
                user = (string) cmd.ExecuteScalar();
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX02403", ex); }

            Disposal.Dispose(cmd);

            DbItem m = _DatabaseExplorer.GetItem(user);

            if((m != null) && (m.Children != null))
            {
                foreach(DbItem i in m.Children)
                {
                    if(i.IsFolder)
                    {
                        foreach(DbItem j in i.Children)
                        {
                            if(_IncludeInAutoComplete(j)) { _DefaultKeyWords.Add(j); }
                        }
                    }
                    else if(_IncludeInAutoComplete(i)) { _DefaultKeyWords.Add(i); }
                }
            }

            foreach(DbItem i in _DatabaseExplorer.Children)
            {
                if(i is OraSchema) { _DefaultKeyWords.Add(i); }
            }

            _DefaultKeywordsGiven = false;
        }


        /// <summary>Determines if an object is included in auto compeltion.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if item is include, otherwise returns FALSE.</returns>
        protected bool _IncludeInAutoComplete(DbItem item)
        {
            if(item is OraTable) { return true; }
            if(item is OraView) { return true; }
            if(item is OraObjectTable) { return true; }
            if(item is OraCluster) { return true; }
            if(item is OraDimension) { return true; }
            if(item is OraDbLink) { return true; }
            if(item is OraType) { return true; }
            if(item is OraPackage) { return true; }
            if(item is OraPackageMethod) { return true; }
            if(item is OraProcedure) { return true; }
            if(item is OraFunction) { return true; }
            if(item is OraSequnece) { return true; }
            if(item is OraSchema) { return true; }
            if(item is OraSynonym) { return true; }

            return false;
        }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbExplorer                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the items root for this explorer.</summary>
        public DbItem[] Items
        {
            get; protected set;
        }


        /// <summary>Initializes auto completion.</summary>
        /// <param name="a">AutoCompletion instance.</param>
        public void InitAutoCompletion(AutoCompletion a)
        {
            a.AddImage("ora::column", Resources.column);
            a.AddImage("ora::type", Resources.type);
            a.AddImage("ora::function", Resources.function);
            a.AddImage("ora::procedure", Resources.procedure);
            a.AddImage("ora::schema", Resources.schema);
            a.AddImage("ora::sequence", Resources.sequence);
            a.AddImage("ora::table", Resources.table);
            a.AddImage("ora::view", Resources.view);
            a.AddImage("ora::objecttable", Resources.objecttable);
            a.AddImage("ora::cluster", Resources.cluster);
            a.AddImage("ora::dimension", Resources.dimension);
            a.AddImage("ora::dblink", Resources.dblink);
            a.AddImage("ora::package", Resources.package);
            a.AddImage("ora::method", Resources.method);
            a.AddImage("ora::synonym", Resources.synonym);

            __RefreshDefaultKeywords();
        }


        /// <summary>Initializes custom menus.</summary>
        /// <param name="menu">Menu provider.</param>
        public void InitMenus(IMenuTargetWindow menu)
        {
            // TODO: implement menu handling.
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IAutoCompletionSource                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Updates the words for a given preceding text.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Word list.</returns>
        public IEnumerable<IAutoCompletionKeyword> UpdateWords(string pretext, char key)
        {
            if((key == '.') || ((key == (char) 27)))
            {
                if(pretext.EndsWith(".")) { pretext = pretext.TrimEnd('.'); }

                List<IAutoCompletionKeyword> rval = new List<IAutoCompletionKeyword>();

                DbItem m = _DatabaseExplorer.GetItem(pretext.LastWord());

                if((m != null) && (m.Children != null))
                {
                    foreach(DbItem i in m.Children)
                    {
                        if(i.IsFolder)
                        {
                            foreach(DbItem j in i.Children)
                            {
                                if(_IncludeInAutoComplete(j)) { rval.Add(j); }
                            }
                        }
                        else if(_IncludeInAutoComplete(i)) { rval.Add(i); }
                    }
                }
                else
                {
                    return _ColumnKeywords;
                }

                _DefaultKeywordsGiven = false;
                return rval;
            }
            else
            {
                _DefaultKeywordsGiven = true;
                return _DefaultKeyWords;
            }
        }


        /// <summary>Returns if the words need to be updated.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Returns TRUE if an update is required, otherwise returns FALSE.</returns>
        public bool UpdateRequired(string pretext, char key)
        {
            if(!_DefaultKeywordsGiven) return true;

            if((key == (char) 27) && pretext.EndsWith(".")) return true;
            return (key == '.');
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDisposable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Disposes the object.</summary>
        public void Dispose()
        {}
    }
}
