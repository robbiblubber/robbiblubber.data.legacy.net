﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Windows.Forms;

using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Controls;
using Robbiblubber.Util;
using Robbiblubber.Util.Debug;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents the MS SQL Server database structure.</summary>
    public class MsExplorer: IExporer, IDisposable, IAutoCompletionSource
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Provider.</summary>
        protected ProviderItem _Provider = null;

        /// <summary>Database object.</summary>
        protected MsDatabase _DatabaseExplorer = null;

        /// <summary>Items.</summary>
        protected DbItem[] _Items = null;
        
        /// <summary>Default keywords.</summary>
        protected List<IAutoCompletionKeyword> _DefaultKeyWords;
        
        /// <summary>Column keywords.</summary>
        protected List<IAutoCompletionKeyword> _ColumnKeywords;
        
        /// <summary>Returns if the default keywords are currently available for auto completion.</summary>
        protected bool _DefaultKeywordsGiven = false;
        
        /// <summary>SQL Server keywords.</summary>
        protected SQLKeywords _Keywords = new SQLKeywords(PathOp.ApplicationDirectory + @"\mssql.keywords");
        
        /// <summary>SQL Server built-in functions.</summary>
        protected SQLKeywords _Functions = new SQLKeywords(PathOp.ApplicationDirectory + @"\mssql.functions");

        /// <summary>Tree node menu.</summary>
        protected IMenuTargetWindow _TreeNodeMenu;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider"></param>
        public MsExplorer(ProviderItem provider)
        {
            Items = new DbItem[] { _DatabaseExplorer = new MsDatabase(_Provider = provider) };
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes the default keywords.</summary>
        protected internal void __RefreshDefaultKeywords()
        {
            _DefaultKeyWords = new List<IAutoCompletionKeyword>();
            _ColumnKeywords = new List<IAutoCompletionKeyword>();

            _DefaultKeyWords.AddRange(_Keywords);
            _DefaultKeyWords.AddRange(_Functions);

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd =_Provider.CreateCommand();
                cmd.CommandText = "SELECT DISTINCT NAME FROM sys.ALL_COLUMNS";
                re = cmd.ExecuteReader();
                while(re.Read())
                {
                    _DefaultKeyWords.Add(new SQLKeywords.KeyWord(re.GetString(0), "mssql::column"));
                    _ColumnKeywords.Add(new SQLKeywords.KeyWord(re.GetString(0), "mssql::column"));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX01102", ex); }

            Disposal.Dispose(re, cmd);

            foreach(DbItem i in _DatabaseExplorer.Children)
            {
                if(i is MsSchema) { _DefaultKeyWords.Add(i); }
            }

            _DefaultKeywordsGiven = false;
        }


        /// <summary>Determines if an object is included in auto compeltion.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if item is include, otherwise returns FALSE.</returns>
        protected bool _IncludeInAutoComplete(DbItem item)
        {
            if(item is MsTable)     { return true; }
            if(item is MsView)      { return true; }
            if(item is MsType)      { return true; }
            if(item is MsProcedure) { return true; }
            if(item is MsSequnece)  { return true; }
            if(item is MsSchema)    { return true; }
            if(item is MsSynonym)   { return true; }

            return false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbExplorer                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the items root for this explorer.</summary>
        public DbItem[] Items
        {
            get; protected set;
        }


        /// <summary>Initializes auto completion.</summary>
        /// <param name="a">AutoCompletion instance.</param>
        public void InitAutoCompletion(AutoCompletion a)
        {
            a.AddImage("mssql::column", Resources.column);
            a.AddImage("mssql::type", Resources.datatype);
            a.AddImage("mssql::function", Resources.function);
            a.AddImage("mssql::function/if", Resources.function_if);
            a.AddImage("mssql::function/tf", Resources.function_tf);
            a.AddImage("mssql::procedure", Resources.procedure);
            a.AddImage("mssql::procedure/clr", Resources.procedure_clr);
            a.AddImage("mssql::procedure/rf", Resources.procedure_rf);
            a.AddImage("mssql::procedure/x", Resources.procedure_x);
            a.AddImage("mssql::schema", Resources.schema);
            a.AddImage("mssql::sequence", Resources.sequence);
            a.AddImage("mssql::table", Resources.table);
            a.AddImage("mssql::view", Resources.view);
            a.AddImage("mssql::synonym", Resources.synonym);

            __RefreshDefaultKeywords();
        }


        /// <summary>Initializes custom menus.</summary>
        /// <param name="menu">Menu provider.</param>
        public void InitMenus(IMenuTargetWindow menu)
        {
            _TreeNodeMenu = menu;

            menu.DatabaseMenus.AddMenu("sqlfwx::menu.editdata".Localize("&Edit Data..."), Resources.edit_table, "edit", Keys.F10);
            menu.DropDownMenus.AddMenu("sqlfwx::menu.editdata".Localize("Edit Data..."), Resources.edit_table, "edit");
            menu.DatabaseMenus.AddSeparator("blank0");
            menu.DropDownMenus.AddSeparator("blank0");

            menu.DatabaseMenus.AddMenu("Drop", Resources.remove, "drop", Keys.None, "Del");
            menu.DropDownMenus.AddMenu("Drop", Resources.remove, "drop");
            menu.DatabaseMenus.AddSeparator("blank1");
            menu.DropDownMenus.AddSeparator("blank1");

            menu.MenuOpening +=_MenuOpening;
            menu.MenuSelected += _MenuSelected;
            menu.TreeKeyDown += _TreeKeyDown;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Menu opening.</summary>
        private void _MenuOpening(object sender, MenuTargetEventArgs e)
        {
            IList<ToolStripItem> list = ((IList<ToolStripItem>) sender);
            foreach(ToolStripItem i in list) { i.Visible = false; i.Enabled = true; }

            if(e.Item is MsView)
            {
                list.GetItem("drop").Text = "sqlfwx::menu.dropview".Localize("&Drop View...");
                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
            else if(e.Item is MsTable)
            {
                list.GetItem("analyze").Text = "sqlfwx::menu.analtab".Localize("&Analyze Table");
                list.GetItem("drop").Text = "sqlfwx::menu.droptab".Localize("&Drop Table...");

                list.GetItem("blank1").Visible =list.GetItem("analyze").Visible = list.GetItem("drop").Visible = true;

                if(((((MsTable) e.Item).PrimaryKeys) != null) && ((((MsTable) e.Item).PrimaryKeys).Length > 0))
                {
                    list.GetItem("edit").Visible = list.GetItem("blank0").Visible = true;
                }
            }
            else if(e.Item is MsIndex)
            {
                list.GetItem("analyze").Text = "sqlfwx::menu.analidx".Localize("&Analyze Index");
                list.GetItem("drop").Text = "sqlfwx::menu.dropidx".Localize("&Drop Index...");

                list.GetItem("blank1").Visible = list.GetItem("analyze").Visible = list.GetItem("drop").Visible = true;
            }
            else if(e.Item is MsTrigger)
            {
                list.GetItem("drop").Text = "sqlfwx::menu.droptrig".Localize("&Drop Trigger...");

                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
            else if(e.Item is MsProcedure)
            {
                if(((MsProcedure) e.Item).IsFunction)
                {
                    list.GetItem("drop").Text = "sqlfwx::menu.dropfunc".Localize("&Drop Function...");
                }
                else
                {
                    list.GetItem("drop").Text = "sqlfwx::menu.dropproc".Localize("&Drop Procedure...");
                }

                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
            else if(e.Item is MsSequnece)
            {
                list.GetItem("drop").Text = "sqlfwx::menu.dropseq".Localize("&Drop Sequence...");

                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
            else if(e.Item is MsRule)
            {
                list.GetItem("drop").Text = "sqlfwx::menu.droprule".Localize("&Drop Rule...");

                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
        }


        /// <summary>Menu selected.</summary>
        private void _MenuSelected(object sender, MenuTargetEventArgs e)
        {
            switch((string) ((ToolStripItem) sender).Tag)
            {
                case "edit":
                    FormDataEditor t = new FormDataEditor(_Provider, e.Item.Name, "LIMIT 20", ((MsTable) e.Item).PrimaryKeys);
                    t.ShowDialog(true);
                    break;
                case "drop":
                    string dcpt = null, dtxt = null;
                    string stmt = null;
                    if(e.Item is MsView)
                    {
                        dcpt = "sqlfwx::udiag.dropview.caption".Localize("Drop View"); dtxt = "sqlfwx::udiag.dropview".Localize("Do you want to drop the view \"$(0)\"?").Replace("$(0)", e.Item.Name);
                        stmt = "DROP VIEW " + e.Item.QualifiedName;
                    }
                    else if(e.Item is MsTable)
                    {
                        dcpt = "sqlfwx::udiag.droptab.caption".Localize("Drop Table"); dtxt = "sqlfwx::udiag.droptab".Localize("Do you want to drop the table \"$(0)\"?").Replace("$(0)", e.Item.Name);
                        stmt = "DROP TABLE " + e.Item.QualifiedName;
                    }
                    else if(e.Item is MsIndex)
                    {
                        dcpt = "sqlfwx::udiag.dropidx.caption".Localize("Drop Index"); dtxt = "sqlfwx::udiag.dropidx".Localize("Do you want to drop the index \"$(0)\"?").Replace("$(0)", e.Item.Name);
                        stmt = "DROP INDEX " + e.Item.Name + " ON " + e.Item.Parent.Parent.QualifiedName;
                    }
                    else if(e.Item is MsTrigger)
                    {
                        dcpt = "sqlfwx::udiag.droptrg.caption".Localize("Drop Trigger"); dtxt = "sqlfwx::udiag.droptrg".Localize("Do you want to drop the trigger \"$(0)\"?").Replace("$(0)", e.Item.Name);
                        stmt = "DROP TRIGGER " + e.Item.QualifiedName;
                    }
                    else if(e.Item is MsProcedure)
                    {
                        if(((MsProcedure) e.Item).IsFunction)
                        {
                            dcpt = "sqlfwx::udiag.dropfunc.caption".Localize("Drop Function"); dtxt = "sqlfwx::udiag.dropfunc".Localize("Do you want to drop the function \"$(0)\"?").Replace("$(0)", e.Item.Name);
                            stmt = "DROP FUNCTION " + e.Item.QualifiedName;
                        }
                        else
                        {
                            dcpt = "sqlfwx::udiag.dropproc.caption".Localize("Drop Procedure"); dtxt = "sqlfwx::udiag.dropproc".Localize("Do you want to drop the procedure \"$(0)\"?").Replace("$(0)", e.Item.Name);
                            stmt = "DROP PROCEDURE " + e.Item.QualifiedName;
                        }
                    }
                    else if(e.Item is MsRule)
                    {
                        dcpt = "sqlfwx::udiag.droprule.caption".Localize("Drop Rule"); dtxt = "sqlfwx::udiag.droprule".Localize("Do you want to drop the rule \"$(0)\"?").Replace("$(0)", e.Item.Name);
                        stmt = "DROP RULE " + e.Item.QualifiedName;
                    }

                    if(MessageBox.Show(dtxt, dcpt, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes) { return; }

                    _TreeNodeMenu.TargetWindow.ShowWait();
                    try
                    {
                        IDbCommand cmd = _Provider.CreateCommand(stmt);
                        cmd.ExecuteNonQuery();
                    }
                    catch(Exception ex) { MessageBox.Show(ex.Message, "sqlfwx::udiag.error".Localize("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error); }

                    _TreeNodeMenu.TargetWindow.HideWait();
                    _TreeNodeMenu.TargetWindow.RefreshTree();
                    break;
            }
        }


        /// <summary>Tree key down.</summary>
        private void _TreeKeyDown(object sender, MenuTargetKeyEventArgs e)
        {
            if(e.Item == null) return;
            if((e.KeyArgs.KeyCode != Keys.Delete) || (e.KeyArgs.Modifiers != Keys.None)) return;

            if((e.Item is MsTable) || (e.Item is MsView) || (e.Item is MsIndex) || (e.Item is MsTrigger) || (e.Item is MsProcedure) || (e.Item is MsRule))
            {
                _MenuSelected(_TreeNodeMenu.DropDownMenus.GetItem("drop"), e);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IAutoCompletionSource                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Updates the words for a given preceding text.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Word list.</returns>
        public IEnumerable<IAutoCompletionKeyword> UpdateWords(string pretext, char key)
        {
            if((key == '.') || ((key == (char) 27)))
            {
                if(pretext.EndsWith(".")) { pretext = pretext.TrimEnd('.'); }

                List<IAutoCompletionKeyword> rval = new List<IAutoCompletionKeyword>();

                DbItem m = _DatabaseExplorer.GetItem(pretext.LastWord());

                if((m != null) && (m.Children != null))
                {
                    foreach(DbItem i in m.Children)
                    {
                        if(i.IsFolder)
                        {
                            foreach(DbItem j in i.Children)
                            {
                                if(_IncludeInAutoComplete(j)) { rval.Add(j); }
                            }
                        }
                        else if(_IncludeInAutoComplete(i)) { rval.Add(i); }
                    }
                }
                else
                {
                    return _ColumnKeywords;
                }

                _DefaultKeywordsGiven = false;
                return rval;
            }
            else
            {
                _DefaultKeywordsGiven = true;
                return _DefaultKeyWords;
            }
        }


        /// <summary>Returns if the words need to be updated.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Returns TRUE if an update is required, otherwise returns FALSE.</returns>
        public bool UpdateRequired(string pretext, char key)
        {
            if(!_DefaultKeywordsGiven) return true;

            if((key == (char) 27) && pretext.EndsWith(".")) return true;
            return (key == '.');
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDisposable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Disposes the object.</summary>
        public void Dispose()
        {}
    }
}
