﻿using System;



namespace Robbiblubber.Data.Timing
{
    /// <summary>This class provides extension methods for server timing.</summary>
    public static class TimingExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the server time for a local time.</summary>
        /// <param name="localTime">Local time.</param>
        /// <returns>Server time.</returns>
        public static DateTime ToServerTime(this DateTime localTime)
        {
            return ServerTime.Instance.GetServerTime(localTime);
        }


        /// <summary>Gets the local time for a server time.</summary>
        /// <param name="serverTime">Server time.</param>
        /// <returns>Local time.</returns>
        public static DateTime ToLocalTime(this DateTime serverTime)
        {
            return ServerTime.Instance.GetLocalTime(serverTime);
        }
    }
}
