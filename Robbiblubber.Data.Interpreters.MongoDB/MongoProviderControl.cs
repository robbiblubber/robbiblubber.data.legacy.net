﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Interpreters.MongoDB
{
    /// <summary>This class implements the Cassandra provider control.</summary>
    public partial class MongoProviderControl: UserControl, IProviderControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parent provider.</summary>
        private ProviderItem _Provider;
        
        /// <summary>Tree node.</summary>
        private TreeNode _Node = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public MongoProviderControl()
        {
            InitializeComponent();
            Node = null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Name changed.</summary>
        private void _TextName_TextChanged(object sender, EventArgs e)
        {
            _Provider.Name = _TextName.Text;

            if(Node != null) { Node.Text = _Provider.Name; }
        }


        /// <summary>Cluster changed.</summary>
        private void _TextCluster_TextChanged(object sender, EventArgs e)
        {
            _Provider.Data = Ddp.Create(_Provider.Data).Set("cluster", _TextCluster.Text).ToString(true);
        }


        /// <summary>Database changed.</summary>
        private void _TextDatabase_TextChanged(object sender, EventArgs e)
        {
            _Provider.Data = Ddp.Create(_Provider.Data).Set("db", _TextDatabase.Text).ToString(true);
        }


        /// <summary>Default keyspace changed.</summary>
        private void _TextWriteConcern_TextChanged(object sender, EventArgs e)
        {
            _Provider.Data = Ddp.Create(_Provider.Data).Set("w", _TextWriteConcern.Text).ToString(true);
        }


        /// <summary>User ID changed.</summary>
        private void _TextUserID_TextChanged(object sender, EventArgs e)
        {
            _Provider.Data = Ddp.Create(_Provider.Data).Set("user", _TextUserID.Text).ToString(true);
        }


        /// <summary>Password changed.</summary>
        private void _TextPassword_TextChanged(object sender, EventArgs e)
        {
            _Provider.Data = Ddp.Create(_Provider.Data).Set("password", _TextPassword.Text).ToString(true);
        }


        /// <summary>Check "Retry Writes" click.</summary>
        private void _CheckWrites_CheckedChanged(object sender, EventArgs e)
        {
            _Provider.Data = Ddp.Create(_Provider.Data).Set("retry", _CheckRetry.Checked ? "true" : "false").ToString(true);
        }


        /// <summary>Button "Test" click.</summary>
        private void _ButtonTest_Click(object sender, EventArgs e)
        {
            try
            {
                _Provider.Test();

                MessageBox.Show("sqlfwx::udiag.test.success".Localize("The connection has been tested successfully."), "sqlfwx::udiag.test.caption".Localize("Test"), MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show("sqlfwx::udiag.test.fail".Localize("The test has failed.") + ' ' + ex.Message, "sqlfwx::udiag.test.caption".Localize("Test"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        /// <summary>Link "Enhanced" click.</summary>
        private void _LinkEnhaced_Click(object sender, EventArgs e)
        {
            FormConnectionString f = new FormConnectionString(Provider.EnhancedString);

            if(f.ShowDialog() == DialogResult.OK)
            {
                Provider.EnhancedString = f.ConnectionString;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProviderControl                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the database provider.</summary>
        public ProviderItem Provider
        {
            get { return _Provider; }
            set
            {
                _Provider = value;

                Ddp ddp = new Ddp(_Provider.Data);

                _TextCluster.Text = ddp["cluster"];
                _TextDatabase.Text = ddp["db"];
                _TextWriteConcern.Text = ddp["w"];
                _TextUserID.Text = ddp["user"];
                _TextPassword.Text = ddp["password"];
                _CheckRetry.Checked = ddp["retry"];
                _TextName.Text = Provider.Name;
            }
        }


        /// <summary>Gets or sets the tree node.</summary>
        public TreeNode Node
        {
            get { return _Node; }
            set { _Node = value; }
        }
    }
}
