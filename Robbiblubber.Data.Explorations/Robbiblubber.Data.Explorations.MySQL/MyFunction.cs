﻿using System;
using System.Data;
using System.Drawing;

using Robbiblubber.Util;



namespace Robbiblubber.Data.Explorations.MySQL
{
    /// <summary>This class represents a MySQL function.</summary>
    public class MyFunction: MyProcedure
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="host">Host.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        internal MyFunction(MyHost host, MyFunctionsFolder folder, string name): base(host, folder, name)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] MyProcedure                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                return Resources.function;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                return "my::function";
            }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = null;

                IDbCommand cmd = null;
                IDataReader re = null;

                try
                {
                    cmd = Host.Provider.CreateCommand();
                    cmd.CommandText = "SHOW CREATE FUNCTION " + Folder.Database.Name + "." + Name;

                    re = cmd.ExecuteReader();
                    if(re.Read())
                    {
                        rval = re.GetString(2);
                    }
                }
                catch(Exception) {}

                Disposal.Dispose(re, cmd);

                return rval;
            }
        }
    }
}
