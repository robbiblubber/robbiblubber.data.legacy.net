﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class implements event arguments for IMenuTargetWindow events.</summary>
    public class MenuTargetEventArgs: EventArgs
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="item">Affected item.</param>
        /// <param name="node">Tree node.</param>
        public MenuTargetEventArgs(DbItem item, TreeNode node): base()
        {
            Item = item;
            Node = node;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the affected item.</summary>
        public DbItem Item { get; }


        /// <summary>Gets the tree node for the affected item.</summary>
        public TreeNode Node { get; }
    }
}
