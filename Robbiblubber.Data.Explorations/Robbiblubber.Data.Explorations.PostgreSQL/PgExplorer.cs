﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Windows.Forms;

using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Controls;
using Robbiblubber.Util;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.PostgreSQL
{
    /// <summary>This class represents the MS SQL Server database structure.</summary>
    public class PgExplorer: IExporer, IDisposable, IAutoCompletionSource
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Provider.</summary>
        protected ProviderItem _Provider = null;
        
        /// <summary>Default keywords.</summary>
        protected List<IAutoCompletionKeyword> _DefaultKeyWords;
        
        /// <summary>Column keywords.</summary>
        protected List<IAutoCompletionKeyword> _ColumnKeywords;

        /// <summary>Returns if the default keywords are currently available for auto completion.</summary>
        protected bool _DefaultKeywordsGiven = false;

        /// <summary>Database explorer root.</summary>
        protected PgDatabase _DatabaseExplorer = null;

        /// <summary>Tree node menu.</summary>
        protected IMenuTargetWindow _TreeNodeMenu;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider"></param>
        public PgExplorer(ProviderItem provider)
        {
            Items = new DbItem[] { _DatabaseExplorer = new PgDatabase(_Provider = provider) };
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes the default keywords.</summary>
        protected internal void __RefreshDefaultKeywords()
        {
            _DefaultKeyWords = new List<IAutoCompletionKeyword>();
            _ColumnKeywords = new List<IAutoCompletionKeyword>();

            _DefaultKeyWords.AddRange(SQLKeywords.Instance);

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = _Provider.CreateCommand();
                cmd.CommandText = "SELECT DISTINCT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _DefaultKeyWords.Add(new SQLKeywords.KeyWord(re.GetString(0), "pg::column"));
                    _ColumnKeywords.Add(new SQLKeywords.KeyWord(re.GetString(0), "pg::column"));
                }
            }
            catch(Exception) {}

            Disposal.Dispose(re, cmd);

            DbItem m = _DatabaseExplorer.GetItem("pg_catalog");

            if((m != null) && (m.Children != null))
            {
                foreach(DbItem i in m.Children)
                {
                    if(i.IsFolder)
                    {
                        foreach(DbItem j in i.Children)
                        {
                            if(_IncludeInAutoComplete(j)) { _DefaultKeyWords.Add(j); }
                        }
                    }
                    else if(_IncludeInAutoComplete(i)) { _DefaultKeyWords.Add(i); }
                }
            }

            foreach(DbItem i in _DatabaseExplorer.Children)
            {
                if(i is PgSchema) { _DefaultKeyWords.Add(i); }
            }

            _DefaultKeywordsGiven = false;
        }


        /// <summary>Determines if an object is included in auto compeltion.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if item is include, otherwise returns FALSE.</returns>
        protected bool _IncludeInAutoComplete(DbItem item)
        {
            if(item is PgTable)    { return true; }
            if(item is PgView)     { return true; }
            if(item is PgDataType) { return true; }
            if(item is PgFunction) { return true; }
            if(item is PgSequnece) { return true; }
            if(item is PgSchema)   { return true; }

            return false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbExplorer                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the items root for this explorer.</summary>
        public DbItem[] Items
        {
            get; protected set;
        }


        /// <summary>Initializes auto completion.</summary>
        /// <param name="a">AutoCompletion instance.</param>
        public void InitAutoCompletion(AutoCompletion a)
        {
            a.AddImage("pg::column", Resources.column);
            a.AddImage("pg::datatype", Resources.userdatatype);
            a.AddImage("pg::function", Resources.function);
            a.AddImage("pg::schema", Resources.schema);
            a.AddImage("pg::sequence", Resources.sequence);
            a.AddImage("pg::table", Resources.table);
            a.AddImage("pg::view", Resources.view);

            __RefreshDefaultKeywords();
        }


        /// <summary>Initializes custom menus.</summary>
        /// <param name="menu">Menu provider.</param>
        public void InitMenus(IMenuTargetWindow menu)
        {
            _TreeNodeMenu = menu;

            menu.DatabaseMenus.AddMenu("sqlfwx::menu.editdata".Localize("&Edit Data..."), Resources.edit_table, "edit", Keys.F10);
            menu.DropDownMenus.AddMenu("sqlfwx::menu.editdata".Localize("Edit Data..."), Resources.edit_table, "edit");
            menu.DatabaseMenus.AddSeparator("blank0");
            menu.DropDownMenus.AddSeparator("blank0");

            menu.DatabaseMenus.AddMenu("sqlfwx::menu.vacuum".Localize("&Vacuum Database"), Resources.vacuum, "vacuum");
            menu.DropDownMenus.AddMenu("sqlfwx::menu.vacuum".Localize("Vacuum Database"), Resources.vacuum, "vacuum");

            menu.DatabaseMenus.AddMenu("&Analyze", Resources.robot, "analyze");
            menu.DropDownMenus.AddMenu("Analyze", Resources.robot, "analyze");

            menu.DatabaseMenus.AddMenu("Drop", Resources.remove, "drop", Keys.None, "Del");
            menu.DropDownMenus.AddMenu("Drop", Resources.remove, "drop");
            menu.DatabaseMenus.AddSeparator("blank1");
            menu.DropDownMenus.AddSeparator("blank1");

            menu.MenuOpening +=_MenuOpening;
            menu.MenuSelected += _MenuSelected;
            menu.TreeKeyDown += _TreeKeyDown;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Menu opening.</summary>
        private void _MenuOpening(object sender, MenuTargetEventArgs e)
        {
            IList<ToolStripItem> list = ((IList<ToolStripItem>) sender);
            foreach(ToolStripItem i in list) { i.Visible = false; i.Enabled = true; }

            if(e.Item is PgDatabase)
            {
                list.GetItem("analyze").Text = "sqlfwx::menu.analdb".Localize("&Analyze Database");
                list.GetItem("blank1").Visible = list.GetItem("vacuum").Visible = list.GetItem("analyze").Visible = true;
            }
            else if(e.Item is PgView)
            {
                list.GetItem("drop").Text = "sqlfwx::menu.dropview".Localize("&Drop View...");
                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
            else if(e.Item is PgTable)
            {
                list.GetItem("analyze").Text = "sqlfwx::menu.analtab".Localize("&Analyze Table");
                list.GetItem("drop").Text = "sqlfwx::menu.droptab".Localize("&Drop Table...");

                list.GetItem("blank1").Visible =list.GetItem("analyze").Visible = list.GetItem("drop").Visible = true;

                if(((((PgTable) e.Item).PrimaryKeys) != null) && ((((PgTable) e.Item).PrimaryKeys).Length > 0))
                {
                    list.GetItem("edit").Visible = list.GetItem("blank0").Visible = true;
                }
            }
            else if(e.Item is PgIndex)
            {
                list.GetItem("drop").Text = "sqlfwx::menu.dropidx".Localize("&Drop Index...");

                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
            else if(e.Item is PgTrigger)
            {
                list.GetItem("drop").Text = "sqlfwx::menu.droptrig".Localize("&Drop Trigger...");

                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
            else if(e.Item is PgSequnece)
            {
                list.GetItem("drop").Text = "sqlfwx::menu.dropseq".Localize("&Drop Sequence...");

                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
            else if(e.Item is PgDataType)
            {
                list.GetItem("drop").Text = "sqlfwx::menu.droptype".Localize("&Drop Type...");

                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
            else if(e.Item is PgFunction)
            {
                list.GetItem("drop").Text = "sqlfwx::menu.dropfunc".Localize("&Drop Function...");

                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
            else if(e.Item is PgRule)
            {
                list.GetItem("drop").Text = "sqlfwx::menu.droprule".Localize("&Drop Rule...");

                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
            else if(e.Item is PgSchema)
            {
                list.GetItem("drop").Text = "sqlfwx::menu.dropschema".Localize("&Drop Schema...");

                list.GetItem("blank1").Visible = list.GetItem("drop").Visible = true;
            }
        }


        /// <summary>Menu selected.</summary>
        private void _MenuSelected(object sender, MenuTargetEventArgs e)
        {
            switch((string) ((ToolStripItem) sender).Tag)
            {
                case "vacuum":
                    _TreeNodeMenu.TargetWindow.ShowWait();
                    try
                    {
                        IDbCommand cmd = _Provider.CreateCommand("VACUUM");
                        cmd.ExecuteNonQuery();
                    }
                    catch(Exception) {}
                    _TreeNodeMenu.TargetWindow.HideWait();
                    break;
                case "edit":
                    FormDataEditor t = new FormDataEditor(_Provider, e.Item.QualifiedName, "", ((PgTable) e.Item).PrimaryKeys);
                    t.ShowDialog(true);
                    break;
                case "analyze":
                    _TreeNodeMenu.TargetWindow.ShowWait();
                    try
                    {
                        IDbCommand cmd = _Provider.CreateCommand((e.Item is PgDatabase) ? "ANALYZE" : ("ANALYZE " + e.Item.QualifiedName));
                        cmd.ExecuteNonQuery();
                    }
                    catch(Exception) {}

                    _TreeNodeMenu.TargetWindow.HideWait();
                    break;
                case "drop":
                    string dcpt = null, dtxt = null;
                    string stmt = null;
                    if(e.Item is PgView)
                    {
                        dcpt = "sqlfwx::udiag.dropview.caption".Localize("Drop View"); dtxt = "sqlfwx::udiag.dropview".Localize("Do you want to drop the view \"$(0)\"?").Replace("$(0)", e.Item.QualifiedName);
                        stmt = "DROP VIEW " + e.Item.QualifiedName;
                    }
                    else if(e.Item is PgTable)
                    {
                        dcpt = "sqlfwx::udiag.droptab.caption".Localize("Drop Table"); dtxt = "sqlfwx::udiag.droptab".Localize("Do you want to drop the table \"$(0)\"?").Replace("$(0)", e.Item.QualifiedName);
                        stmt = "DROP TABLE " + e.Item.QualifiedName;
                    }
                    else if(e.Item is PgIndex)
                    {
                        dcpt = "sqlfwx::udiag.dropidx.caption".Localize("Drop Index"); dtxt = "sqlfwx::udiag.dropidx".Localize("Do you want to drop the index \"$(0)\"?").Replace("$(0)", e.Item.QualifiedName);
                        stmt = "DROP INDEX " + e.Item.QualifiedName;
                    }
                    else if(e.Item is PgTrigger)
                    {
                        dcpt = "sqlfwx::udiag.droptrg.caption".Localize("Drop Trigger"); dtxt = "sqlfwx::udiag.droptrg".Localize("Do you want to drop the trigger \"$(0)\"?").Replace("$(0)", e.Item.QualifiedName);
                        stmt = "DROP TRIGGER " + e.Item.QualifiedName;
                    }
                    else if(e.Item is PgSequnece)
                    {
                        dcpt = "sqlfwx::udiag.dropseq.caption".Localize("Drop Sequence"); dtxt = "sqlfwx::udiag.dropseq".Localize("Do you want to drop the sequence \"$(0)\"?").Replace("$(0)", e.Item.QualifiedName);
                        stmt = "DROP SEQUENCE " + e.Item.QualifiedName;
                    }
                    else if(e.Item is PgDataType)
                    {
                        dcpt = "sqlfwx::udiag.droptype.caption".Localize("Drop Type"); dtxt = "sqlfwx::udiag.droptype".Localize("Do you want to drop the type \"$(0)\"?").Replace("$(0)", e.Item.QualifiedName);
                        stmt = "DROP TYPE " + e.Item.QualifiedName;
                    }
                    else if(e.Item is PgFunction)
                    {
                        dcpt = "sqlfwx::udiag.dropfunc.caption".Localize("Drop Function"); dtxt = "sqlfwx::udiag.dropfunc".Localize("Do you want to drop the function \"$(0)\"?").Replace("$(0)", e.Item.QualifiedName);
                        stmt = "DROP FUNCTION " + e.Item.QualifiedName;
                    }
                    else if(e.Item is PgRule)
                    {
                        dcpt = "sqlfwx::udiag.droprule.caption".Localize("Drop Rule"); dtxt = "sqlfwx::udiag.droprule".Localize("Do you want to drop the rule \"$(0)\"?").Replace("$(0)", e.Item.Name);
                        stmt = "DROP RULE " + e.Item.Name + " ON " + e.Item.Parent.Parent.QualifiedName;
                    }
                    else if(e.Item is PgSchema)
                    {
                        dcpt = "sqlfwx::udiag.dropschema.caption".Localize("Drop Schema"); dtxt = "sqlfwx::udiag.dropschema".Localize("Do you want to drop the schema \"$(0)\"?").Replace("$(0)", e.Item.QualifiedName);
                        stmt = "DROP SCHEMA " + e.Item.QualifiedName;
                    }

                    if(MessageBox.Show(dtxt, dcpt, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes) { return; }

                    _TreeNodeMenu.TargetWindow.ShowWait();
                    try
                    {
                        IDbCommand cmd = _Provider.CreateCommand(stmt);
                        cmd.ExecuteNonQuery();
                    }
                    catch(Exception ex) { MessageBox.Show(ex.Message, "sqlfwx::udiag.error".Localize("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error); }

                    _TreeNodeMenu.TargetWindow.HideWait();
                    _TreeNodeMenu.TargetWindow.RefreshTree();
                    break;
            }
        }


        /// <summary>Tree key down.</summary>
        private void _TreeKeyDown(object sender, MenuTargetKeyEventArgs e)
        {
            if(e.Item == null) return;
            if((e.KeyArgs.KeyCode != Keys.Delete) || (e.KeyArgs.Modifiers != Keys.None)) return;

            if((e.Item is PgTable) || (e.Item is PgView) || (e.Item is PgIndex) || (e.Item is PgTrigger) || (e.Item is PgSequnece) || (e.Item is PgDataType) || (e.Item is PgFunction) || (e.Item is PgRule) || (e.Item is PgSchema))
            {
                _MenuSelected(_TreeNodeMenu.DropDownMenus.GetItem("drop"), e);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IAutoCompletionSource                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Updates the words for a given preceding text.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Word list.</returns>
        public IEnumerable<IAutoCompletionKeyword> UpdateWords(string pretext, char key)
        {
            if((key == '.') || ((key == (char) 27)))
            {
                if(pretext.EndsWith(".")) { pretext = pretext.TrimEnd('.'); }

                List<IAutoCompletionKeyword> rval = new List<IAutoCompletionKeyword>();

                DbItem m = _DatabaseExplorer.GetItem(pretext.LastWord());

                if((m != null) && (m.Children != null))
                {
                    foreach(DbItem i in m.Children)
                    {
                        if(i.IsFolder)
                        {
                            foreach(DbItem j in i.Children)
                            {
                                if(_IncludeInAutoComplete(j)) { rval.Add(j); }
                            }
                        }
                        else if(_IncludeInAutoComplete(i)) { rval.Add(i); }
                    }
                }
                else
                {
                    return _ColumnKeywords;
                }

                _DefaultKeywordsGiven = false;
                return rval;
            }
            else
            {
                _DefaultKeywordsGiven = true;
                return _DefaultKeyWords;
            }
        }


        /// <summary>Returns if the words need to be updated.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Returns TRUE if an update is required, otherwise returns FALSE.</returns>
        public bool UpdateRequired(string pretext, char key)
        {
            if(!_DefaultKeywordsGiven) return true;

            if((key == (char) 27) && pretext.EndsWith(".")) return true;
            return (key == '.');
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDisposable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Disposes the object.</summary>
        public void Dispose()
        {}
    }
}
