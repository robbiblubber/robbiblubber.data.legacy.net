﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra aggregate.</summary>
    public class CasAggregate: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="argtypes">Argument types.</param>
        /// <param name="rtype">Return type.</param>
        /// <param name="statefunc">State function.</param>
        /// <param name="statetype">State type</param>
        /// <param name="initcond">Initial state.</param>
        /// <param name="finalfunc">Final function.</param>
        internal CasAggregate(CasAggregatesFolder folder, string name, IList<string> argtypes, string rtype, string statefunc, string statetype, string initcond, string finalfunc)
        {
            _Parent = Folder = folder;
            FullName = SimpleName = name;

            FullName += "(";
            for(int i = 0; i < argtypes.Count; i++)
            {
                if(i > 0) { FullName += ", "; }
                FullName += argtypes[i].ToUpper();
            }
            FullName += ")";

            ArgumentTypes = argtypes;
            ReturnType = rtype;
            StateFunction = statefunc;
            StateType = statetype;
            InitialStateCondition = initcond;
            FinalFunction = finalfunc;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent folder.</summary>
        public CasAggregatesFolder Folder { get; protected set; }


        /// <summary>Gets the keyspace.</summary>
        public CasKeyspace Keyspace { get { return Folder.Keyspace; } }


        /// <summary>Gets the aggregate simple name.</summary>
        public string SimpleName { get; protected set; }


        /// <summary>Gets the aggregate full name.</summary>
        public string FullName { get; protected set; }
        

        /// <summary>Gets the aggregate argument types.</summary>
        public IList<string> ArgumentTypes { get; protected set; }


        /// <summary>Gets the aggregate return type.</summary>
        public string ReturnType { get; protected set; }


        /// <summary>Gets the aggregate state function.</summary>
        public string StateFunction { get; protected set; }


        /// <summary>Gets the aggregate state type.</summary>
        public string StateType { get; protected set; }


        /// <summary>Gets the aggregate initial state condition.</summary>
        public string InitialStateCondition { get; protected set; }


        /// <summary>Gets the aggregate final function.</summary>
        public string FinalFunction { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return FullName; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                return Resources.function_tf;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                return "cas::aggregate";
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                return ("CREATE AGGREGATE " + Keyspace.Name + "." + FullName +
                        "\r\nSFUNC " + StateFunction +
                        "\r\nSTYPE " + StateType.ToUpper() +
                        "\r\nFINALFUNC " + FinalFunction +
                        "\r\nINITCOND " + InitialStateCondition + ";");
            }
        }
    }
}
