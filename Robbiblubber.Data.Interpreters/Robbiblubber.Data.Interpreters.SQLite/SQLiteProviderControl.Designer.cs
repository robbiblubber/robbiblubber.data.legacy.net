﻿namespace Robbiblubber.Data.Interpreters.SQLite
{
    partial class SQLiteProviderControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SQLiteProviderControl));
            this._TextFile = new System.Windows.Forms.TextBox();
            this._ButtonCreate = new System.Windows.Forms.Button();
            this._ButtonBrowse = new System.Windows.Forms.Button();
            this._ButtonTest = new System.Windows.Forms.Button();
            this._LabelFile = new System.Windows.Forms.Label();
            this._LabelName = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._LinkEnhaced = new Robbiblubber.Util.Controls.LinkControl();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // _TextFile
            // 
            this._TextFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextFile.Location = new System.Drawing.Point(30, 85);
            this._TextFile.Name = "_TextFile";
            this._TextFile.Size = new System.Drawing.Size(345, 25);
            this._TextFile.TabIndex = 1;
            this._TextFile.TextChanged += new System.EventHandler(this._TextFile_TextChanged);
            // 
            // _ButtonCreate
            // 
            this._ButtonCreate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCreate.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonCreate.Image")));
            this._ButtonCreate.Location = new System.Drawing.Point(381, 85);
            this._ButtonCreate.Name = "_ButtonCreate";
            this._ButtonCreate.Size = new System.Drawing.Size(25, 25);
            this._ButtonCreate.TabIndex = 2;
            this._ButtonCreate.Tag = "||sqlfwx::udiag.connect.create";
            this._ToolTip.SetToolTip(this._ButtonCreate, "Create file");
            this._ButtonCreate.UseVisualStyleBackColor = true;
            this._ButtonCreate.Click += new System.EventHandler(this._ButtonCreate_Click);
            // 
            // _ButtonBrowse
            // 
            this._ButtonBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonBrowse.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonBrowse.Image")));
            this._ButtonBrowse.Location = new System.Drawing.Point(408, 85);
            this._ButtonBrowse.Name = "_ButtonBrowse";
            this._ButtonBrowse.Size = new System.Drawing.Size(25, 25);
            this._ButtonBrowse.TabIndex = 3;
            this._ButtonBrowse.Tag = "||sqlfwx::udiag.connect.browse";
            this._ToolTip.SetToolTip(this._ButtonBrowse, "Browse");
            this._ButtonBrowse.UseVisualStyleBackColor = true;
            this._ButtonBrowse.Click += new System.EventHandler(this._ButtonBrowse_Click);
            // 
            // _ButtonTest
            // 
            this._ButtonTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonTest.Location = new System.Drawing.Point(286, 284);
            this._ButtonTest.Name = "_ButtonTest";
            this._ButtonTest.Size = new System.Drawing.Size(147, 29);
            this._ButtonTest.TabIndex = 5;
            this._ButtonTest.Tag = "sqlfwx::common.button.test";
            this._ButtonTest.Text = "&Test";
            this._ButtonTest.UseVisualStyleBackColor = true;
            this._ButtonTest.Click += new System.EventHandler(this._ButtonTest_Click);
            // 
            // _LabelFile
            // 
            this._LabelFile.AutoSize = true;
            this._LabelFile.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelFile.Location = new System.Drawing.Point(27, 69);
            this._LabelFile.Name = "_LabelFile";
            this._LabelFile.Size = new System.Drawing.Size(77, 13);
            this._LabelFile.TabIndex = 1;
            this._LabelFile.Tag = "sqlfwx::udiag.connect.file";
            this._LabelFile.Text = "Database &file:";
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.Location = new System.Drawing.Point(27, 19);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Tag = "sqlfwx::udiag.connect.name";
            this._LabelName.Text = "&Name:";
            // 
            // _TextName
            // 
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(30, 35);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(403, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._TextName_TextChanged);
            // 
            // _LinkEnhaced
            // 
            this._LinkEnhaced.AutoSize = true;
            this._LinkEnhaced.Cursor = System.Windows.Forms.Cursors.Hand;
            this._LinkEnhaced.DisabledColor = System.Drawing.Color.Gray;
            this._LinkEnhaced.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LinkEnhaced.LinkText = "Enhanced...";
            this._LinkEnhaced.Location = new System.Drawing.Point(30, 287);
            this._LinkEnhaced.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LinkEnhaced.Name = "_LinkEnhaced";
            this._LinkEnhaced.Size = new System.Drawing.Size(72, 21);
            this._LinkEnhaced.TabIndex = 4;
            this._LinkEnhaced.TabStop = false;
            this._LinkEnhaced.Tag = "sqlfwx::common.link.enhanced";
            this._LinkEnhaced.Underline = false;
            this._LinkEnhaced.Click += new System.EventHandler(this._LinkEnhaced_Click);
            // 
            // SQLiteProviderControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._LinkEnhaced);
            this.Controls.Add(this._LabelName);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelFile);
            this.Controls.Add(this._ButtonTest);
            this.Controls.Add(this._ButtonBrowse);
            this.Controls.Add(this._ButtonCreate);
            this.Controls.Add(this._TextFile);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "SQLiteProviderControl";
            this.Size = new System.Drawing.Size(463, 338);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _TextFile;
        private System.Windows.Forms.Button _ButtonCreate;
        private System.Windows.Forms.Button _ButtonBrowse;
        private System.Windows.Forms.Button _ButtonTest;
        private System.Windows.Forms.Label _LabelFile;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextName;
        private Robbiblubber.Util.Controls.LinkControl _LinkEnhaced;
        private System.Windows.Forms.ToolTip _ToolTip;
    }
}
