﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server groups folder.</summary>
    public class MsGroupsFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>s
        internal MsGroupsFolder(MsDatabase database)
        {
            _Parent = Database = database;
            _Name = "sqlfwx::tree.folder.groups".Localize("Groups");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; protected set; }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mssql::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            IDbCommand cmd = null;
            IDataReader re = null;

            _Children = new List<DbItem>();

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT NAME FROM sys.DATABASE_PRINCIPALS WHERE TYPE IN ('G', 'X') ORDER BY NAME";
                re = cmd.ExecuteReader();

                _Children = new List<DbItem>();
                while(re.Read())
                {
                    _Children.Add(new MsGroup(Database, this, re.GetString(0)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0120a", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
