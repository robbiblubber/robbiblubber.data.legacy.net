﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle index.</summary>
    public class OraIndex: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Index name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="unique">Unique flag.</param>
        internal OraIndex(OraDatabase database, OraIndexesFolder folder, string name, bool unique)
        {
            Database = database;
            _Parent = Folder = folder;
            IsUnique = unique;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public OraDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public OraIndexesFolder Folder { get; private set; }


        /// <summary>Gets if the index is unique.</summary>
        public bool IsUnique { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                if(IsUnique) { return Resources.index_unique; }

                return Resources.index;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                if(IsUnique) { return "ora::index/unique"; }

                return "ora::index";
            }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            List<string> cols = new List<string>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("SELECT COLUMN_NAME FROM SYS.ALL_IND_COLUMNS WHERE TABLE_OWNER = '" + Folder.Table.Folder.Schema.Name + "' AND TABLE_NAME = '" + Folder.Table.Name + "' AND INDEX_NAME = '" + Name + "' ORDER BY COLUMN_POSITION");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    cols.Add(re.GetString(0));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0241a", ex); }

            Disposal.Dispose(re, cmd);

            _Children = new List<DbItem>();

            if(Folder.Table.Columns != null)
            {
                foreach(OraColumn i in Folder.Table.Columns.Children)
                {
                    if(cols.Contains(i.Name))
                    {
                        _Children.Add(i);
                        cols.Remove(i.Name);
                    }
                }
            }

            foreach(string i in cols)
            {
                _Children.Add(new OraIndexColumn(Database, this, i));
            }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                IDbCommand cmd = null;

                try
                {
                    cmd = Database.Provider.CreateCommand("SELECT DBMS_MetaData.Get_DDL('INDEX', '" + Name +"', '" + Folder.Table.Folder.Schema.Name + "') FROM SYS.DUAL");
                    string rval = (string) cmd.ExecuteScalar();
                    Disposal.Dispose(cmd);

                    return rval.Trim(' ', '\r', '\n', '\t', ';') + ";";
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX0241b", ex); }

                return "DDL not accessible.";
            }
        }
    }
}
