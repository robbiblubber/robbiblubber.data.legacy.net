﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Explorations.MSAccess
{
    /// <summary>This class represents an Access database.</summary>
    public class AccessDatabase: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Database name.</summary>
        private string _Name;


        /// <summary>Access application object.</summary>
        private Microsoft.Office.Interop.Access.Application _App = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider">Provider.</param>
        internal AccessDatabase(ProviderItem provider)
        {
            Provider = provider;
            _Name = Ddp.Create(Provider.Data).Get<string>("file");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider.</summary>
        public ProviderItem Provider { get; private set; }
        

        /// <summary>Gets the access application.</summary>
        public Microsoft.Office.Interop.Access.Application AccessApplication
        {
            get
            {
                if(_App == null)
                {
                    _App = new Microsoft.Office.Interop.Access.Application();
                    _App.OpenCurrentDatabase(Name);
                }

                return _App;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Closes the application object if open.</summary>
        public void CloseApplication()
        {
            if(_App != null)
            {
                _App.CloseCurrentDatabase();
                _App.Quit(Microsoft.Office.Interop.Access.AcQuitOption.acQuitSaveNone);
                _App = null;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.msaccessdb; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mdb::database"; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return null; }
        }


        /// <summary>Refreshes the item.</summary>
        public override void Refresh()
        {}


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            _Children.Add(new AccessTablesFolder(this));
            _Children.Add(new AccessViewsFolder(this));
        }
    }
}
