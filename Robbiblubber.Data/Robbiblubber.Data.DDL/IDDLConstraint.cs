﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>Classes representing DDL constraints implement this interface.</summary>
    public interface IDDLConstraint
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the constraint name.</summary>
        string ConstraintName { get; }


        /// <summary>Gets the table name.</summary>
        string TableName { get; }
    }
}
