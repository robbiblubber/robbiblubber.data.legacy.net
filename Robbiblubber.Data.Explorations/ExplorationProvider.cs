﻿using System;
using System.Collections.Generic;

using Robbiblubber.Util.Debug;
using Robbiblubber.Data.Explorations.MSSQL;
using Robbiblubber.Data.Explorations.MySQL;
using Robbiblubber.Data.Explorations.ODBC;
using Robbiblubber.Data.Explorations.Oracle;
using Robbiblubber.Data.Explorations.PostgreSQL;
using Robbiblubber.Data.Explorations.SQLite;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations
{
    /// <summary>This class implements an exploration provider.</summary>
    public class ExplorationProvider: IExploration
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Explorer dictionary.</summary>
        protected Dictionary<ProviderItem, IExporer> _Explorers = new Dictionary<ProviderItem, IExporer>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public ExplorationProvider()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbExploration                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the explorer for a provider.</summary>
        /// <param name="provider">Provider.</param>
        /// <returns>Explorer.</returns>
        public IExporer this[ProviderItem provider]
        {
            get
            {
                if(!_Explorers.ContainsKey(provider))
                {
                    switch(provider.ProviderName)
                    {
                        case "Robbiblubber.Data.Providers.MSSQL.MSSQLProvider":
                            _Explorers.Add(provider, new MsExplorer(provider)); break;
                        case "Robbiblubber.Data.Providers.MySQL.MySQLProvider":
                        case "Robbiblubber.Data.Providers.MariaDB.MariaDbProvider":
                            _Explorers.Add(provider, new MyExplorer(provider)); break;
                        case "Robbiblubber.Data.Providers.Oracle.OracleProvider":
                            _Explorers.Add(provider, new OraExplorer(provider)); break;
                        case "Robbiblubber.Data.Providers.PostgreSQL.PostgreProvider":
                            _Explorers.Add(provider, new PgExplorer(provider)); break;
                        case "Robbiblubber.Data.Providers.SQLite.SQLiteProvider":
                            _Explorers.Add(provider, new LiteExplorer(provider)); break;
                        case "Robbiblubber.Data.Providers.ODBC.OdbcProvider":
                            _Explorers.Add(provider, new OdbcExplorer(provider)); break;
                    }
                }

                try
                {
                    return _Explorers[provider];
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX00800", ex); }

                return null;
            }
        }
    }
}
