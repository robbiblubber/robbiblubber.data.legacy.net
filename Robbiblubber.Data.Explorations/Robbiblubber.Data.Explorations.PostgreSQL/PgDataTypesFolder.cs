﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.PostgreSQL
{
    /// <summary>This class represents a PostgreSQL data types folder.</summary>
    public class PgDataTypesFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="schema">Parent schema.</param>
        internal PgDataTypesFolder(PgDatabase database, PgSchema schema)
        {
            Database = database;
            _Parent = Schema = schema;
            _Name = "sqlfwx::tree.folder.types".Localize("Types");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public PgDatabase Database { get; protected set; }


        /// <summary>Gets the parent schema.</summary>
        public PgSchema Schema { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "pg::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT TYPNAME, TYPTYPE FROM PG_CATALOG.PG_TYPE WHERE TYPELEM = 0 AND TYPNAMESPACE = (SELECT OID FROM PG_CATALOG.PG_NAMESPACE WHERE nspname = '" + Schema.Name + "') AND TYPNAME NOT IN (SELECT TABLENAME FROM PG_CATALOG.PG_TABLES WHERE SCHEMANAME = '" + Schema.Name + "') ORDER BY TYPNAME";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new PgDataType(Database, this, re.GetString(0), re.GetChar(1).ToString()));
                }
            }
            catch(Exception) {}

            Disposal.Dispose(re, cmd);
        }
    }
}
