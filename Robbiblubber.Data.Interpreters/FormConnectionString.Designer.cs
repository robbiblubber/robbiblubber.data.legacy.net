﻿namespace Robbiblubber.Data.Interpreters
{
    partial class FormConnectionString
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConnectionString));
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._ButtonOK = new System.Windows.Forms.Button();
            this._LabelConnectionString = new System.Windows.Forms.Label();
            this._TextConnectionString = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(543, 94);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(147, 29);
            this._ButtonCancel.TabIndex = 2;
            this._ButtonCancel.Tag = "sqlfwx::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(390, 94);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(147, 29);
            this._ButtonOK.TabIndex = 1;
            this._ButtonOK.Tag = "sqlfwx::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            // 
            // _LabelConnectionString
            // 
            this._LabelConnectionString.AutoSize = true;
            this._LabelConnectionString.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelConnectionString.Location = new System.Drawing.Point(20, 29);
            this._LabelConnectionString.Name = "_LabelConnectionString";
            this._LabelConnectionString.Size = new System.Drawing.Size(143, 13);
            this._LabelConnectionString.TabIndex = 0;
            this._LabelConnectionString.Tag = "sqlfwx::udiag.cstr.string";
            this._LabelConnectionString.Text = "Specify Connection String:";
            // 
            // _TextConnectionString
            // 
            this._TextConnectionString.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextConnectionString.Location = new System.Drawing.Point(23, 45);
            this._TextConnectionString.Name = "_TextConnectionString";
            this._TextConnectionString.Size = new System.Drawing.Size(667, 25);
            this._TextConnectionString.TabIndex = 0;
            // 
            // FormConnectionString
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(713, 145);
            this.Controls.Add(this._LabelConnectionString);
            this.Controls.Add(this._TextConnectionString);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._ButtonCancel);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormConnectionString";
            this.Tag = "sqlfwx::udiag.cstr.caption";
            this.Text = "Enhanced Database Options";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Button _ButtonOK;
        private System.Windows.Forms.Label _LabelConnectionString;
        private System.Windows.Forms.TextBox _TextConnectionString;
    }
}