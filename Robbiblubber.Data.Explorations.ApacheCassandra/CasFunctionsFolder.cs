﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra functions folder.</summary>
    public class CasFunctionsFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="keyspace">Keyspace.</param>
        internal CasFunctionsFolder(CasKeyspace keyspace)
        {
            _Parent = keyspace;
            _Name = "Functions";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the keyspace instance.</summary>
        public CasKeyspace Keyspace 
        {
            get { return (CasKeyspace) _Parent; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "cas::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Keyspace.Database.Provider.CreateCommand("SELECT FUNCTION_NAME, LANGUAGE, CALLED_ON_NULL_INPUT, ARGUMENT_NAMES, ARGUMENT_TYPES, RETURN_TYPE, BODY FROM System_Schema.FUNCTIONS WHERE KEYSPACE_NAME = :k");
                cmd.AddParameter(":k", Keyspace.Name);

                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new CasFunction(this, re.GetString(0), re.GetString(1), re.GetBoolean(2), (IList<string>) re.GetValue(3), (IList<string>) re.GetValue(4), re.GetString(5), re.GetString(6)));
                }

                _Children.Sort((a, b) => a.Name.CompareTo(b.Name));
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0010b", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
