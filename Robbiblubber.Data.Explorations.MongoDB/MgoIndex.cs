﻿using System;
using System.Drawing;

using Robbiblubber.Data.Interpreters;

using MongoDB.Bson;



namespace Robbiblubber.Data.Explorations.MongoDB
{
    /// <summary>This class represents a MongoDB index.</summary>
    public class MgoIndex: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>BSON data.</summary>
        private BsonDocument _Data;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parent">Parent folder.</param>
        /// <param name="data">BSON data.</param>
        internal MgoIndex(MgoIndexesFolder parent, BsonDocument data)
        {
            _Parent = parent;
            _Data = data;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent cluster.</summary>
        public MgoDatabase Database
        {
            get { return ((MgoCollectionsFolder) _Parent).Database; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Data["name"].AsString; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.index; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mgo::index"; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return _Data.Pretty(); }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }
    }
}
