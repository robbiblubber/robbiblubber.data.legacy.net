﻿using System;



namespace Robbiblubber.Data.SQL
{
    /// <summary>This class provides a basic implementation of a statement analyzer.</summary>
    public class SQLStatement: ISQLStatement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Source.</summary>
        protected string _Source = null;


        /// <summary>Source in lowercase.</summary>
        protected string _LowerSource = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // cosntructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SQLStatement()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="source">Source.</param>
        public SQLStatement(string source)
        {
            Source = source;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the statement source.</summary>
        public string Source
        {
            get { return _Source; }
            set { _LowerSource = (_Source = value.TrimEnd(';', ' ', (char) 9, (char) 10, (char) 13).Trim()).ToLower(); }
        }


        /// <summary>Gets if the statement is DDL.</summary>
        public virtual bool IsDDL
        {
            get { return (_LowerSource.StartsWith("create") || _LowerSource.StartsWith("drop") || _LowerSource.StartsWith("alter")); }
        }


        /// <summary>Gets if the statement is commit.</summary>
        public virtual bool IsCommit
        {
            get { return _LowerSource == "commit"; }
        }


        /// <summary>Gets if the statement is rollback.</summary>
        public virtual bool IsRollback
        {
            get { return _LowerSource == "rollback"; }
        }


        /// <summary>Gets if the statement is a SELECT query.</summary>
        public virtual bool IsSelect
        {
            get { return _LowerSource.StartsWith("select"); }
        }


        /// <summary>Gets if the statement is an INSERT statement.</summary>
        public virtual bool IsInsert
        {
            get { return _LowerSource.StartsWith("insert"); }
        }


        /// <summary>Gets if the statement is an UPDATE statement.</summary>
        public virtual bool IsUpdate
        {
            get { return _LowerSource.StartsWith("update"); }
        }


        /// <summary>Gets if the statement is a DELETE statement.</summary>
        public virtual bool IsDelete
        {
            get { return _LowerSource.StartsWith("delete"); }
        }


        /// <summary>Gets if the statement is an INSERT statement.</summary>
        public virtual bool IsDML
        {
            get { return (IsInsert || IsUpdate || IsDelete); }
        }
    }
}
