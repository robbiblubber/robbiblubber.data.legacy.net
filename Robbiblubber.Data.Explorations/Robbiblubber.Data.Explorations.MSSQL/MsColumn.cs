﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server column.</summary>
    public class MsColumn: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Column name.</summary>
        private string _Name;


        /// <summary>Referenced table.</summary>
        private string _ReferencedTable = null;


        /// <summary>Referenced column.</summary>
        private string _ReferencedColumn = null;


        /// <summary>Rules read.</summary>
        private bool _RulesRead = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="dataType">Data type.</param>
        /// <param name="pk">Primary key.</param>
        /// <param name="fk">Foreign key.</param>
        /// <param name="notNull">Not null.</param>
        internal MsColumn(MsDatabase database, MsColumnsFolder folder, string name, string dataType, bool pk, bool fk, bool notNull)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
            DataType = dataType;
            IsPrimaryKey = pk;
            IsForeignKey = fk;
            IsNotNull = notNull;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public MsColumnsFolder Folder { get; private set; }


        /// <summary>Gets the data type.</summary>
        public string DataType { get; private set; }


        /// <summary>Gets if the field is a primary key.</summary>
        public bool IsPrimaryKey { get; private set; }


        /// <summary>Gets if the field is a foreign key.</summary>
        public bool IsForeignKey { get; private set; }


        /// <summary>Gets if the field is not nullable.</summary>
        public bool IsNotNull { get; private set; }


        /// <summary>Gets the referenced table.</summary>
        public string ReferencedTable
        {
            get
            {
                _Read();
                return _ReferencedTable;
            }
        }


        /// <summary>Gets the referenced column.</summary>
        public string ReferencedColumn
        {
            get
            {
                _Read();
                return _ReferencedColumn;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Reads referenced table and column.</summary>
        private void _Read()
        {
            if(_ReferencedTable != null) return;

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT X.TABLE_SCHEMA, X.TABLE_NAME, X.COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS C, INFORMATION_SCHEMA.KEY_COLUMN_USAGE U, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE X " +
                                    "WHERE C.CONSTRAINT_NAME = U.CONSTRAINT_NAME AND X.CONSTRAINT_NAME = C.CONSTRAINT_NAME AND C.CONSTRAINT_TYPE = 'FOREIGN KEY' " +
                                    "AND C.TABLE_SCHEMA = '" + Folder.Table.Folder.Schema.Name + "' AND C.TABLE_NAME = '" + Folder.Table.Name + "' AND U.COLUMN_NAME = '" + Name + "';";
                re = cmd.ExecuteReader();

                if(re.Read())
                {
                    _ReferencedTable = re.GetString(0) + "." + re.GetString(1);
                    _ReferencedColumn = re.GetString(2);
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX01201", ex); }

            Disposal.Dispose(re, cmd);
        }


        /// <summary>Reads the rules for this column.</summary>
        private void _GetRules()
        {
            if(_RulesRead) return;

            _Children = new List<DbItem>();
            _RulesRead = true;

            if(Folder.Table is MsView) return;

            int ruleID = 0;

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT C.RULE_OBJECT_ID FROM sys.COLUMNS C, sys.TABLES T, sys.SCHEMAS S WHERE C.OBJECT_ID = T.OBJECT_ID AND T.SCHEMA_ID = S.SCHEMA_ID AND C.NAME = '" + Name + "' AND T.NAME = '" + Folder.Table.Name + "' AND S.NAME = '" + Folder.Table.Folder.Schema.Name + "'";
                ruleID = (int) cmd.ExecuteScalar();
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX01202", ex); }

            Disposal.Dispose(cmd);

            if(ruleID == 0) return;

            string schema = null, rule = null;

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT O.NAME, S.NAME FROM sys.OBJECTS O, sys.SCHEMAS S WHERE O.SCHEMA_ID = S.SCHEMA_ID AND O.OBJECT_ID = " + ruleID.ToString();
                re = cmd.ExecuteReader();

                if(re.Read())
                {
                    rule = re.GetString(0);
                    schema = re.GetString(1);
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX01203", ex); }

            Disposal.Dispose(re, cmd);

            if(rule == null) return;
            
            _Children.Add(Database.GetSchema(schema).GetRule(rule));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                if(Folder.Table is MsView) { return Resources.column_view; }

                if(IsPrimaryKey) { return Resources.column_pk; }
                if(IsForeignKey) { return Resources.column_fk; }
                if(IsNotNull) { return Resources.column_nn; }
                
                return Resources.column;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                if(Folder.Table is MsView) { return "mssql::column/view"; }

                if(IsPrimaryKey) { return "mssql::column/pk"; }
                if(IsForeignKey) { return "mssql::column/fk"; }
                if(IsNotNull)    { return "mssql::column/nn"; }
                
                return "mssql::column";
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return true; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get
            {
                _GetRules();
                return _Children;
            }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = Name + "   " + DataType;
                if(IsNotNull)    { rval += "   NOT NULL"; }
                if(IsPrimaryKey) { rval += "   PRIMARY KEY"; }
                if(IsForeignKey) { rval += "   REFERENCES " + ReferencedTable + "(" + ReferencedColumn + ")"; }
                rval += ";";

                return rval;
            }
        }


        /// <summary>Gets if the object defines a query.</summary>
        public override bool HasQuery
        {
            get { return true; }
        }


        /// <summary>Gets the query for this object.</summary>
        public override string Query
        {
            get { return "SELECT [" + Name + "] FROM " + Folder.Table.QualifiedName; }
        }


        /// <summary>Gets the qualified name for this object.</summary>
        public override string QualifiedName
        {
            get { return Name; }
        }
    }
}
