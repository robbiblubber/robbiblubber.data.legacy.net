﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

using Robbiblubber.Data.Providers;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class implements a provider manager.</summary>
    public abstract class ProviderManager: IProviderManager
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected static members                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Provider manager cache.</summary>
        protected static Dictionary<string, IProviderManager> _Cache = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets all provider managers available.</summary>
        public static IProviderManager[] Managers
        {
            get
            {
                LoadProviders();

                return _Cache.Values.ToArray();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a provider manager for a provider name.</summary>
        /// <param name="providerName">Provider name.</param>
        /// <returns>Provider manager.</returns>
        public static IProviderManager GetManager(string providerName)
        {
            LoadProviders();

            if(!_Cache.ContainsKey(providerName))
            {
                _Cache.Add(providerName, new UnknownManager()); 
            }

            return _Cache[providerName];
        }


        /// <summary>Loads all available provider managers.</summary>
        public static void LoadProviders()
        {
            if(_Cache != null) return;

            _Cache = new Dictionary<string, IProviderManager>();

            foreach(object i in ClassOp.LoadSubtypesOf(typeof(IProviderManager)))
            {
                Debug.Print(i.GetType().FullName);

                if(!((IProviderManager) i).Valid) continue;

                try
                {
                    _Cache.Add(((IProviderManager) i).ProviderName, (IProviderManager) i);
                }
                catch(Exception) {}
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProviderManager                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider name.</summary>
        public abstract string ProviderName { get; }


        /// <summary>Gets the manager display name.</summary>
        public abstract string DisplayName { get; }


        /// <summary>Gets the provider control.</summary>

        public abstract IProviderControl Control { get; }


        /// <summary>Gets the provider icon.</summary>
        public abstract Image ProviderIcon { get; }


        /// <summary>Gets the connction icon.</summary>
        public abstract Image ConnectionIcon { get; }

        
        /// <summary>Gets if the provider manager is valid.</summary>
        public virtual bool Valid
        {
            get { return (ProviderBase.GetProvider(ProviderName) != null); }
        }


        /// <summary>Spawns a new provider.</summary>
        /// <param name="id">ID.</param>
        /// <param name="parent">Parent folder.</param>
        /// <returns>Provider.</returns>
        public virtual ProviderItem Spawn(string id, Folder parent)
        {
            return new ProviderItem(ProviderName, id, parent);
        }


        /// <summary>Spawns a saved provider.</summary>
        /// <param name="sec">Configuration data.</param>
        /// <param name="parent">Parent folder.</param>
        /// <returns>Provider.</returns>
        public virtual ProviderItem Spawn(DdpElement sec, Folder parent)
        {
            return new ProviderItem(sec, parent);
        }
    }
}
