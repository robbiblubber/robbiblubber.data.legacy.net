﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.PostgreSQL
{
    /// <summary>This class represents a PostgreSQL trigger.</summary>
    public class PgTrigger: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Trigger name.</summary>
        private string _Name;


        /// <summary>Folders.</summary>
        private List<PgTriggerAction> _Actions = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        internal PgTrigger(PgDatabase database, PgTriggersFolder folder, string name)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public PgDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public PgTriggersFolder Folder { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                return Resources.trigger;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                return "pg::trigger";
            }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get
            {
                if(_Actions == null)
                {
                    _Actions = new List<PgTriggerAction>();

                    IDbCommand cmd = Database.Provider.CreateCommand();
                    cmd.CommandText = "SELECT EVENT_MANIPULATION FROM INFORMATION_SCHEMA.TRIGGERS WHERE EVENT_OBJECT_SCHEMA = '" + Folder.Table.Folder.Schema.Name + "' AND EVENT_OBJECT_TABLE = '" + Folder.Table.Name + "' AND TRIGGER_NAME = '" + Name + "' ORDER BY EVENT_MANIPULATION";
                    IDataReader re = cmd.ExecuteReader();

                    while(re.Read())
                    {
                        _Actions.Add(new PgTriggerAction(Database, this, re.GetString(0)));
                    }
                    Disposal.Dispose(re, cmd);
                }

                return _Actions;
            }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = null;

                IDbCommand cmd = null;
                IDataReader re = null;

                try
                {
                    cmd = Database.Provider.CreateCommand();
                    cmd.CommandText = "SELECT ACTION_TIMING, EVENT_MANIPULATION, ACTION_ORIENTATION, ACTION_STATEMENT FROM INFORMATION_SCHEMA.TRIGGERS WHERE EVENT_OBJECT_SCHEMA = '" + Folder.Table.Folder.Schema.Name + "' AND EVENT_OBJECT_TABLE = '" + Folder.Table.Name + "' AND TRIGGER_NAME = '" + Name + "'";
                    re = cmd.ExecuteReader();

                    rval = "CREATE TRIGGER " + Name;
                    if(re.Read())
                    {
                        rval += ("\r\n   " + re.GetString(0).Trim() + " " + re.GetString(1).Trim() + " ON " + Folder.Table.Name + "\r\n");
                        if(re.GetString(2) == "ROW") { rval += "   FOR EACH ROW\r\n"; }
                        rval += ("   " + re.GetString(3).TrimEnd(' ', ';') + ";");
                    }
                }
                catch(Exception) { rval = null; }

                Disposal.Dispose(re, cmd);

                return rval;
            }
        }


        /// <summary>Gets the qualified name for this object.</summary>
        public override string QualifiedName
        {
            get { return Folder.Table.Folder.Schema.Name + "." + Name; }
        }
    }
}
