﻿using System;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Providers.MongoDB
{
    /// <summary>This class provides a SQLite-specific implementation of the SQLStatement class.</summary>
    public class MongoStatement: SQLStatement, ISQLStatement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // cosntructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MongoStatement() : base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="source">Source.</param>
        public MongoStatement(string source) : base(source)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLStatement                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the statement is a SELECT query.</summary>
        public override bool IsSelect
        {
            get { return (_LowerSource.Contains(".find(")); }
        }
    }
}
