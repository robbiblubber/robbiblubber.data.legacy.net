﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server data types folder.</summary>
    public class MsTypesFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="schema">Parent schema.</param>
        internal MsTypesFolder(MsDatabase database, MsSchema schema)
        {
            Database = database;
            _Parent = Schema = schema;
            _Name = "Types";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; protected set; }


        /// <summary>Gets the parent schema.</summary>
        public MsSchema Schema { get; protected set; }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mssql::folder"; }
        }
        

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            IDbCommand cmd = null;
            IDataReader re = null;

            _Children = new List<DbItem>();

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT T.NAME, T.IS_USER_DEFINED FROM sys.TYPES T, sys.SCHEMAS S WHERE S.NAME = '" + Schema.Name + "' AND S.SCHEMA_ID = T.SCHEMA_ID ORDER BY T.NAME";
                re = cmd.ExecuteReader();

                _Children = new List<DbItem>();
                while(re.Read())
                {
                    _Children.Add(new MsType(Database, this, re.GetString(0), re.GetBoolean(1)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX01222", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
