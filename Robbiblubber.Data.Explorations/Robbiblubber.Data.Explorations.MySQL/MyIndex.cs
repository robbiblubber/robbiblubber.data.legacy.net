﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MySQL
{
    /// <summary>This class represents a MySQL index.</summary>
    public class MyIndex: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Index name.</summary>
        private string _Name;


        /// <summary>Columns.</summary>
        private List<string> _Columns = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="host">Host.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="unique">Unique flag.</param>
        internal MyIndex(MyHost host, MyIndexesFolder folder, string name, bool unique)
        {
            Host = host;
            _Parent = Folder = folder;
            IsUnique = unique;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the host instance.</summary>
        public MyHost Host { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public MyIndexesFolder Folder { get; private set; }


        /// <summary>Gets if the index is unique.</summary>
        public bool IsUnique { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the column names.</summary>
        /// <returns>List of column names.</returns>
        private List<string> _GetColumns()
        {
            if(_Columns == null)
            {
                _Columns = new List<string>();

                IDbCommand cmd = Host.Provider.CreateCommand();
                cmd.CommandText = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA = '" + Folder.Table.Folder.Database.Name + "' AND TABLE_NAME = '" + Folder.Table.Name + "' AND INDEX_NAME = '" + Name + "' ORDER BY SEQ_IN_INDEX";

                IDataReader re = cmd.ExecuteReader();
                while(re.Read())
                {
                    _Columns.Add(re.GetString(0));
                }
                Disposal.Dispose(re, cmd);
            }

            return _Columns;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                if(IsUnique) { return Resources.index_unique; }

                return Resources.index;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                if(IsUnique) { return "my::index/unique"; }

                return "my::index";
            }
        }
        
        
        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = "CREATE ";

                if(IsUnique) { rval += "UNIQUE "; }
                rval += ("INDEX " + Folder.Table.Folder.Database.Name + "." + Name + " ON " + Folder.Table.Folder.Database.Name + "." + Folder.Table.Name + " (");

                foreach(string i in _GetColumns())
                {
                    rval += (i + ", ");
                }

                return (rval.TrimEnd(' ', ',') + ")");
            }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            foreach(MyColumn i in Folder.Table.Columns.Children)
            {
                if(_GetColumns().Contains(i.Name))
                {
                    _Children.Add(i);
                }
            }
        }
    }
}
