﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.PostgreSQL
{
    /// <summary>This class represents a PostgreSQL trigger action.</summary>
    public class PgTriggerAction: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Action name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="trigger">Parent trigger.</param>
        /// <param name="name">Name.</param>
        internal PgTriggerAction(PgDatabase database, PgTrigger trigger, string name)
        {
            Database = database;
            _Parent = Trigger = trigger;
            _Name = name.ToLower();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public PgDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public PgTrigger Trigger { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                if(Name == "insert") { return Resources.action_insert; }
                if(Name == "update") { return Resources.action_update; }
                if(Name == "delete") { return Resources.action_delete; }

                return Resources.action_unknown;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                if(Name == "insert") { return "pg::action/insert"; }
                if(Name == "update") { return "pg::action/update"; }
                if(Name == "delete") { return "pg::action/delete"; }

                return "pg::action/unknown";
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }
    }
}
