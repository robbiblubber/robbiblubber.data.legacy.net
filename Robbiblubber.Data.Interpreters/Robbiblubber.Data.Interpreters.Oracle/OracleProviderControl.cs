﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Interpreters.Oracle
{
    /// <summary>This class implements the Oracle provider control.</summary>
    public partial class OracleProviderControl: UserControl, IProviderControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parent provider.</summary>
        private ProviderItem _Provider;


        /// <summary>Tree node.</summary>
        private TreeNode _Node = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public OracleProviderControl()
        {
            InitializeComponent();
            Node = null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Data source changed.</summary>
        private void _TextDataSource_TextChanged(object sender, EventArgs e)
        {
            _Provider.Data = Ddp.Create(_Provider.Data).Set("tns", _TextDataSource.Text).ToString(true);
        }


        /// <summary>User ID changed.</summary>
        private void _TextUserID_TextChanged(object sender, EventArgs e)
        {
            _Provider.Data = Ddp.Create(_Provider.Data).Set("userid", _TextUserID.Text).ToString(true);
        }


        /// <summary>Password changed.</summary>
        private void _TextPassword_TextChanged(object sender, EventArgs e)
        {
            _Provider.Data = Ddp.Create(_Provider.Data).Set("password", _TextPassword.Text).ToString(true);
        }


        /// <summary>Provider name changed.</summary>
        private void _TextName_TextChanged(object sender, EventArgs e)
        {
            _Provider.Name = _TextName.Text;

            if(Node != null) { Node.Text = _Provider.Name; }
        }

        
        /// <summary>Button "Test" click.</summary>
        private void _ButtonTest_Click(object sender, EventArgs e)
        {
            try
            {
                _Provider.Test();

                MessageBox.Show("sqlfwx::udiag.test.success".Localize("The connection has been tested successfully."), "sqlfwx::udiag.test.caption".Localize("Test"), MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show("sqlfwx::udiag.test.fail".Localize("The test has failed.") + ' ' + ex.Message, "sqlfwx::udiag.test.caption".Localize("Test"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        /// <summary>Link "Enhanced" click.</summary>
        private void _LinkEnhaced_Click(object sender, EventArgs e)
        {
            FormConnectionString f = new FormConnectionString(Provider.EnhancedString);

            if(f.ShowDialog() == DialogResult.OK)
            {
                Provider.EnhancedString = f.ConnectionString;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProviderControl                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the database provider.</summary>
        public ProviderItem Provider
        {
            get { return _Provider; }
            set
            {
                _Provider = value;

                Ddp ddp = new Ddp(_Provider.Data);

                _TextDataSource.Text = ddp["tns"];
                _TextUserID.Text = ddp["userid"];
                _TextPassword.Text = ddp["password"];
                _TextName.Text = Provider.Name;
            }
        }


        /// <summary>Gets or sets the tree node.</summary>
        public TreeNode Node
        {
            get { return _Node; }
            set { _Node = value; }
        }
    }
}
