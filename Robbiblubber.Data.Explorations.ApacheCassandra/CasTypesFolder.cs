﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra types folder.</summary>
    public class CasTypesFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="keyspace">Keyspace.</param>
        internal CasTypesFolder(CasKeyspace keyspace)
        {
            _Parent = Keyspace = keyspace;
            _Name = "Types";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the keyspace instance.</summary>
        public CasKeyspace Keyspace { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "cas::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Keyspace.Database.Provider.CreateCommand("SELECT TYPE_NAME, FIELD_NAMES, FIELD_TYPES FROM System_Schema.TYPES WHERE KEYSPACE_NAME = :k");
                cmd.AddParameter(":k", Keyspace.Name);

                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new CasType(this, re.GetString(0), (IList<string>) re.GetValue(1), (IList<string>) re.GetValue(2)));
                }

                _Children.Sort((a, b) => a.Name.CompareTo(b.Name));
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0010b", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
