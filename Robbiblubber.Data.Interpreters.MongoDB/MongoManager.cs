﻿using System;
using System.Drawing;



namespace Robbiblubber.Data.Interpreters.MongoDB
{
    /// <summary>This class implements the MongoDB manager.</summary>
    public class MongoManager: ProviderManager, IProviderManager
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Control.</summary>
        private IProviderControl _Control = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProviderManager                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider name.</summary>
        public override string ProviderName
        {
            get { return "Robbiblubber.Data.Providers.MongoDB.MongoProvider"; }
        }


        /// <summary>Gets the manager display name.</summary>
        public override string DisplayName
        {
            get { return "MongoDB"; }
        }


        /// <summary>Gets the provider control.</summary>

        public override IProviderControl Control
        {
            get
            {
                if(_Control == null) { _Control = new MongoProviderControl(); }

                return _Control;
            }
        }


        /// <summary>Gets the provider icon.</summary>
        public override Image ProviderIcon
        {
            get { return Resources.mongo; }
        }


        /// <summary>Gets the connction icon.</summary>
        public override Image ConnectionIcon
        {
            get { return Resources.mongodb; }
        }
    }
}
