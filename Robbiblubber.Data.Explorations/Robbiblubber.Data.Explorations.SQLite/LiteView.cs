﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.SQLite
{
    /// <summary>This class represents a SQLite view.</summary>
    public class LiteView: LiteTable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Folder.</param>
        /// <param name="name">Name.</param>
        internal LiteView(LiteDatabase database, LiteTablesFolder folder, string name): base(database, folder, name)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] LiteTable                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.view; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "lite::view"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            _Children.Add(new LiteColumnsFolder(Database, this));
            _Children.Add(new LiteTriggerFolder(Database, this));
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                IDbCommand cmd = null;
                string rval = "";

                try
                {
                    cmd = Database.Provider.CreateCommand("SELECT SQL FROM SQLITE_MASTER WHERE TYPE = 'view' AND NAME = '" + Name + "'");
                    rval = (string) cmd.ExecuteScalar();
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX00110", ex); }

                Disposal.Dispose(cmd);

                return (rval.Trim(' ', ';') + ";");
            }
        }
    }
}
