﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra indexes folder.</summary>
    public class CasIndexesFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="table">Parent table.</param>
        internal CasIndexesFolder(CasTable table)
        {
            _Parent = Table = table;
            _Name = "Indexes";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the keyspace instance.</summary>
        public CasKeyspace Keyspace
        {
            get { return Table.Keyspace; }
        }


        /// <summary>Gets the parent table.</summary>
        public CasTable Table { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "cas::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Keyspace.Database.Provider.CreateCommand("SELECT INDEX_NAME, KIND, OPTIONS FROM System_Schema.INDEXES WHERE KEYSPACE_NAME = :k AND TABLE_NAME = :t");
                cmd.AddParameter(":k", Keyspace.Name);
                cmd.AddParameter(":t", Table.Name);

                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new CasIndex(this, re.GetString(0), re.GetString(1), (IDictionary<string, string>) re.GetValue(2)));
                }

                _Children.Sort((a, b) => ((CasColumn) a)._SortKey.CompareTo(((CasColumn) b)._SortKey));
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0010b", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
