﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle type attributes folder.</summary>
    public class OraTypeAttributesFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="type">Parent type.</param>
        internal OraTypeAttributesFolder(OraDatabase database, OraType type)
        {
            Database = database;
            _Parent = Type = type;
            _Name = "Attributes";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public OraDatabase Database { get; private set; }


        /// <summary>Gets the parent type.</summary>
        public OraType Type { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "ora::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("SELECT ATTR_NAME, ATTR_TYPE_NAME, LENGTH, PRECISION, SCALE, ATTR_TYPE_MOD FROM SYS.ALL_TYPE_ATTRS WHERE OWNER = '" + Type.Folder.Schema.Name + "' AND TYPE_NAME = '" + Type.Name + "' ORDER BY ATTR_NO");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    string dataType = (re.GetString(1));
                    if(re.IsDBNull(3))
                    {
                        if(!re.IsDBNull(2)) { dataType += ("(" +  re.GetInt32(2).ToString() + ")"); }
                    }
                    else
                    {
                        dataType += ("(" + (re.IsDBNull(4) ? re.GetInt32(3).ToString() : re.GetInt32(3).ToString() + "," + re.GetInt32(4).ToString()) + ")");
                    }
                    string mod = (re.IsDBNull(5) ? "" : re.GetString(5));

                    _Children.Add(new OraTypeAttribute(Database, this, re.GetString(0), dataType, mod));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0243a", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
