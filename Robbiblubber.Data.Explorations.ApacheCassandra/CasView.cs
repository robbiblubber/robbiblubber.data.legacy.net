﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra view.</summary>
    public class CasView: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Table name.</summary>
        protected string _Name;


        /// <summary>Columns.</summary>
        protected IList<string> _Columns = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="folder">Folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="baseTable">Base table.</param>
        /// <param name="where">Where clause.</param>
        internal CasView(CasViewsFolder folder, string name, string baseTable, string where)
        {
            _Parent = Folder = folder;
            _Name = name;
            BaseTable = baseTable;
            WhereClause = where;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent folder.</summary>
        public CasViewsFolder Folder { get; protected set; }


        /// <summary>Gets the keyspace.</summary>
        public CasKeyspace Keyspace 
        { 
            get { return Folder.Keyspace; }
        }


        /// <summary>Gets the view base table.</summary>
        public string BaseTable { get; protected set; }


        /// <summary>Gets the view where clause.</summary>
        public string WhereClause { get; protected set; }


        /// <summary>Gets the colums folder.</summary>
        public IList<string> Columns
        {
            get
            {
                if(_Columns == null)
                {
                    _Columns = new List<string>();

                    IDbCommand cmd = Keyspace.Database.Provider.CreateCommand("SELECT * FROM " + Keyspace.Name + "." + Name + " LIMIT 1");
                    IDataReader re = cmd.ExecuteReader();

                    for(int i = 0; i < Keyspace.Database.Provider.CountFields(re); i++)
                    {
                        _Columns.Add(re.GetName(i));
                    }

                    Disposal.Dispose(re, cmd);
                }

                return _Columns;
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.view; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "cas::view"; }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                return ("CREATE MATERIALIZED VIEW " + Keyspace.Name + "." + Name +
                       "\r\nAS SELECT " + Columns.Chain(", ") +
                       "\r\nFROM " + Keyspace.Name + "." + BaseTable +
                       "\r\nWHERE " + WhereClause + ";");
            }
        }
    }
}
