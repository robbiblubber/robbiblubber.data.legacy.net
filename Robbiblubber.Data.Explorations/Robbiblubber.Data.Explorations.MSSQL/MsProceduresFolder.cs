﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server proceduress folder.</summary>
    public class MsProceduresFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="schema">Parent schema.</param>
        internal MsProceduresFolder(MsDatabase database, MsSchema schema)
        {
            Database = database;
            _Parent = Schema = schema;
            _Name = "sqlfwx::tree.folder.procedures".Localize("Procedures");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; protected set; }


        /// <summary>Gets the parent schema.</summary>
        public MsSchema Schema { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mssql::folder"; }
        }
        

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            IDbCommand cmd = null;
            IDataReader re = null;

            _Children = new List<DbItem>();

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT O.NAME, O.TYPE FROM sys.OBJECTS O, sys.SCHEMAS S WHERE S.NAME = '" + Schema.Name + "' AND S.SCHEMA_ID = O.SCHEMA_ID AND O.TYPE IN ('P', 'PC', 'RF', 'X') ORDER BY O.NAME";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new MsProcedure(Database, this, re.GetString(0), re.GetString(1)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX01212", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
