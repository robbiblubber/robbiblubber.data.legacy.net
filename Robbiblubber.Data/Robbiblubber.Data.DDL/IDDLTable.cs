﻿using System;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.DDL
{
    /// <summary>Classes representing DDL tables implement this interface.</summary>
    public interface IDDLTable: IDDLStatement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the table name.</summary>
        string TableName { get; }


        /// <summary>Gets the table columns.</summary>
        IDDLColumn[] Columns { get; }


        /// <summary>Gets the table constraints.</summary>
        IDDLConstraint[] Constraints { get; }


        /// <summary>Gets the table indexes.</summary>
        IDDLIndex[] Indexes { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a column to the table.</summary>
        /// <param name="columnName">Column name.</param>
        /// <param name="dataType">Column data type.</param>
        /// <param name="notNull">Not null option.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>DDL table.</returns>
        IDDLTable AddColumn(string columnName, SQLDataType dataType, bool notNull = false, string defaultValue = null);


        /// <summary>Adds a primary key constraint to the table.</summary>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        IDDLTable AddPrimaryKey(params string[] columnNames);


        /// <summary>Adds a named primary key to the table.</summary>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        IDDLTable AddNamedPrimaryKey(string constraintName, params string[] columnNames);


        /// <summary>Adds a foreign key constraint to the table.</summary>
        /// <param name="columnName">Column name.</param>
        /// <param name="referencedTableName">Referenced table name.</param>
        /// <param name="referencedColumnName">Referenced column name.</param>
        /// <param name="cascadeDelete">Delete cascade flag.</param>
        /// <returns>DDL table.</returns>
        IDDLTable AddForeignKey(string columnName, string referencedTableName, string referencedColumnName, bool cascadeDelete = false);


        /// <summary>Adds a named foreign key constraint to the table.</summary>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="columnName">Column name.</param>
        /// <param name="referencedTableName">Referenced table name.</param>
        /// <param name="referencedColumnName">Referenced column name.</param>
        /// <param name="cascadeDelete">Delete cascade flag.</param>
        /// <returns>DDL table.</returns>
        IDDLTable AddNamedForeignKey(string constraintName, string columnName, string referencedTableName, string referencedColumnName, bool cascadeDelete = false);


        /// <summary>Adds a check constraint to the table.</summary>
        /// <param name="condition">Check condition.</param>
        /// <returns>DDL table.</returns>
        IDDLTable AddCheck(string condition);


        /// <summary>Adds a named check constraint to the table.</summary>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="condition">Check condition.</param>
        /// <returns>DDL table.</returns>
        IDDLTable AddNamedCheck(string constraintName, string condition);


        /// <summary>Adds an index to the table.</summary>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        IDDLTable AddIndex(params string[] columnNames);


        /// <summary>Adds an index to the table.</summary>
        /// <param name="indexName">Index name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        IDDLTable AddNamedIndex(string indexName, params string[] columnNames);


        /// <summary>Adds a unique index to the table.</summary>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        IDDLTable AddUniqueIndex(params string[] columnNames);


        /// <summary>Adds a named unique index to the table.</summary>
        /// <param name="indexName">Index name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        IDDLTable AddNamedUniqueIndex(string indexName, params string[] columnNames);
    }
}
