﻿using System;
using System.Data;
using System.Linq;



namespace Robbiblubber.Data.SQL
{
    /// <summary>This class represents a SQL data type.</summary>
    public sealed class SQLDataType
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Object ID.</summary>
        public static readonly SQLDataType OBJECT_ID = new SQLDataType("ObjectID", "sd", 56, DbType.String);

        /// <summary>String.</summary>
        public static readonly SQLDataType STRING = new SQLDataType("String", "s", 0, DbType.String);

        /// <summary>Text.</summary>
        public static readonly SQLDataType TEXT = new SQLDataType("Text", "sx", 0, DbType.String);

        /// <summary>Integer.</summary>
        public static readonly SQLDataType INTEGER = new SQLDataType("Integer", "i", 0, DbType.Int32);
        
        /// <summary>Boolean.</summary>
        public static readonly SQLDataType BOOLEAN = new SQLDataType("Boolean", "b", 0, DbType.Boolean);

        /// <summary>String.</summary>
        public static readonly SQLDataType BOOLEAN_INTEGER = new SQLDataType("Boolean Integer", "ib", 0, DbType.Int32);

        /// <summary>Timestamp.</summary>
        public static readonly SQLDataType TIMESTAMP = new SQLDataType("Timestamp", "t", 0, DbType.DateTime);
        
        /// <summary>8 bit Integer.</summary>
        public static readonly SQLDataType INT8 = new SQLDataType("Int8", "i", 8, DbType.Byte);

        /// <summary>16 bit Integer.</summary>
        public static readonly SQLDataType INT16 = new SQLDataType("Int16", "i", 16, DbType.Int16);

        /// <summary>32 bit Integer.</summary>
        public static readonly SQLDataType INT32 = new SQLDataType("Int32", "i", 32, DbType.Int32);

        /// <summary>16 bit Integer.</summary>
        public static readonly SQLDataType INT64 = new SQLDataType("Int64", "i", 64, DbType.Int64);
        
        /// <summary>Float.</summary>
        public static readonly SQLDataType FLOAT = new SQLDataType("Float", "ff", 0, DbType.Single);

        /// <summary>Double.</summary>
        public static readonly SQLDataType DOUBLE = new SQLDataType("Double", "fd", 0, DbType.Double);
        
        /// <summary>Double.</summary>
        public static readonly SQLDataType DECIMAL = new SQLDataType("Decimal", "fx", 0, DbType.Decimal);

        /// <summary>Binary.</summary>
        public static readonly SQLDataType BINARY = new SQLDataType("Binary", "x", 0, DbType.Binary);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a data type from its number.</summary>
        /// <param name="type">Type number, name or constant.</param>
        /// <returns>Data type.</returns>
        public static SQLDataType Parse(string type)
        {
            string t = type.Replace(" ", "_").ToLower().Replace("timestamp", "t")
                           .Replace("boolean_integer", "ib").Replace("boolean_int", "ib").Replace("booleaninteger", "ib").Replace("boolint", "ib")
                           .Replace("boolean", "b").Replace("bool", "b")
                           .Replace("integer", "i").Replace("int", "i").Replace("int16", "i[16]").Replace("short", "i[16]")
                           .Replace("int8", "i[8]").Replace("byte", "i[8]").Replace("int32", "i[32]").Replace("int64", "i[64]").Replace("long", "i[64]")
                           .Replace("string", "s").Replace("object_id", "sd").Replace("objectid", "sd").Replace("object", "sd").Replace("text", "sx")
                           .Replace("float", "ff").Replace("single", "ff").Replace("double", "fd").Replace("decimal", "fx")
                           .Replace("binary", "x");

            int l = 0;
            if(t.Contains("["))
            {
                l = int.Parse(t.Trim("abcdefghijklmnopqrstuvwxyz[]".ToArray()));
                t = t.Trim("[1234567890]".ToArray());
            }

            switch(t)
            {
                case "t":  return TIMESTAMP;
                case "s":  return ((l == 0) ? STRING : new SQLDataType("String", "s", l, DbType.String));
                case "o":  return ((l == 0) ? OBJECT_ID : new SQLDataType("ObjectID", "id", l, DbType.String));
                case "sx": return ((l == 0) ? TEXT : new SQLDataType("Text", "sx", l, DbType.String));
                case "i":  if(l == 0) return INTEGER;
                           else if(l == 8) return INT8;
                           else if(l == 16) return INT16;
                           else if(l == 32) return INT32;
                           else if(l == 64) return INT32;
                           return new SQLDataType("Integer", "i", l, DbType.VarNumeric);
                case "ff": return FLOAT;
                case "fd": return DOUBLE;
                case "fx": return DECIMAL;
                case "b":  return BOOLEAN;
                case "x":  return BINARY;
            }

            throw new ArgumentOutOfRangeException("Unable to parse data type \"" + type + "\"");
        }


        /// <summary>Returns a data type for a string of a given length.</summary>
        /// <param name="length">Length.</param>
        /// <returns>Data type.</returns>
        public static SQLDataType String(int length)
        {
            return Parse("s[" + length.ToString() + "]");
        }


        /// <summary>Returns a data type for an object ID of a given length.</summary>
        /// <param name="length">Length.</param>
        /// <returns>Data type.</returns>
        public static SQLDataType ObjectID(int length)
        {
            return Parse("sd[" + length.ToString() + "]");
        }


        /// <summary>Returns a data type for a text of a given length.</summary>
        /// <param name="length">Length.</param>
        /// <returns>Data type.</returns>
        public static SQLDataType Text(int length)
        {
            return Parse("sx[" + length.ToString() + "]");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="name">Data type name.</param>
        /// <param name="shortName">Short data type name.</param>
        /// <param name="length">Data type length.</param>
        /// <param name="dbType">Database type.</param>
        private SQLDataType(string name, string shortName, int length, DbType dbType)
        {
            Name = name;
            ShortName = shortName;
            Length = length;
            DbType = dbType;
        }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the data type name.</summary>
        public string Name { get; private set; }

        /// <summary>Gets the data type short name.</summary>
        public string ShortName { get; private set; }

        /// <summary>Gets the data type length.</summary>
        public int Length { get; private set; }
        
        /// <summary>Gets the database type for this data type.</summary>
        public DbType DbType { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets a column value.</summary>
        /// <param name="re">Data reader.</param>
        /// <param name="c">Column name.</param>
        /// <returns>Value.</returns>
        public object GetValue(IDataReader re, string c)
        {
            switch(DbType)
            {
                case DbType.String: case DbType.AnsiString: case DbType.StringFixedLength: case DbType.AnsiStringFixedLength:
                    return re.GetString(c, null);
                case DbType.Int32:
                    if(ShortName == "ib") { return (!re.GetInteger(c, 0).Equals(0)); }
                    return re.GetInteger(c, 0);
                case DbType.Int16:
                    return re.GetShort(c, 0);
                case DbType.Int64:
                    return re.GetLong(c, 0);
                case DbType.Boolean:
                    return re.GetBool(c, false);
                case DbType.DateTime: case DbType.Time: case DbType.Date: case DbType.DateTime2:
                    return re.GetTimestamp(c, DateTime.MinValue);
                case DbType.VarNumeric: case DbType.Decimal:
                    return (re.IsDBNull(re.GetOrdinal(c)) ? ((decimal) 0) : re.GetDecimal(re.GetOrdinal(c)));
                case DbType.Single:
                    return (re.IsDBNull(re.GetOrdinal(c)) ? ((float) 0) : re.GetFloat(re.GetOrdinal(c)));
                case DbType.Double:
                    return (re.IsDBNull(re.GetOrdinal(c)) ? ((double) 0) : re.GetDouble(re.GetOrdinal(c)));
            }

            return (re.IsDBNull(re.GetOrdinal(c)) ? null : re.GetValue(re.GetOrdinal(c)));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Determies whether a specific object is equal to the current object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Returns TRUE if the objects are equal, otherwise returns FALSE.</returns>
        public override bool Equals(object obj)
        {
            if(obj is SQLDataType)
            {
                return (((SQLDataType) obj).ToString() == ToString());
            }

            return false;
        }


        /// <summary>Returns the hash code for this instance.</summary>
        /// <returns>Hash code.</returns>
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }


        /// <summary>Returns a string representation of this object.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return ((Length < 1) ? ShortName : (ShortName + "[" + Length.ToString() + "]"));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // operators                                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Determines if two data types are equal..</summary>
        /// <param name="a">First data type.</param>
        /// <param name="b">Second data type.</param>
        /// <returns>Returns TRUE if the objects are equal, otherwise returns FALSE.</returns>
        public static bool operator ==(SQLDataType a, SQLDataType b)
        {
            return (a.ToString() == b.ToString());
        }


        /// <summary>Determines if two data types are equal..</summary>
        /// <param name="a">First data type.</param>
        /// <param name="b">Second data type.</param>
        /// <returns>Returns TRUE if the objects are equal, otherwise returns FALSE.</returns>
        public static bool operator !=(SQLDataType a, SQLDataType b)
        {
            return (a.ToString() != b.ToString());
        }
    }
}
