﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra type.</summary>
    public class CasType: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Table name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="folder">Folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="fnames">Field names.</param>
        /// <param name="ftypes">Field types.</param>
        internal CasType(CasTypesFolder folder, string name, IList<string> fnames, IList<string> ftypes)
        {
            _Parent = Folder = folder;
            _Name = name;
            FieldNames = fnames;
            FieldTypes = ftypes;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent folder.</summary>
        public CasTypesFolder Folder { get; protected set; }


        /// <summary>Gets the keyspace.</summary>
        public CasKeyspace Keyspace
        {
            get { return Folder.Keyspace; }
        }


        /// <summary>Gets the type field names.</summary>
        public IList<string> FieldNames { get; protected set; }


        /// <summary>Gets the type field types.</summary>
        public IList<string> FieldTypes { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.type; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "cas::type"; }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = ("CREATE TYPE " + Keyspace.Name + "." + Name + "\r\n");

                for(int i = 0; i < FieldNames.Count; i++)
                {
                    if(i == 0)
                    {
                        rval += "(";
                    }
                    else { rval += ",\r\n "; }
                    rval += FieldNames[i] + " " +FieldTypes[i].ToUpper();
                }
                rval += ");";

                return rval;
            }
        }
    }
}
