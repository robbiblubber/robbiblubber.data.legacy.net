﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Microsoft.Office.Interop.Access.Dao;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSAccess
{
    /// <summary>This class represents an Access indexes folder.</summary>
    public class AccessIndexesFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="table">Parent table.</param>
        internal AccessIndexesFolder(AccessDatabase database, AccessTable table)
        {
            Database = database;
            Table = table;
            _Name = "Indexes";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the Database instance.</summary>
        public AccessDatabase Database { get; private set; }


        /// <summary>Gets the parent table.</summary>
        public AccessTable Table { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.msacc_folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mdb::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            foreach(Microsoft.Office.Interop.Access.Dao.Index i in Database.AccessApplication.CurrentDb().TableDefs[Table.Name].Indexes)
            {
                _Children.Add(new AccessIndex(Database, this, i.Name, i.Unique));
            }
        }
    }
}
