﻿using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class implements a SQL text export provider.</summary>
    public class SQLTextExport: ISQLExport, ITargetWindow
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>SQL Interpreter.</summary>
        private SQLInterpreter _Interpreter;


        /// <summary>File name.</summary>
        private string _FileName = null;


        /// <summary>Text.</summary>
        private string _Text;


        /// <summary>Original target.</summary>
        private ITargetWindow _OriginalTarget;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Exports the object.</summary>
        private void _Export()
        {
            _OriginalTarget = _Interpreter.Target;
            _Text = "";

            _Interpreter.Target = this;
            _Interpreter.Execute();
            _Interpreter.Target = _OriginalTarget;

            File.WriteAllText(_FileName, _Text);

            _OriginalTarget = null;
            _Text = null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ISQLExport                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the menu text for the export provider.</summary>
        public string MenuText
        {
            get { return "&Text file..."; }
        }


        /// <summary>Gets the menu icon for the export provider.</summary>
        public Image MenuIcon
        {
            get { return Resources.text; }
        }


        /// <summary>Gets the menu sort index for the export provider.</summary>
        public int SortIndex
        {
            get { return 1000; }
        }


        /// <summary>Exports the SQL results.</summary>
        /// <param name="i">SQL interpreter.</param>
        public void Export(SQLInterpreter i)
        {
            _Interpreter = i;

            SaveFileDialog d = new SaveFileDialog();
            d.Filter = "sqlfwx::udiag.filter.txt".Localize("Text files") + " (*.txt)|*.txt|" + "sqlfwx::udiag.filter.all".Localize("All files") + " (*.*)|*.*";

            if(d.ShowDialog() == DialogResult.OK)
            {
                _FileName = d.FileName;
                i.Target.ShowWait("sqlfwx::udiag.wait.exporting".Localize("Exporting..."));
                
                i.Target.ExecuteSynchronized(new ThreadStart(_Export));

                i.Target.HideWait();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ITargetWindow                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database provider.</summary>
        ProviderItem ITargetWindow.Provider
        {
            get { return _OriginalTarget.Provider; }
        }


        /// <summary>Gets or sets the view mode.</summary>
        ViewMode ITargetWindow.View
        {
            get { return ViewMode.TEXT; }
        }


        /// <summary>Gets the data grid.</summary>
        DataGridView ITargetWindow.Grid
        {
            get { return null; }
        }


        /// <summary>Gets the selected item.</summary>
        DbItem ITargetWindow.SelectedItem 
        { 
            get { return null; }
        }


        /// <summary>Gets the selected node.</summary>
        TreeNode ITargetWindow.SelectedNode 
        { 
            get { return null; }
        }


        /// <summary>Writes to console.</summary>
        /// <param name="text">Text.</param>
        void ITargetWindow.Write(string text)
        {
            _Text += text;
        }


        /// <summary>Writes to grid.</summary>
        /// <param name="text">Text.</param>
        void ITargetWindow.WriteToGrid(string text)
        {}


        /// <summary>Sets grid data source.</summary>
        /// <param name="dataSource">Data source.</param>
        void ITargetWindow.DataToGrid(object dataSource)
        {}


        /// <summary>Shows the wait window.</summary>
        void ITargetWindow.ShowWait()
        {}


        /// <summary>Shows the wait window.</summary>
        /// <param name="text">Text.</param>
        void ITargetWindow.ShowWait(string text)
        {}


        /// <summary>Hides the wait window.</summary>
        void ITargetWindow.HideWait()
        {}


        /// <summary>Refreshes the tree view.</summary>
        /// <param name="n">Tree node.</param>
        void ITargetWindow.RefreshTree(TreeNode n)
        {}


        /// <summary>Executes code in separate thread.</summary>
        /// <param name="c">Thread delegate.</param>
        void ITargetWindow.ExecuteSynchronized(ThreadStart c)
        {}


        /// <summary>Executes code in separate thread.</summary>
        /// <param name="c">Thread delegate.</param>
        /// <param name="p">Paramter.</param>
        void ITargetWindow.ExecuteSynchronized(ParameterizedThreadStart c, object p)
        {}
    }
}
