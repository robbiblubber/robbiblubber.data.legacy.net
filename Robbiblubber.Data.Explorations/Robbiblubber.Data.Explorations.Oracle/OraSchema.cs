﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle schema.</summary>
    public class OraSchema: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Schema name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="name">Name.</param>
        internal OraSchema(OraDatabase database, string name)
        {
            _Parent = Database = database;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public OraDatabase Database { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.schema; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "ora::schema"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            _Children.Add(new OraTablesFolder(Database, this));
            _Children.Add(new OraObjectTablesFolder(Database, this));
            _Children.Add(new OraTypesFolder(Database, this));
            _Children.Add(new OraClustersFolder(Database, this));
            _Children.Add(new OraDimensionsFolder(Database, this));
            _Children.Add(new OraViewsFolder(Database, this));
            _Children.Add(new OraMViewsFolder(Database, this));
            _Children.Add(new OraSequencesFolder(Database, this));
            _Children.Add(new OraLibrariesFolder(Database, this));
            _Children.Add(new OraPackagesFolder(Database, this));
            _Children.Add(new OraProceduresFolder(Database, this));
            _Children.Add(new OraFunctionsFolder(Database, this));
            _Children.Add(new OraOperatorsFolder(Database, this));
            _Children.Add(new OraSynonymsFolder(Database, this));
            _Children.Add(new OraDbLinksFolder(Database, this));
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                try
                {
                    IDbCommand cmd = Database.Provider.CreateCommand("SELECT DBMS_MetaData.Get_DDL('USER', '" + Name +"') FROM SYS.DUAL");
                    string rval = (string) cmd.ExecuteScalar();
                    Disposal.Dispose(cmd);

                    return rval.Trim(' ', '\r', '\n', '\t', ';') + ";";
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX0242e", ex); }

                return "DDL not accessible."; ;
            }
        }
    }
}
