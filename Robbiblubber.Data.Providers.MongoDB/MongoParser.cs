﻿using System;
using System.Collections.Generic;
using System.Reflection;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Providers.MongoDB
{
    /// <summary>This class provides a SQLite-specific implementation of the SQLParser class.</summary>
    public class MongoParser: Parser, IParser
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the function used to add milliseconds to a timestamp.</summary>
        public static string AddToTimestampFunc
        {
            get; set;
        } = null;


        /// <summary>Gets or sets the function used to get the current timestamp.</summary>
        public static string DualTable
        {
            get; set;
        } = null;


        /// <summary>Gets or sets the function used to convert a string to lower case.</summary>
        public static string LowerFunc
        {
            get; set;
        } = null;


        /// <summary>Gets or sets the function used to convert a string to upper case.</summary>
        public static string UpperFunc
        {
            get; set;
        } = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a SQL data type for a database data type expression.</summary>
        /// <param name="dbDataType">Database data type string.</param>
        /// <returns>SQL data type.</returns>
        public SQLDataType ToSQLDataType(string dbDataType)
        {
            return SQLDataType.TEXT;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLParser                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parses a source string.</summary>
        /// <param name="source">Source string.</param>
        /// <returns>Parsed SQL statements.</returns>
        public override ISQLStatement[] Parse(string source)
        {
            List<string> strings = _Parse(source);

            ISQLStatement[] rval = new ISQLStatement[strings.Count];

            for(int i = 0; i < strings.Count; i++)
            {
                rval[i] = new MongoStatement(strings[i]);
            }

            return rval;
        }


        /// <summary>Adds a time interval to a database date/time expression.</summary>
        /// <param name="value">Database expression that represents a date/time value.</param>
        /// <param name="interval">Time interval to add.</param>
        /// <returns>Database timestamp operation string.</returns>
        public override string AddTimeInterval(string value, TimeSpan interval)
        {
            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLParser                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a well-formatted query function call for the current timestamp.</summary>
        /// <returns>Timestamp query string.</returns>
        public override string CurrentTimestampFunction
        {
            get { return null; }
        }


        /// <summary>Gets the database concatination operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string ConcatOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the database table as operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string AsOperator
        {
            get { return null; }
        }


        /// <summary>Gets the add operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string AddOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the subtraction operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string SubtractOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the multiplication operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string MultiplyOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the division operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string DivideOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the modulo operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string ModuloOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the negation operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string NegateOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the binary or operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string BinaryOrOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the binary and operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string BinaryAndOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the exclusive or operator.</summary>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support this operator.</exception>
        public override string ExOrOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the not equal operator.</summary>
        /// <remarks>This operator is documented but will not work.</remarks>
        public override string NotEqualOperator
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Gets the specific FROM part for dual selects.</summary>
        public override string FromDual
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Returns a well-formatted query function call for next value from the given database sequence.</summary>
        /// <param name="sequence">Sequence Name.</param>
        /// <returns>Sequence query string.</returns>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support sequences.</exception>
        public override string SequenceNextValue(string sequence)
        {
            throw new NotSupportedException();
        }


        /// <summary>Returns the database lowercase value for a given expression.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Lowercase expression.</returns>
        public override string Lowercase(string expression)
        {
            throw new NotSupportedException();
        }


        /// <summary>Returns the database lowercase value for a given expression.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Lowercase expression.</returns>
        public override string Uppercase(string expression)
        {
            throw new NotSupportedException();
        }


        /// <summary>Returns a database data type name for a data type.</summary>
        /// <param name="dataType">Data type.</param>
        /// <returns>Data type name.</returns>
        public override string ToDataTypeName(SQLDataType dataType)
        {
            throw new NotSupportedException();
        }


        /// <summary>Returns a string representation of an object.</summary>
        /// <param name="obj">Object</param>
        /// <returns>String.</returns>
        /// <param name="style">Data format.</param>
        /// <param name="multiLine">Multi-line flag.</param>
        /// <param name="width">Line width.</param>
        public override string ToString(object obj, SQLComplexDataTypeFormatStyle style, bool multiLine, int width)
        {
            return base.ToString(obj, style, multiLine, width);
        }
    }
}
