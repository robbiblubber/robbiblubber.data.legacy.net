﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle functions folder.</summary>
    public class OraFunctionsFolder: OraProceduresFolder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="schema">Parent schema.</param>
        internal OraFunctionsFolder(OraDatabase database, OraSchema schema): base(database, schema)
        {
            _Name = "Functions";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] OraProceduresFolder                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("SELECT DISTINCT OBJECT_NAME FROM SYS.ALL_PROCEDURES WHERE OWNER = '" + Schema.Name + "' AND OBJECT_TYPE = 'FUNCTION' ORDER BY OBJECT_NAME");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new OraFunction(Database, this, re.GetString(0)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX02419", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
