﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra roles folder.</summary>
    public class CasRolesFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="db">Database.</param>
        internal CasRolesFolder(CasDatabase db)
        {
            _Parent = db;
            _Name = "Roles";
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="role">Role.</param>
        internal CasRolesFolder(CasRole role)
        {
            _Parent = role;
            _Name = "Grants";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public CasDatabase Database
        { 
            get
            {
                if(_Parent is CasDatabase) { return (CasDatabase) _Parent; ; }
                
                return ((CasRole) _Parent).Database;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "cas::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("SELECT ROLE, MEMBER_OF, CAN_LOGIN, IS_SUPERUSER FROM System_Auth.ROLES");

                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    if(_Parent is CasRole)
                    {
                        if(!(((CasRole) _Parent).MemberOf.Contains(re.GetString(0)))) continue;
                    }

                    ICollection<string> mem = (re.IsDBNull(1) ? new HashSet<string>() : (ICollection<string>) re.GetValue(1));
                    _Children.Add(new CasRole(this, re.GetString(0), mem, re.GetBoolean(2), re.GetBoolean(3)));
                }

                _Children.Sort((a, b) => a.Name.CompareTo(b.Name));
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0010b", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
