﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle columns folder.</summary>
    public class OraColumnsFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="table">Parent table.</param>
        internal OraColumnsFolder(OraDatabase database, OraTable table)
        {
            Database = database;
            _Parent = Table = table;
            _Name = "Columns";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public OraDatabase Database { get; private set; }


        /// <summary>Gets the parent table.</summary>
        public OraTable Table { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "ora::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            List<string> pk = new List<string>();
            List<string> fk = new List<string>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("SELECT X.CONSTRAINT_TYPE, C.COLUMN_NAME FROM SYS.ALL_CONS_COLUMNS C, SYS.ALL_CONSTRAINTS X WHERE C.CONSTRAINT_NAME = X.CONSTRAINT_NAME AND X.CONSTRAINT_TYPE IN ('P', 'R') AND X.OWNER = '" + Table.Folder.Schema.Name + "' AND X.TABLE_NAME = '" + Table.Name + "' AND C.TABLE_NAME = '" + Table.Name + "'");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    if(re.GetString(0) == "P")
                    {
                        pk.Add(re.GetString(1));
                    }
                    else if(re.GetString(0) == "R")
                    {
                        fk.Add(re.GetString(1));
                    }
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0240b", ex); }

            Disposal.Dispose(re, cmd);

            _Children = new List<DbItem>();

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DATA_PRECISION, DATA_SCALE, NULLABLE FROM SYS.ALL_TAB_COLUMNS WHERE OWNER = '" + Table.Folder.Schema.Name + "' AND TABLE_NAME = '" + Table.Name + "' ORDER BY COLUMN_ID";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    string dataType = (re.GetString(1));
                    if(re.IsDBNull(3))
                    {
                        if(!re.IsDBNull(2)) { dataType += ("(" +  re.GetInt32(2).ToString() + ")"); }
                    }
                    else
                    {
                        dataType += ("(" + (re.IsDBNull(4) ? re.GetInt32(3).ToString() : re.GetInt32(3).ToString() + "," + re.GetInt32(4).ToString()) + ")");
                    }
                    _Children.Add(new OraColumn(Database, this, re.GetString(0), dataType, pk.Contains(re.GetString(0)), fk.Contains(re.GetString(0)), (re.GetString(5) == "NO")));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0240a", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
