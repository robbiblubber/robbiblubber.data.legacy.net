﻿namespace Robbiblubber.Data.Interpreters
{
    partial class FormConnect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConnect));
            this._PanelSettings = new System.Windows.Forms.Panel();
            this._ComboProvider = new Robbiblubber.Util.Controls.RichComboBox();
            this._IlistConnect = new System.Windows.Forms.ImageList(this.components);
            this._ButtonOK = new System.Windows.Forms.Button();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _PanelSettings
            // 
            this._PanelSettings.Location = new System.Drawing.Point(12, 48);
            this._PanelSettings.Name = "_PanelSettings";
            this._PanelSettings.Size = new System.Drawing.Size(463, 338);
            this._PanelSettings.TabIndex = 1;
            // 
            // _ComboProvider
            // 
            this._ComboProvider.AutoSize = true;
            this._ComboProvider.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._ComboProvider.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ComboProvider.ImageList = this._IlistConnect;
            this._ComboProvider.Location = new System.Drawing.Point(12, 12);
            this._ComboProvider.Name = "_ComboProvider";
            this._ComboProvider.SelectedIndex = -1;
            this._ComboProvider.SelectedItem = null;
            this._ComboProvider.Size = new System.Drawing.Size(463, 25);
            this._ComboProvider.TabIndex = 0;
            this._ComboProvider.SelectedIndexChanged += new System.EventHandler(this._ComboProvider_SelectedIndexChanged);
            // 
            // _IlistConnect
            // 
            this._IlistConnect.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this._IlistConnect.ImageSize = new System.Drawing.Size(16, 16);
            this._IlistConnect.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(175, 392);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(147, 29);
            this._ButtonOK.TabIndex = 2;
            this._ButtonOK.Tag = "sqlfwx::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(328, 392);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(147, 29);
            this._ButtonCancel.TabIndex = 3;
            this._ButtonCancel.Tag = "sqlfwx;;common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            // 
            // FormConnect
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(488, 433);
            this.Controls.Add(this._PanelSettings);
            this.Controls.Add(this._ComboProvider);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._ButtonCancel);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormConnect";
            this.Tag = "sqlfwx::udiag.connect.caption";
            this.Text = "Connect to Database";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel _PanelSettings;
        private Robbiblubber.Util.Controls.RichComboBox _ComboProvider;
        private System.Windows.Forms.Button _ButtonOK;
        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.ImageList _IlistConnect;
    }
}