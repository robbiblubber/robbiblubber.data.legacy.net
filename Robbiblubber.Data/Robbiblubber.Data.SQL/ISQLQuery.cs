﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Data.SQL
{
    /// <summary>Classes that build SQL queries implement this interface.</summary>
    public interface ISQLQuery
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the SQL text.</summary>
        string Text { get; }
        

        /// <summary>Gets or sets the parameters for the query.</summary>
        List<Tuple<string, object>> Parameters { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a copy of this SQL query.</summary>
        /// <returns>Cloned SQL query.</returns>
        ISQLQuery Clone();


        /// <summary>Adds a primary query table to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <returns>SQL query.</returns>
        ISQLQuery From(ISQLTable tab);


        /// <summary>Adds selection fields to the query.</summary>
        /// <param name="field">Field.</param>
        /// <returns>SQL query.</returns>
        ISQLQuery Select(params ISQLColumn[] field);


        /// <summary>Makes the query distinct.</summary>
        /// <returns>SQL query.</returns>
        ISQLQuery Distinct();


        /// <summary>Adds a LEFT OUTER JOIN to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <param name="cond">Join conditions.</param>
        /// <returns>SQL query.</returns>
        ISQLQuery LeftOuterJoin(ISQLTable tab, params string[] cond);


        /// <summary>Adds a RIGHT OUTER JOIN to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <param name="cond">Join conditions.</param>
        /// <returns>SQL query.</returns>
        ISQLQuery RightOuterJoin(ISQLTable tab, params string[] cond);


        /// <summary>Adds a FULL OUTER JOIN to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <param name="cond">Join conditions.</param>
        /// <returns>SQL query.</returns>
        ISQLQuery FullOuterJoin(ISQLTable tab, params string[] cond);


        /// <summary>Adds an INNER JOIN to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <param name="cond">Join conditions.</param>
        /// <returns>SQL query.</returns>
        ISQLQuery Join(ISQLTable tab, params string[] cond);


        /// <summary>Adds a WHERE condition to the query.</summary>
        /// <param name="cond">Where conditions.</param>
        /// <returns>SQL query.</returns>
        ISQLQuery Where(params string[] cond);


        /// <summary>Adds an AND where condition to the query.</summary>
        /// <param name="cond">Where conditions.</param>
        /// <returns>SQL query.</returns>
        ISQLQuery And(params string[] cond);


        /// <summary>Adds an OR where condition to the query.</summary>
        /// <param name="cond">Where conditions.</param>
        /// <returns>SQL query.</returns>
        ISQLQuery Or(params string[] cond);


        /// <summary>Adds ORDER BY clauses to the query.</summary>
        /// <param name="field">Fields.</param>
        /// <returns>SQL Query.</returns>
        ISQLQuery OrderBy(params ISQLColumn[] field);


        /// <summary>Adds ORDER BY DESC clauses to the query.</summary>
        /// <param name="field">Fields.</param>
        /// <returns>SQL Query.</returns>
        ISQLQuery OrderByDesc(params ISQLColumn[] field);


        /// <summary>Adds GROUP BY clauses to the query.</summary>
        /// <param name="field">Fields.</param>
        /// <returns>SQL Query.</returns>
        ISQLQuery GroupBy(params ISQLColumn[] field);


        /// <summary>Adds HAVING clauses to the query.</summary>
        /// <param name="cond">Having conditions.</param>
        /// <returns>SQL Query.</returns>
        ISQLQuery Having(params string[] cond);


        /// <summary>Adds a parameter to the query.</summary>
        /// <param name="name">Parameter name.</param>
        /// <param name="value">Parameter value.</param>
        /// <returns>SQL query.</returns>
        ISQLQuery AddParameter(string name, object value);


        /// <summary>Adds a UNION statement to the query.</summary>
        /// <param name="query">SQL query.</param>
        /// <returns>SQL query.</returns>
        ISQLQuery Union(ISQLQuery query);


        /// <summary>Adds a UNION ALL statement to the query.</summary>
        /// <param name="query">SQL query.</param>
        /// <returns>SQL query.</returns>
        ISQLQuery UnionAll(ISQLQuery query);
    }
}
