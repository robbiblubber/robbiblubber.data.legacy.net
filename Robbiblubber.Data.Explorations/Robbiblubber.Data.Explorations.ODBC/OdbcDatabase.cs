﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.ODBC
{
    /// <summary>This class represents a PostgreSQL database.</summary>
    public class OdbcDatabase: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Database name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider">Provider.</param>
        internal OdbcDatabase(ProviderItem provider)
        {
            Provider = provider;
            _Name = Ddp.Create(provider.Data).Get<string>("dsn");

            _Children = new List<DbItem>();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider.</summary>
        public ProviderItem Provider { get; private set; }
        
        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.odbcdb; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "odbc::database"; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return _Name + " {" + ((string) Provider.GetData("driver")) + "}"; }
        }
    }
}
