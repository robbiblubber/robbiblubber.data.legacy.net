﻿using System;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Providers.MSSQL
{
    /// <summary>This class provides a parser for MS SQL Server.</summary>
    public class MSSQLParser: Parser, IParser
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a SQL data type for a database data type expression.</summary>
        /// <param name="dbDataType">Database data type string.</param>
        /// <returns>SQL data type.</returns>
        public SQLDataType ToSQLDataType(string dbDataType)
        {
            switch(dbDataType.ToUpper())
            {
                case "TEXT": return SQLDataType.TEXT;
                case "TIMESTAMP": return SQLDataType.TIMESTAMP;
                case "TINYINT": return SQLDataType.INT8;
                case "SMALLINT": return SQLDataType.INT16;
                case "INT": return SQLDataType.INT32;
                case "BIGINT": return SQLDataType.INT64;
                case "BOOLEAN": return SQLDataType.BOOLEAN;
            }

            if(dbDataType.ToUpper().StartsWith("DECIMAL")) { return SQLDataType.DECIMAL; }

            if(dbDataType.ToUpper().StartsWith("FLOAT"))
            {
                string l = dbDataType.ToUpper().Trim("ABCDEFGHIJKLMNOPQRSTUVWXYZ()".ToCharArray());
                if(string.IsNullOrWhiteSpace(l)) { l = "8"; }

                return ((int.Parse(l) > 4) ? SQLDataType.DOUBLE : SQLDataType.FLOAT);
            }

            if(dbDataType.ToUpper().StartsWith("VARC") || dbDataType.ToUpper().StartsWith("CHAR"))
            {
                string l = dbDataType.ToUpper().Trim("ABCDEFGHIJKLMNOPQRSTUVWXYZ()".ToCharArray());
                if(string.IsNullOrWhiteSpace(l)) { return SQLDataType.STRING; }

                return SQLDataType.String(int.Parse(l));
            }

            return SQLDataType.TEXT;
        }


        /// <summary>Gets a well-formatted query function call for last auto increment value.</summary>
        public override string LastValue
        {
            get { return "Scope_Identity()"; }
        }
    }
}
