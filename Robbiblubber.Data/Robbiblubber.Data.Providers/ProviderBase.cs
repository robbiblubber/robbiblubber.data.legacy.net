﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;

using Robbiblubber.Util;
using Robbiblubber.Util.Coding;
using Robbiblubber.Data.DDL;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Providers
{
    /// <summary>This class implements a provider base class.</summary>
    public abstract class ProviderBase: IProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Connection.</summary>
        protected IDbConnection _Connection = null;
        
        /// <summary>SQL parser.</summary>
        protected IParser _Parser = new Parser();
        
        /// <summary>Current transaction.</summary>
        protected IDbTransaction _Transaction = null;

        /// <summary>CreateAdadpter() method.</summary>
        protected MethodInfo _CreateAdapter = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public ProviderBase()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Provider configuration data.</param>
        public ProviderBase(string data)
        {
            if(data.StartsWith("xps5"))
            {
                DataKey = data;
            }
            else { Data = data; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a provider.</summary>
        /// <param name="data">Provider data or name.</param>
        /// <returns>Provider.</returns>
        public static IProvider GetProvider(string data)
        {
            if(data.Substring(4, 4) == "xps5")
            {
                data = GZip.Decompress(data.Substring(8), Base62.Instance);
            }

            IProvider rval = null;

            if(data.Contains("provider=") || data.Contains("provider ="))
            {
                rval = (IProvider) ClassOp.Load(Ddp.Create(data).Get<string>("provider"), data);
            }
            else
            {
                rval = (IProvider) ClassOp.Load(data);
            }

            return rval;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProvider                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider name.</summary>
        public string ProviderName
        {
            get { return GetType().FullName; }
        }


        /// <summary>Gets or sets the provider configuration data.</summary>
        public abstract string Data { get; set; }
        
        
        /// <summary>Gets or sets the provider configuration data key.</summary>        
        public virtual string DataKey 
        { 
            get 
            {
                string s = StringOp.Random(4) + "xps5" + GZip.Compress(Data, Base62.Instance);
                return s; 
            }
            set { Data = GZip.Decompress(Data.Substring(8), Base62.Instance); }
        }


        /// <summary>Gets or sets an enhanced connection string.</summary>
        public string EnhancedString { get; set; }


        /// <summary>Gets the current connection.</summary>
        public IDbConnection Connection
        {
            get { return _Connection; }
        }


        /// <summary>Gets the SQL parser for this provider.</summary>
        public IParser Parser
        {
            get { return _Parser; }
        }


        /// <summary>Gets or sets the current transaction.</summary>
        public IDbTransaction Transaction
        {
            get { return _Transaction; }
            set { _Transaction = value; }
        }


        /// <summary>Connects to the database.</summary>
        public abstract void Connect();


        /// <summary>Disconnects from the database.</summary>
        public virtual void Disconnect()
        {
            _Connection.Close();
            _Connection.Dispose();

            _Connection = null;
        }


        /// <summary>Tests the connection.</summary>
        public virtual void Test()
        {
            IDbConnection keep = _Connection;
            Exception e = null;

            try
            {
                Connect();
                Disconnect();
            }
            catch(Exception ex) { e = ex; }

            _Connection = keep;

            if(e != null) { throw e; }
        }


        /// <summary>Creates a data adapter.</summary>
        /// <param name="sql">SQL statement.</param>
        /// <returns>Data adapter.</returns>
        public virtual IDbDataAdapter CreateAdapter(string sql)
        {
            if(_CreateAdapter == null)
            {
                Type cl = ClassOp.LoadType("Robbiblubber.Data.Interpreters.SQLDataAdapterFactory");
                _CreateAdapter = cl.GetMethod("CreateDataAdapter");
            }
            IDbDataAdapter rval = (IDbDataAdapter) _CreateAdapter.Invoke(null, new object[] { (DbConnection) _Connection });
            rval.SelectCommand = CreateCommand(sql);

            return rval;
        }


        /// <summary>Creates a database command.</summary>
        /// <param name="sql">SQL statement.</param>
        /// <returns>Command object.</returns>
        public virtual IDbCommand CreateCommand(string sql = null)
        {
            IDbCommand rval = _Connection.CreateCommand();
            if(sql != null) { rval.CommandText = sql; }
            if(_Transaction != null) { rval.Transaction = _Transaction; }

            return rval;
        }


        /// <summary>Creates a database command.</summary>
        /// <param name="query">SQL query.</param>
        /// <returns>Command object.</returns>
        public virtual IDbCommand CreateCommand(ISQLQuery query)
        {
            IDbCommand rval = CreateCommand(query.Text);
            rval.AddParameters(query.Parameters);

            return rval;
        }


        /// <summary>Returns the number of fields in a data reader.</summary>
        /// <param name="re">Data reader.</param>
        /// <returns>Number of fields.</returns>
        public virtual int CountFields(IDataReader re)
        {
            return re.FieldCount;
        }


        /// <summary>Returns a data table representing the data reader.</summary>
        /// <param name="re">Reader.</param>
        /// <returns>Data table.</returns>
        public virtual DataTable GetDataTable(IDataReader re)
        {
            DataTable rval = new DataTable();
            DataRow row;
            int fieldCount = CountFields(re);
            for(int i = 0; i < fieldCount; i++) { rval.Columns.Add(re.GetName(i), re.GetFieldType(i)); }
            
            while(re.Read())
            {
                row = rval.NewRow();
                for(int i = 0; i < fieldCount; i++) { row[re.GetName(i)] = re.GetValue(i); }
                rval.Rows.Add(row);
            }

            return rval;
        }


        /// <summary>Returns a data table of string values representing the data reader.</summary>
        /// <param name="re">Reader.</param>
        /// <returns>Data table.</returns>
        public virtual DataTable GetStringDataTable(IDataReader re)
        {
            DataTable rval = new DataTable();
            DataRow row;
            int fieldCount = CountFields(re);
            for(int i = 0; i < fieldCount; i++) { rval.Columns.Add(re.GetName(i), typeof(string)); }

            while(re.Read())
            {
                row = rval.NewRow();
                for(int i = 0; i < fieldCount; i++) { row[re.GetName(i)] = Parser.ToString(re.GetValue(i), Providers.Parser.ComplexDataTypeFormat, false, 0); }
                rval.Rows.Add(row);
            }

            return rval;
        }


        /// <summary>Gets specific provider data.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Data.</returns>
        public virtual object GetData(string key)
        {
            return null;
        }


        /// <summary>Returns the next sequence value of the given database sequence.</summary>
        /// <param name="sequence">Sequence name.</param>
        /// <returns>Next value.</returns>
        public virtual long NextValue(string sequence)
        {
            IDbCommand cmd = Connection.CreateCommand("SELECT " + Parser.SequenceNextValue(sequence) + (" " + Parser.FromDual).Trim());
            long rval = Convert.ToInt64(cmd.ExecuteScalar());
            cmd.Dispose();

            return rval;
        }


        /// <summary>Gets the last auto increment value.</summary>
        /// <returns>Last value.</returns>
        public virtual long LastAutoValue()
        {
            IDbCommand cmd = Connection.CreateCommand("SELECT " + Parser.LastValue + (" " + Parser.FromDual).Trim());
            long rval = Convert.ToInt64(cmd.ExecuteScalar());
            cmd.Dispose();

            return rval;
        }


        /// <summary>Returns the current database time.</summary>
        /// <returns>Current database time.</returns>
        public virtual DateTime CurrentTimestamp
        {
            get
            {
                IDbCommand cmd = Connection.CreateCommand("SELECT " + Parser.CurrentTimestampFunction);
                DateTime rval = ((DateTime) cmd.ExecuteScalar());
                cmd.Dispose();

                return rval;
            }
        }


        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        public abstract IDDLTable GetTable(string tableName);


        /// <summary>Gets the table DDL for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table DDL.</returns>
        public virtual IDDLTable[] GetTables(string filter)
        {
            List<IDDLTable> rval = new List<IDDLTable>();
            foreach(string i in GetTablesNames(filter)) { rval.Add(GetTable(i)); }

            return rval.ToArray();
        }


        /// <summary>Gets the table DDL for all tables.</summary>
        /// <returns>Table DDL.</returns>
        public virtual IDDLTable[] GetTables()
        {
            return GetTables(null);
        }


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        public abstract string[] GetTablesNames(string filter);


        /// <summary>Gets the table names for all tables.</summary>
        /// <returns>Table names.</returns>
        public virtual string[] GetTablesNames()
        {
            return GetTablesNames(null);
        }


        /// <summary>Returns a string representation of this instance.</summary>
        /// <param name="key">Determines if the data key will be returned.</param>
        /// <returns>String.</returns>
        public virtual string ToString(bool key)
        {
            return (key ? DataKey : Data);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return ToString(false);
        }
    }
}
