﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle object tables folder.</summary>
    public class OraObjectTablesFolder: OraTablesFolder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="schema">Parent schema.</param>
        internal OraObjectTablesFolder(OraDatabase database, OraSchema schema): base(database, schema)
        {
            _Name = "Object Tables";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("SELECT TABLE_NAME, TABLE_TYPE FROM SYS.ALL_OBJECT_TABLES WHERE OWNER = '" + Schema.Name + "' ORDER BY TABLE_NAME");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new OraObjectTable(Database, this, re.GetString(0), re.GetString(1)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX02423", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
