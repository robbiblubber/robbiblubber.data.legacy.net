﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server permissions folder.</summary>
    public class MsPermissionsFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="owner">Owner.</param>
        internal MsPermissionsFolder(MsDatabase database, DbItem owner)
        {
            _Parent = Database = database;
            _Name = "sqlfwx::tree.folder.permissions".Localize("Permissions");
            Owner = owner;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; protected set; }


        /// <summary>Gets the owner.</summary>
        public DbItem Owner { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mssql::folder"; }
        }
        

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            IDbCommand cmd = Database.Provider.CreateCommand();
            IDataReader re = null;

            _Children = new List<DbItem>();
            long pid = -1;

            try
            {
                if(Owner is MsUser)
                {
                    cmd.CommandText = "SELECT PRINCIPAL_ID FROM sys.DATABASE_PRINCIPALS WHERE NAME = '" + Owner.Name + "' AND TYPE IN ('C', 'E', 'K', 'S', 'U')";
                    pid = Convert.ToInt64(cmd.ExecuteScalar());
                }
                else if(Owner is MsGroup)
                {
                    cmd.CommandText = "SELECT PRINCIPAL_ID FROM sys.DATABASE_PRINCIPALS WHERE NAME = '" + Owner.Name + "' AND TYPE IN ('G', 'X')";
                    pid = Convert.ToInt64(cmd.ExecuteScalar());
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0120d", ex); }

            Disposal.Dispose(cmd);

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT PERMISSION_NAME FROM sys.DATABASE_PERMISSIONS WHERE MAJOR_ID = 0 AND GRANTEE_PRINCIPAL_ID = " + pid.ToString() + " ORDER BY PERMISSION_NAME";
                re = cmd.ExecuteReader();
                
                while(re.Read())
                {
                    _Children.Add(new MsPermission(Database, this, re.GetString(0)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0120e", ex); }

            Disposal.Dispose(re, cmd);

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT P.PERMISSION_NAME, M.NAME FROM master..SYSOBJECTS M, sys.DATABASE_PERMISSIONS P WHERE M.ID = P.MAJOR_ID AND P.GRANTEE_PRINCIPAL_ID = " + pid.ToString() + " ORDER BY PERMISSION_NAME";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new MsPermission(Database, this, re.GetString(0), re.GetString(1)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0120f", ex); }

            Disposal.Dispose(re, cmd);

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT P.PERMISSION_NAME, O.NAME FROM sys.OBJECTS O, sys.DATABASE_PERMISSIONS P WHERE O.OBJECT_ID = P.MAJOR_ID AND P.GRANTEE_PRINCIPAL_ID = " + pid.ToString() + " ORDER BY P.PERMISSION_NAME";
                re = cmd.ExecuteReader();
                while(re.Read())
                {
                    _Children.Add(new MsPermission(Database, this, re.GetString(0), re.GetString(1)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX01210", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
