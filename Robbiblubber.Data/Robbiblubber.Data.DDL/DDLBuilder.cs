﻿using Robbiblubber.Data.Providers;
using System;
using System.Collections.Generic;



namespace Robbiblubber.Data.DDL
{
    /// <summary>This class provides a basic implementation of an DDL builder.</summary>
    public class DDLBuilder: IDDLBuilder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>SQL Parser.</summary>
        protected internal IParser _Parser = null;
        
        /// <summary>Statements.</summary>
        protected List<IDDLStatement> _Statements = new List<IDDLStatement>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="p">Parser.</param>
        public DDLBuilder(IParser p)
        {
            _Parser = p;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a DDL object.</summary>
        /// <param name="parser">Parser.</param>
        /// <returns>DDL.</returns>
        public DDLBuilder Create(IParser parser)
        {
            return new DDLBuilder(parser);
        }


        /// <summary>Creates a DDL table.</summary>
        /// <param name="parser">Parser.</param>
        /// <param name="tableName">Table name.</param>
        /// <returns>DDL table.</returns>
        public static DDLTable CreateTable(IParser parser, string tableName)
        {
            return new DDLBuilder(parser).AddTable(tableName);
        }


        /// <summary>Creates a DDL index.</summary>
        /// <param name="parser">Parser.</param>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns></returns>
        public static DDLIndex CreateIndex(IParser parser, string tableName, params string[] columnNames)
        {
            return new DDLBuilder(parser).AddIndex(tableName, columnNames);
        }


        /// <summary>Creates a named DDL index.</summary>
        /// <param name="parser">Parser.</param>
        /// <param name="indexName">Index name.</param>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns></returns>
        public static DDLIndex CreateNamedIndex(IParser parser, string indexName, string tableName, params string[] columnNames)
        {
            return new DDLBuilder(parser).AddNamedIndex(indexName, tableName, columnNames);
        }


        /// <summary>Creates a DDL unique index.</summary>
        /// <param name="parser">Parser.</param>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns></returns>
        public static DDLIndex CreateUniqueIndex(IParser parser, string tableName, params string[] columnNames)
        {
            return new DDLBuilder(parser).AddUniqueIndex(tableName, columnNames);
        }


        /// <summary>Creates a named DDL unique index.</summary>
        /// <param name="parser">Parser.</param>
        /// <param name="indexName">Index name.</param>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns></returns>
        public static DDLIndex CreateNamedUniqueIndex(IParser parser, string indexName, string tableName, params string[] columnNames)
        {
            return new DDLBuilder(parser).AddNamedUniqueIndex(indexName, tableName, columnNames);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a create table statement to the DDL.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>DDL table statement.</returns>
        public virtual DDLTable AddTable(string tableName)
        {
            DDLTable rval = new DDLTable(this, tableName);
            _Statements.Add(rval);

            return rval;
        }


        /// <summary>Adds a create index statement to the DDL.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL index statement.</returns>
        public virtual DDLIndex AddIndex(string tableName, params string[] columnNames)
        {
            DDLIndex rval = new DDLIndex(this, tableName, false, columnNames);
            _Statements.Add(rval);

            return rval;
        }


        /// <summary>Adds a create index statement to the DDL.</summary>
        /// <param name="indexName">Index name.</param>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL index statement.</returns>
        public virtual DDLIndex AddNamedIndex(string indexName, string tableName, params string[] columnNames)
        {
            DDLIndex rval = new DDLIndex(this, indexName, tableName, false, columnNames);
            _Statements.Add(rval);

            return rval;
        }


        /// <summary>Adds a create unique index statement to the DDL.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL index statement.</returns>
        public virtual DDLIndex AddUniqueIndex(string tableName, params string[] columnNames)
        {
            DDLIndex rval = new DDLIndex(this, tableName, true, columnNames);
            _Statements.Add(rval);

            return rval;
        }


        /// <summary>Adds a create unique index statement to the DDL.</summary>
        /// <param name="indexName">Index name.</param>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL index statement.</returns>
        public virtual DDLIndex AddNamedUniqueIndex(string indexName, string tableName, params string[] columnNames)
        {
            DDLIndex rval = new DDLIndex(this, indexName, tableName, true, columnNames);
            _Statements.Add(rval);

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDDLStatement                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a statements list for this DDL.</summary>
        public virtual string[] Text 
        {
            get { return _Parser.ParseDDL(this); }
        }


        /// <summary>Gets a list of child DDL statements.</summary>
        IDDLStatement[] IDDLStatement.ChildStatements 
        { 
            get { return _Statements.ToArray(); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDDL                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a create table statement to the DDL.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>DDL table statement.</returns>
        IDDLTable IDDLBuilder.AddTable(string tableName)
        {
            return AddTable(tableName);
        }


        /// <summary>Adds a create index statement to the DDL.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL index statement.</returns>
        IDDLIndex IDDLBuilder.AddIndex(string tableName, params string[] columnNames)
        {
            return AddIndex(tableName, columnNames);
        }


        /// <summary>Adds a create index statement to the DDL.</summary>
        /// <param name="indexName">Index name.</param>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL index statement.</returns>
        IDDLIndex IDDLBuilder.AddNamedIndex(string indexName, string tableName, params string[] columnNames)
        {
            return AddNamedIndex(indexName, tableName, columnNames);
        }


        /// <summary>Adds a create unique index statement to the DDL.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL index statement.</returns>
        IDDLIndex IDDLBuilder.AddUniqueIndex(string tableName, params string[] columnNames)
        {
            return AddUniqueIndex(tableName, columnNames);
        }


        /// <summary>Adds a create unique index statement to the DDL.</summary>
        /// <param name="indexName">Index name.</param>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL index statement.</returns>
        IDDLIndex IDDLBuilder.AddNamedUniqueIndex(string indexName, string tableName, params string[] columnNames)
        {
            return AddNamedUniqueIndex(indexName, tableName, columnNames);
        }
    }
}
