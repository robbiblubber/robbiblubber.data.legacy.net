﻿namespace Robbiblubber.Data.Interpreters.MongoDB
{
    partial class MongoProviderControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._LinkEnhaced = new Robbiblubber.Util.Controls.LinkControl();
            this._LabelPassword = new System.Windows.Forms.Label();
            this._TextPassword = new System.Windows.Forms.TextBox();
            this._LabelUserID = new System.Windows.Forms.Label();
            this._TextUserID = new System.Windows.Forms.TextBox();
            this._LabelWriteConcern = new System.Windows.Forms.Label();
            this._TextWriteConcern = new System.Windows.Forms.TextBox();
            this._LabelName = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._LabelCluster = new System.Windows.Forms.Label();
            this._ButtonTest = new System.Windows.Forms.Button();
            this._TextCluster = new System.Windows.Forms.TextBox();
            this._CheckRetry = new System.Windows.Forms.CheckBox();
            this._LabelDatabase = new System.Windows.Forms.Label();
            this._TextDatabase = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // _LinkEnhaced
            // 
            this._LinkEnhaced.AutoSize = true;
            this._LinkEnhaced.Cursor = System.Windows.Forms.Cursors.Hand;
            this._LinkEnhaced.DisabledColor = System.Drawing.Color.Gray;
            this._LinkEnhaced.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LinkEnhaced.LinkText = "Enhanced...";
            this._LinkEnhaced.Location = new System.Drawing.Point(30, 287);
            this._LinkEnhaced.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LinkEnhaced.Name = "_LinkEnhaced";
            this._LinkEnhaced.Size = new System.Drawing.Size(72, 21);
            this._LinkEnhaced.TabIndex = 7;
            this._LinkEnhaced.TabStop = false;
            this._LinkEnhaced.Tag = "sqlfwx::common.link.enhanced";
            this._LinkEnhaced.Underline = false;
            this._LinkEnhaced.Click += new System.EventHandler(this._LinkEnhaced_Click);
            // 
            // _LabelPassword
            // 
            this._LabelPassword.AutoSize = true;
            this._LabelPassword.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelPassword.Location = new System.Drawing.Point(237, 171);
            this._LabelPassword.Name = "_LabelPassword";
            this._LabelPassword.Size = new System.Drawing.Size(59, 13);
            this._LabelPassword.TabIndex = 4;
            this._LabelPassword.Tag = "sqlfwx::udiag.connect.pwd";
            this._LabelPassword.Text = "&Password:";
            // 
            // _TextPassword
            // 
            this._TextPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextPassword.Location = new System.Drawing.Point(240, 187);
            this._TextPassword.Name = "_TextPassword";
            this._TextPassword.PasswordChar = '*';
            this._TextPassword.Size = new System.Drawing.Size(193, 25);
            this._TextPassword.TabIndex = 4;
            this._TextPassword.UseSystemPasswordChar = true;
            this._TextPassword.TextChanged += new System.EventHandler(this._TextPassword_TextChanged);
            // 
            // _LabelUserID
            // 
            this._LabelUserID.AutoSize = true;
            this._LabelUserID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelUserID.Location = new System.Drawing.Point(27, 171);
            this._LabelUserID.Name = "_LabelUserID";
            this._LabelUserID.Size = new System.Drawing.Size(47, 13);
            this._LabelUserID.TabIndex = 3;
            this._LabelUserID.Tag = "sqlfwx::udiag.connect.uid";
            this._LabelUserID.Text = "&User ID:";
            // 
            // _TextUserID
            // 
            this._TextUserID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextUserID.Location = new System.Drawing.Point(30, 187);
            this._TextUserID.Name = "_TextUserID";
            this._TextUserID.Size = new System.Drawing.Size(193, 25);
            this._TextUserID.TabIndex = 3;
            this._TextUserID.TextChanged += new System.EventHandler(this._TextUserID_TextChanged);
            // 
            // _LabelWriteConcern
            // 
            this._LabelWriteConcern.AutoSize = true;
            this._LabelWriteConcern.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelWriteConcern.Location = new System.Drawing.Point(27, 224);
            this._LabelWriteConcern.Name = "_LabelWriteConcern";
            this._LabelWriteConcern.Size = new System.Drawing.Size(84, 13);
            this._LabelWriteConcern.TabIndex = 5;
            this._LabelWriteConcern.Tag = "sqlfwx::udiag.connect.writecencern";
            this._LabelWriteConcern.Text = "&Write Concern:";
            // 
            // _TextWriteConcern
            // 
            this._TextWriteConcern.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextWriteConcern.Location = new System.Drawing.Point(30, 240);
            this._TextWriteConcern.Name = "_TextWriteConcern";
            this._TextWriteConcern.Size = new System.Drawing.Size(193, 25);
            this._TextWriteConcern.TabIndex = 5;
            this._TextWriteConcern.TextChanged += new System.EventHandler(this._TextWriteConcern_TextChanged);
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.Location = new System.Drawing.Point(27, 19);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Tag = "sqlfwx::udiag.connect.name";
            this._LabelName.Text = "&Name:";
            // 
            // _TextName
            // 
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(30, 35);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(403, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._TextName_TextChanged);
            // 
            // _LabelCluster
            // 
            this._LabelCluster.AutoSize = true;
            this._LabelCluster.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelCluster.Location = new System.Drawing.Point(27, 69);
            this._LabelCluster.Name = "_LabelCluster";
            this._LabelCluster.Size = new System.Drawing.Size(46, 13);
            this._LabelCluster.TabIndex = 1;
            this._LabelCluster.Tag = "sqlfwx::udiag.connect.cluster";
            this._LabelCluster.Text = "&Cluster:";
            // 
            // _ButtonTest
            // 
            this._ButtonTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonTest.Location = new System.Drawing.Point(286, 284);
            this._ButtonTest.Name = "_ButtonTest";
            this._ButtonTest.Size = new System.Drawing.Size(147, 29);
            this._ButtonTest.TabIndex = 8;
            this._ButtonTest.Tag = "sqlfwx::common.button.test";
            this._ButtonTest.Text = "&Test";
            this._ButtonTest.UseVisualStyleBackColor = true;
            this._ButtonTest.Click += new System.EventHandler(this._ButtonTest_Click);
            // 
            // _TextCluster
            // 
            this._TextCluster.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextCluster.Location = new System.Drawing.Point(30, 85);
            this._TextCluster.Name = "_TextCluster";
            this._TextCluster.Size = new System.Drawing.Size(403, 25);
            this._TextCluster.TabIndex = 1;
            this._TextCluster.TextChanged += new System.EventHandler(this._TextCluster_TextChanged);
            // 
            // _CheckRetry
            // 
            this._CheckRetry.AutoSize = true;
            this._CheckRetry.Checked = true;
            this._CheckRetry.CheckState = System.Windows.Forms.CheckState.Checked;
            this._CheckRetry.Location = new System.Drawing.Point(286, 240);
            this._CheckRetry.Name = "_CheckRetry";
            this._CheckRetry.Size = new System.Drawing.Size(98, 21);
            this._CheckRetry.TabIndex = 6;
            this._CheckRetry.Tag = "sqlfwx::udiag.connect.retry";
            this._CheckRetry.Text = "&Retry Writes";
            this._CheckRetry.UseVisualStyleBackColor = true;
            this._CheckRetry.CheckedChanged += new System.EventHandler(this._CheckWrites_CheckedChanged);
            // 
            // _LabelDatabase
            // 
            this._LabelDatabase.AutoSize = true;
            this._LabelDatabase.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDatabase.Location = new System.Drawing.Point(27, 120);
            this._LabelDatabase.Name = "_LabelDatabase";
            this._LabelDatabase.Size = new System.Drawing.Size(58, 13);
            this._LabelDatabase.TabIndex = 2;
            this._LabelDatabase.Tag = "sqlfwx::udiag.connect.database";
            this._LabelDatabase.Text = "&Database:";
            // 
            // _TextDatabase
            // 
            this._TextDatabase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDatabase.Location = new System.Drawing.Point(30, 136);
            this._TextDatabase.Name = "_TextDatabase";
            this._TextDatabase.Size = new System.Drawing.Size(403, 25);
            this._TextDatabase.TabIndex = 2;
            this._TextDatabase.TextChanged += new System.EventHandler(this._TextDatabase_TextChanged);
            // 
            // MongoProviderControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._LabelDatabase);
            this.Controls.Add(this._TextDatabase);
            this.Controls.Add(this._CheckRetry);
            this.Controls.Add(this._LinkEnhaced);
            this.Controls.Add(this._LabelPassword);
            this.Controls.Add(this._TextPassword);
            this.Controls.Add(this._LabelUserID);
            this.Controls.Add(this._TextUserID);
            this.Controls.Add(this._LabelWriteConcern);
            this.Controls.Add(this._TextWriteConcern);
            this.Controls.Add(this._LabelName);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelCluster);
            this.Controls.Add(this._ButtonTest);
            this.Controls.Add(this._TextCluster);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MongoProviderControl";
            this.Size = new System.Drawing.Size(463, 338);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Robbiblubber.Util.Controls.LinkControl _LinkEnhaced;
        private System.Windows.Forms.Label _LabelPassword;
        private System.Windows.Forms.TextBox _TextPassword;
        private System.Windows.Forms.Label _LabelUserID;
        private System.Windows.Forms.TextBox _TextUserID;
        private System.Windows.Forms.Label _LabelWriteConcern;
        private System.Windows.Forms.TextBox _TextWriteConcern;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.Label _LabelCluster;
        private System.Windows.Forms.Button _ButtonTest;
        private System.Windows.Forms.TextBox _TextCluster;
        private System.Windows.Forms.CheckBox _CheckRetry;
        private System.Windows.Forms.Label _LabelDatabase;
        private System.Windows.Forms.TextBox _TextDatabase;
    }
}
