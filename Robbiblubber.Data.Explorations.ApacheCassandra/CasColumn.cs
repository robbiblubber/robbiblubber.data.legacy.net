﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra column.</summary>
    public class CasColumn: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Table name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="kind">Column kind.</param>
        /// <param name="type">Column type.</param>
        /// <param name="pos">Position.</param>
        internal CasColumn(CasColumnsFolder folder, string name, string kind, string type, int pos)
        {
            _Parent = Folder = folder;
            _Name = name;

            PartitionKey = (kind.ToLower() == "partition_key");
            Clustering = (kind.ToLower() == "clustering");

            DataType = type.ToUpper();
            Position = pos;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent table.</summary>
        public CasTable Table 
        { 
            get { return Folder.Table; }
        }


        /// <summary>Gets the parent folder.</summary>
        public CasColumnsFolder Folder { get; protected set; }


        /// <summary>Gets the keyspace.</summary>
        public CasKeyspace Keyspace 
        { 
            get { return Folder.Table.Keyspace; }
        }


        /// <summary>Gets if the column is a partition key.</summary>
        public bool PartitionKey { get; protected set; }


        /// <summary>Gets if the column is clustering.</summary>
        public bool Clustering { get; protected set; }


        /// <summary>Gets the column data type.</summary>
        public string DataType { get; protected set; }


        /// <summary>Gets the column position.</summary>
        public int Position { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected properties                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the sort key.</summary>
        protected internal string _SortKey
        {
            get
            {
                if(PartitionKey) { return "0" + Position.ToString().PadLeft(3, '0') + "" + Name; }
                if(Clustering)   { return "1" + Position.ToString().PadLeft(3, '0') + "" + Name; }
                return "9999" + Name;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                if(PartitionKey) { return Resources.column_pk; }
                if(Clustering) { return Resources.column_clu; }
                return Resources.column;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get 
            {
                if(PartitionKey) { return "cas::column_pk"; }
                if(Clustering)   { return "cas::column_clu"; }
                return "cas::column";
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return (Name + "   " + DataType + (PartitionKey ? "   PRIMARY KEY" : (Clustering ? "   CLUSTERING" : "")) + ";"); }
        }
    }
}
