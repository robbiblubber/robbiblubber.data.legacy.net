﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class allows to control the settings pages.</summary>
    public static class Settings
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Settings pages.</summary>
        private static List<SettingsPage> _Pages = new List<SettingsPage>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a page to the settings window.</summary>
        /// <param name="key">Page key.</param>
        /// <param name="title">Page title.</param>
        /// <param name="control">Control class.</param>
        public static void AddPage(string key, string title, Type control)
        {
            _Pages.Add(new SettingsPage(key, title, control));
        }


        /// <summary>Shows the settings dialog.</summary>
        /// <returns>Dialog result.</returns>
        public static ISettingsWindow CreateDialog()
        {
            FormSettings rval = new FormSettings();

            foreach(SettingsPage i in _Pages)
            {
                rval.AddPage(i);
            }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] SettingsPage                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        internal class SettingsPage
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                     //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="key">Key.</param>
            /// <param name="title">Title.</param>
            /// <param name="control">Control class.</param>
            internal SettingsPage(string key, string title, Type control)
            {
                Key = key;
                Title = title;
                Control = control;
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // internal properties                                                                                              //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets or sets the page key.</summary>
            internal string Key { get; set; }


            /// <summary>Gets or sets the page title.</summary>
            internal string Title { get; set; }


            /// <summary>Gets or sets the control class.</summary>
            internal Type Control { get; set; }
        }
    }
}
