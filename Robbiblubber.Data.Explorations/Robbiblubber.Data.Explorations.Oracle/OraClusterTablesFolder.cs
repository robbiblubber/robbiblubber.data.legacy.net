﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle tables folder.</summary>
    public class OraClusterTablesFolder: OraTablesFolder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="cluster">Parent cluster.</param>
        internal OraClusterTablesFolder(OraDatabase database, OraCluster cluster): base(database, cluster.Folder.Schema)
        {
            _Parent = Cluster = cluster;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent cluster.</summary>
        public OraCluster Cluster { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("SELECT DISTINCT TABLE_NAME FROM SYS.DBA_CLU_COLUMNS WHERE OWNER = '" + Schema.Name + "' AND CLUSTER_NAME = '" + Cluster.Name + "' ORDER BY TABLE_NAME");
                re = cmd.ExecuteReader();

                _Children = new List<DbItem>();
                while(re.Read())
                {
                    _Children.Add(new OraTable(Database, this, re.GetString(0)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX02408", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
