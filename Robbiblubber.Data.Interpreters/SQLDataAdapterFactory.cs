﻿using System;
using System.Data.Common;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class creates standard data adapters.</summary>
    public static class SQLDataAdapterFactory
    {
        /// <summary>Creates a data adapter for a connection.</summary>
        /// <param name="connection">Connection.</param>
        public static DbDataAdapter CreateDataAdapter(DbConnection connection)
        {
            return DbProviderFactories.GetFactory(connection).CreateDataAdapter();
        }
    }
}
