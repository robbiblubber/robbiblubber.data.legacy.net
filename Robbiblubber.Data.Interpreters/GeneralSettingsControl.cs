﻿using System;
using System.Windows.Forms;

using Robbiblubber.Data.Interpreters.History;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class implements the general settings control.</summary>
    public partial class GeneralSettingsControl: UserControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public GeneralSettingsControl()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets if the "Create history" checkbox is checked.</summary>
        public bool HistoryEnabled
        {
            get { return _CheckCreateHistory.Checked; }
            set { _CheckCreateHistory.Checked = value; }
        }


        /// <summary>Gets or sets the selected "Purge history" settings.</summary>
        public SQLHistory.DuplicateStrategy PurgeHistoryDuplicates
        {
            get
            {
                if(_OptionHistoryAlwaysPurge.Checked) { return SQLHistory.DuplicateStrategy.ALWAYS_PURGE; }
                if(_OptionHistoryNeverPurge.Checked) { return SQLHistory.DuplicateStrategy.NEVER_PURGE; }

                return SQLHistory.DuplicateStrategy.PURGE_CONNECTION;
            }
            set
            {
                switch(value)
                {
                    case SQLHistory.DuplicateStrategy.ALWAYS_PURGE:
                        _OptionHistoryAlwaysPurge.Checked = true;
                        break;
                    case SQLHistory.DuplicateStrategy.NEVER_PURGE:
                        _OptionHistoryNeverPurge.Checked = true;
                        break;
                    default:
                        _OptionHistoryPurgeConnection.Checked = true;
                        break;
                }
            }
        }


        /// <summary>Gets or sets if the "Use transactions" checkbox is checked.</summary>
        public bool UseTransactions
        {
            get { return _CheckUseTransactions.Checked; }
            set { _CheckUseTransactions.Checked = value; }
        }


        /// <summary>Gets or sets the selected transaction behavior.</summary>
        public SQLInterpreter.TransactionOption TransactionBehavior
        {
            get
            {
                if(_OptionTransactionsEnabled.Checked) { return SQLInterpreter.TransactionOption.ENABLED; }
                if(_OptionTransactionsDisabled.Checked) { return SQLInterpreter.TransactionOption.DISABLED; }

                return SQLInterpreter.TransactionOption.DEFAULT;
            }
            set
            {
                switch(value)
                {
                    case SQLInterpreter.TransactionOption.ENABLED:
                        _OptionTransactionsEnabled.Checked = true;
                        break;
                    case SQLInterpreter.TransactionOption.DISABLED:
                        _OptionTransactionsDisabled.Checked = true;
                        break;
                    default:
                        _OptionTransactionsDefault.Checked = true;
                        break;
                }
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Initializes the control with current settings.</summary>
        /// <param name="interpreter">SQL interpreter.</param>
        public void InitSettings(SQLInterpreter interpreter)
        {
            HistoryEnabled = SQLHistory.HistoryEnabled;
            PurgeHistoryDuplicates = SQLHistory.PurgeDuplicates;

            UseTransactions = interpreter.UseTransactions;
            TransactionBehavior = interpreter.TransactionBehavior;
            _OptionTransactionsDefault.Enabled = _OptionTransactionsEnabled.Enabled = _OptionTransactionsDisabled.Enabled = (interpreter.Target.Provider != null);
        }


        /// <summary>Applies the control settings to the current environment.</summary>
        /// <param name="interpreter">SQL interpreter.</param>
        public void ApplySettings(SQLInterpreter interpreter)
        {
            SQLHistory.HistoryEnabled = HistoryEnabled;
            SQLHistory.PurgeDuplicates = PurgeHistoryDuplicates;

            interpreter.UseTransactions = UseTransactions;
            if(_OptionTransactionsDefault.Enabled) { interpreter.TransactionBehavior = TransactionBehavior; }
        }
    }
}
