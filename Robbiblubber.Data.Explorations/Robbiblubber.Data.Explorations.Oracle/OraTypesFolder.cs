﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle types folder.</summary>
    public class OraTypesFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="schema">Parent schema.</param>
        internal OraTypesFolder(OraDatabase database, OraSchema schema)
        {
            Database = database;
            Table = null;
            _Parent = Schema = schema;
            _Name = "Types";
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="table">Parent object table.</param>
        internal OraTypesFolder(OraDatabase database, OraObjectTable table): this(database, table.Folder.Schema)
        {
            Table = table;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public OraDatabase Database { get; protected set; }


        /// <summary>Gets the parent schema.</summary>
        public OraSchema Schema { get; protected set; }


        /// <summary>Gets the parent object table.</summary>
        public OraObjectTable Table { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "ora::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            if(Table == null)
            {
                IDbCommand cmd = null;
                IDataReader re = null;

                try
                {
                    cmd = Database.Provider.CreateCommand("SELECT TYPE_NAME FROM SYS.ALL_TYPES WHERE OWNER = '" + Schema.Name + "' ORDER BY TYPE_NAME");
                    re = cmd.ExecuteReader();

                    while(re.Read())
                    {
                        _Children.Add(new OraType(Database, this, re.GetString(0)));
                    }
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX0243c", ex); }

                Disposal.Dispose(re, cmd);
            }
            else
            {
                _Children.Add(new OraType(Database, this, Table.TypeName));
            }
        }
    }
}
