﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra role.</summary>
    public class CasRole: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Table name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="folder">Folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="memberOf">Granted roles.</param>
        /// <param name="canLogin">Can login flag.</param>
        /// <param name="isSuper">Superuser flag.</param>
        internal CasRole(CasRolesFolder folder, string name, ICollection<string> memberOf, bool canLogin, bool isSuper)
        {
            _Parent = Folder = folder;
            _Name = name;
            MemberOf = memberOf;
            CanLogin = canLogin;
            IsSuperuser = isSuper;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent folder.</summary>
        public CasRolesFolder Folder { get; protected set; }


        /// <summary>Gets the database instance.</summary>
        public CasDatabase Database
        {
            get { return Folder.Database; }
        }


        /// <summary>Gets the granted roles.</summary>
        public ICollection<string> MemberOf { get; protected set; }


        /// <summary>Gets if the role can log in.</summary>
        public bool CanLogin { get; protected set; }


        /// <summary>Gets if the role can log in.</summary>
        public bool IsSuperuser { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get 
            { 
                if((_Parent.Parent is CasRole) || (!CanLogin))
                {
                    return (IsSuperuser ? Resources.role_super : Resources.role);
                }

                return (IsSuperuser ? Resources.user_super : Resources.user);
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                if((_Parent.Parent is CasRole) || (!CanLogin))
                {
                    return (IsSuperuser ? "cas::role_super" : "cas::role");
                }

                return (IsSuperuser ? "cas::user_super" : "cas::user");
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return (_Parent.Parent is CasDatabase); }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get 
            {
                if(_Parent.Parent is CasDatabase)
                {
                    if(_Children == null)
                    {
                        _Children = new List<DbItem>();
                        _Children.Add(new CasRolesFolder(this));
                    }

                    return _Children;
                }

                return null;
            }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = ("CREATE ROLE " + Name);

                if(CanLogin || IsSuperuser)
                {
                    rval += "\r\nWITH";

                    if(CanLogin) { rval += " LOGIN = true"; }
                    if(IsSuperuser)
                    {
                        if(CanLogin) { rval += " AND"; }
                        rval += "SUPERUSER = true";
                    }
                }
                rval += ";";

                return rval;
            }
        }
    }
}
