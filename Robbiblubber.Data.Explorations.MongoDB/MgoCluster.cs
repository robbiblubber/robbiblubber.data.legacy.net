﻿using System;
using System.Collections.Generic;
using System.Drawing;

using MongoDB.Bson;
using MongoDB.Driver;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MongoDB
{
    /// <summary>This class represents a MongoDB cluster.</summary>
    public class MgoCluster: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Database name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider">Provider.</param>
        internal MgoCluster(ProviderItem provider)
        {
            Provider = provider;
            _Name = provider.Name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider.</summary>
        public ProviderItem Provider { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.cluster; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mgo::cluster"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IAsyncCursor<BsonDocument> cur = Provider.GetClient().ListDatabases();

            while(cur.MoveNext())
            {
                foreach(BsonDocument i in cur.Current) { _Children.Add(new MgoDatabase(this, i)); }
            }
            cur.Dispose();
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return null; }
        }


        /// <summary>Refreshes the item.</summary>
        public override void Refresh()
        {
            base.Refresh();

            if(Exploration.Instance[Provider] is MgoExplorer)
            {
                ((MgoExplorer) Exploration.Instance[Provider]).__RefreshDefaultKeywords();
            }
        }
    }
}
