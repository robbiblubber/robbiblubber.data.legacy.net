﻿using System;
using System.Drawing;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations
{
    /// <summary>This class provides a generic implemntation of DbItem.</summary>
    public sealed class GenericItem: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item name.</summary>
        private string _Name;

        /// <summary>Icon key..</summary>
        private string _IconKey;

        /// <summary>Icon.</summary>
        private Image _Icon;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="name">Item name.</param>
        /// <param name="icon">Icon.</param>
        /// <param name="iconKey">Icon key.</param>
        public GenericItem(string name, Image icon, string iconKey)
        {
            _Name = name;
            _Icon = icon;
            _IconKey = iconKey;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Get the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return _Icon; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return _IconKey; }
        }
    }
}
