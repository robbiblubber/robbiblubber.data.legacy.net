CREATE TABLE HISTORY
(
   TSTAMP      CHAR(14)     NOT NULL,
   SQLH        VARCHAR(24)  NOT NULL,
   FAVORITE    INTEGER      NOT NULL,
   CONNECTION  VARCHAR(96)  NOT NULL,
   SQL         TEXT   
);

CREATE INDEX IX_HISTORY_TSTAMP ON HISTORY(TSTAMP, SQLH);
CREATE INDEX IX_HISTORY_CONNECTION ON HISTORY(CONNECTION);
CREATE INDEX IX_HISTORY_FAVORITE ON HISTORY(FAVORITE);


CREATE TABLE FAVORITES
(
   SQLH        VARCHAR(24)  NOT NULL
);


CREATE TABLE CONNECTIONS
(
	CONNECTION	VARCHAR(96)	NOT NULL
);
