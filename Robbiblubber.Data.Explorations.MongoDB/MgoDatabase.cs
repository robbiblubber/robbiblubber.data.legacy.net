﻿using System;
using System.Drawing;
using System.Collections.Generic;

using Robbiblubber.Data.Interpreters;

using MongoDB.Bson;
using MongoDB.Bson.IO;

namespace Robbiblubber.Data.Explorations.MongoDB
{
    /// <summary>This class represents a MongoDB database.</summary>
    public class MgoDatabase: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>BSON data.</summary>
        private BsonDocument _Data;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="cluster">Parent cluster.</param>
        /// <param name="data">BSON data.</param>
        internal MgoDatabase(MgoCluster cluster, BsonDocument data)
        {
            _Parent = Cluster = cluster;
            _Data = data;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent cluster.</summary>
        public MgoCluster Cluster { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Data["name"].AsString; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.database; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mgo::database"; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return _Data.Pretty(); }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            _Children.Add(new MgoCollectionsFolder(this, false));
            _Children.Add(new MgoCollectionsFolder(this, true));
        }
    }
}
