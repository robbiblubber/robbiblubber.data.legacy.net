﻿using System;

using Robbiblubber.Util.Controls;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>Database explorers implement this interface.</summary>
    public interface IExporer: IDisposable, IAutoCompletionSource
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the items root for this explorer.</summary>
        DbItem[] Items { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Initializes auto completion.</summary>
        /// <param name="a">AutoCompletion instance.</param>
        void InitAutoCompletion(AutoCompletion a);


        /// <summary>Initializes custom menus.</summary>
        /// <param name="menu">Menu provider.</param>
        void InitMenus(IMenuTargetWindow menu);
    }
}
