﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.PostgreSQL
{
    /// <summary>This class represents a PostgreSQL columns folder.</summary>
    public class PgColumnsFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="table">Parent table.</param>
        internal PgColumnsFolder(PgDatabase database, PgTable table)
        {
            Database = database;
            _Parent = Table = table;
            _Name = "sqlfwx::tree.folder.columns".Localize("Columns");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public PgDatabase Database { get; private set; }


        /// <summary>Gets the parent table.</summary>
        public PgTable Table { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "pg::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            List<string> pk = new List<string>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT U.COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS C, INFORMATION_SCHEMA.KEY_COLUMN_USAGE U WHERE C.TABLE_NAME = U.TABLE_NAME AND C.TABLE_SCHEMA = U.TABLE_SCHEMA AND C.TABLE_SCHEMA = '" + Table.Folder.Schema.Name + "' AND C.TABLE_NAME = '" + Table.Name + "' AND C.CONSTRAINT_TYPE = 'PRIMARY KEY' AND U.POSITION_IN_UNIQUE_CONSTRAINT IS NULL";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    pk.Add(re.GetString(0));
                }
            }
            catch(Exception) {}

            Disposal.Dispose(re, cmd);

            List<string> fk = new List<string>();

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT U.COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS C, INFORMATION_SCHEMA.KEY_COLUMN_USAGE U WHERE C.TABLE_NAME = U.TABLE_NAME AND C.TABLE_SCHEMA = U.TABLE_SCHEMA AND C.TABLE_SCHEMA = '" + Table.Folder.Schema.Name + "' AND C.TABLE_NAME = '" + Table.Name + "' AND C.CONSTRAINT_TYPE = 'FOREIGN KEY' AND U.POSITION_IN_UNIQUE_CONSTRAINT IS NOT NULL";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    fk.Add(re.GetString(0));
                }
            }
            catch(Exception) {}

            Disposal.Dispose(re, cmd);

            _Children = new List<DbItem>();

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT COLUMN_NAME, IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" + Table.Folder.Schema.Name + "' AND TABLE_NAME = '" + Table.Name + "' ORDER BY ORDINAL_POSITION";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    string dataType = (re.GetString(2) + ((re.IsDBNull(3) ? "" : ("(" + re.GetInt32(3) + ")"))));
                    _Children.Add(new PgColumn(Database, this, re.GetString(0), dataType, pk.Contains(re.GetString(0)), fk.Contains(re.GetString(0)), (re.GetString(1) == "NO")));
                }
            }
            catch(Exception) {}

            Disposal.Dispose(re, cmd);
        }
    }
}
