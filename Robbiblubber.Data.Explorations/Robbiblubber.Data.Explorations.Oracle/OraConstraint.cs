﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle constraint.</summary>
    public class OraConstraint: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Constraint name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="type">Constraint type.</param>
        internal OraConstraint(OraDatabase database, OraConstraintsFolder folder, string name, string type)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
            ConstraintType = type;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public OraDatabase Database { get; protected set; }


        /// <summary>Gets the parent folder.</summary>
        public OraConstraintsFolder Folder { get; protected set; }


        /// <summary>Gets the constraint type.</summary>
        public string ConstraintType { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                switch(ConstraintType)
                {
                    case "P": return Resources.constraint_pk;
                    case "U": return Resources.constraint_unique;
                    case "R": return Resources.constraint_fk;
                    case "O": return Resources.constraint_readonly;
                    default: return Resources.constraint_check;
                }
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                switch(ConstraintType)
                {
                    case "P": return "ora::constraint_pk";
                    case "U": return "ora::constraint_unique";
                    case "R": return "ora::constraint_fk";
                    case "O": return "ora::constraint_readonly";
                    default: return "ora::constraint_check";
                }
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                try
                {
                    IDbCommand cmd = Database.Provider.CreateCommand("SELECT DBMS_MetaData.Get_DDL('" + ((ConstraintType == "R") ? "REF_CONSTRAINT" : "CONSTRAINT") + "', '" + Name +"', '" + Folder.Table.Folder.Schema.Name + "') FROM SYS.DUAL");
                    string rval = (string) cmd.ExecuteScalar();
                    Disposal.Dispose(cmd);

                    return rval.Trim(' ', '\r', '\n', '\t', ';') + ";";
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX0240c", ex); }

                return "DDL not accessible.";
            }
        }
    }
}
