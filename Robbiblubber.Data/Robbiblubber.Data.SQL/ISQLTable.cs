﻿using System;



namespace Robbiblubber.Data.SQL
{
    /// <summary>Classes representing tables in a SQL query implement this interface.</summary>
    public interface ISQLTable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the table name.</summary>
        string TableName { get; }


        /// <summary>Gets the table alias.</summary>
        string TableAlias { get; }
    }
}
