﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server triggers folder.</summary>
    public class MsTriggersFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="table">Parent table.</param>
        internal MsTriggersFolder(MsDatabase database, MsTable table)
        {
            Database = database;
            _Parent = Table = table;
            _Name = "Triggers";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; private set; }


        /// <summary>Gets the parent table.</summary>
        public MsTable Table { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mssql::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            string t = ((Table is MsView) ? "sys.VIEWS" : "sys.TABLES");

            IDbCommand cmd = null;
            IDataReader re = null;
            
            _Children = new List<DbItem>();

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT G.NAME, S.NAME, T.NAME FROM sys.TRIGGERS G, " + t + " T, sys.SCHEMAS S WHERE G.PARENT_ID = T.OBJECT_ID AND T.SCHEMA_ID = S.SCHEMA_ID AND S.NAME = '" + Table.Folder.Schema.Name + "' AND T.NAME = '" + Table.Name + "' ORDER BY G.NAME";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new MsTrigger(Database, this, re.GetString(0)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX01221", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
