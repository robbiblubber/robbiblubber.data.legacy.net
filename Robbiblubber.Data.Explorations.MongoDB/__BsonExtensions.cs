﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Bson.IO;

namespace Robbiblubber.Data.Explorations.MongoDB
{
    /// <summary>This class provides extension methods for BSON documents.</summary>
    internal static class __BsonExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Formats a BSON document.</summary>
        /// <param name="bson">BSON docuemnt.</param>
        /// <returns>Formatted output.</returns>
        public static string Pretty(this BsonDocument bson)
        {
            JsonWriterSettings settings = new JsonWriterSettings();
            settings.Indent = true;

            return bson.ToJson(settings);
        }


        /// <summary>Formats a JSON document.</summary>
        /// <param name="json">JSON docuemnt.</param>
        /// <returns>Formatted output.</returns>
        public static string Pretty(this string json)
        {
            return BsonDocument.Parse(json).Pretty();
        }
    }
}
