﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;
using MongoDB.Driver;
using MongoDB.Bson;
using Robbiblubber.Data.Explorations.MongoDB;

namespace Robbiblubber.Data.Explorations.MongoDB
{
    /// <summary>This class represents a MongoDB collections folder.</summary>
    public class MgoCollectionsFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;
        
        /// <summary>Parent collection name.</summary>
        protected string _Collection = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="isViews">Determines if the folder is a views folder.</param>
        internal MgoCollectionsFolder(MgoDatabase database, bool isViews)
        {
            _Parent = database;
            IsViews = isViews;
            _Name = (isViews ? "Views" : "Collections");
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="collection">Collection.</param>
        internal MgoCollectionsFolder(MgoCollection collection)
        {
            _Parent = collection.Database;
            _Collection = collection.Name;
            IsViews = true;
            _Name = "Views";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MgoDatabase Database
        {
            get { return (MgoDatabase) _Parent; }
        }


        /// <summary>Gets if the folder is a views folder</summary>
        public bool IsViews
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mgo::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IAsyncCursor<BsonDocument> cur = Database.Cluster.Provider.GetClient().GetDatabase(Database.Name).ListCollections();

            while(cur.MoveNext())
            {
                foreach(BsonDocument i in cur.Current) 
                {
                    if(i["options"].AsBsonDocument.Contains("viewOn"))
                    {
                        if(!IsViews) continue;
                        if((_Collection != null) && (i["options"].AsBsonDocument["viewOn"].AsString != _Collection)) continue;
                    }
                    else { if(IsViews) continue; }

                    _Children.Add(new MgoCollection(this, i)); 
                }
            }
            cur.Dispose();
        }
    }
}
