﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Util.Controls;



namespace Robbiblubber.Data.Interpreters.History
{
    /// <summary>This class provides an auto completion source for the SQL history.</summary>
    public class SQLHistoryAutoCompletionSource: IAutoCompletionSource
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Favorits flag.</summary>
        private bool _OnlyFavorites = false;

        /// <summary>Initialized flag.</summary>
        private bool _Initialized = false;

        /// <summary>Provider.</summary>
        private ProviderItem _Provider = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="target">Target.</param>
        public SQLHistoryAutoCompletionSource(IHistoryTargetWindow target)
        {
            Target = target;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the target window.</summary>
        public IHistoryTargetWindow Target
        {
            get; private set;
        }


        /// <summary>Gets or sets the </summary>
        public bool OnlyFavorites
        {
            get { return _OnlyFavorites; }
            set
            {
                if(_OnlyFavorites != value)
                {
                    _OnlyFavorites = value;
                    _Initialized = false;
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IAutoCompletionSource                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        /// <summary>Updates the words for a given preceding text.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Word list.</returns>
        public bool UpdateRequired(string pretext, char key)
        {
            if(Target != null)
            {
                if(Target.Provider != _Provider)
                {
                    _Provider = Target.Provider;
                    _Initialized = false;
                }
            }

            return (!_Initialized);
        }


        /// <summary>Returns if the words need to be updated.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Returns TRUE if an update is required, otherwise returns FALSE.</returns>
        public IEnumerable<IAutoCompletionKeyword> UpdateWords(string pretext, char key)
        {
            return SQLHistory._GetAutoCompletion(Target.Provider, _OnlyFavorites);
        }
    }
}
