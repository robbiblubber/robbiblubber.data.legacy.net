﻿using System;



namespace Robbiblubber.Data.SQL
{
    /// <summary>Classes representing columns in a SQL query implement this interface.</summary>
    public interface ISQLColumn
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the table alias.</summary>
        string TableAlias { get; }


        /// <summary>Gets the column name.</summary>
        string ColumnName { get; }
        

        /// <summary>Gets the column alias.</summary>
        string ColumnAlias { get; }


        /// <summary>Gets the column expression as required in a where clause.</summary>
        string ColumnExpression { get; }
    }
}
