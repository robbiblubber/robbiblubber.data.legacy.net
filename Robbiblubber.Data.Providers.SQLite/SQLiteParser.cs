﻿using System;
using System.Collections.Generic;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Providers.SQLite
{
    /// <summary>This class provides a SQLite-specific implementation of the SQLParser class.</summary>
    public class SQLiteParser: Parser, IParser
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Returns a SQL data type for a database data type expression.</summary>
        /// <param name="dbDataType">Database data type string.</param>
        /// <returns>SQL data type.</returns>
        public SQLDataType ToSQLDataType(string dbDataType)
        {
            switch(dbDataType.ToUpper())
            {
                case "TEXT": return SQLDataType.TEXT;
                case "TIMESTAMP": return SQLDataType.TIMESTAMP;
                case "TINYINT": return SQLDataType.INT8;
                case "SMALLINT": return SQLDataType.INT16;
                case "INT":
                case "INTEGER": return SQLDataType.INT32;
                case "BIGINT": return SQLDataType.INT64;
                case "BOOLEAN": return SQLDataType.BOOLEAN;
                case "REAL": return SQLDataType.FLOAT;
                case "DOUBLE PRECISION": return SQLDataType.DOUBLE;
            }

            if(dbDataType.ToUpper().StartsWith("NUMERIC") || dbDataType.ToUpper().StartsWith("NUMBER") || dbDataType.ToUpper().StartsWith("NUMBER"))
            {
                return SQLDataType.DECIMAL;
            }

            if(dbDataType.ToUpper().StartsWith("VARC") || dbDataType.ToUpper().StartsWith("CHAR"))
            {
                string l = dbDataType.ToUpper().Trim("ABCDEFGHIJKLMNOPQRSTUVWXYZ()".ToCharArray());
                if(string.IsNullOrWhiteSpace(l)) { return SQLDataType.STRING; }

                return SQLDataType.String(int.Parse(l));
            }

            return SQLDataType.TEXT;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLParser                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parses a source string.</summary>
        /// <param name="source">Source string.</param>
        /// <returns>Parsed SQL statements.</returns>
        public override ISQLStatement[] Parse(string source)
        {
            List<string> strings = _Parse(source);

            ISQLStatement[] rval = new ISQLStatement[strings.Count];

            for(int i = 0; i < strings.Count; i++)
            {
                rval[i] = new SQLiteStatement(strings[i]);
            }

            return rval;
        }


        /// <summary>Gets a well-formatted query function call for last auto increment value.</summary>
        public override string LastValue
        {
            get { return "Last_Insert_RowID()"; }
        }


        /// <summary>Adds a time interval to a database date/time expression.</summary>
        /// <param name="value">Database expression that represents a date/time value.</param>
        /// <param name="interval">Time interval to add.</param>
        /// <returns>Database timestamp operation string.</returns>
        public override string AddTimeInterval(string value, TimeSpan interval)
        {
            string sign = ((interval < TimeSpan.Zero) ? "-" : "+");
            if(interval.Seconds == 0)
            {
                return "DateTime(" + value + ", '" + sign + Math.Abs(interval.TotalMinutes).ToString() + " minutes')";
            }
            else
            {
                return "DateTime(" + value + ", '" + sign + Math.Abs(interval.TotalSeconds).ToString() + " seconds')";
            }
        }
    }
}
