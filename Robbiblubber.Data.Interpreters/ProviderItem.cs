﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util;
using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Data.Providers;
using Robbiblubber.Data.DDL;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This implements a common provider item.</summary>
    public class ProviderItem: IProvider, ISortable, IAutoCompletionSource
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Base provider.</summary>
        protected IProvider _Base = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="baseProviderName">Base provider name.</param>
        /// <param name="id">ID.</param>
        /// <param name="parent">Parent folder.</param>
        public ProviderItem(string baseProviderName, string id, Folder parent): this(ProviderBase.GetProvider(baseProviderName), id, parent)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="baseProvider">Base provider.</param>
        /// <param name="id">ID.</param>
        /// <param name="parent">Parent folder.</param>
        public ProviderItem(IProvider baseProvider, string id, Folder parent)
        {
            _Base = baseProvider;

            if(id == null)
            {
                ID = StringOp.Unique(24);
            }
            else { ID = id; }

            if(parent == null) return;
            

            if(parent.Items.Count() == 0)
            {
                SortIndex = 0;
            }
            else
            {
                SortIndex = (parent.Items.Max(m => m.SortIndex) + 1);
            }

            Parent = parent;
            
            Parent.Providers.Add(this);
        }



        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="sec">Configuration data.</param>
        /// <param name="parent">Parent folder.</param>
        public ProviderItem(DdpElement sec, Folder parent)
        {
            ID = sec.Name.Substring(10);
            Name = sec.Get<string>("name");
            SortIndex = sec.Get<int>("sort");

            string data = sec.Get<string>("data");
            _Base = ProviderBase.GetProvider(sec.Get<string>("data"));

            Parent = parent;
            if(Parent != null) { Parent.Providers.Add(this); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the provider manager.</summary>
        public virtual IProviderManager Manager
        {
            get
            {
                return ProviderManager.GetManager(ProviderName);
            }
        }


        /// <summary>Gets the items in this database.</summary>
        public virtual DbItem[] Items
        {
            get
            {
                try
                {
                    return Exploration.Instance[this].Items;
                }
                catch(Exception) {}

                return null;
            }
        }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves data to configuration file.</summary>
        /// <param name="sec">Section.</param>
        public void Save(DdpElement sec)
        {
            sec.Set("name", Name);
            sec.Set("data", Data);
            sec.Set("parent", Parent.ID);
            sec.Set("sort", SortIndex);
        }
        

        /// <summary>Initializes auto completion.</summary>
        /// <param name="a">AutoCompletion instance.</param>
        public virtual void InitAutoCompletion(AutoCompletion a)
        {
            if(Exploration.Instance[this] != null) { Exploration.Instance[this].InitAutoCompletion(a); }
        }


        /// <summary>Initializes custom menus.</summary>
        /// <param name="menu">Menu provider.</param>
        public virtual void InitMenus(IMenuTargetWindow menu)
        {
            if(Exploration.Instance[this] != null) { Exploration.Instance[this].InitMenus(menu); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProvider                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider name.</summary>
        public virtual string ProviderName
        {
            get
            {
                if(_Base == null) { return "sqlfwx::prov.unknown".Localize("Unknown"); }
                return _Base.ProviderName;
            }
        }


        /// <summary>Gets or sets the provider configuration data.</summary>
        public virtual string Data
        {
            get
            {
                if(_Base == null) { return ""; }
                return _Base.Data;
            }
            set
            {
                if(_Base != null) { _Base.Data = value; }
            }
        }
        
        
        /// <summary>Gets or sets the provider configuration data key.</summary>        
        public virtual string DataKey
        {
            get
            {
                if(_Base == null) { return ""; }
                return _Base.DataKey;
            }
            set
            {
                if(_Base != null) { _Base.DataKey = value; }
            }
        }


        /// <summary>Gets or sets an enhanced connection string.</summary>
        public virtual string EnhancedString
        {
            get
            {
                if(_Base == null) { return ""; }
                return _Base.EnhancedString;
            }
            set
            {
                if(_Base != null) { _Base.EnhancedString = value; }
            }
        }


        /// <summary>Gets the current connection.</summary>
        public virtual IDbConnection Connection
        {
            get
            {
                if(_Base == null) { return null; }
                return _Base.Connection;
            }
        }


        /// <summary>Gets the SQL parser for this provider.</summary>
        public virtual IParser Parser
        {
            get
            {
                if(_Base == null) { return null; }
                return _Base.Parser;
            }
        }


        /// <summary>Gets or sets the current transaction.</summary>
        public virtual IDbTransaction Transaction
        {
            get { return _Base.Transaction; }
            set { _Base.Transaction = value; }
        }
        

        /// <summary>Connects to the database.</summary>
        public virtual void Connect()
        {
            if(_Base != null) { _Base.Connect(); }
        }


        /// <summary>Disconnects from the database.</summary>
        public virtual void Disconnect()
        {
            if(_Base != null) { _Base.Disconnect(); }
        }


        /// <summary>Tests the connection.</summary>
        public virtual void Test()
        {
            if(_Base == null) throw new InvalidOperationException("No provider.");
            _Base.Test();
        }


        /// <summary>Returns the number of fields in a data reader.</summary>
        /// <param name="re">Data reader.</param>
        /// <returns>Number of fields.</returns>
        public virtual int CountFields(IDataReader re)
        {
            if(_Base == null) { return 0; }
            return _Base.CountFields(re);
        }


        /// <summary>Returns a data table representing the data reader.</summary>
        /// <param name="re">Reader.</param>
        /// <returns>Data table.</returns>
        public virtual DataTable GetDataTable(IDataReader re)
        {
            if(_Base == null) { return null; }
            return _Base.GetDataTable(re);
        }


        /// <summary>Returns a data table of string values representing the data reader.</summary>
        /// <param name="re">Reader.</param>
        /// <returns>Data table.</returns>
        public virtual DataTable GetStringDataTable(IDataReader re)
        {
            if(_Base == null) { return null; }
            return _Base.GetStringDataTable(re);
        }


        /// <summary>Creates a data adapter.</summary>
        /// <param name="sql">SQL statement.</param>
        /// <returns>Data adapter.</returns>
        public virtual IDbDataAdapter CreateAdapter(string sql)
        {
            if(_Base == null) { return null; }
            return _Base.CreateAdapter(sql);
        }


        /// <summary>Creates a database command.</summary>
        /// <param name="sql">SQL statement.</param>
        /// <returns>Command object.</returns>
        public virtual IDbCommand CreateCommand(string sql = null)
        {
            if(_Base == null) { return null; }
            return _Base.CreateCommand(sql);
        }


        /// <summary>Creates a database command.</summary>
        /// <param name="query">SQL query.</param>
        /// <returns>Command object.</returns>
        public virtual IDbCommand CreateCommand(ISQLQuery query)
        {
            if(_Base == null) { return null; }
            return _Base.CreateCommand(query);
        }


        /// <summary>Gets specific provider data.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Data.</returns>
        public virtual object GetData(string key)
        {
            if(_Base == null) { return null; }
            return _Base.GetData(key);
        }


        /// <summary>Returns the next sequence value of the given database sequence.</summary>
        /// <param name="sequence">Sequence name.</param>
        /// <returns>Next value.</returns>
        public virtual long NextValue(string sequence)
        {
            if(_Base == null) return -1;
            return _Base.NextValue(sequence);
        }


        /// <summary>Gets the last auto increment value.</summary>
        /// <returns>Last value.</returns>
        public virtual long LastAutoValue()
        {
            if(_Base == null) return -1;
            return _Base.LastAutoValue();
        }


        /// <summary>Returns the current database time.</summary>
        /// <returns>Current database time.</returns>
        public virtual DateTime CurrentTimestamp
        {
            get
            {
                if(_Base == null) return DateTime.Now;
                return _Base.CurrentTimestamp;
            }
        }


        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        public virtual IDDLTable GetTable(string tableName)
        {
            if(_Base == null) return null;
            return _Base.GetTable(tableName);
        }


        /// <summary>Gets the table DDL for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table DDL.</returns>
        public virtual IDDLTable[] GetTables(string filter)
        {
            if(_Base == null) return new IDDLTable[0];
            return _Base.GetTables(filter);
        }


        /// <summary>Gets the table DDL for all tables.</summary>
        /// <returns>Table DDL.</returns>
        public virtual IDDLTable[] GetTables()
        {
            if(_Base == null) return new IDDLTable[0];
            return _Base.GetTables();
        }


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        public virtual string[] GetTablesNames(string filter)
        {
            if(_Base == null) return new string[0];
            return _Base.GetTablesNames(filter);
        }


        /// <summary>Gets the table names for all tables.</summary>
        /// <returns>Table names.</returns>
        public virtual string[] GetTablesNames()
        {
            if(_Base == null) return new string[0];
            return _Base.GetTablesNames();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ISortable                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider ID.</summary>
        public string ID { get; set; }


        /// <summary>Gets or sets the provider name.</summary>
        public string Name { get; set; }


        /// <summary>Gets or sets the parent folder.</summary>
        public Folder Parent { get; set; }


        /// <summary>Gets or sets the sort index.</summary>
        public int SortIndex { get; set; }


        /// <summary>Gets if the object can move up.</summary>
        public bool CanMoveUp
        {
            get
            {
                if(Parent == null) { return false; }
                return Parent.__CanMoveUp(this);
            }
        }


        /// <summary>Gets if the object can move down.</summary>
        public bool CanMoveDown
        {
            get
            {
                if(Parent == null) { return false; }
                return Parent.__CanMoveDown(this);
            }
        }


        /// <summary>Moves the item up.</summary>
        public void MoveUp()
        {
            if(Parent != null) { Parent.__MoveUp(this); }
        }


        /// <summary>Moves the item down.</summary>
        public void MoveDown()
        {
            if(Parent != null) { Parent.__MoveDown(this); }
        }


        /// <summary>Exports the item.</summary>
        /// <param name="cfg">Configuration file.</param>
        public void Export(Ddp cfg)
        {
            Save(cfg["provider::" + ID]);
        }


        /// <summary>Returns a string representation of this instance.</summary>
        /// <param name="key">Determines if the data key will be returned.</param>
        /// <returns>String.</returns>
        public string ToString(bool key)
        {
            return ((_Base == null) ? null : _Base.ToString(key));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IAutoCompletionSource                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets keywords for auto completion.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Key word list.</returns>
        IEnumerable<IAutoCompletionKeyword> IAutoCompletionSource.UpdateWords(string pretext, char key)
        {
            if(Exploration.Instance[this] != null) { return Exploration.Instance[this].UpdateWords(pretext, key); }
            return new IAutoCompletionKeyword[0];
        }


        /// <summary>Returns if the words need to be updated.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Returns TRUE if an update is required, otherwise returns FALSE.</returns>
        bool IAutoCompletionSource.UpdateRequired(string pretext, char key)
        {
            if(Exploration.Instance[this] != null) { return Exploration.Instance[this].UpdateRequired(pretext, key); }
            return false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return ToString(false);
        }
    }
}
