﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Interpreters.MSAccess
{
    /// <summary>This class implements the MS Access provider control.</summary>
    public partial class MSAccessProviderControl: UserControl, IProviderControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parent provider.</summary>
        private ProviderItem _Provider;


        /// <summary>Tree node.</summary>
        private TreeNode _Node = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MSAccessProviderControl()
        {
            InitializeComponent();
            Node = null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>File name changed.</summary>
        private void _TextFile_TextChanged(object sender, EventArgs e)
        {
            _Provider.Data = Ddp.Create(_Provider.Data).Set("file", _TextFile.Text).ToString(true);
        }


        /// <summary>Provider name changed.</summary>
        private void _TextName_TextChanged(object sender, EventArgs e)
        {
            _Provider.Name = _TextName.Text;

            if(Node != null) { Node.Text = _Provider.Name; }
        }


        /// <summary>Button "Test" click.</summary>
        private void _ButtonTest_Click(object sender, EventArgs e)
        {
            try
            {
                _Provider.Test();

                MessageBox.Show("sqlfwx::udiag.test.success".Localize("The connection has been tested successfully."), "sqlfwx::udiag.test.caption".Localize("Test"), MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show("sqlfwx::udiag.test.fail".Localize("The test has failed.") + ' ' + ex.Message, "sqlfwx::udiag.test.caption".Localize("Test"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        /// <summary>Button "Browse" click.</summary>
        private void _ButtonBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.Filter = "sqlfwx::udiag.filter.access".Localize("Access Databases") + " (*.accdb, *.mdb)|*.accdb;*.mdb|" + "sqlfwx::udiag.filter.all".Localize("All files") + "|*.*";

            if(d.ShowDialog() == DialogResult.OK)
            {
                _TextFile.Text = d.FileName;
            }
        }


        /// <summary>Link "Enhanced" click.</summary>
        private void _LinkEnhaced_Click(object sender, EventArgs e)
        {
            FormConnectionString f = new FormConnectionString(Provider.EnhancedString);

            if(f.ShowDialog() == DialogResult.OK)
            {
                Provider.EnhancedString = f.ConnectionString;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProviderControl                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the database provider.</summary>
        public ProviderItem Provider
        {
            get { return _Provider; }
            set
            {
                _Provider = value;

                _TextFile.Text = Ddp.Create(_Provider.Data).Get<string>("file");
                _TextName.Text = Provider.Name;
            }
        }


        /// <summary>Gets or sets the tree node.</summary>
        public TreeNode Node
        {
            get { return _Node; }
            set { _Node = value; }
        }
    }
}
