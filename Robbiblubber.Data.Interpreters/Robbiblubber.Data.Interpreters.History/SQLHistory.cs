﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Data.SQLite;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Util.Coding;
using Robbiblubber.Util.Controls;



namespace Robbiblubber.Data.Interpreters.History
{
    /// <summary>Thic class implements the SQL history.</summary>
    public static class SQLHistory
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static constants                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>SQL history file.</summary>
        private static readonly string _HISTORY_FILE = PathOp.SystemUserConfigurationPath + @"\robbiblubber.org\SQL\sql.history";

        /// <summary>Configuration file.</summary>
        private static readonly string _CONFIG_FILE = PathOp.SystemUserConfigurationPath + @"\robbiblubber.org\SQL\db.connections";
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Connection list.</summary>
        internal static List<Connection> _Connections = null;
        
        /// <summary>Connection.</summary>
        private static SQLiteConnection _Database = null;
        
        /// <summary>History window.</summary>
        internal static FormHistory _HistoryWindow = null;
        
        /// <summary>Determines if the SQL history is enabled.</summary>
        private static bool _Enabled = true;
        
        /// <summary>Determines if the SQL history allows duplicates.</summary>
        public static DuplicateStrategy _Duplicates = DuplicateStrategy.PURGE_CONNECTION;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a history entry for the provider.</summary>
        /// <param name="provider">Provider.</param>
        /// <param name="sql">SQL.</param>
        public static void AddHistory(this ProviderItem provider, string sql)
        {
            if(!_Enabled) return;

            sql = sql.Replace("\r\n", "\n").Replace("\n", "\r\n");
            string hash = SHA1.GetHash(sql, Base64.Instance);
            bool fav = false;

            SQLiteCommand cmd = null;

            try
            {
                if(_Duplicates != DuplicateStrategy.NEVER_PURGE)
                {
                    cmd = Database.CreateCommand();
                    cmd.CommandText = "DELETE FROM HISTORY WHERE SQLH = :h";
                    cmd.Parameters.Add(":h", DbType.String).Value = hash;

                    if(_Duplicates == DuplicateStrategy.PURGE_CONNECTION)
                    {
                        cmd.CommandText += " AND CONNECTION = :c";
                        cmd.Parameters.Add(":c", DbType.String).Value = ((provider.Name == null) ? "Unknown" : provider.Name);
                    }

                    fav = (cmd.ExecuteNonQuery() > 0);
                    Disposal.Dispose(cmd);
                }

                cmd = Database.CreateCommand();
                cmd.CommandText = "INSERT INTO HISTORY (TSTAMP, SQLH, FAVORITE, CONNECTION, SQL) VALUES (:d, :h, 0, :c, :q)";
                cmd.Parameters.Add(":d", DbType.String).Value = DateTime.Now.ToString("yyyyMMddHHmmss");
                cmd.Parameters.Add(":h", DbType.String).Value = hash;
                cmd.Parameters.Add(":c", DbType.String).Value = ((provider.Name == null) ? "Unknown" : provider.Name);
                cmd.Parameters.Add(":q", DbType.String).Value = sql;

                cmd.ExecuteNonQuery();
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0410d", ex); }

            Disposal.Dispose(cmd);

            if(fav)
            {
                try
                {
                    cmd = Database.CreateCommand();
                    cmd.CommandText = "UPDATE HISTORY SET FAVORITE = 1 WHERE SQLH IN (SELECT SQLH FROM FAVORITES)";
                    cmd.ExecuteNonQuery();
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX0410e", ex); }

                Disposal.Dispose(cmd);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets if the SQL history is enabled.</summary>
        public static bool HistoryEnabled
        {
            get { return _Enabled; }
            set { _Enabled = value; }
        }


        /// <summary>Gets or sets if duplicates are allowed.</summary>
        public static DuplicateStrategy PurgeDuplicates
        {
            get { return _Duplicates; }
            set { _Duplicates = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Shows the history browser window.</summary>
        /// <param name="target">Target window.</param>
        /// <param name="fav">Browse only favorites.</param>
        public static void BrowseHistory(IHistoryTargetWindow target, bool fav = false)
        {
            if(_HistoryWindow == null)
            {
                _HistoryWindow = new FormHistory(target);
            }

            _HistoryWindow.ShowDialog(FormHistory.ViewOption.BROWSE, fav);
        }


        /// <summary>Shows the history dump window.</summary>
        /// <param name="target">Target window.</param>
        public static void DumpHistory(IHistoryTargetWindow target)
        {
            if(_HistoryWindow == null)
            {
                _HistoryWindow = new FormHistory(target);
            }

            _HistoryWindow.ShowDialog(FormHistory.ViewOption.DUMP);
        }


        /// <summary>Shows the history clean up window.</summary>
        /// <param name="target">Target window.</param>
        public static void CleanupHistory(IHistoryTargetWindow target)
        {
            if(_HistoryWindow == null)
            {
                _HistoryWindow = new FormHistory(target);
            }

            _HistoryWindow.ShowDialog(FormHistory.ViewOption.CLEANUP);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static properties                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a list of connections from history.</summary>
        internal static List<Connection> Connections
        {
            get
            {
                if(_Connections == null)
                {
                    Dictionary<string, string> installed = new Dictionary<string, string>();
                    Ddp cfg = Ddp.Open(_CONFIG_FILE, GZip.BASE64);

                    foreach(DdpElement i in cfg.Sections.Where(m => m.Name.StartsWith("provider::")))
                    {
                        try
                        {
                            Ddp data = Ddp.Create(i.Get<string>("data"));
                            installed.Add(i.Get<string>("name"), data.Get<string>("provider"));
                        }
                        catch(Exception ex) { DebugOp.Dump("SQLFWX0410f", ex); }
                    }

                    _Connections = new List<Connection>();
                    _Connections.Add(new Connection("*", "All Databases", "*"));
                    _Connections.Add(new Connection("??", "Unknown Databases", "??"));

                    IDbCommand cmd = null;
                    IDataReader re = null;

                    try
                    {
                        cmd = Database.CreateCommand();
                        cmd.CommandText = "SELECT DISTINCT CONNECTION FROM HISTORY ORDER BY CONNECTION";

                        re = cmd.ExecuteReader();
                        while(re.Read())
                        {
                            string img = "?";
                            if(installed.ContainsKey(re.GetString(0))) { img = installed[re.GetString(0)]; }

                            _Connections.Add(new Connection(re.GetString(0), re.GetString(0), img));
                        }
                    }
                    catch(Exception ex) { DebugOp.Dump("SQLFWX04120", ex); }

                    Disposal.Dispose(re, cmd);
                }

                return _Connections;
            }
        }
        

        /// <summary>Gets the history database connection.</summary>
        internal static SQLiteConnection Database
        {
            get
            {
                if(_Database == null)
                {
                    try
                    {
                        if(!File.Exists(_HISTORY_FILE))
                        {
                            File.Copy(PathOp.ApplicationDirectory + @"\template.history", _HISTORY_FILE);
                        }

                        _Database = new SQLiteConnection("Data Source=" + _HISTORY_FILE + ";Version=3;");
                        _Database.Open();
                    }
                    catch(Exception ex)
                    {
                        _Database = null;
                        DebugOp.Dump("SQLFWX04121", ex);
                    }
                }

                return _Database;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets a list of auto complete keywords for the current history.</summary>
        /// <param name="provider">Provider.</param>
        /// <param name="fav">Favorites only flag.</param>
        /// <returns>List of keywords.</returns>
        internal static List<IAutoCompletionKeyword> _GetAutoCompletion(ProviderItem provider, bool fav)
        {
            List<IAutoCompletionKeyword> rval = new List<IAutoCompletionKeyword>();

            IDbCommand cmd = Database.CreateCommand();
            string and = " WHERE ";
            cmd.CommandText = "SELECT DISTINCT SQL FROM (SELECT TSTAMP, SQL FROM HISTORY";

            if(provider != null) 
            { 
                cmd.CommandText += (and + "CONNECTION = :c"); and = " AND ";
                cmd.AddParameter(":c", provider.Name);
            }
            if(fav) { cmd.CommandText += (and + "FAVORITE != 0"); }
            cmd.CommandText += " ORDER BY TSTAMP DESC)";

            IDataReader re = cmd.ExecuteReader();
            while(re.Read())
            {
                rval.Add(new SQLKeywords.KeyWord(re.GetString(0)));
            }
            re.Close();
            Disposal.Dispose(re, cmd);

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] Connection                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Thic class represents a connection.</summary>
        internal class Connection
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="name">Name.</param>
            /// <param name="displayName">Display name.</param>
            /// <param name="iconKey">Icon key.</param>
            internal Connection(string name, string displayName, string iconKey)
            {
                Name = name;
                DisplayName = displayName;
                IconKey = iconKey;
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // internal members                                                                                             //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Connection name.</summary>
            internal string Name;

            /// <summary>Connection display name.</summary>
            internal string DisplayName;

            /// <summary>Connection icon key.</summary>
            internal string IconKey;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] Connection                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This class implements a statement list.</summary>
        public class StatementList: IEnumerable<Statement>
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // private members                                                                                              //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>List items.</summary>
            private List<Statement> _Items = new List<Statement>();



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Creates a new instance of this class.</summary>
            internal StatementList()
            {}



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public properties                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets the statement with the given index.</summary>
            /// <param name="i">Index.</param>
            /// <returns>Statement.</returns>
            public Statement this[int i]
            {
                get { return _Items[i]; }
            }


            /// <summary>Gets the number of items in the list.</summary>
            public int Count
            {
                get { return _Items.Count; }
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public methods                                                                                               //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Deletes an item.</summary>
            /// <param name="i">Index.</param>
            public void Delete(int i)
            {
                Statement del = _Items[i];

                _Items.Remove(del);

                SQLiteCommand cmd = null;

                try
                {
                    cmd = Database.CreateCommand();
                    cmd.CommandText = "DELETE FROM HISTORY WHERE TSTAMP = :d AND SQLH = :h";
                    cmd.Parameters.Add(":d", DbType.String).Value = del.Timestamp;
                    cmd.Parameters.Add(":h", DbType.String).Value = del.Hash;
                    cmd.ExecuteNonQuery();
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX04122", ex); }

                Disposal.Dispose(cmd);
            }


            /// <summary>Favorites an item.</summary>
            /// <param name="i">Index.</param>
            public void Favorite(int i)
            {
                Statement fav = _Items[i];

                fav.Favorite = true;

                SQLiteCommand cmd = null;

                try
                {
                    cmd = Database.CreateCommand();
                    cmd.CommandText = "INSERT INTO FAVORITES (SQLH) VALUES (:h)";
                    cmd.Parameters.Add(":h", DbType.String).Value = fav.Hash;
                    cmd.ExecuteNonQuery();
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX04123", ex); }

                Disposal.Dispose(cmd);

                try
                {
                    cmd = Database.CreateCommand();
                    cmd.CommandText = "UPDATE HISTORY SET FAVORITE = 1 WHERE TSTAMP = :d AND SQLH = :h";
                    cmd.Parameters.Add(":d", DbType.String).Value = fav.Timestamp;
                    cmd.Parameters.Add(":h", DbType.String).Value = fav.Hash;
                    cmd.ExecuteNonQuery();
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX04124", ex); }

                Disposal.Dispose(cmd);
            }


            /// <summary>Unfavorites an item.</summary>
            /// <param name="i">Index.</param>
            /// <param name="remove">Remove from list.</param>
            public void Unfavorite(int i, bool remove = false)
            {
                Statement fav = _Items[i];

                fav.Favorite = false;
                if(remove) { _Items.Remove(fav); }

                SQLiteCommand cmd = null;

                try
                {
                    cmd = Database.CreateCommand();
                    cmd.CommandText = "DELETE FROM FAVORITES WHERE SQLH = :h";
                    cmd.Parameters.Add(":h", DbType.String).Value = fav.Hash;
                    cmd.ExecuteNonQuery();
                    Disposal.Dispose(cmd);
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX04125", ex); }

                try
                {
                    cmd = Database.CreateCommand();
                    cmd.CommandText = "UPDATE HISTORY SET FAVORITE = 0 WHERE TSTAMP = :d AND SQLH = :h";
                    cmd.Parameters.Add(":d", DbType.String).Value = fav.Timestamp;
                    cmd.Parameters.Add(":h", DbType.String).Value = fav.Hash;
                    cmd.ExecuteNonQuery();
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX04126", ex); }

                Disposal.Dispose(cmd);
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // internal methods                                                                                             //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Adds a statement.</summary>
            /// <param name="s">Statement.</param>
            internal void Add(Statement s)
            {
                _Items.Add(s);
            }


            /// <summary>Adds a statement.</summary>
            /// /// <param name="timestamp">Timestamp.</param>
            /// <param name="key">Key.</param>
            /// <param name="fav">Favorite flag.</param>
            /// <param name="sql">SQL.</param>
            internal void Add(string timestamp, string key, int fav, string sql)
            {
                _Items.Add(new Statement(timestamp, key, fav, sql));
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // [override] object                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets a string representation of the list.</summary>
            /// <returns>String.</returns>
            public override string ToString()
            {
                string rval = "";
                bool first = true;

                foreach(Statement i in _Items)
                {
                    if(first)
                    {
                        first = false;
                    }
                    else { rval += "\r\n\r\n"; }

                    rval += i.SQL;
                }

                return rval;
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // [interface] IEnumerable                                                                                      //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Returns an enumerator for this list.</summary>
            /// <returns>Enumerator.</returns>
            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Items.GetEnumerator();
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // [interface] IEnumerable<Statement>                                                                           //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Returns an enumerator for this list.</summary>
            /// <returns>Enumerator.</returns>
            IEnumerator<Statement> IEnumerable<Statement>.GetEnumerator()
            {
                return _Items.GetEnumerator();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] Statement                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Thic class defines a SQL history filter.</summary>
        public class Statement
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Creates a new instance of this class.</summary>
            /// /// <param name="timestamp">Timestamp.</param>
            /// <param name="key">Key.</param>
            /// <param name="fav">Favorite flag.</param>
            /// <param name="sql">SQL.</param>
            internal Statement(string timestamp, string key, int fav, string sql)
            {
                Hash = key;
                Timestamp = timestamp;
                SQL = sql;
                Favorite = (fav != 0);
            }


            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public properties                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets the statement hash.</summary>
            public string Hash { get; internal set; }


            /// <summary>Gets the statement timestamp.</summary>
            public string Timestamp { get; internal set; }


            /// <summary>Gets the statement SQL string.</summary>
            public string SQL { get; internal set; }


            /// <summary>Gets the Favorite state.</summary>
            public bool Favorite { get; internal set; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] Filter                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Thic class defines a SQL history filter.</summary>
        public class Filter
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public properties                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets or sets if the filter has a from date.</summary>
            public bool HasFrom { get; set; }

            
            /// <summary>Gets or sets the from date.</summary>
            public DateTime From { get; set; }


            /// <summary>Gets or sets if the filter has a to date.</summary>
            public bool HasTo { get; set; }


            /// <summary>Gets or sets the to date.</summary>
            public DateTime To { get; set; }


            /// <summary>Gets or sets the database name.</summary>
            public string Database { get; set; }


            /// <summary>Gets or sets the search expression.</summary>
            public string Expression { get; set; }


            /// <summary>Gets or sets if only favorite SQL should be included.</summary>
            public bool Favorite { get; set; }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public methods                                                                                               //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Executes the filter.</summary>
            /// <returns>Statement list.</returns>
            public StatementList Execute()
            {
                StatementList rval = new StatementList();

                SQLiteCommand cmd = null;
                IDataReader re = null;

                try
                {
                    cmd = _BuildCommand("SELECT TSTAMP, SQLH, FAVORITE, SQL FROM HISTORY");
                    re = cmd.ExecuteReader();
                    while(re.Read())
                    {
                        rval.Add(re.GetString(0), re.GetString(1), re.GetInt32(2), re.GetString(3));
                    }
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX04127", ex); }

                Disposal.Dispose(re, cmd);

                return rval;
            }


            /// <summary>Counts the elements in the result list.</summary>
            /// <returns>Number of elements.</returns>
            public int CountElements()
            {
                SQLiteCommand cmd = null;
                object rval = 0;

                try
                {
                    cmd = _BuildCommand("SELECT Count(*) FROM HISTORY");
                    rval = cmd.ExecuteScalar();
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX04128", ex); }

                Disposal.Dispose(cmd);

                return Convert.ToInt32(rval);
            }


            /// <summary>Deletes all elements from the history.</summary>
            public void DeleteAll()
            {
                SQLiteCommand cmd = null;

                try
                {
                    cmd = _BuildCommand("DELETE FROM HISTORY", false);
                    cmd.ExecuteNonQuery();
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX04129", ex); }

                Disposal.Dispose(cmd);
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // private methods                                                                                              //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Builds an SQL command.</summary>
            /// <param name="initial">Initial SQL.</param>
            /// <param name="order">Determines if the statement should include an order by clause.</param>
            /// <returns>SQL command.</returns>
            private SQLiteCommand _BuildCommand(string initial, bool order = true)
            {
                SQLiteCommand cmd = null;

                try
                {
                    cmd = SQLHistory.Database.CreateCommand();

                    cmd.CommandText = initial;
                    string conj = " WHERE";

                    if(Favorite)
                    {
                        cmd.CommandText += (conj + " FAVORITE = 1");
                        conj = " AND";
                    }
                    if(HasFrom)
                    {
                        cmd.CommandText += (conj + " TSTAMP >= :from");
                        cmd.Parameters.Add(":from", DbType.String).Value = (From.ToString("yyyyMMdd") + "000000");
                        conj = " AND";
                    }
                    if(HasTo)
                    {
                        cmd.CommandText += (conj + " TSTAMP <= :to");
                        cmd.Parameters.Add(":to", DbType.String).Value = (To.ToString("yyyyMMdd") + "999999");
                        conj = " AND";
                    }

                    if(!string.IsNullOrWhiteSpace(Database) && (Database.Trim() == "??"))
                    {
                        IDbTransaction t = SQLHistory.Database.BeginTransaction();
                        IDbCommand xmd = SQLHistory.Database.CreateCommand();
                        xmd.CommandText = "DELETE FROM CONNECTIONS";
                        xmd.Transaction = t;
                        xmd.ExecuteNonQuery();
                        xmd.Dispose();

                        Ddp cfg = Ddp.Open(_CONFIG_FILE, GZip.BASE64);

                        foreach(DdpElement i in cfg.Sections.Where(m => m.Name.StartsWith("provider::")))
                        {
                            try
                            {
                                xmd = SQLHistory.Database.CreateCommand();
                                xmd.CommandText = "INSERT INTO CONNECTIONS VALUES (:c)";
                                xmd.AddParameter(":c", i.Get<string>("name"));
                                xmd.Transaction = t;
                                xmd.ExecuteNonQuery();
                                xmd.Dispose();
                            }
                            catch(Exception ex) { DebugOp.Dump("SQLFWX0410f", ex); }
                        }
                        t.Commit();

                        cmd.CommandText += (conj + " CONNECTION NOT IN (SELECT CONNECTION FROM CONNECTIONS)");
                        conj = " AND";
                    }
                    else if((!string.IsNullOrWhiteSpace(Database)) && (Database.Trim() != "*"))
                    {
                        cmd.CommandText += (conj + " CONNECTION = :db");
                        cmd.Parameters.Add(":db", DbType.String).Value = Database;
                        conj = " AND";
                    }

                    if((!string.IsNullOrWhiteSpace(Expression)) && (Expression.Trim() != "*"))
                    {
                        cmd.CommandText += (conj + " Lower(SQL) LIKE :q");

                        string q = Expression.ToLower().Replace("*", "%").Replace("?", "_");
                        if(!q.Contains("%")) { q = ("%" + q + "%"); }

                        cmd.Parameters.Add(":q", DbType.String).Value = q;
                    }

                    if(order) { cmd.CommandText += " ORDER BY TSTAMP DESC"; }
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX0412a", ex); }

                return cmd;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [enum] DuplicateStrategy                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Enumeration of duplicate strategies.</summary>
        public enum DuplicateStrategy: int
        {
            /// <summary>Allow duplicate SQL in history and never purge duplicates.</summary>
            NEVER_PURGE = 0,
            /// <summary>Purge duplicate SQL for a connection.</summary>
            PURGE_CONNECTION = 1,
            /// <summary>Always purge duplicate SQL.</summary>
            ALWAYS_PURGE = 2
        }
    }
}
