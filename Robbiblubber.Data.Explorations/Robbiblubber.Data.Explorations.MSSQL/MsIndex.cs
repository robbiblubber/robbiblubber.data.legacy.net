﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a PostgreSQL index.</summary>
    public class MsIndex: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Index name.</summary>
        private string _Name;


        /// <summary>Descending column list.</summary>
        private List<string> _Desc;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="unique">Unique flag.</param>
        internal MsIndex(MsDatabase database, MsIndexesFolder folder, string name, bool unique)
        {
            Database = database;
            _Parent = Folder = folder;
            IsUnique = unique;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public MsIndexesFolder Folder { get; private set; }


        /// <summary>Gets if the index is unique.</summary>
        public bool IsUnique { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                if(IsUnique) { return Resources.index_unique; }

                return Resources.index;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                if(IsUnique) { return "mssql::index/unique"; }

                return "mssql::index";
            }
        }
        
        
        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string fields = "";

                foreach(MsColumn i in Children)
                {
                    if(fields != "") { fields += ", "; }
                    fields += i.QualifiedName;
                    if(_Desc.Contains(i.Name)) { fields += " DESC"; }
                }
                return "CREATE " + (IsUnique ? "UNIQUE " : "") + "INDEX [" + Name + "] ON " + Folder.Table.QualifiedName + "(" + fields + ");";
            }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            IDbCommand cmd = null;
            IDataReader re = null;

            _Children = new List<DbItem>();
            _Desc = new List<string>();

            List<string> cols = new List<string>();

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT C.NAME, X.IS_DESCENDING_KEY FROM sys.COLUMNS C, sys.TABLES T, sys.SCHEMAS S, sys.INDEXES I, sys.INDEX_COLUMNS X " +
                                  "WHERE C.OBJECT_ID = T.OBJECT_ID AND I.OBJECT_ID = T.OBJECT_ID AND X.OBJECT_ID = T.OBJECT_ID AND X.INDEX_ID = I.INDEX_ID AND T.SCHEMA_ID = S.SCHEMA_ID " +
                                  "AND X.COLUMN_ID = C.COLUMN_ID AND S.NAME = '" + Folder.Table.Folder.Schema.Name + "' AND T.NAME = '" + Folder.Table.Name + "' AND I.NAME = '" + Name + "' ORDER BY X.KEY_ORDINAL";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    if(re.GetBoolean(1)) { _Desc.Add(re.GetString(0)); }
                    cols.Add(re.GetString(0));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0120b", ex); }

            Disposal.Dispose(re, cmd);
            
            foreach(MsColumn i in Folder.Table.Columns.Children)
            {
                if(cols.Contains(i.Name))
                {
                    _Children.Add(i);
                }
            }
        }
    }
}
