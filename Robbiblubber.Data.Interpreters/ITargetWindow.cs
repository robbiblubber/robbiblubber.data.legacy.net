﻿using System;
using System.Threading;
using System.Windows.Forms;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This interface defines a target window.</summary>
    public interface ITargetWindow
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the target view mode.</summary>
        ViewMode View { get; }


        /// <summary>Gets the target database provider.</summary>
        ProviderItem Provider { get; }


        /// <summary>Gets the target data grid.</summary>
        DataGridView Grid { get; }


        /// <summary>Gets the selected item.</summary>
        DbItem SelectedItem { get; }


        /// <summary>Gets the selected node.</summary>
        TreeNode SelectedNode { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes to console.</summary>
        /// <param name="text">Text.</param>
        void Write(string text);


        /// <summary>Writes to grid.</summary>
        /// <param name="text">Text.</param>
        void WriteToGrid(string text);


        /// <summary>Sets grid data source.</summary>
        /// <param name="dataSource">Data source.</param>
        void DataToGrid(object dataSource);


        /// <summary>Shows the wait window.</summary>
        void ShowWait();


        /// <summary>Shows the wait window.</summary>
        /// <param name="text">Text.</param>
        void ShowWait(string text);


        /// <summary>Hides the wait window.</summary>
        void HideWait();


        /// <summary>Refreshes the tree view.</summary>
        /// <param name="n">Tree node.</param>
        void RefreshTree(TreeNode n = null);


        /// <summary>Executes code in separate thread.</summary>
        /// <param name="c">Thread delegate.</param>
        void ExecuteSynchronized(ThreadStart c);


        /// <summary>Executes code in separate thread.</summary>
        /// <param name="c">Thread delegate.</param>
        /// <param name="p">Paramter.</param>
        void ExecuteSynchronized(ParameterizedThreadStart c, object p);
    }
}
