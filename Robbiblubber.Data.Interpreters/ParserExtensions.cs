﻿using System;
using System.Windows.Forms;

using Robbiblubber.Data.Providers;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class parses SQL Text depndeing on the current position in a text box.</summary>
    public static class ParserExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the current statement from an SQL text in a control.</summary>
        /// <param name="parser">Parser.</param>
        /// <param name="tb">Text box.</param>
        /// <returns>Statement.</returns>
        public static string CurrentStatement(this IParser parser, TextBoxBase tb)
        {
            return CurrentStatement(parser, tb.Text, tb.SelectionStart);
        }


        /// <summary>Gets the current statement from an SQL text depending on the position.</summary>
        /// <param name="parser">Parser.</param>
        /// <param name="sql">SQL text.</param>
        /// <param name="pos">Position.</param>
        /// <returns>Statement.</returns>
        public static string CurrentStatement(this IParser parser, string sql, int pos)
        {
            ISQLStatement[] rval = parser.Parse(sql.Substring(sql._StartOfStatement(pos)));

            if(rval.Length > 0) { return rval[0].Source; }
            return "";
        }


        /// <summary>Gets the statements to the current statement from an SQL text in a control.</summary>
        /// <param name="parser">Parser.</param>
        /// <param name="tb">Text box.</param>
        /// <returns>Statement.</returns>
        public static string ParseToCurrent(this IParser parser, TextBoxBase tb)
        {
            return ParseToCurrent(parser, tb.Text, tb.SelectionStart);
        }


        /// <summary>Gets the statements to the current statement from an SQL text depending on the position.</summary>
        /// <param name="parser">Parser.</param>
        /// <param name="sql">SQL text.</param>
        /// <param name="pos">Position.</param>
        /// <returns>Statement.</returns>
        public static string ParseToCurrent(this IParser parser, string sql, int pos)
        {
            return sql.Substring(0, sql._EndOfStatement(pos));
        }


        /// <summary>Gets the statements from the current statement on from an SQL text in a control.</summary>
        /// <param name="parser">Parser.</param>
        /// <param name="tb">Text box.</param>
        /// <returns>Statement.</returns>
        public static string ParseFromCurrent(this IParser parser, TextBoxBase tb)
        {
            return ParseFromCurrent(parser, tb.Text, tb.SelectionStart);
        }


        /// <summary>Gets the statements from the current statement on from an SQL text depending on the position.</summary>
        /// <param name="parser">Parser.</param>
        /// <param name="sql">SQL text.</param>
        /// <param name="pos">Position.</param>
        /// <returns>Statement.</returns>
        public static string ParseFromCurrent(this IParser parser, string sql, int pos)
        {
            return sql.Substring(sql._StartOfStatement(pos));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the start position of the current statement.</summary>
        /// <param name="sql">SQL text.</param>
        /// <param name="pos">Position.</param>
        /// <returns>Position</returns>
        private static int _StartOfStatement(this string sql, int pos)
        {
            bool instr = _InStr(sql, pos);

            for(int i = pos; i >= 0; i--)
            {
                if(sql[i] == '\'') { instr = (!instr); }
                if(instr) continue;

                if(sql[i] == ';') { return (i + 1); }
            }

            return 0;
        }


        /// <summary>Returns if the current position is (probably) within a string.</summary>
        /// <param name="sql">SQL text.</param>
        /// <param name="pos">Position.</param>
        /// <returns>Returns TRUE if the current position is within a string, otherwise returns FALSE.</returns>
        private static bool _InStr(this string sql, int pos)
        {
            int rval = 0;

            for(int i = pos; i >= 0; i--)
            {
                if(sql[i] == '\'') { rval++; }
            }

            return ((rval % 2) == 1);
        }


        /// <summary>Returns the end position of the current statement.</summary>
        /// <param name="sql">SQL text.</param>
        /// <param name="pos"></param>
        /// <returns></returns>
        private static int _EndOfStatement(this string sql, int pos)
        {
            bool instr = false;

            for(int i = pos; i < sql.Length; i++)
            {
                if(sql[i] == '\'') { instr = (!instr); }
                if(instr) continue;

                if(sql[i] == ';') { return i; }
            }

            return (sql.Length - 1);
        }
    }
}
