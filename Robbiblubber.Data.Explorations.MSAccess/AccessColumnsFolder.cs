﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Microsoft.Office.Interop.Access.Dao;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSAccess
{
    /// <summary>This class represents an Access columns folder.</summary>
    public class AccessColumnsFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="table">Parent table.</param>
        internal AccessColumnsFolder(AccessDatabase database, AccessTable table)
        {
            Database = database;
            _Parent = Table = table;
            _Name = "Columns";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public AccessDatabase Database { get; private set; }


        /// <summary>Gets the parent table.</summary>
        public AccessTable Table { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.msacc_folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mdb::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            Fields fields = null;
            List<string> pk = new List<string>();
            List<string> fk = new List<string>();

            if(Table is AccessView)
            {
                fields = Database.AccessApplication.CurrentDb().QueryDefs[Table.Name].Fields;
            }
            else
            {
                TableDef tab = Database.AccessApplication.CurrentDb().TableDefs[Table.Name];
                
                foreach(Microsoft.Office.Interop.Access.Dao.Index i in tab.Indexes)
                {
                    if(i.Primary)
                    {
                        foreach(Field j in (IEnumerable<Field>) i.Fields) { pk.Add(j.Name); }
                    }
                }
                
                foreach(Relation i in Database.AccessApplication.CurrentDb().Relations)
                {
                    if(i.Table == tab.Name)
                    {
                        foreach(Field j in i.Fields) { fk.Add(j.Name); }
                    }
                }

                fields = tab.Fields;
            }

            foreach(Field i in fields)
            {
                string dt = "Unknown";
                
                switch(i.Type)
                {
                    case (short) DataTypeEnum.dbAttachment:
                        dt = "Attachment"; break;
                    case (short) DataTypeEnum.dbBigInt:
                        dt = "Big Integer"; break;
                    case (short) DataTypeEnum.dbBinary:
                        dt = "Binary"; break;
                    case (short) DataTypeEnum.dbBoolean:
                        dt = "Boolean"; break;
                    case (short) DataTypeEnum.dbByte:
                        dt = "Byte"; break;
                    case (short) DataTypeEnum.dbChar:
                        dt = "Char"; break;
                    case (short) DataTypeEnum.dbComplexByte:
                        dt = "Complex Byte"; break;
                    case (short) DataTypeEnum.dbComplexDecimal:
                        dt = "Complex Decimal"; break;
                    case (short) DataTypeEnum.dbComplexDouble:
                        dt = "Complex Double"; break;
                    case (short) DataTypeEnum.dbComplexGUID:
                        dt = "Complex GUID"; break;
                    case (short) DataTypeEnum.dbComplexInteger:
                        dt = "Complex Integer"; break;
                    case (short) DataTypeEnum.dbComplexLong:
                        dt = "Complex Long"; break;
                    case (short) DataTypeEnum.dbComplexSingle:
                        dt = "Complex Single"; break;
                    case (short) DataTypeEnum.dbComplexText:
                        dt = "Complex Text"; break;
                    case (short) DataTypeEnum.dbCurrency:
                        dt = "Currency"; break;
                    case (short) DataTypeEnum.dbDate:
                        dt = "Date"; break;
                    case (short) DataTypeEnum.dbDecimal:
                        dt = "Decimal"; break;
                    case (short) DataTypeEnum.dbDouble:
                        dt = "Double"; break;
                    case (short) DataTypeEnum.dbFloat:
                        dt = "Float"; break;
                    case (short) DataTypeEnum.dbGUID:
                        dt = "GUID"; break;
                    case (short) DataTypeEnum.dbInteger:
                        dt = "Integer"; break;
                    case (short) DataTypeEnum.dbLong:
                        dt = "Long"; break;
                    case (short) DataTypeEnum.dbLongBinary:
                        dt = "Long Binary"; break;
                    case (short) DataTypeEnum.dbMemo:
                        dt = "Memo"; break;
                    case (short) DataTypeEnum.dbNumeric:
                        dt = "Numeric"; break;
                    case (short) DataTypeEnum.dbSingle:
                        dt = "Single"; break;
                    case (short) DataTypeEnum.dbText:
                        dt = "Text"; break;
                    case (short) DataTypeEnum.dbTime:
                        dt = "Time"; break;
                    case (short) DataTypeEnum.dbTimeStamp:
                        dt = "Time Stamp"; break;
                    case (short) DataTypeEnum.dbVarBinary:
                        dt = "VarBinary"; break;
                }
                
                _Children.Add(new AccessColumn(Database, this, i.Name, dt, pk.Contains(i.Name), fk.Contains(i.Name), i.Required));
            }
        }
    }
}
