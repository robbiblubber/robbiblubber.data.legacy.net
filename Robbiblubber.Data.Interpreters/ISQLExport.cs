﻿using System;
using System.Drawing;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>SQL export providers implement this interface.</summary>
    public interface ISQLExport
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the menu text for the export provider.</summary>
        string MenuText { get; }


        /// <summary>Gets the menu icon for the export provider.</summary>
        Image MenuIcon { get; }
        

        /// <summary>Gets the menu sort index for the export provider.</summary>
        int SortIndex { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Exports the SQL results.</summary>
        /// <param name="i">SQL interpreter.</param>
        void Export(SQLInterpreter i);
    }
}
