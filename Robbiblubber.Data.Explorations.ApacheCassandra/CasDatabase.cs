﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents a Cassandra database.</summary>
    public class CasDatabase: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Database name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider">Provider.</param>
        internal CasDatabase(ProviderItem provider)
        {
            Provider = provider;
            _Name = provider.Name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider.</summary>
        public ProviderItem Provider { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.casdb; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "cas::database"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Provider.CreateCommand("SELECT KEYSPACE_NAME, DURABLE_WRITES, REPLICATION FROM System_Schema.KEYSPACES");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new CasKeyspace(this, re.GetString(0), re.GetBoolean(1), (IDictionary<string, string>) re.GetValue(2)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0010b", ex); }

            Disposal.Dispose(re, cmd);

            _Children.Add(new CasRolesFolder(this));
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return null; }
        }


        /// <summary>Refreshes the item.</summary>
        public override void Refresh()
        {
            base.Refresh();

            if(Exploration.Instance[Provider] is CasExplorer)
            {
                ((CasExplorer) Exploration.Instance[Provider]).__RefreshDefaultKeywords();
            }
        }
    }
}
