﻿using System;
using System.Data;
using System.Collections.Generic;

using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Controls;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Explorations.Cassandra
{
    /// <summary>This class represents the Cassandra database structure.</summary>
    public class CasExplorer: IExporer, IDisposable, IAutoCompletionSource
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Provider.</summary>
        protected ProviderItem _Provider = null;

        /// <summary>Default keywords.</summary>
        protected List<IAutoCompletionKeyword> _DefaultKeyWords;

        /// <summary>Returns if the default keywords are currently available for auto completion.</summary>
        protected bool _DefaultKeywordsGiven = false;

        /// <summary>SQLite keywords.</summary>
        protected SQLKeywords _KeyWords = new SQLKeywords(PathOp.ApplicationDirectory + @"\cassandra.keywords");

        /// <summary>Database explorer root.</summary>
        protected CasDatabase _DatabaseExplorer = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider"></param>
        public CasExplorer(ProviderItem provider)
        {
            Items = new DbItem[] { _DatabaseExplorer = new CasDatabase(_Provider = provider) };
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes the default keywords.</summary>
        protected internal void __RefreshDefaultKeywords()
        {
            _DefaultKeyWords = new List<IAutoCompletionKeyword>();

            _DefaultKeyWords.AddRange(_KeyWords);
            
            foreach(string i in _GetColumnNames())
            {
                _DefaultKeyWords.Add(new SQLKeywords.KeyWord(i, "cas::column"));
            }
            foreach(string i in _GetTableNames())
            {
                _DefaultKeyWords.Add(new SQLKeywords.KeyWord(i, "cas::table"));
            }
            foreach(string i in _GetKeyspaceNames())
            {
                _DefaultKeyWords.Add(new SQLKeywords.KeyWord(i, "cas::keyspace"));
            }
            _DefaultKeywordsGiven = false;
        }


        /// <summary>Gets a list of unique column names.</summary>
        /// <returns>String list.</returns>
        protected List<string> _GetColumnNames()
        {
            List<string> rval = new List<string>();

            IDbCommand cmd = _Provider.CreateCommand("SELECT COLUMN_NAME FROM System_Schema.COLUMNS");
            IDataReader re = cmd.ExecuteReader();
            while(re.Read())
            {
                if(!rval.Contains(re.GetString(0))) { rval.Add(re.GetString(0)); }
            }
            Disposal.Dispose(re, cmd);

            return rval;
        }


        /// <summary>Gets a list of unique column names.</summary>
        /// <returns>String list.</returns>
        protected List<string> _GetTableNames()
        {
            List<string> rval = new List<string>();

            IDbCommand cmd = _Provider.CreateCommand("SELECT TABLE_NAME FROM System_Schema.TABLES");
            IDataReader re = cmd.ExecuteReader();
            while(re.Read())
            {
                if(!rval.Contains(re.GetString(0))) { rval.Add(re.GetString(0)); }
            }
            Disposal.Dispose(re, cmd);

            return rval;
        }


        /// <summary>Gets a list of unique column names.</summary>
        /// <returns>String list.</returns>
        protected List<string> _GetKeyspaceNames()
        {
            List<string> rval = new List<string>();

            IDbCommand cmd = _Provider.CreateCommand("SELECT KEYSPACE_NAME FROM System_Schema.KEYSPACES");
            IDataReader re = cmd.ExecuteReader();
            while(re.Read())
            {
                rval.Add(re.GetString(0));
            }
            Disposal.Dispose(re, cmd);

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbExplorer                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the items root for this explorer.</summary>
        public DbItem[] Items
        {
            get; protected set;
        }


        /// <summary>Initializes auto completion.</summary>
        /// <param name="a">AutoCompletion instance.</param>
        public void InitAutoCompletion(AutoCompletion a)
        {
            a.AddImage("cas::column", Resources.column);
            a.AddImage("cas::table", Resources.table);
            a.AddImage("cas::view", Resources.view);

            __RefreshDefaultKeywords();
        }


        /// <summary>Initializes custom menus.</summary>
        /// <param name="menu">Menu provider.</param>
        public void InitMenus(IMenuTargetWindow menu)
        {
            // TODO: implement menu handling.
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IAutoCompletionSource                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Updates the words for a given preceding text.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Word list.</returns>
        public IEnumerable<IAutoCompletionKeyword> UpdateWords(string pretext, char key)
        {
            _DefaultKeywordsGiven = false;
            return _DefaultKeyWords;
        }


        /// <summary>Returns if the words need to be updated.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Returns TRUE if an update is required, otherwise returns FALSE.</returns>
        public bool UpdateRequired(string pretext, char key)
        {
            return (!_DefaultKeywordsGiven);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDisposable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Disposes the object.</summary>
        public void Dispose()
        {}
    }
}
