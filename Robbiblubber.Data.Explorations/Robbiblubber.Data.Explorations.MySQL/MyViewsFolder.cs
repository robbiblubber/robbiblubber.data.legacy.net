﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MySQL
{
    /// <summary>This class represents a MySQL views folder.</summary>
    public class MyViewsFolder: MyTablesFolder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="host">Host.</param>
        /// <param name="database">Parent database.</param>
        internal MyViewsFolder(MyHost host, MyDatabase database): base(host, database)
        {
            _Name = "Views";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] PgTablesFolder                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Host.Provider.CreateCommand();
                cmd.CommandText = "SHOW FULL TABLES IN " + Database.Name + " WHERE TABLE_TYPE LIKE '%VIEW%'";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new MyView(Host, this, re.GetString(0)));
                }
            }
            catch(Exception) {}

            Disposal.Dispose(re, cmd);
        }
    }
}
