﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server role.</summary>
    public class MsRole: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sequence name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        internal MsRole(MsDatabase database, MsRolesFolder folder, string name)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public MsRolesFolder Folder { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                return Resources.role;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                return "mssql::role";
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                return null;
            }
        }
    }
}
