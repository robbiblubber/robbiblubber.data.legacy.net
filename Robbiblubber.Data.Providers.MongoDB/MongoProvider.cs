﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Data.DDL;
using Robbiblubber.Data.Mongo;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Providers.MongoDB
{
    /// <summary>This class implements the MongoDB database provider.</summary>
    public class MongoProvider: ProviderBase, IProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MongoProvider(): base()
        {
            _Parser = new MongoParser();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Provider configuration data.</param>
        public MongoProvider(string data): base(data)
        {
            _Parser = new MongoParser();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="cluster">Cluster.</param>
        /// <param name="database">Database.</param>
        /// <param name="username">User name.</param>
        /// <param name="password">Password.</param>
        /// <param name="writeconcern">Write concern.</param>
        /// <param name="retrywrites">Retry writes settings.</param>
        public MongoProvider(string cluster, string database = null, string username = null, string password = null, string writeconcern = "majority", bool retrywrites = true): this()
        {
            Cluster = cluster;
            Database = database;
            UserName = username;
            Password = password;
            WriteConcern = writeconcern;
            RetryWrites = retrywrites;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a provider for a connection string.</summary>
        /// <param name="connectionString">Connection string.</param>
        /// <returns>SQLite provider object.</returns>
        public static MongoProvider ForConnectionsString(string connectionString)
        {
            Ddp ddp = new Ddp();
            ddp.Set("enhanced", connectionString);

            return new MongoProvider(ddp.ToString());
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the MongoDB cluster URL.</summary>
        public string Cluster { get; set; }


        /// <summary>Gets or sets the database.</summary>
        public string Database { get; set; } = null;


        /// <summary>Gets or sets the user name.</summary>
        public string UserName { get; set; } = null;


        /// <summary>Gets or sets the user password.</summary>
        public string Password { get; set; } = null;


        /// <summary>Gets or sets the retry writes flag.</summary>
        public bool RetryWrites { get; set; } = true;


        /// <summary>Gets or sets the write concern settings.</summary>
        public string WriteConcern { get; set; } = "majority";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLProvider                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the provider configuration data.</summary>
        public override string Data
        {
            get
            {
                Ddp ddp = new Ddp();

                ddp.Set("provider", ProviderName);
                ddp.Set("cluster", Cluster);
                ddp.Set("db", Database);
                ddp.Set("user", UserName);
                ddp.Set("password", Password);
                ddp.Set("retry", RetryWrites);
                ddp.Set("w", WriteConcern);
                ddp.Set("enhanced", EnhancedString);

                return ddp.ToString();
            }
            set
            {
                Ddp ddp = new Ddp(value);

                Cluster = ddp.Get<string>("cluster");
                Database = ddp.Get<string>("db", null);
                UserName = ddp.Get<string>("user", null);
                Password = ddp.Get<string>("password", null);
                RetryWrites = ddp.Get<bool>("retry", true);
                WriteConcern = ddp.Get<string>("w", "majority");
                EnhancedString = ddp.Get<string>("enhanced");
            }
        }


        /// <summary>Connects to the database.</summary>
        public override void Connect()
        {
            if(string.IsNullOrWhiteSpace(EnhancedString))
            {
                string cstr = "cluster=" + Cluster + "; database=" + Database + ";user=" + UserName + ";password=" + Password +
                              "; retryWrites=" + (RetryWrites ? "true" : "false") + "; w=" + WriteConcern + ";";
                _Connection = new MongoConnection(cstr);
            }
            else
            {
                _Connection = new MongoConnection(EnhancedString);
            }

            _Connection.Open();
        }


        /// <summary>Returns the next sequence value of the given database sequence.</summary>
        /// <param name="sequence">Sequence name.</param>
        /// <returns>Next value.</returns>
        /// <exception cref="NotSupportedException">Thrown because Cassandra does not support sequences.</exception>
        public override long NextValue(string sequence)
        {
            throw new NotSupportedException();
        }



        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        /// <remarks>CHECK constraints will not be retrieved.</remarks>
        public override IDDLTable GetTable(string tableName)
        {
            throw new NotSupportedException();
        }


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        public override string[] GetTablesNames(string filter)
        {
            throw new NotSupportedException();
        }
    }
}
