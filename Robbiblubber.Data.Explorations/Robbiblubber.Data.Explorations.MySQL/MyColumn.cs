﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MySQL
{
    /// <summary>This class represents a MySQL column.</summary>
    public class MyColumn: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Column name.</summary>
        private string _Name;


        /// <summary>Referenced table.</summary>
        private string _ReferencedTable = null;


        /// <summary>Referenced column.</summary>
        private string _ReferencedColumn = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="host">Host.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="dataType">Data type.</param>
        /// <param name="pk">Primary key.</param>
        /// <param name="fk">Foreign key.</param>
        /// <param name="notNull">Not null.</param>
        internal MyColumn(MyHost host, MyColumnsFolder folder, string name, string dataType, bool pk, bool fk, bool notNull)
        {
            Host = host;
            _Parent = Folder = folder;
            _Name = name;
            DataType = dataType;
            IsPrimaryKey = pk;
            IsForeignKey = fk;
            IsNotNull = notNull;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the host instance.</summary>
        public MyHost Host { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public MyColumnsFolder Folder { get; private set; }


        /// <summary>Gets the data type.</summary>
        public string DataType { get; private set; }


        /// <summary>Gets if the field is a primary key.</summary>
        public bool IsPrimaryKey { get; private set; }


        /// <summary>Gets if the field is a foreign key.</summary>
        public bool IsForeignKey { get; private set; }


        /// <summary>Gets if the field is not nullable.</summary>
        public bool IsNotNull { get; private set; }


        /// <summary>Gets the referenced table.</summary>
        public string ReferencedTable
        {
            get
            {
                _Read();
                return _ReferencedTable;
            }
        }


        /// <summary>Gets the referenced column.</summary>
        public string ReferencedColumn
        {
            get
            {
                _Read();
                return _ReferencedColumn;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Reads referenced table and column.</summary>
        private void _Read()
        {
            if(_ReferencedTable != null) return;

            IDbCommand cmd = Host.Provider.CreateCommand();
            cmd.CommandText = "SELECT REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_SCHEMA = '" + Folder.Table.Folder.Database.Name + "' AND TABLE_NAME = '" + Folder.Table.Name + "' AND COLUMN_NAME = '" + Name + "'";
            IDataReader re = cmd.ExecuteReader();

            if(re.Read())
            {
                _ReferencedTable = re.GetString(0);
                _ReferencedColumn = re.GetString(1);
            }
            Disposal.Dispose(re, cmd);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                if(Folder.Table is MyView) { return Resources.column_view; }

                if(IsPrimaryKey) { return Resources.column_pk; }
                if(IsForeignKey) { return Resources.column_fk; }
                if(IsNotNull) { return Resources.column_nn; }

                return Resources.column;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                if(Folder.Table is MyView) { return "my::column/view"; }

                if(IsPrimaryKey) { return "my::column/pk"; }
                if(IsForeignKey) { return "my::column/fk"; }
                if(IsNotNull) { return "my::column/nn"; }

                return "my::column";
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = Name + "   " + DataType.ToUpper();
                if(IsNotNull)    { rval += "   NOT NULL"; }
                if(IsPrimaryKey) { rval += "   PRIMARY KEY"; }
                if(IsForeignKey) { rval += "   REFERENCES " + ReferencedTable + "(" + ReferencedColumn + ")"; }

                return rval;
            }
        }


        /// <summary>Gets if the object defines a query.</summary>
        public override bool HasQuery
        {
            get { return true; }
        }


        /// <summary>Gets the query for this object.</summary>
        public override string Query
        {
            get { return "SELECT " + Name + " FROM " + Folder.Table.Folder.Database.Name + "." + Folder.Table.Name; }
        }
    }
}
