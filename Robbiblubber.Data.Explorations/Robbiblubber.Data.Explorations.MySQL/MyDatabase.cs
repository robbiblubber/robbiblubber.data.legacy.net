﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MySQL
{
    /// <summary>This class represents a MySQL database.</summary>
    public class MyDatabase: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Database name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="host">Host.</param>
        /// <param name="name">Name.</param>
        /// <param name="connected">Determines if the database is connectd.</param>
        internal MyDatabase(MyHost host,  string name, bool connected)
        {
            _Parent = Host = host;
            _Name = name;
            Connected = connected;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider.</summary>
        public MyHost Host { get; private set; }


        /// <summary>Gets if the database is connectd.</summary>
        public bool Connected { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return (Connected ? Resources.dblink : Resources.database); }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return (Connected ? "my::connected" : "my::database"); }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            _Children.Add(new MyTablesFolder(Host, this));
            _Children.Add(new MyViewsFolder(Host, this));
            _Children.Add(new MyProceduresFolder(Host, this));
            _Children.Add(new MyFunctionsFolder(Host, this));
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return "CREATE DATABASE " + Name + ";"; }
        }
    }
}
