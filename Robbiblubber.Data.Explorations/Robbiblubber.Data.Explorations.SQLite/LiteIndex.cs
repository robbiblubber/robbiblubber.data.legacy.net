﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.SQLite
{
    /// <summary>This class represents a SQLite index.</summary>
    public class LiteIndex: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Index name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="unique">Unique flag.</param>
        internal LiteIndex(LiteDatabase database, LiteIndexesFolder folder, string name, bool unique)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
            IsUnique = unique;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public LiteDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public LiteIndexesFolder Folder { get; private set; }


        /// <summary>Gets if the index is unique.</summary>
        public bool IsUnique { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }

                
        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return (IsUnique ? Resources.index_unique : Resources.index); }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return (IsUnique ? "lite::index/unique" : "lite::index"); }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            List<string> cols = new List<string>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("PRAGMA Index_Info(" + Name + ")");

                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    cols.Add(re.GetString(2));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX00105", ex); }

            Disposal.Dispose(re, cmd);

            _Children = new List<DbItem>();
            foreach(LiteColumn i in Folder.Table.Columns.Children)
            {
                if(cols.Contains(i.Name))
                {
                    _Children.Add(i);
                }
            }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                IDbCommand cmd = null;
                string rval = "";
                try
                {
                    cmd = Database.Provider.CreateCommand("SELECT SQL FROM SQLITE_MASTER WHERE TYPE = 'index' AND TBL_NAME = '" + Folder.Table.Name + "' AND NAME = '" + Name + "'");
                    rval = (string) cmd.ExecuteScalar();
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX00106", ex); }

                Disposal.Dispose(cmd);

                return (rval.Trim(' ', ';') + ";");
            }
        }
    }
}
