﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MySQL
{
    /// <summary>This class represents a MySQL host.</summary>
    public class MyHost: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Host name.</summary>
        private string _Name;

        /// <summary>Determines if the instance is being initialized.</summary>
        private bool _Initial = true;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider">Provider.</param>
        internal MyHost(ProviderItem provider)
        {
            Provider = provider;
            Refresh();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider.</summary>
        public ProviderItem Provider { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.server; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "my::host"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            string dbname = "Database";

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Provider.CreateCommand();
                cmd.CommandText = "SELECT Database()";
                dbname = (string) cmd.ExecuteScalar();
            }
            catch(Exception) {}

            Disposal.Dispose(cmd);

            try
            {
                cmd = Provider.CreateCommand();
                cmd.CommandText = "SHOW DATABASES";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new MyDatabase(this, re.GetString(0), (re.GetString(0).ToLower() == dbname.ToLower())));
                }
            }
            catch(Exception) {}

            Disposal.Dispose(re, cmd);

            _Children.Add(new MyUsersFolder(this));
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return null; }
        }


        /// <summary>Refreshes the item.</summary>
        public override void Refresh()
        {
            IDbCommand cmd = null;

            try
            {
                cmd = Provider.CreateCommand();
                cmd.CommandText = "SELECT @@HOSTNAME";
                _Name = (string) cmd.ExecuteScalar();
            }
            catch(Exception) { _Name = "Host"; }
            Disposal.Dispose(cmd);

            _Children = null;

            if(!_Initial)
            {
                if(Exploration.Instance[Provider] is MyExplorer) { ((MyExplorer) Exploration.Instance[Provider]).__RefreshDefaultKeywords(); }
            }
            _Initial = false;
        }
    }
}
