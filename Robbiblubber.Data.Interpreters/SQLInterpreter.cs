﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Data.Interpreters.History;
using Robbiblubber.Data.SQL;
using Robbiblubber.Data.Providers;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class parses and interpretes SQL blocks.</summary>
    public class SQLInterpreter: IEnumerable<ISQLStatement>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Source text.</summary>
        private string _Source = "";


        /// <summary>Statement list.</summary>
        private ISQLStatement[] _Statements;
        

        /// <summary>Configuration loaded.</summary>
        private bool _Loaded = false;


        /// <summary>Use transactions.</summary>
        private bool _UseTransactions = false;


        /// <summary>Transaction behavior.</summary>
        private TransactionOption _TransactionBehavior = TransactionOption.DEFAULT;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="target">Target window.</param>
        public SQLInterpreter(ITargetWindow target)
        {
            Target = target;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the target window.</summary>
        public ITargetWindow Target { get; set; }


        /// <summary>Gets or sets if transactions will be used.</summary>
        public bool UseTransactions
        {
            get
            {
                _Load();
                return _UseTransactions;
            }
            set
            {
                if(value != _UseTransactions)
                {
                    _UseTransactions = value;
                    _Save();

                    ToggleTransactions();
                }
            }
        }


        /// <summary>Gets or sets session settings for transactions.</summary>
        public TransactionOption TransactionBehavior
        {
            get { return _TransactionBehavior; }
            set
            {
                if(value != _TransactionBehavior)
                {
                    _TransactionBehavior = value;
                    ToggleTransactions();
                }
            }
        }


        /// <summary>Gets if transactions are currently active.</summary>
        public bool TransactionsActive
        {
            get
            {
                if(TransactionBehavior == TransactionOption.DEFAULT) { return UseTransactions; }

                return (TransactionBehavior == TransactionOption.ENABLED);
            }
        }


        /// <summary>Gets or sets the SQL source.</summary>
        public string Source
        {
            get { return _Source; }
            set
            {
                _Source = value;

                if(Target.Provider != null) { _Statements = Target.Provider.Parser.Parse(_Source); }
            }
        }


        /// <summary>Gets the element at the specified index.</summary>
        /// <param name="index">Index.</param>
        /// <returns>String.</returns>
        public ISQLStatement this[int index]
        {
            get { return _Statements[index]; }
        }


        /// <summary>Gets the number of elements in the collection.</summary>
        public int Count
        {
            get { return _Statements.Length; }
        }


        /// <summary>Gets if the source contains DDL statements.</summary>
        public bool IsDDL
        {
            get
            {
                if(_Statements == null) { return false; }

                foreach(SQLStatement i in _Statements)
                {
                    if(i.IsDDL) { return true; }
                }

                return false;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Executes the stack of statements.</summary>
        /// <param name="sql">SQL.</param>
        public void Execute(string sql)
        {
            Source = sql;
            Execute();
        }


        /// <summary>Executes the stack of statements.</summary>
        public void Execute()
        {
            if(Target.Provider == null)
            {
                if(Target.View == ViewMode.TEXT)
                {
                    Target.Write("sqlfwx::int.notconnected".Localize("Not connected.") + "\r\n");
                }
                else
                {
                    Target.WriteToGrid("sqlfwx::int.notconnected".Localize("Not connected."));
                }
                return;
            }

            for(int i = 0; i < _Statements.Length; i++)
            {
                Target.Provider.AddHistory(_Statements[i].Source);

                if((Target.View == ViewMode.TEXT) && (_Statements.Length > 1))
                {
                    Target.Write(((i > 0) ? "\r\n" : "") + _Statements[i].Source + ";\r\n");
                }

                if(_Statements[i].IsCommit)
                {
                    Commit();
                }
                else if(_Statements[i].IsRollback)
                {
                    Rollback();
                }
                else if((_Statements[i].Source.ToLower() == "/transactions on") || (_Statements[i].Source.ToLower() == "/transactions enabled"))
                {
                    UseTransactions = true;
                    _WriteLine("sqlfwx::int.tron".Localize("Transactions enabled."));
                }
                else if((_Statements[i].Source.ToLower() == "/transactions off") || (_Statements[i].Source.ToLower() == "/transactions disabled"))
                {
                    UseTransactions = true;
                    _WriteLine("sqlfwx::int.troff".Localize("Transactions disabled."));
                }
                else if((_Statements[i].Source.ToLower() == "/session transactions on") || (_Statements[i].Source.ToLower() == "/session transactions enabled"))
                {
                    TransactionBehavior = TransactionOption.ENABLED;
                    _WriteLine("sqlfwx::int.stron".Localize("Transactions enabled for this session."));
                }
                else if((_Statements[i].Source.ToLower() == "/session transactions off") || (_Statements[i].Source.ToLower() == "/session transactions disabled"))
                {
                    TransactionBehavior = TransactionOption.DISABLED;
                    _WriteLine("sqlfwx::int.stroff".Localize("Transactions disabled for this session."));
                }
                else if(_Statements[i].Source.ToLower() == "/session transactions default")
                {
                    TransactionBehavior = TransactionOption.DEFAULT;
                    _WriteLine(UseTransactions ? "sqlfwx::int.tron".Localize("Transactions enabled.") : "sqlfwx::int.troff".Localize("Transactions disabled."));
                }
                else if(_Statements[i].Source.ToLower() == "/transactions")
                {
                    switch(TransactionBehavior)
                    {
                        case TransactionOption.ENABLED:
                            _WriteLine("sqlfwx::int.stron".Localize("Transactions enabled for this session."));
                            break;
                        case TransactionOption.DISABLED:
                            _WriteLine("sqlfwx::int.stroff".Localize("Transactions disabled for this session."));
                            break;
                        default:
                            _WriteLine(UseTransactions ? "sqlfwx::int.tron".Localize("Transactions enabled.") : "sqlfwx::int.troff".Localize("Transactions disabled."));
                            break;
                    }
                }
                else if(_Statements[i].IsDML || _Statements[i].IsDDL)
                {
                    IDbCommand cmd = null;

                    try
                    {
                        cmd = Target.Provider.CreateCommand(_Statements[i].Source);
                        int resultv = cmd.ExecuteNonQuery();
                        string rows = resultv.ToString() + ' ' + ((resultv == 1) ? "sqlfwx::int.row".Localize("row") : "sqlfwx::int.rows".Localize("rows"));
                        string result = "";

                        if(_Statements[i].IsDDL)
                        {
                            result = "sqlfwx::int.exsucc".Localize("Statement executed successfully.") + "\r\n";
                        }
                        else if(_Statements[i].IsInsert)
                        {
                            result = "sqlfwx::int.insert".Localize("$(n) inserted.");
                        }
                        else if(_Statements[i].IsDelete)
                        {
                            result = "sqlfwx::int.delete".Localize("$(n) deleted.");
                        }
                        else { result = "sqlfwx::int.update".Localize("$(n) updated."); }

                        result = result.Replace("$(n)", rows);
                        _WriteLine(result);
                    }
                    catch(ThreadAbortException)
                    {
                        Disposal.Dispose(cmd);
                    }
                    catch(Exception ex) { _WriteLine(ex.Message); }

                    Disposal.Dispose(cmd);
                }
                else if(Target.View == ViewMode.GRID)
                {
                    IDbCommand cmd = null;
                    IDataReader re = null;

                    try
                    {
                        cmd = Target.Provider.CreateCommand(_Statements[i].Source);
                        re = cmd.ExecuteReader();

                        if(Target.Provider.CountFields(re) == 0)
                        {
                            Target.WriteToGrid("sqlfwx::int.exsucc".Localize("Statement executed successfully."));
                        }
                        else
                        {
                            Target.DataToGrid(Target.Provider.GetStringDataTable(re));
                        }
                    }
                    catch(ThreadAbortException) { }
                    catch(Exception ex) { Target.WriteToGrid(ex.Message + "\r\n\r\n"); }

                    Disposal.Recycle(re, cmd);
                }
                else
                {
                    IDbCommand cmd = null;
                    IDataReader re = null;

                    try
                    {
                        cmd = Target.Provider.CreateCommand(_Statements[i].Source);
                        re = cmd.ExecuteReader();

                        _Data data = new _Data(Target.Provider, re);
                        if(data.FieldCount == 0)
                        {
                            Target.Write("sqlfwx::int.exsucc".Localize("Statement executed successfully.") + "\r\n");
                        }
                        else
                        {
                            Target.Write(data.ToString());
                            Target.Write("\r\n");
                        }
                    }
                    catch(ThreadAbortException) {}
                    catch(Exception ex) { Target.Write(ex.Message + "\r\n\r\n"); }

                    Disposal.Recycle(re, cmd);
                }
            }
        }


        /// <summary>Commits a transaction.</summary>
        public void Commit()
        {
            string result = "sqlfwx::int.commit".Localize("Transaction committed.");

            if(Target.Provider == null)
            {
                result = "sqlfwx::int.notconnected".Localize("Not connected.");
            }
            else if(Target.Provider.Transaction == null)
            {
                result = "sqlfwx::int.notr".Localize("No transaction.");
            }
            else try
            {
                Target.Provider.Transaction.Commit();
                Target.Provider.Transaction.Dispose();
                Target.Provider.Transaction = Target.Provider.Connection.BeginTransaction();
            }
            catch(Exception ex) { result = ex.Message; }

            _WriteLine(result);
        }


        /// <summary>Rolls back a transaction.</summary>
        public void Rollback()
        {
            string result = "sqlfwx::int.rollback".Localize("Transaction rolled back.");

            if(Target.Provider == null)
            {
                result = "sqlfwx::int.notconnected".Localize("Not connected.");
            }
            else if(Target.Provider.Transaction == null)
            {
                result = "sqlfwx::int.notr".Localize("No transaction.");
            }
            else try
                {
                    Target.Provider.Transaction.Rollback();
                    Target.Provider.Transaction.Dispose();
                    Target.Provider.Transaction = Target.Provider.Connection.BeginTransaction();
                }
                catch(Exception ex) { result = ex.Message; }

            _WriteLine(result);
        }


        /// <summary>Activates or deactivates transactions.</summary>
        public void ToggleTransactions()
        {
            if(Target.Provider == null) { return; }
            
            try
            {
                if(TransactionsActive)
                {
                    if(Target.Provider.Transaction == null)
                    {
                        if(Target.Provider != null) { Target.Provider.Transaction = Target.Provider.Connection.BeginTransaction(); }
                    }
                }
                else
                {
                    if(Target.Provider.Transaction != null)
                    {
                        Target.Provider.Transaction.Rollback();
                        Target.Provider.Transaction = null;
                    }
                }
            }
            catch(Exception ex)
            {
                DebugOp.Dump("SQLFWX04130", ex);
                MessageBox.Show("sqlfwx::udiag.trfail.text".Localize("Failed to toggle transactions."), "sqlfwx::udiag.trfail.caption".Localize("Transactions"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads configuration.</summary>
        private void _Load()
        {
            if(_Loaded) return;

            Ddp cfg = Ddp.Open(PathOp.SystemLocalConfigurationPath + @"\SQL\interpreter.config");
            _UseTransactions = cfg.Get("transactions/enabled", false);
        }


        /// <summary>Saves configuration.</summary>
        private void _Save()
        {
            Ddp cfg = Ddp.Open(PathOp.SystemLocalConfigurationPath + @"\SQL\interpreter.config");
            cfg.Set("transactions/enabled", _UseTransactions);
            cfg.Save();
        }


        /// <summary>Writes text to target output.</summary>
        /// <param name="text">Text.</param>
        private void _WriteLine(string text)
        {
            if(Target.View == ViewMode.GRID)
            {
                Target.WriteToGrid(text);
            }
            else { Target.Write(text + "\r\n"); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Statements.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable <Statement>                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<ISQLStatement> IEnumerable<ISQLStatement>.GetEnumerator()
        {
            return (IEnumerator<ISQLStatement>) _Statements.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [enum] TransactionOption                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This enumeration defines transaction options.</summary>
        public enum TransactionOption: int
        {
            /// <summary>Use default transaction settings.</summary>
            DEFAULT = 0,
            /// <summary>Enable transactions for this session (/session transactions on).</summary>
            ENABLED = 1,
            /// <summary>Disable transactions for this session (/session transactions off).</summary>
            DISABLED = 2
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] _Data                                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>This class encapsulates sql data.</summary>
        protected class _Data
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="provider">Provider.</param>
            /// <param name="re">Data reader.</param>
            public _Data(IProvider provider, IDataReader re)
            {
                Provider = provider;
                FieldCount = Provider.CountFields(re);

                if(FieldCount == 0) return;

                Headers = new List<string>();
                for(int i = 0; i < FieldCount; i++) { Headers.Add(re.GetName(i)); }

                Rows = new List<_Row>();
                while(re.Read())
                {
                    Rows.Add(new _Row(this, re));
                }

                Widths = new List<int>();
                for(int i = 0; i < FieldCount; i++)
                {
                    int l = Headers[i].Length;
                    int x;
                    foreach(_Row j in Rows)
                    {
                        if(l < (x = j.Lines[i].Max(m => m.Length))) { l = x; }
                    }
                    Widths.Add(l + 1);
                }
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public members                                                                                               //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Provider.</summary>
            public IProvider Provider;

            /// <summary>Field count.</summary>
            public int FieldCount;
            
            /// <summary>Headers.</summary>
            public List<string> Headers;

            /// <summary>Rows.</summary>
            public List<_Row> Rows;

            /// <summary>Gets the column widths.</summary>
            public List<int> Widths;




            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // [override] object                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Returns a string representation of this instance.</summary>
            /// <returns>String.</returns>
            public override string ToString()
            {
                StringBuilder rval = new StringBuilder();
                for(int i = 0; i < FieldCount; i++)
                {
                    rval.Append(Headers[i].PadRight(Widths[i], ' '));
                }
                rval.Append("\r\n");

                for(int i = 0; i < FieldCount; i++)
                {
                    rval.Append("".PadRight(Widths[i], '-'));
                }
                rval.Append("\r\n");

                foreach(_Row cur in Rows)
                {
                    for(int n = 0; n < cur.Height; n++)
                    {
                        for(int i = 0; i < FieldCount; i++)
                        {
                            if(n < cur.Lines[i].Length)
                            {
                                rval.Append(cur.Lines[i][n].PadRight(Widths[i]));
                            }
                            else { rval.Append("".PadRight(Widths[i])); }
                        }
                        rval.Append("\r\n");
                    }
                }

                return rval.ToString();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] _Row                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This class encapsulates row data.</summary>
        protected class _Row
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="parent">Parent data object.</param>
            /// <param name="re">Data reader.</param>
            public _Row(_Data parent, IDataReader re)
            {
                Lines = new List<string[]>();
                for(int i = 0; i < parent.FieldCount; i++)
                {
                    Lines.Add(parent.Provider.Parser.GetDataString(re, i).Replace("\r\n", "\n").Replace("\r", "\n").Split('\n'));
                }
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public members                                                                                               //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Headers.</summary>
            public List<string[]> Lines;



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public properties                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            /// <summary>Gets the height for this row.</summary>
            public int Height
            {
                get { return Lines.Max(m => m.Length); }
            }
        }
    }
}
