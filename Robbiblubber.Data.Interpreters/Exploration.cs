﻿using System;
using System.Collections.Generic;

using Robbiblubber.Util;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class implements a self-configuring database exploration provider.</summary>
    public sealed class Exploration: IExploration
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Instance.</summary>
        private static Exploration _Instance = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Exploration lists.</summary>
        private List<IExploration> _Explorations;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        private Exploration()
        {
            _Explorations = new List<IExploration>();

            foreach(object i in ClassOp.LoadSubtypesOf(typeof(IExploration)))
            {
                _Explorations.Add((IExploration) i);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the singleton instance of this class.</summary>
        public static Exploration Instance
        {
            get
            {
                if(_Instance == null) { _Instance = new Exploration(); }

                return _Instance;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbExploration                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the explorer for a provider.</summary>
        /// <param name="provider">Provider.</param>
        /// <returns>Explorer.</returns>
        public IExporer this[ProviderItem provider]
        {
            get
            {
                IExporer rval = null;

                foreach(IExploration i in _Explorations)
                {
                    if((rval = i[provider]) != null) break;
                }

                return rval;
            }
        }
    }
}
