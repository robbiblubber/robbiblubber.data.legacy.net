﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Interpreters.History
{
    /// <summary>Thic class implements the history browser window.</summary>
    internal partial class FormHistory: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Target application.</summary>
        private IHistoryTargetWindow _Target = null;

        /// <summary>History.</summary>
        private SQLHistory.StatementList _History = null;
        
        /// <summary>Current position.</summary>
        private int _Pos = 0;

        /// <summary>View option.</summary>
        private ViewOption View = ViewOption.BROWSE;
                
        /// <summary>Show only favorites.</summary>
        private bool _OnlyFavorites = false;

        /// <summary>Initialized flag.</summary>
        private bool _Initialized = false;
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="target">Target.</param>
        public FormHistory(IHistoryTargetWindow target)
        {
            InitializeComponent();

            _Target = target;

            _ButtonCopyText.Tag = _ButtonApplyText.Tag = 0L;
            _TextSQL.MouseWheel += _TextSQL_MouseWheel;

            this.Localize();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Shows the window.</summary>
        /// <param name="view">View option.</param>
        /// <param name="fav">Show only favorites.</param>
        public void ShowDialog(ViewOption view, bool fav = false)
        {
            View = view;

            if(_OnlyFavorites != fav)
            {
                _OnlyFavorites = fav;
                _Initialized = false;
            }

            if(!_Initialized)
            {
                if(_IlistConnections.Images.Count == 3)
                {
                    foreach(IProviderManager i in ProviderManager.Managers)
                    {
                        _IlistConnections.Images.Add(i.ProviderName, i.ConnectionIcon);
                    }
                }

                _ComboConnection.Items.Clear();
                foreach(SQLHistory.Connection i in SQLHistory.Connections)
                {
                    _ComboConnection.Items.Add(new RichComboItem(i.DisplayName, i.IconKey, i.Name));
                }
                _ComboConnection.SelectedIndex = 0;
            }

            if(View == ViewOption.BROWSE)
            {
                if(fav)
                {
                    Icon = Icon.FromHandle(new Bitmap(Resources.favorite).GetHicon());
                    Text = "sqlfwx::udiag.bhistory.fav.caption".Localize("Browse Favorites");
                }
                else
                {
                    Icon = Icon.FromHandle(new Bitmap(Resources.history).GetHicon());
                    Text = "sqlfwx::udiag.bhistory.hist.caption".Localize("Browse History");
                }

                _ButtonApply.Text = "sqlfwx::common.button.apply".Localize("&Apply");
                _TextSQL.Visible = _ButtonFavorite.Visible = _ButtonDelete.Visible = _ButtonApplyText.Visible = _ButtonCopyText.Visible =
                                   _ButtonFirst.Visible = _ButtonPrevious.Visible = _ButtonNext.Visible = _ButtonLast.Visible = true;
                Height = 580;

                _ButtonClose.Location = new Point(1200, 100);

                if(!_Initialized)
                {
                    RichComboItem v = null;
                    if(_Target.Provider != null)
                    {
                        foreach(RichComboItem i in _ComboConnection.Items)
                        {
                            if(i.Text == _Target.Provider.Name) { v = i; break; }
                        }
                    }

                    if((v != null) || _OnlyFavorites)
                    {
                        if(v != null) _ComboConnection.SelectedItem = v;
                        _ButtonApply_Click(null, null);
                        _Initialized = true;
                    }
                }

                _Show();
            }
            else
            {
                if(view == ViewOption.DUMP)
                {
                    Icon = Icon.FromHandle(new Bitmap(Resources.dumphistory).GetHicon());
                    Text = "sqlfwx::udiag.bhistory.dump.caption".Localize("Dump History");
                }
                else
                {
                    Icon = Icon.FromHandle(new Bitmap(Resources.cleanup_history).GetHicon());
                    Text = "sqlfwx::udiag.bhistory.cleanup.caption".Localize("Clean Up History");
                }

                _ButtonApply.Text = "sqlfwx::common.button.ok".Localize("&OK");
                _TextSQL.Visible = _ButtonFavorite.Visible = _ButtonDelete.Visible = _ButtonApplyText.Visible = _ButtonCopyText.Visible =
                                   _ButtonFirst.Visible = _ButtonPrevious.Visible = _ButtonNext.Visible = _ButtonLast.Visible = false;
                Height = 234;
                _ButtonClose.Location = new Point(184, 144);
            }

            ShowDialog();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Shows the current item.</summary>
        private void _Show()
        {
            if((_History == null) || (_History.Count == 0))
            {
                _TextSQL.Enabled = _ButtonFavorite.Enabled = _ButtonDelete.Enabled = _ButtonApplyText.Enabled = _ButtonCopyText.Enabled =
                                   _ButtonFirst.Enabled = _ButtonPrevious.Enabled = _ButtonNext.Enabled = _ButtonLast.Enabled = false;
                _TextSQL.Text = "";
                _LabelPosition.Text = "0/0";
                return;
            }

            SQLHistory.Statement stmt = _History[_Pos];
            
            if(stmt.Favorite)
            {
                _ButtonFavorite.Image = Resources.removefavorite;
                _ToolTip.SetToolTip(_ButtonFavorite, "sqlfwx::udiag.bhistory.remfav".Localize("Remove from favorite SQL"));
            }
            else
            {
                _ButtonFavorite.Image = Resources.addfavorite;
                _ToolTip.SetToolTip(_ButtonFavorite, "sqlfwx::udiag.bhistory.addfav".Localize("Add to favorite SQL"));
            }
            _ButtonFavorite.Enabled = true;

            _TextSQL.Enabled = _ButtonDelete.Enabled = _ButtonApplyText.Enabled = _ButtonCopyText.Enabled = true;
            _TextSQL.Text = _History[_Pos].SQL;

            _ButtonFirst.Enabled = _ButtonPrevious.Enabled = (_Pos > 0);
            _ButtonLast.Enabled = _ButtonNext.Enabled = (_Pos < (_History.Count - 1));

            _LabelPosition.Text = ((_Pos + 1).ToString() + "/" + _History.Count.ToString());
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Button "Close" click.</summary>
        private void _ButtonClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Button "Apply" click.</summary>
        private void _ButtonApply_Click(object sender, EventArgs e)
        {
            SQLHistory.Filter f = new SQLHistory.Filter();

            f.Database = (string) ((RichComboItem) _ComboConnection.SelectedItem).Tag;
            f.HasFrom = _DtpFrom.Checked;
            f.From = _DtpFrom.Value;
            f.HasTo = _DtpTo.Checked;
            f.To = _DtpTo.Value;
            f.Expression = _TextSearch.Text;
            f.Favorite = _OnlyFavorites;

            if(View == ViewOption.BROWSE)
            {
                _History = f.Execute();

                _Pos = 0;
                _Show();
            }
            else if(View == ViewOption.DUMP)
            {
                _History = f.Execute();
                Hide();

                _Pos = 0;

                string tmp = PathOp.GetTempFile(".txt");
                File.WriteAllText(tmp, _History.ToString());
                try
                {
                    Process.Start(tmp);
                }
                catch(Exception) {}
                Close();
            }
            else if(View == ViewOption.CLEANUP)
            {
                string mtext = "sqlfwx::udiag.delhistory.text".Localize("Do you really want to delete $(n) entries from SQL history?").Replace("$(n)", f.CountElements().ToString());
                switch(MessageBox.Show(mtext, "sqlfwx::udiag.delhistory.caption".Localize("Clean up"), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    case DialogResult.Yes:
                        f.DeleteAll();
                        _History = null;
                        SQLHistory._Connections = null;
                        _Pos = 0;
                        _Initialized = false;
                        Close();
                        break;
                    case DialogResult.No:
                        Close();
                        break;
                }
            }
        }


        /// <summary>Button "Next" click.</summary>
        private void _ButtonNext_Click(object sender, EventArgs e)
        {
            _Pos++;
            _Show();
        }


        /// <summary>Button "Last" click.</summary>
        private void _ButtonLast_Click(object sender, EventArgs e)
        {
            _Pos = (_History.Count - 1);
            _Show();
        }


        /// <summary>Button "Previous" click.</summary>
        private void _ButtonPrevious_Click(object sender, EventArgs e)
        {
            _Pos--;
            _Show();
        }


        /// <summary>Button "First" click.</summary>
        private void _ButtonFirst_Click(object sender, EventArgs e)
        {
            _Pos = 0;
            _Show();
        }


        /// <summary>Button "Copy" click.</summary>
        private void _ButtonCopyText_Click(object sender, EventArgs e)
        {
            if((DateTime.Now.Ticks - ((long) _ButtonCopyText.Tag)) < 4000000)
            {
                Close();
                return;
            }

            Clipboard.SetText(_TextSQL.Text);
            _ButtonCopyText.Tag = DateTime.Now.Ticks;
        }


        /// <summary>Button "Apply text" click.</summary>
        private void _ButtonApplyText_Click(object sender, EventArgs e)
        {
            if((DateTime.Now.Ticks - ((long) _ButtonApplyText.Tag)) < 4000000)
            {
                Close();
                return;
            }
            
            _Target.Apply(_TextSQL.Text);
            _ButtonApplyText.Tag = DateTime.Now.Ticks;
        }


        /// <summary>Button "Delete" click.</summary>
        private void _ButtonDelete_Click(object sender, EventArgs e)
        {
            _History.Delete(_Pos);
            if(_Pos > 0)
            {
                if(_Pos >= _History.Count) { _Pos--; }
            }
            _Show();
        }


        /// <summary>Button "Favorite" click.</summary>
        private void _ButtonFavorite_Click(object sender, EventArgs e)
        {
            if(_History[_Pos].Favorite)
            {
                _History.Unfavorite(_Pos, _OnlyFavorites);
            }
            else { _History.Favorite(_Pos); }

            _Show();
        }


        /// <summary>Text box mouse wheel.</summary>
        private void _TextSQL_MouseWheel(object sender, MouseEventArgs e)
        {
            _Pos += (e.Delta / Math.Abs(e.Delta));

            if(_Pos < 0)
            {
                _Pos = 0;
            }
            else if(_Pos >= _History.Count) { _Pos = (_History.Count - 1); }

            _Show();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [enum] ViewOption                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>View options enumeration.</summary>
        internal enum ViewOption: int
        {
            /// <summary>Shows the browse dialog.</summary>
            BROWSE = 0,
            /// <summary>Shows the dump dialog.</summary>
            DUMP = 1,
            /// <summary>Shows the clean up dialog.</summary>
            CLEANUP = 2
        }
    }
}
