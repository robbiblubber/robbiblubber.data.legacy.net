﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.PostgreSQL
{
    /// <summary>This class represents a PostgreSQL table.</summary>
    public class PgTable: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Table name.</summary>
        protected string _Name;
        
        /// <summary>Definition.</summary>
        protected string _Definition = null;

        /// <summary>Primary keys.</summary>
        protected string[] _PrimaryKeys = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        internal PgTable(PgDatabase database, PgTablesFolder folder, string name)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public PgDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public PgTablesFolder Folder { get; private set; }


        /// <summary>Gets the colums folder.</summary>
        public PgColumnsFolder Columns
        {
            get
            {
                foreach(DbItem i in Children)
                {
                    if(i is PgColumnsFolder) { return ((PgColumnsFolder) i); }
                }

                return null;
            }
        }


        /// <summary>Gets the primary keys for the table.</summary>
        public string[] PrimaryKeys
        {
            get
            {
                if(_PrimaryKeys == null)
                {
                    List<string> pks = new List<string>();

                    foreach(PgColumn i in Columns.Children)
                    {
                        if(i.IsPrimaryKey) { pks.Add(i.Name); }
                    }
                    _PrimaryKeys = pks.ToArray();
                }

                return _PrimaryKeys;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.table; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "pg::table"; }
        }

        
        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();
            _Children.Add(new PgColumnsFolder(Database, this));
            _Children.Add(new PgIndexesFolder(Database, this));
            _Children.Add(new PgRulesFolder(Database, this));
            _Children.Add(new PgTriggersFolder(Database, this));
        }
        

        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = "CREATE TABLE " + Folder.Schema.Name + "." + Name + "\r\n(";

                try
                {
                    int ln = Columns.Children.Max(m => m.Name.Length) + 3;
                    int lt = (Columns.Children).Max(m => ((PgColumn) m).DataType.Length) + 3;
                    bool first = true;

                    foreach(PgColumn i in Columns.Children)
                    {
                        if(first)
                        {
                            first = false;
                        }
                        else
                        {
                            rval += ",";
                        }

                        rval += ("\r\n   " + i.Name.PadRight(ln) + i.DataType.ToUpper().PadRight(lt) + (i.IsNotNull ? "NOT NULL   " : "           "));
                        if(i.IsPrimaryKey) { rval += "PRIMARY KEY   "; }
                        if(i.IsForeignKey) { rval += "REFERENCES " + i.ReferencedTable + "(" + i.ReferencedColumn + ")"; }

                        rval = rval.Trim();
                    }

                    rval += "\r\n);";
                }
                catch(Exception) { rval = ""; }

                return rval;
            }
        }


        /// <summary>Gets if the object defines a query.</summary>
        public override bool HasQuery
        {
            get { return true; }
        }


        /// <summary>Gets the query for this object.</summary>
        public override string Query
        {
            get
            {
                string f = "";
                bool first = true;

                foreach(PgColumn i in Columns.Children)
                {
                    if(first)
                    {
                        first = false;
                    }
                    else { f += ", "; }

                    f += i.Name;
                }
                return "SELECT " + f + " FROM " + QualifiedName;
            }
        }


        /// <summary>Gets the qualified name for this object.</summary>
        public override string QualifiedName
        {
            get { return Folder.Schema.Name + "." + Name; }
        }
    }
}
