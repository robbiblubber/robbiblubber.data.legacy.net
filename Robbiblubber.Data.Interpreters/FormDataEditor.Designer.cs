﻿namespace Robbiblubber.Data.Interpreters
{
    partial class FormDataEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDataEditor));
            this._ToolStrip = new System.Windows.Forms.ToolStrip();
            this._LabelFilter = new System.Windows.Forms.ToolStripLabel();
            this._TextFilter = new System.Windows.Forms.ToolStripTextBox();
            this._GridTable = new System.Windows.Forms.DataGridView();
            this._MenuMain = new System.Windows.Forms.MenuStrip();
            this._MenuTable = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuTableBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuClose = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEdit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuCut = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuFind = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFindNext = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuShowHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._TimerTime = new System.Windows.Forms.Timer(this.components);
            this._FindSearch = new Robbiblubber.Util.Controls.FindControl();
            this._ToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._GridTable)).BeginInit();
            this._MenuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // _ToolStrip
            // 
            this._ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._LabelFilter,
            this._TextFilter});
            this._ToolStrip.Location = new System.Drawing.Point(0, 24);
            this._ToolStrip.Margin = new System.Windows.Forms.Padding(12);
            this._ToolStrip.Name = "_ToolStrip";
            this._ToolStrip.Size = new System.Drawing.Size(1091, 25);
            this._ToolStrip.TabIndex = 0;
            // 
            // _LabelFilter
            // 
            this._LabelFilter.Image = ((System.Drawing.Image)(resources.GetObject("_LabelFilter.Image")));
            this._LabelFilter.Name = "_LabelFilter";
            this._LabelFilter.Size = new System.Drawing.Size(16, 22);
            this._LabelFilter.Tag = "||sqlfwx::button.filter.tooltip";
            // 
            // _TextFilter
            // 
            this._TextFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextFilter.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._TextFilter.Name = "_TextFilter";
            this._TextFilter.Padding = new System.Windows.Forms.Padding(4);
            this._TextFilter.Size = new System.Drawing.Size(344, 25);
            // 
            // _GridTable
            // 
            this._GridTable.AllowUserToResizeRows = false;
            this._GridTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._GridTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this._GridTable.Location = new System.Drawing.Point(0, 49);
            this._GridTable.Name = "_GridTable";
            this._GridTable.Size = new System.Drawing.Size(1091, 558);
            this._GridTable.TabIndex = 1;
            this._GridTable.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this._GridTable_CellBeginEdit);
            this._GridTable.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._GridTable_CellEndEdit);
            this._GridTable.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._GridTable_DataError);
            this._GridTable.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this._GridTable_EditingControlShowing);
            this._GridTable.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this._GridTable_RowLeave);
            this._GridTable.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this._GridTable_UserAddedRow);
            this._GridTable.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this._GridTable_UserDeletingRow);
            // 
            // _MenuMain
            // 
            this._MenuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuTable,
            this._MenuEdit,
            this._MenuHelp});
            this._MenuMain.Location = new System.Drawing.Point(0, 0);
            this._MenuMain.Name = "_MenuMain";
            this._MenuMain.Size = new System.Drawing.Size(1091, 24);
            this._MenuMain.TabIndex = 2;
            // 
            // _MenuTable
            // 
            this._MenuTable.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuRefresh,
            this._MenuTableBlank0,
            this._MenuClose});
            this._MenuTable.Name = "_MenuTable";
            this._MenuTable.Size = new System.Drawing.Size(46, 20);
            this._MenuTable.Tag = "sqlfwx::menu.table";
            this._MenuTable.Text = "&Table";
            // 
            // _MenuRefresh
            // 
            this._MenuRefresh.Image = ((System.Drawing.Image)(resources.GetObject("_MenuRefresh.Image")));
            this._MenuRefresh.Name = "_MenuRefresh";
            this._MenuRefresh.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this._MenuRefresh.Size = new System.Drawing.Size(180, 22);
            this._MenuRefresh.Tag = "sqlfwx::menu.refresh";
            this._MenuRefresh.Text = "&Refresh";
            this._MenuRefresh.Click += new System.EventHandler(this._MenuRefresh_Click);
            // 
            // _MenuTableBlank0
            // 
            this._MenuTableBlank0.Name = "_MenuTableBlank0";
            this._MenuTableBlank0.Size = new System.Drawing.Size(177, 6);
            // 
            // _MenuClose
            // 
            this._MenuClose.Image = ((System.Drawing.Image)(resources.GetObject("_MenuClose.Image")));
            this._MenuClose.Name = "_MenuClose";
            this._MenuClose.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F4)));
            this._MenuClose.Size = new System.Drawing.Size(180, 22);
            this._MenuClose.Tag = "sqlfwx::menu.close";
            this._MenuClose.Text = "&Close";
            this._MenuClose.Click += new System.EventHandler(this._MenuClose_Click);
            // 
            // _MenuEdit
            // 
            this._MenuEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuUndo,
            this.toolStripMenuItem1,
            this._MenuCut,
            this._MenuCopy,
            this._MenuPaste,
            this.toolStripMenuItem3,
            this._MenuFind,
            this._MenuFindNext});
            this._MenuEdit.Name = "_MenuEdit";
            this._MenuEdit.Size = new System.Drawing.Size(39, 20);
            this._MenuEdit.Tag = "sqlfwx::menu.edit";
            this._MenuEdit.Text = "&Edit";
            // 
            // _MenuUndo
            // 
            this._MenuUndo.Image = ((System.Drawing.Image)(resources.GetObject("_MenuUndo.Image")));
            this._MenuUndo.Name = "_MenuUndo";
            this._MenuUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this._MenuUndo.Size = new System.Drawing.Size(180, 22);
            this._MenuUndo.Tag = "sqlfwx::menu.undo";
            this._MenuUndo.Text = "&Undo";
            this._MenuUndo.Click += new System.EventHandler(this._MenuUndo_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(177, 6);
            // 
            // _MenuCut
            // 
            this._MenuCut.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCut.Image")));
            this._MenuCut.Name = "_MenuCut";
            this._MenuCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this._MenuCut.Size = new System.Drawing.Size(180, 22);
            this._MenuCut.Tag = "sqlfwx::menu.cut";
            this._MenuCut.Text = "C&ut";
            this._MenuCut.Click += new System.EventHandler(this._MenuCut_Click);
            // 
            // _MenuCopy
            // 
            this._MenuCopy.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCopy.Image")));
            this._MenuCopy.Name = "_MenuCopy";
            this._MenuCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this._MenuCopy.Size = new System.Drawing.Size(180, 22);
            this._MenuCopy.Tag = "sqlfwx::menu.copy";
            this._MenuCopy.Text = "&Copy";
            this._MenuCopy.Click += new System.EventHandler(this._MenuCopy_Click);
            // 
            // _MenuPaste
            // 
            this._MenuPaste.Image = ((System.Drawing.Image)(resources.GetObject("_MenuPaste.Image")));
            this._MenuPaste.Name = "_MenuPaste";
            this._MenuPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this._MenuPaste.Size = new System.Drawing.Size(180, 22);
            this._MenuPaste.Tag = "sqlfwx::menu.paste";
            this._MenuPaste.Text = "&Paste";
            this._MenuPaste.Click += new System.EventHandler(this._MenuPaste_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(177, 6);
            // 
            // _MenuFind
            // 
            this._MenuFind.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFind.Image")));
            this._MenuFind.Name = "_MenuFind";
            this._MenuFind.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this._MenuFind.Size = new System.Drawing.Size(180, 22);
            this._MenuFind.Tag = "sqlfwx::menu.find";
            this._MenuFind.Text = "&Find...";
            this._MenuFind.Click += new System.EventHandler(this._MenuFind_Click);
            // 
            // _MenuFindNext
            // 
            this._MenuFindNext.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFindNext.Image")));
            this._MenuFindNext.Name = "_MenuFindNext";
            this._MenuFindNext.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this._MenuFindNext.Size = new System.Drawing.Size(180, 22);
            this._MenuFindNext.Tag = "sqlfwx::menu.next";
            this._MenuFindNext.Text = "Find &Next";
            this._MenuFindNext.Click += new System.EventHandler(this._MenuFindNext_Click);
            // 
            // _MenuHelp
            // 
            this._MenuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuShowHelp});
            this._MenuHelp.Name = "_MenuHelp";
            this._MenuHelp.Size = new System.Drawing.Size(44, 20);
            this._MenuHelp.Tag = "sqlfwx::menu.help";
            this._MenuHelp.Text = "&Help";
            // 
            // _MenuShowHelp
            // 
            this._MenuShowHelp.Image = ((System.Drawing.Image)(resources.GetObject("_MenuShowHelp.Image")));
            this._MenuShowHelp.Name = "_MenuShowHelp";
            this._MenuShowHelp.Size = new System.Drawing.Size(180, 22);
            this._MenuShowHelp.Tag = "sqlfwx::menu.showhelp";
            this._MenuShowHelp.Text = "Show &Help";
            // 
            // _TimerTime
            // 
            this._TimerTime.Interval = 150;
            this._TimerTime.Tick += new System.EventHandler(this._TimerTime_Tick);
            // 
            // _FindSearch
            // 
            this._FindSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FindSearch.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._FindSearch.Location = new System.Drawing.Point(615, 65);
            this._FindSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._FindSearch.Name = "_FindSearch";
            this._FindSearch.Pinned = false;
            this._FindSearch.SearchText = "";
            this._FindSearch.Size = new System.Drawing.Size(452, 125);
            this._FindSearch.TabIndex = 3;
            this._FindSearch.Visible = false;
            this._FindSearch.Search += new System.EventHandler(this._FindSearch_Search);
            // 
            // FormDataEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 607);
            this.Controls.Add(this._FindSearch);
            this.Controls.Add(this._GridTable);
            this.Controls.Add(this._ToolStrip);
            this.Controls.Add(this._MenuMain);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this._MenuMain;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormDataEditor";
            this.Text = "Data Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormDataEditor_FormClosing);
            this._ToolStrip.ResumeLayout(false);
            this._ToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._GridTable)).EndInit();
            this._MenuMain.ResumeLayout(false);
            this._MenuMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip _ToolStrip;
        private System.Windows.Forms.ToolStripLabel _LabelFilter;
        private System.Windows.Forms.ToolStripTextBox _TextFilter;
        private System.Windows.Forms.DataGridView _GridTable;
        private System.Windows.Forms.MenuStrip _MenuMain;
        private System.Windows.Forms.ToolStripMenuItem _MenuTable;
        private System.Windows.Forms.ToolStripMenuItem _MenuRefresh;
        private System.Windows.Forms.ToolStripSeparator _MenuTableBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuClose;
        private System.Windows.Forms.ToolStripMenuItem _MenuEdit;
        private System.Windows.Forms.ToolStripMenuItem _MenuHelp;
        private System.Windows.Forms.ToolStripMenuItem _MenuCut;
        private System.Windows.Forms.ToolStripMenuItem _MenuCopy;
        private System.Windows.Forms.ToolStripMenuItem _MenuPaste;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem _MenuFind;
        private System.Windows.Forms.ToolStripMenuItem _MenuFindNext;
        private System.Windows.Forms.ToolStripMenuItem _MenuShowHelp;
        private System.Windows.Forms.Timer _TimerTime;
        private System.Windows.Forms.ToolStripMenuItem _MenuUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private Util.Controls.FindControl _FindSearch;
    }
}