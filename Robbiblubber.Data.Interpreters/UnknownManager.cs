﻿using System;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class implements a dummy manager for unknown database providers.</summary>
    public sealed class UnknownManager: IProviderManager
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Control.</summary>
        private IProviderControl _Control = null;




        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProviderManager                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider name.</summary>
        public string ProviderName
        {
            get { return "Unknown"; }
        }


        /// <summary>Gets the manager display name.</summary>
        public string DisplayName
        {
            get { return "sqlfwx::prov.unknown".Localize("Unknown"); }
        }


        /// <summary>Gets the provider control.</summary>

        public IProviderControl Control
        {
            get
            {
                if(_Control == null) { _Control = new UnknownProviderControl(); }

                return _Control;
            }
        }


        /// <summary>Gets the provider icon.</summary>
        public Image ProviderIcon
        {
            get { return Resources.unknown_db; }
        }


        /// <summary>Gets the connction icon.</summary>
        public Image ConnectionIcon
        {
            get { return Resources.unknown_db; }
        }


        /// <summary>Gets if the provider manager is valid.</summary>
        public bool Valid
        {
            get { return false; }
        }


        /// <summary>Spawns a new provider.</summary>
        /// <param name="id">ID.</param>
        /// <param name="parent">Parent folder.</param>
        /// <returns>Provider.</returns>
        public ProviderItem Spawn(string id, Folder parent)
        {
            return null;
        }


        /// <summary>Spawns a saved provider.</summary>
        /// <param name="sec">Configuration data.</param>
        /// <param name="parent">Parent folder.</param>
        /// <returns>Provider.</returns>
        public ProviderItem Spawn(DdpElement sec, Folder parent)
        {
            return null;
        }
    }
}