﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class implements the connection string window.</summary>
    public partial class FormConnectionString: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormConnectionString()
        {
            InitializeComponent();

            this.Localize();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="connectionString">Connection string.</param>
        public FormConnectionString(string connectionString): this()
        {
            ConnectionString = connectionString;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the connection string.</summary>
        public string ConnectionString
        {
            get { return _TextConnectionString.Text; }
            set { _TextConnectionString.Text = value; }
        }
    }
}
