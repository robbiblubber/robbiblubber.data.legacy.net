﻿using System;
using System.Data;

using Robbiblubber.Util;
using Robbiblubber.Data.Providers;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class represents an unknown provider.</summary>
    public class UnknownProviderItem: ProviderItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected static memvers                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Unknown manager instance.</summary>
        protected static UnknownManager _Manager = null;


        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="baseProviderName">Base provider name.</param>
        /// <param name="id">ID.</param>
        /// <param name="parent">Parent folder.</param>
        public UnknownProviderItem(string baseProviderName, string id, Folder parent): base(baseProviderName, id, parent)
        {}



        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="baseProvider">Base provider.</param>
        /// <param name="id">ID.</param>
        /// <param name="parent">Parent folder.</param>
        public UnknownProviderItem(IProvider baseProvider, string id, Folder parent): base(baseProvider, id, parent)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="sec">Configuration data.</param>
        /// <param name="parent">Parent folder.</param>
        public UnknownProviderItem(DdpElement sec, Folder parent): base(sec, parent)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProvider                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider name.</summary>
        public override string ProviderName
        {
            get { return GetType().FullName; }
        }


        /// <summary>Gets or sets the provider configuration data.</summary>
        public override string Data
        {
            get { return ""; }
            set {}
        }


        /// <summary>Gets or sets an enhanced connection string.</summary>
        public override string EnhancedString
        {
            get { return ""; }
            set {}
        }


        /// <summary>Gets the current connection.</summary>
        public override IDbConnection Connection
        {
            get { return null; }
        }


        /// <summary>Gets the SQL parser for this provider.</summary>
        public override IParser Parser
        {
            get { return null; }
        }


        /// <summary>Gets or sets the current transaction.</summary>
        public override IDbTransaction Transaction
        {
            get { return null; }
            set {}
        }


        /// <summary>Connects to the database.</summary>
        public override void Connect()
        {}


        /// <summary>Disconnects from the database.</summary>
        public override void Disconnect()
        {}


        /// <summary>Tests the connection.</summary>
        public override void Test()
        {}


        /// <summary>Creates a data adapter.</summary>
        /// <param name="sql">SQL statement.</param>
        /// <returns>Data adapter.</returns>
        public override IDbDataAdapter CreateAdapter(string sql)
        {
            return null;
        }


        /// <summary>Creates a database command.</summary>
        /// <param name="sql">SQL statement.</param>
        /// <returns>Command object.</returns>
        public override IDbCommand CreateCommand(string sql = null)
        {
            return null;
        }


        /// <summary>Gets specific provider data.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Data.</returns>
        public override object GetData(string key)
        {
            return null;
        }


        /// <summary>Gets the provider manager.</summary>
        public override IProviderManager Manager
        {
            get
            {
                if(_Manager == null) { _Manager = new UnknownManager(); }

                return _Manager;
            }
        }


        /// <summary>Gets the items in this database.</summary>
        public override DbItem[] Items
        {
            get
            {
                return null;
            }
        }
    }
}
