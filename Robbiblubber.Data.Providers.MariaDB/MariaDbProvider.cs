﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using MySqlConnector;

using Robbiblubber.Util;
using Robbiblubber.Data.DDL;



namespace Robbiblubber.Data.Providers.MariaDB
{
    /// <summary>This class implements the MariaDb database provider.</summary>
    public class MariaDbProvider: ProviderBase, IProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MariaDbProvider(): base()
        {
            _Parser = new MariaDbParser();
        }



        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Provider configuration data.</param>
        public MariaDbProvider(string data) : base(data)
        {
            _Parser = new MariaDbParser();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the server.</summary>
        public string Server { get; set; }


        /// <summary>Gets or sets the database name.</summary>
        public string Database { get; set; }


        /// <summary>Gets or sets the user ID.</summary>
        public string UserID { get; set; }


        /// <summary>Gets or sets the password.</summary>
        [PasswordPropertyTextAttribute]
        public string Password { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the table DDL for a number of tables.</summary>
        /// <param name="dbName">Database name.</param>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table DDL.</returns>
        public virtual IDDLTable[] GetTables(string dbName, string filter)
        {
            List<IDDLTable> rval = new List<IDDLTable>();
            foreach(string i in GetTablesNames(dbName, filter)) { rval.Add(GetTable(i)); }

            return rval.ToArray();
        }


        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="dbName">Database name.</param>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        public virtual IDDLTable GetTable(string dbName, string tableName)
        {
            return GetTable(dbName + "." + tableName);
        }


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="dbName">Database name.</param>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        public virtual string[] GetTablesNames(string dbName, string filter)
        {
            if(filter == null) { filter = "%"; }
            filter = filter.Replace("*", "%").Replace("?", "_");

            IDbCommand cmd;
            List<string> rval = new List<string>();

            if(dbName == null)
            {
                cmd = CreateCommand("SELECT DATABASE()");
                dbName = (string) cmd.ExecuteScalar();
                Disposal.Dispose(cmd);
            }

            cmd = CreateCommand();
            cmd.CommandText = "SHOW FULL TABLES IN " + dbName + " WHERE TABLE_TYPE LIKE '%TABLE%' AND TABLES_IN_" + dbName.ToUpper() + " LIKE ':f'";
            cmd.AddParameter(":f", filter);
            IDataReader re = cmd.ExecuteReader();

            while(re.Read())
            {
                rval.Add(dbName + "." + re.GetString(0));
            }
            Disposal.Dispose(re, cmd);

            return rval.ToArray();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLProvider                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the provider configuration data.</summary>
        public override string Data
        {
            get
            {
                Ddp ddp = new Ddp();

                ddp["provider"].Value = ProviderName;
                ddp["server"].Value = Server;
                ddp["database"].Value = Database;
                ddp["userid"].Value = UserID;
                ddp["password"].Value = Password;
                ddp["enhanced"].Value = EnhancedString;

                return ddp.ToString();
            }
            set
            {
                Ddp ddp = new Ddp(value);

                Server = ddp["server"];
                Database = ddp["database"];
                UserID = ddp["userid"];
                Password = ddp["password"];
                EnhancedString = ddp["enhanced"];
            }
        }


        /// <summary>Connects to the database.</summary>
        public override void Connect()
        {
            _Connection = new MySqlConnection("server=" + Server + ";database=" + Database + ";uid=" + UserID + ";pwd=" + Password + ";");
            _Connection.Open();
        }


        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        public override IDDLTable GetTable(string tableName)
        {
            string[] tab = tableName.Split('.');

            DDLTable rval = new DDLBuilder(Parser).AddTable(tableName);

            IDbCommand cmd = CreateCommand("SHOW FULL TABLES IN " + tab[0]);
            IDataReader re = cmd.ExecuteReader();

            List<string> tmp = new List<string>();
            while(re.Read())
            {
                rval.AddColumn(re.GetString(0), ((MariaDbParser) Parser).ToSQLDataType(re.GetString(1)), re.GetString(2).ToUpper() == "NO", re.GetString(4));

                if(re.GetString(3).ToUpper() == "PRI") { tmp.Add(re.GetString(0)); }
            }
            Disposal.Dispose(re, cmd);

            if(tmp.Count > 0) rval.AddPrimaryKey(tmp.ToArray());

            cmd = CreateCommand("SELECT CONSTRAINT_NAME, COLUMN_NAME, REFERENCED_TABLE_SCHEMA, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_SCHEMA = :dbn AND TABLE_NAME = :tab AND REFERENCED_TABLE_NAME IS NOT NULL");
            cmd.AddParameter(":dbn", tab[0]);
            cmd.AddParameter(":tab", tab[1]);
            re = cmd.ExecuteReader();

            while(re.Read())
            {
                rval.AddNamedForeignKey(re.GetString(0), re.GetString(1), re.GetString(2) + "." + re.GetString(3), re.GetString(4));
            }
            Disposal.Dispose(re, cmd);

            cmd = CreateCommand("SELECT DISTINCT INDEX_NAME, NON_UNIQUE FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA = :dbn AND TABLE_NAME = :tab ORDER BY INDEX_NAME");
            cmd.AddParameter(":dbn", tab[0]);
            cmd.AddParameter(":tab", tab[1]);
            re = cmd.ExecuteReader();

            List<Tuple<string, bool>> idx = new List<Tuple<string, bool>>();
            while(re.Read())
            {
                idx.Add(new Tuple<string, bool>(re.GetString(0), re.GetInt32(1) == 0));
            }
            Disposal.Dispose(re, cmd);

            foreach(Tuple<string, bool> i in idx)
            {
                cmd = CreateCommand("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA = :dbn AND TABLE_NAME = :tab AND INDEX_NAME = :idx ORDER BY SEQ_IN_INDEX");
                cmd.AddParameter(":dbn", tab[0]);
                cmd.AddParameter(":tab", tab[1]);
                cmd.AddParameter(":idx", i.Item1);
                re = cmd.ExecuteReader();

                List<string> cols = new List<string>();
                while(re.Read())
                {
                    cols.Add(re.GetString(0));
                }
                Disposal.Dispose(re, cmd);

                if(i.Item2) { rval.AddNamedUniqueIndex(i.Item1, cols.ToArray()); } else { rval.AddNamedIndex(i.Item1, cols.ToArray()); }
            }

            return rval;
        }


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        public override string[] GetTablesNames(string filter)
        {
            string[] tab = (filter.Contains(".") ? filter.Split('.') : (new string[] { null, filter }));
            return GetTablesNames(tab[0], tab[1]);
        }


        /// <summary>Gets the table DDL for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table DDL.</returns>
        public override IDDLTable[] GetTables(string filter)
        {
            string[] tab = (filter.Contains(".") ? filter.Split('.') : (new string[] { null, filter }));
            return GetTables(tab[0], tab[1]);
        }
    }
}
