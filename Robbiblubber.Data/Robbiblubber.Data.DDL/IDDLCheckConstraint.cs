﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>Classes representing a DDL check constraint implement this interface.</summary>
    public interface IDDLCheckConstraint: IDDLConstraint
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the constraint condition.</summary>
        string Condition { get; }
    }
}
