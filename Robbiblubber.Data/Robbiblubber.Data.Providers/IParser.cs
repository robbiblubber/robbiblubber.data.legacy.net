﻿using System;
using System.Data;

using Robbiblubber.Data.DDL;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Providers
{
    /// <summary>SQL parsers implement this interface.</summary>
    public interface IParser
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a well-formatted query function call for the current timestamp.</summary>
        /// <returns>Timestamp query string.</returns>
        string CurrentTimestampFunction { get; }


        /// <summary>Gets the database concatination operator.</summary>
        string ConcatOperator { get; }


        /// <summary>Gets the database table as operator.</summary>
        string AsOperator { get; }


        /// <summary>Gets the add operator.</summary>
        string AddOperator { get; }


        /// <summary>Gets the subtraction operator.</summary>
        string SubtractOperator { get; }


        /// <summary>Gets the multiplication operator.</summary>
        string MultiplyOperator { get; }


        /// <summary>Gets the division operator.</summary>
        string DivideOperator { get; }


        /// <summary>Gets the modulo operator.</summary>
        string ModuloOperator { get; }


        /// <summary>Gets the negation operator.</summary>
        string NegateOperator { get; }


        /// <summary>Gets the binary or operator.</summary>
        string BinaryOrOperator { get; }


        /// <summary>Gets the binary and operator.</summary>
        string BinaryAndOperator { get; }


        /// <summary>Gets the exclusive or operator.</summary>
        string ExOrOperator { get; }


        /// <summary>Gets the not operator.</summary>
        string NotOperator { get; }


        /// <summary>Gets the logical or operator.</summary>
        string OrOperator { get; }


        /// <summary>Gets the logical and operator.</summary>
        string AndOperator { get; }
        

        /// <summary>Gets the equal operator.</summary>
        string EqualOperator { get; }


        /// <summary>Gets the equals null operator.</summary>
        string EqualNullOperator { get; }


        /// <summary>Gets the not equal operator.</summary>
        string NotEqualOperator { get; }


        /// <summary>Gets the not equals null operator.</summary>
        string NotEqualNullOperator { get; }


        /// <summary>Gets the greater than operator.</summary>
        string GreaterOperator { get; }


        /// <summary>Gets the greater than or equal operator.</summary>
        string GreaterOrEqualOperator { get; }


        /// <summary>Gets the less than operator.</summary>
        string LessOperator { get; }


        /// <summary>Gets the less than or equal operator.</summary>
        string LessOrEqualOperator { get; }
        

        /// <summary>Gets the specific FROM part for dual selects.</summary>
        string FromDual { get; }


        /// <summary>Gets a well-formatted query function call for last auto increment value.</summary>
        string LastValue { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parses a source string.</summary>
        /// <param name="source">Source string.</param>
        /// <returns>Parsed SQL statements.</returns>
        ISQLStatement[] Parse(string source);


        /// <summary>Creates a query object.</summary>
        /// <returns>Query.</returns>
        ISQLQuery CreateQuery();


        /// <summary>Reads a field value as a string.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="i">Index.</param>
        /// <param name="style">Data format.</param>
        /// <param name="multiLine">Multi-line flag.</param>
        /// <param name="width">Line width.</param>
        /// <returns>String.</returns>
        string GetDataString(IDataReader re, int i, SQLComplexDataTypeFormatStyle style, bool multiLine, int width);


        /// <summary>Reads a field value as a string.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="i">Index.</param>
        /// <returns>String.</returns>
        string GetDataString(IDataReader re, int i);


        /// <summary>Returns if an object is a string.</summary>
        /// <param name="obj">Object</param>
        /// <returns>Returns FALSE if the object is a numeric datatype, otherwise returns TRUE.</returns>
        bool IsString(object obj);


        /// <summary>Returns a string representation of an object.</summary>
        /// <param name="obj">Object</param>
        /// <returns>String.</returns>
        /// <param name="style">Data format.</param>
        /// <param name="multiLine">Multi-line flag.</param>
        /// <param name="width">Line width.</param>
        string ToString(object obj, SQLComplexDataTypeFormatStyle style, bool multiLine, int width);


        /// <summary>Returns a string representation of an object.</summary>
        /// <param name="obj">Object</param>
        /// <returns>String.</returns>
        string ToString(object obj);


        /// <summary>Returns a well-formatted query function call for next value from the given database sequence.</summary>
        /// <param name="sequence">Sequence Name.</param>
        /// <returns>Sequence query string.</returns>
        string SequenceNextValue(string sequence);


        /// <summary>Adds a time interval to a database date/time expression.</summary>
        /// <param name="value">Database expression that represents a date/time value.</param>
        /// <param name="interval">Time interval to add.</param>
        /// <returns>Database timestamp operation string.</returns>
        string AddTimeInterval(string value, TimeSpan interval);


        /// <summary>Returns the database lowercase value for a given expression.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Lowercase expression.</returns>
        string Lowercase(string expression);


        /// <summary>Returns the database uppercase value for a given expression.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Uppercase expression.</returns>
        string Uppercase(string expression);


        /// <summary>Creates a bind variable name using the given variable name.</summary>
        /// <param name="name">Variable name.</param>
        /// <returns>Bind variable name.</returns>
        string ToBindVariableName(string name);


        /// <summary>Returns a database data type name for a data type.</summary>
        /// <param name="dataType">Data type.</param>
        /// <returns>Data type name.</returns>
        string ToDataTypeName(SQLDataType dataType);


        /// <summary>Parses a DDL statement.</summary>
        /// <param name="ddl">DDL statement.</param>
        /// <returns>Statement list.</returns>
        string[] ParseDDL(IDDLStatement ddl);
    }
}
