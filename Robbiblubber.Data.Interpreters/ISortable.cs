﻿using System;

using Robbiblubber.Util;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>Sortable objects implement this interface.</summary>
    public interface ISortable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the object ID.</summary>
        string ID { get; set; }


        /// <summary>Gets or sets the parent folder.</summary>
        Folder Parent { get; set; }


        /// <summary>Gets or sets the object name.</summary>
        string Name { get; set; }


        /// <summary>Gets or sets the sort index.</summary>
        int SortIndex { get; set; }


        /// <summary>Gets if the object can move up.</summary>
        bool CanMoveUp { get; }


        /// <summary>Gets if the object can move down.</summary>
        bool CanMoveDown { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Moves the item up.</summary>
        void MoveUp();


        /// <summary>Moves the item down.</summary>
        void MoveDown();


        /// <summary>Exports the item.</summary>
        /// <param name="cfg">Configuration file.</param>
        void Export(Ddp cfg);
    }
}
