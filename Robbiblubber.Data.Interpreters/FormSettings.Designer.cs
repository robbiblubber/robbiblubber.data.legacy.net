﻿namespace Robbiblubber.Data.Interpreters
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSettings));
            this._ButtonOK = new System.Windows.Forms.Button();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._TabSettings = new System.Windows.Forms.TabControl();
            this.SuspendLayout();
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(406, 409);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(147, 29);
            this._ButtonOK.TabIndex = 1;
            this._ButtonOK.Tag = "sqlfwx::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(559, 409);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(147, 29);
            this._ButtonCancel.TabIndex = 2;
            this._ButtonCancel.Tag = "sqlfwx::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // _TabSettings
            // 
            this._TabSettings.Location = new System.Drawing.Point(12, 12);
            this._TabSettings.Name = "_TabSettings";
            this._TabSettings.SelectedIndex = 0;
            this._TabSettings.Size = new System.Drawing.Size(705, 379);
            this._TabSettings.TabIndex = 0;
            // 
            // FormSettings
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(728, 458);
            this.Controls.Add(this._TabSettings);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._ButtonCancel);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSettings";
            this.Tag = "sqlfwx::udiag.settings.caption";
            this.Text = "Settings";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _ButtonOK;
        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.TabControl _TabSettings;
    }
}