﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle trigger.</summary>
    public class OraTrigger: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Trigger name.</summary>
        private string _Name;

        /// <summary>Trigger schema.</summary>
        private string _Schema;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="schema">Trigger schema.</param>
        internal OraTrigger(OraDatabase database, OraTriggersFolder folder, string name, string schema)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
            _Schema = schema;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public OraDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public OraTriggersFolder Folder { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                return Resources.trigger;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                return "ora::trigger";
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                try
                {
                    IDbCommand cmd = Database.Provider.CreateCommand("SELECT DBMS_MetaData.Get_DDL('TRIGGER', '" + Name +"', '" + _Schema + "') FROM SYS.DUAL");
                    string rval = (string) cmd.ExecuteScalar();
                    Disposal.Dispose(cmd);

                    return rval.Trim(' ', '\r', '\n', '\t', ';') + ";";
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX02437", ex); }

                return "DDL not accessible.";
            }
        }
    }
}
