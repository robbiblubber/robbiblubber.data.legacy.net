﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Driver;

using Robbiblubber.Data.Mongo;
using Robbiblubber.Data.Providers;



namespace Robbiblubber.Data.Explorations.MongoDB
{
    /// <summary>This class provides extension methods for working with the underlying MongoClient of a MongoProvider.</summary>
    internal static class __ProviderExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the MongoDB client.</summary>
        /// <param name="provider">Provider.</param>
        /// <returns>Client instance.</returns>
        public static IMongoClient GetClient(this IProvider provider)
        {
            return (IMongoClient) ((MongoConnection) provider.Connection).Client;
        }
    }
}
