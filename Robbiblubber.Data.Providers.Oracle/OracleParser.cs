﻿using System;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Providers.Oracle
{
    /// <summary>This class provides a Oracle-specific implementation of the SQLParser class.</summary>
    public class OracleParser: Parser, IParser
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Returns a SQL data type for a database data type expression.</summary>
        /// <param name="dbDataType">Database data type string.</param>
        /// <returns>SQL data type.</returns>
        public SQLDataType ToSQLDataType(string dbDataType)
        {
            switch(dbDataType.ToUpper())
            {
                case "BINARY_FLOAT": return SQLDataType.FLOAT;
                case "BINARY_DOUBLE": return SQLDataType.DOUBLE;
                case "DATA": return SQLDataType.TIMESTAMP;
                case "CLOB": case "NLOB": return SQLDataType.TEXT;
            }

            if(dbDataType.ToUpper().StartsWith("NUMBER"))
            {
                if(dbDataType.Contains("("))
                {
                    if(dbDataType.Contains(",")) return SQLDataType.DECIMAL;
                    if(int.Parse(dbDataType.ToUpper().Trim("ABCDEFGHIJKLMNOPQRSTUVWXYZ()".ToCharArray())) < 10) return SQLDataType.INT32;

                    return SQLDataType.INT64;
                }

            }

            if(dbDataType.ToUpper().StartsWith("TIMESTAMP"))
            {
                return SQLDataType.TIMESTAMP;
            }

            if(dbDataType.ToUpper().StartsWith("VARC") || dbDataType.ToUpper().StartsWith("CHAR"))
            {
                string l = dbDataType.ToUpper().Trim("ABCDEFGHIJKLMNOPQRSTUVWXYZ()".ToCharArray());
                if(string.IsNullOrWhiteSpace(l)) { return SQLDataType.STRING; }

                return SQLDataType.String(int.Parse(l));
            }

            return SQLDataType.TEXT;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLParser                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the specific FROM part for dual selects.</summary>
        public override string FromDual
        {
            get { return "FROM DUAL"; }
        }
    }
}
