﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle cluster.</summary>
    public class OraCluster: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Cluster name.</summary>
        protected string _Name;


        /// <summary>Definition.</summary>
        protected string _Definition = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        internal OraCluster(OraDatabase database, OraClustersFolder folder, string name)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public OraDatabase Database { get; protected set; }


        /// <summary>Gets the parent folder.</summary>
        public OraClustersFolder Folder { get; private set; }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.cluster; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "ora::cluster"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            _Children.Add(new OraClusterColumnsFolder(Database, this));
            _Children.Add(new OraClusterTablesFolder(Database, this));
        }
        

        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                try
                {
                    IDbCommand cmd = Database.Provider.CreateCommand("SELECT DBMS_MetaData.Get_DDL('CLUSTER', '" + Name +"', '" + Folder.Schema.Name + "') FROM SYS.DUAL");
                    string rval = (string) cmd.ExecuteScalar();
                    Disposal.Dispose(cmd);

                    return rval.Trim(' ', '\r', '\n', '\t', ';') + ";";
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX02405", ex); }

                return "DDL not accessible.";
            }
        }


        /// <summary>Gets the qualified name for this object.</summary>
        public override string QualifiedName
        {
            get { return Folder.Schema.Name + "." + Name; }
        }
    }
}
