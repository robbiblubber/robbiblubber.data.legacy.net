﻿using System;
using System.Drawing;

using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util;




namespace Robbiblubber.Data.Interpreters.ApacheCassandra
{
    /// <summary>This class implements the Cassandra manager.</summary>
    public sealed class CassandraManager: ProviderManager, IProviderManager
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Control.</summary>
        private IProviderControl _Control = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProviderManager                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider name.</summary>
        public override string ProviderName
        {
            get 
            { 
                return "Robbiblubber.Data.Providers.ApacheCassandra.CassandraProvider"; 
            }
        }


        /// <summary>Gets the manager display name.</summary>
        public override string DisplayName
        {
            get { return "Cassandra"; }
        }


        /// <summary>Gets the provider control.</summary>

        public override IProviderControl Control
        {
            get
            {
                if(_Control == null) { _Control = new CassandraProviderControl(); }

                return _Control;
            }
        }


        /// <summary>Gets the provider icon.</summary>
        public override Image ProviderIcon
        {
            get { return Resources.cassandra; }
        }


        /// <summary>Gets the connction icon.</summary>
        public override Image ConnectionIcon
        {
            get { return Resources.casdb; }
        }
    }
}
