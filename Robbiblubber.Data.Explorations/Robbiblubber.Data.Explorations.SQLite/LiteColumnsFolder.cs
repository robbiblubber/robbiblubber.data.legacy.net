﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.SQLite
{
    /// <summary>This class represents a SQLite columns folder.</summary>
    public class LiteColumnsFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="table">Parent table.</param>
        internal LiteColumnsFolder(LiteDatabase database, LiteTable table)
        {
            Database = database;
            _Parent = Table = table;
            _Name = "sqlfwx::tree.folder.columns".Localize("Columns");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public LiteDatabase Database { get; private set; }


        /// <summary>Gets the parent table.</summary>
        public LiteTable Table { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "lite::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            List<string> fk = new List<string>();
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("PRAGMA Foreign_Key_List(" + Table.Name + ")");
                re = cmd.ExecuteReader();
                while(re.Read())
                {
                    fk.Add(re.GetString(3));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX00102", ex); }

            Disposal.Dispose(re, cmd);

            try
            {
                cmd = Database.Provider.CreateCommand("PRAGMA Table_Info(" + Table.Name + ")");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new LiteColumn(Database, this, re.GetString(1), re.GetString(2), (re.GetInt32(5) != 0), fk.Contains(re.GetString(1)), (re.GetInt32(3) != 0)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX00103", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
