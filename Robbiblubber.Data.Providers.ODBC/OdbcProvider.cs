﻿using System;
using System.ComponentModel;
using System.Data.Odbc;

using Robbiblubber.Data.DDL;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Providers.ODBC
{
    /// <summary>This class implements the SQL Server database provider.</summary>
    public class OdbcProvider: ProviderBase, IProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public OdbcProvider(): base()
        {}



        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Provider configuration data.</param>
        public OdbcProvider(string data): base(data)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the server.</summary>
        public string DSN { get; set; }


        /// <summary>Gets or sets the user ID.</summary>
        public string UserID { get; set; }


        /// <summary>Gets or sets the password.</summary>
        [PasswordPropertyTextAttribute]
        public string Password { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLProvider                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the provider configuration data.</summary>
        public override string Data
        {
            get
            {
                Ddp ddp = new Ddp();

                ddp["provider"].Value = ProviderName;
                ddp["dsn"].Value = DSN;
                ddp["file"].Value = DSN;
                ddp["userid"].Value = UserID;
                ddp["password"].Value = Password;
                ddp["enhanced"].Value = EnhancedString;

                return ddp.ToString();
            }
            set
            {
                Ddp ddp = new Ddp(value);

                DSN = ddp["dsn"];
                UserID = ddp["userid"];
                Password = ddp["password"];
                EnhancedString = ddp["enhanced"];
            }
        }


        /// <summary>Connects to the database.</summary>
        public override void Connect()
        {
            if(EnhancedString.Trim() == "")
            {
                _Connection = new OdbcConnection("DSN=" + DSN + ";" + ((!string.IsNullOrWhiteSpace(UserID)) ? ("UID=" + UserID + ";") : "") + ((!string.IsNullOrWhiteSpace(Password)) ? ("Pwd=" + Password + ";") : ""));
            }
            else
            {
                _Connection = new OdbcConnection(EnhancedString);
            }
            _Connection.Open();
        }


        /// <summary>Gets specific provider data.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Data.</returns>
        public override object GetData(string key)
        {
            if(key == "driver")
            {
                return ((OdbcConnection) _Connection).Driver;
            }

            return base.GetData(key);
        }


        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        public override IDDLTable GetTable(string tableName)
        {
            throw new NotSupportedException();
        }


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        public override string[] GetTablesNames(string filter)
        {
            throw new NotSupportedException();
        }
    }
}
