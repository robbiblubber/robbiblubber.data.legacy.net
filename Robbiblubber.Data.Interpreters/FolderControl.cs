﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This class implements the folder control.</summary>
    public partial class FolderControl: UserControl, IProviderControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Instance.</summary>
        private static FolderControl _Instance = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Tree node.</summary>
        private TreeNode _Node = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FolderControl()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a folder control instance.</summary>
        public static FolderControl Instance
        {
            get
            {
                if(_Instance == null) { _Instance = new FolderControl(); }

                return _Instance;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Provider name changed.</summary>
        private void _TextName_TextChanged(object sender, EventArgs e)
        {
            Node.Text = ((Folder) _Node.Tag).Name = _TextName.Text;
        }
        

        /// <summary>Button "Add folder" click.</summary>
        private void _ButtonAddFolder_Click(object sender, EventArgs e)
        {
            ((FormDatabases) _Node.TreeView.Parent).AddFolder();
        }


        /// <summary>Button "Add connection" click.</summary>
        private void _ButtonAddConnection_Click(object sender, EventArgs e)
        {
            ((FormDatabases) _Node.TreeView.Parent).AddConnection();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProviderControl                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the database provider.</summary>
        public ProviderItem Provider
        {
            get { return null; }
            set {}
        }


        /// <summary>Gets or sets the tree node.</summary>
        public TreeNode Node
        {
            get { return _Node; }
            set
            {
                _Node = value;

                _TextName.Text = ((Folder) _Node.Tag).Name;
                _LabelName.Visible = _TextName.Visible = (((Folder) _Node.Tag).Parent != null);
            }
        }
    }
}
