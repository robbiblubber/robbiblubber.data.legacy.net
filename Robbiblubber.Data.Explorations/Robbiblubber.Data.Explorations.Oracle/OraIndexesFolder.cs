﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle indexes folder.</summary>
    public class OraIndexesFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="table">Parent table.</param>
        internal OraIndexesFolder(OraDatabase database, OraTable table)
        {
            Database = database;
            _Parent = Table = table;
            _Name = "Indexes";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public OraDatabase Database { get; private set; }


        /// <summary>Gets the parent table.</summary>
        public OraTable Table { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "ora::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("SELECT INDEX_NAME, UNIQUENESS FROM SYS.ALL_INDEXES WHERE TABLE_OWNER = '" + Table.Folder.Schema.Name + "' AND TABLE_NAME = '" + Table.Name + "' ORDER BY INDEX_NAME");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new OraIndex(Database, this, re.GetString(0), re.GetString(1) == "UNIQUE"));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0241c", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
