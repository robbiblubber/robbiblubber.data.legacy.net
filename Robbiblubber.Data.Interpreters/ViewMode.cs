﻿using System;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>View mode enumeration.</summary>
    public enum ViewMode
    {
        /// <summary>Text view mode.</summary>
        TEXT = 0,
        /// <summary>Grid view mode.</summary>
        GRID = 1
    }
}
