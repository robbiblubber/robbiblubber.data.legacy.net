﻿namespace Robbiblubber.Data.Interpreters.ODBC
{
    partial class OdbcProviderControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._TextDSN = new System.Windows.Forms.TextBox();
            this._ButtonTest = new System.Windows.Forms.Button();
            this._LabelDSN = new System.Windows.Forms.Label();
            this._LabelName = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._LabelUserID = new System.Windows.Forms.Label();
            this._TextUserID = new System.Windows.Forms.TextBox();
            this._LabelPassword = new System.Windows.Forms.Label();
            this._TextPassword = new System.Windows.Forms.TextBox();
            this._LinkEnhaced = new Robbiblubber.Util.Controls.LinkControl();
            this.SuspendLayout();
            // 
            // _TextDSN
            // 
            this._TextDSN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDSN.Location = new System.Drawing.Point(30, 85);
            this._TextDSN.Name = "_TextDSN";
            this._TextDSN.Size = new System.Drawing.Size(403, 25);
            this._TextDSN.TabIndex = 1;
            this._TextDSN.TextChanged += new System.EventHandler(this._TextDSN_TextChanged);
            // 
            // _ButtonTest
            // 
            this._ButtonTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonTest.Location = new System.Drawing.Point(286, 284);
            this._ButtonTest.Name = "_ButtonTest";
            this._ButtonTest.Size = new System.Drawing.Size(147, 29);
            this._ButtonTest.TabIndex = 4;
            this._ButtonTest.Tag = "sqlfwx::common.button.test";
            this._ButtonTest.Text = "&Test";
            this._ButtonTest.UseVisualStyleBackColor = true;
            this._ButtonTest.Click += new System.EventHandler(this._ButtonTest_Click);
            // 
            // _LabelDSN
            // 
            this._LabelDSN.AutoSize = true;
            this._LabelDSN.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDSN.Location = new System.Drawing.Point(27, 69);
            this._LabelDSN.Name = "_LabelDSN";
            this._LabelDSN.Size = new System.Drawing.Size(32, 13);
            this._LabelDSN.TabIndex = 1;
            this._LabelDSN.Tag = "sqlfwx::udiag.connect.dsn";
            this._LabelDSN.Text = "&DSN:";
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.Location = new System.Drawing.Point(27, 19);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Tag = "sqlfwx::udiag.connect.name";
            this._LabelName.Text = "&Name:";
            // 
            // _TextName
            // 
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(30, 35);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(403, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._TextName_TextChanged);
            // 
            // _LabelUserID
            // 
            this._LabelUserID.AutoSize = true;
            this._LabelUserID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelUserID.Location = new System.Drawing.Point(27, 119);
            this._LabelUserID.Name = "_LabelUserID";
            this._LabelUserID.Size = new System.Drawing.Size(47, 13);
            this._LabelUserID.TabIndex = 2;
            this._LabelUserID.Tag = "sqlfwx::udiag.connect.uid";
            this._LabelUserID.Text = "&User ID:";
            // 
            // _TextUserID
            // 
            this._TextUserID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextUserID.Location = new System.Drawing.Point(30, 135);
            this._TextUserID.Name = "_TextUserID";
            this._TextUserID.Size = new System.Drawing.Size(403, 25);
            this._TextUserID.TabIndex = 2;
            this._TextUserID.TextChanged += new System.EventHandler(this._TextUserID_TextChanged);
            // 
            // _LabelPassword
            // 
            this._LabelPassword.AutoSize = true;
            this._LabelPassword.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelPassword.Location = new System.Drawing.Point(27, 169);
            this._LabelPassword.Name = "_LabelPassword";
            this._LabelPassword.Size = new System.Drawing.Size(59, 13);
            this._LabelPassword.TabIndex = 3;
            this._LabelPassword.Tag = "sqlfwx::udiag.connect.pwd";
            this._LabelPassword.Text = "&Password:";
            // 
            // _TextPassword
            // 
            this._TextPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextPassword.Location = new System.Drawing.Point(30, 185);
            this._TextPassword.Name = "_TextPassword";
            this._TextPassword.PasswordChar = '*';
            this._TextPassword.Size = new System.Drawing.Size(403, 25);
            this._TextPassword.TabIndex = 3;
            this._TextPassword.UseSystemPasswordChar = true;
            this._TextPassword.TextChanged += new System.EventHandler(this._TextPassword_TextChanged);
            // 
            // _LinkEnhaced
            // 
            this._LinkEnhaced.AutoSize = true;
            this._LinkEnhaced.Cursor = System.Windows.Forms.Cursors.Hand;
            this._LinkEnhaced.DisabledColor = System.Drawing.Color.Gray;
            this._LinkEnhaced.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LinkEnhaced.LinkText = "Enhanced...";
            this._LinkEnhaced.Location = new System.Drawing.Point(30, 287);
            this._LinkEnhaced.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LinkEnhaced.Name = "_LinkEnhaced";
            this._LinkEnhaced.Size = new System.Drawing.Size(72, 21);
            this._LinkEnhaced.TabIndex = 4;
            this._LinkEnhaced.TabStop = false;
            this._LinkEnhaced.Tag = "sqlfwx::common.link.enhanced";
            this._LinkEnhaced.Underline = false;
            this._LinkEnhaced.Click += new System.EventHandler(this._LinkEnhaced_Click);
            // 
            // OdbcProviderControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._LinkEnhaced);
            this.Controls.Add(this._LabelPassword);
            this.Controls.Add(this._TextPassword);
            this.Controls.Add(this._LabelUserID);
            this.Controls.Add(this._TextUserID);
            this.Controls.Add(this._LabelName);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelDSN);
            this.Controls.Add(this._ButtonTest);
            this.Controls.Add(this._TextDSN);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "OdbcProviderControl";
            this.Size = new System.Drawing.Size(463, 338);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _TextDSN;
        private System.Windows.Forms.Button _ButtonTest;
        private System.Windows.Forms.Label _LabelDSN;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.Label _LabelUserID;
        private System.Windows.Forms.TextBox _TextUserID;
        private System.Windows.Forms.Label _LabelPassword;
        private System.Windows.Forms.TextBox _TextPassword;
        private Robbiblubber.Util.Controls.LinkControl _LinkEnhaced;
    }
}
