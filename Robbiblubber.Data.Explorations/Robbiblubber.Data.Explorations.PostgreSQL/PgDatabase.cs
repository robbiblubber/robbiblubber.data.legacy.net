﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.PostgreSQL
{
    /// <summary>This class represents a PostgreSQL database.</summary>
    public class PgDatabase: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Database name.</summary>
        private string _Name;
        
        /// <summary>Determines if the instance is being initialized.</summary>
        private bool _Initial = true;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider">Provider.</param>
        internal PgDatabase(ProviderItem provider)
        {
            Provider = provider;
            Refresh();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider.</summary>
        public ProviderItem Provider { get; private set; }
        
        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.postgresdb; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "pg::database"; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return "CREATE DATABASE " + Name + ";"; }
        }


        /// <summary>Refreshes the item.</summary>
        public override void Refresh()
        {
            IDbCommand cmd = Provider.CreateCommand();
            cmd.CommandText = "SELECT Current_Database()";
            _Name = (string) cmd.ExecuteScalar();
            Disposal.Dispose(cmd);

            _Children = null;

            if(!_Initial)
            {
                if((PgExplorer) Exploration.Instance[Provider] is PgExplorer) { ((PgExplorer) Exploration.Instance[Provider]).__RefreshDefaultKeywords(); }
            }
            _Initial = false;
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            _Children = new List<DbItem>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Provider.CreateCommand();
                cmd.CommandText = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA ORDER BY SCHEMA_NAME";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new PgSchema(this, re.GetString(0)));
                }
            }
            catch(Exception) {}

            Disposal.Dispose(re, cmd);
            
            _Children.Add(new PgUsersFolder(this));
        }
    }
}
