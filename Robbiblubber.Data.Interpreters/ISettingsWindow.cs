﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>A settings window implements this interface.</summary>
    public interface ISettingsWindow
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Shows the dialog.</summary>
        /// <returns>Result.</returns>
        DialogResult ShowDialog();


        /// <summary>Gets a control by key.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Control.</returns>
        Control GetControl(string key);
    }
}
