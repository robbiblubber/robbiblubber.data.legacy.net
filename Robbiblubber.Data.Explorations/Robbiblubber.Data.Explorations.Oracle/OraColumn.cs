﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.Oracle
{
    /// <summary>This class represents an Oracle column.</summary>
    public class OraColumn: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Column name.</summary>
        private string _Name;


        /// <summary>Referenced table.</summary>
        private string _ReferencedTable = null;


        /// <summary>Referenced column.</summary>
        private string _ReferencedColumn = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="dataType">Data type.</param>
        /// <param name="pk">Primary key.</param>
        /// <param name="fk">Foreign key.</param>
        /// <param name="notNull">Not null.</param>
        internal OraColumn(OraDatabase database, OraColumnsFolder folder, string name, string dataType, bool pk, bool fk, bool notNull)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
            DataType = dataType;
            IsPrimaryKey = pk;
            IsForeignKey = fk;
            IsNotNull = notNull;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public OraDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public OraColumnsFolder Folder { get; private set; }


        /// <summary>Gets the data type.</summary>
        public string DataType { get; private set; }


        /// <summary>Gets if the field is a primary key.</summary>
        public bool IsPrimaryKey { get; private set; }


        /// <summary>Gets if the field is a foreign key.</summary>
        public bool IsForeignKey { get; private set; }


        /// <summary>Gets if the field is not nullable.</summary>
        public bool IsNotNull { get; private set; }


        /// <summary>Gets the referenced table.</summary>
        public string ReferencedTable
        {
            get
            {
                _Read();
                return _ReferencedTable;
            }
        }


        /// <summary>Gets the referenced column.</summary>
        public string ReferencedColumn
        {
            get
            {
                _Read();
                return _ReferencedColumn;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Reads referenced table and column.</summary>
        private void _Read()
        {
            if(_ReferencedTable != null) return;

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("SELECT T.OWNER, T.TABLE_NAME, T.COLUMN_NAME FROM SYS.ALL_CONS_COLUMNS T, SYS.ALL_CONS_COLUMNS C, SYS.ALL_CONSTRAINTS X WHERE C.OWNER = '" + Folder.Table.Folder.Schema.Name + "' AND C.TABLE_NAME = '" + Folder.Table.Name + "' AND C.COLUMN_NAME = '" + Name + "' AND C.CONSTRAINT_NAME = X.CONSTRAINT_NAME AND X.CONSTRAINT_TYPE = 'R' AND X.R_CONSTRAINT_NAME = T.CONSTRAINT_NAME");
                re = cmd.ExecuteReader();

                if(re.Read())
                {
                    _ReferencedTable = re.GetString(0) + "." + re.GetString(1);
                    _ReferencedColumn = re.GetString(2);
                }
            }
            catch(Exception ex)
            {
                DebugOp.Dump("SQLFWX02409", ex);

                _ReferencedTable = "?";
                _ReferencedColumn = "?";
            }

            Disposal.Dispose(re, cmd);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                if(Folder.Table is OraView) { return Resources.column_view; }

                if(IsPrimaryKey) { return Resources.column_pk; }
                if(IsForeignKey) { return Resources.column_fk; }
                if(IsNotNull) { return Resources.column_nn; }

                return Resources.column;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                if(Folder.Table is OraView) { return "ora::column/view"; }

                if(IsPrimaryKey) { return "ora::column/pk"; }
                if(IsForeignKey) { return "ora::column/fk"; }
                if(IsNotNull) { return "ora::column/nn"; }

                return "ora::column";
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = Name + "   " + DataType.ToUpper();
                if(IsNotNull)    { rval += "   NOT NULL"; }
                if(IsPrimaryKey) { rval += "   PRIMARY KEY"; }
                if(IsForeignKey) { rval += "   REFERENCES " + ReferencedTable + "(" + ReferencedColumn + ")"; }
                rval += ";";

                return rval;
            }
        }
    }
}
