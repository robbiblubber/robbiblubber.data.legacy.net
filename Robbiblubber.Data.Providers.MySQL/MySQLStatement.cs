﻿using Robbiblubber.Data.SQL;
using System;




namespace Robbiblubber.Data.Providers.MySQL
{
    /// <summary>This class provides a MariaDB-specific implementation of the SQLStatement class.</summary>
    public class MySQLStatement: SQLStatement, ISQLStatement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // cosntructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MySQLStatement() : base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="source">Source.</param>
        public MySQLStatement(string source) : base(source)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLStatement                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the statement is a SELECT query.</summary>
        public override bool IsSelect
        {
            get { return (_LowerSource.StartsWith("select") || _LowerSource.StartsWith("show") || _LowerSource.StartsWith("describe")); }
        }
    }
}
