﻿using Robbiblubber.Data.SQL;
using System;
using System.Collections.Generic;



namespace Robbiblubber.Data.DDL
{
    /// <summary>This class represents a DDL table.</summary>
    public class DDLTable: IDDLTable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>DDL.</summary>
        protected DDLBuilder _Ddl;

        /// <summary>Table name.</summary>
        protected string _TableName;
        
        /// <summary>Columns.</summary>
        protected List<IDDLColumn> _Columns = new List<IDDLColumn>();
        
        /// <summary>Constraints.</summary>
        protected List<IDDLConstraint> _Constraints = new List<IDDLConstraint>();

        /// <summary>Indexes.</summary>
        protected List<IDDLIndex> _Indexes = new List<IDDLIndex>();


        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="ddl">Parent DDL.</param>
        /// <param name="tableName">Table name.</param>
        public DDLTable(DDLBuilder ddl, string tableName)
        {
            _Ddl = ddl;
            _TableName = tableName;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a column to the table.</summary>
        /// <param name="columnName">Column name.</param>
        /// <param name="dataType">Column data type.</param>
        /// <param name="notNull">Not null option.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns></returns>
        public virtual DDLTable AddColumn(string columnName, SQLDataType dataType, bool notNull = false, string defaultValue = null)
        {
            _Columns.Add(new DDLColumn(_TableName, columnName, dataType, notNull, defaultValue));
            return this;
        }


        /// <summary>Adds a primary key to the table.</summary>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        public virtual DDLTable AddPrimaryKey(params string[] columnNames)
        {
            return AddNamedPrimaryKey(null, columnNames);
        }


        /// <summary>Adds a named primary key to the table.</summary>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        public virtual DDLTable AddNamedPrimaryKey(string constraintName, params string[] columnNames)
        {
            foreach(IDDLConstraint i in _Constraints)
            {
                if(i is IDDLPrimaryKeyConstraint) { throw new InvalidOperationException("Cannot add more than one primary key constraint to table."); }
            }

            _Constraints.Add(new DDLPrimaryKeyConstraint(_TableName, constraintName, columnNames));
            return this;
        }


        /// <summary>Adds a foreign key constraint to the table.</summary>
        /// <param name="columnName">Column name.</param>
        /// <param name="referencedTable">Referenced table name.</param>
        /// <param name="referencedColumn">Referenced column name.</param>
        /// <param name="cascadeDelete">Delete cascade flag.</param>
        /// <returns>DDL table.</returns>
        public virtual DDLTable AddForeignKey(string columnName, string referencedTable, string referencedColumn, bool cascadeDelete = false)
        {
            return AddNamedForeignKey(null, columnName, referencedTable, referencedColumn, cascadeDelete);
        }


        /// <summary>Adds a named foreign key constraint to the table.</summary>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="columnName">Column name.</param>
        /// <param name="referencedTableName">Referenced table name.</param>
        /// <param name="referencedColumnName">Referenced column name.</param>
        /// <param name="cascadeDelete">Delete cascade flag.</param>
        /// <returns>DDL Table.</returns>
        public virtual DDLTable AddNamedForeignKey(string constraintName, string columnName, string referencedTableName, string referencedColumnName, bool cascadeDelete = false)
        {
            _Constraints.Add(new DDLForeignKeyConstraint(_TableName, constraintName, columnName, referencedTableName, referencedColumnName, cascadeDelete));
            return this;
        }


        /// <summary>Adds a check constraint to the table.</summary>
        /// <param name="condition">Check condition.</param>
        /// <returns>DDL Table.</returns>
        public virtual DDLTable AddCheck(string condition)
        {
            return AddNamedCheck(null, condition);
        }


        /// <summary>Adds a named check constraint to the table.</summary>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="condition">Check condition.</param>
        /// <returns>DDL Table.</returns>
        public virtual DDLTable AddNamedCheck(string constraintName, string condition)
        {
            _Constraints.Add(new DDLCheckConstraint(_TableName, constraintName, condition));
            return this;
        }


        /// <summary>Adds an index to the table.</summary>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        public virtual IDDLTable AddIndex(params string[] columnNames)
        {
            _Indexes.Add(new DDLIndex(_Ddl, _TableName, false, columnNames));
            return this;
        }


        /// <summary>Adds an index to the table.</summary>
        /// <param name="indexName">Index name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        public virtual IDDLTable AddNamedIndex(string indexName, params string[] columnNames)
        {
            _Indexes.Add(new DDLIndex(_Ddl, indexName, _TableName, false, columnNames));
            return this;
        }


        /// <summary>Adds a unique index to the table.</summary>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        public virtual IDDLTable AddUniqueIndex(params string[] columnNames)
        {
            _Indexes.Add(new DDLIndex(_Ddl, _TableName, true, columnNames));
            return this;
        }


        /// <summary>Adds a named unique index to the table.</summary>
        /// <param name="indexName">Index name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        public virtual IDDLTable AddNamedUniqueIndex(string indexName, params string[] columnNames)
        {
            _Indexes.Add(new DDLIndex(_Ddl, indexName, _TableName, true, columnNames));
            return this;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDDLStatement                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a statements list for this DDL.</summary>
        public virtual string[] Text
        {
            get { return _Ddl._Parser.ParseDDL(this); }
        }


        /// <summary>Gets a list of child DDL statements.</summary>
        IDDLStatement[] IDDLStatement.ChildStatements
        {
            get { return _Indexes.ToArray(); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDDLTable                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the table name.</summary>
        string IDDLTable.TableName 
        { 
            get { return _TableName; }
        }


        /// <summary>Gets the table columns.</summary>
        IDDLColumn[] IDDLTable.Columns { get { return _Columns.ToArray(); } }


        /// <summary>Gets the table constraints.</summary>
        IDDLConstraint[] IDDLTable.Constraints { get { return _Constraints.ToArray(); } }


        /// <summary>Gets the table indexes.</summary>
        IDDLIndex[] IDDLTable.Indexes { get { return _Indexes.ToArray(); } }


        /// <summary>Adds a column to the table.</summary>
        /// <param name="columnName">Column name.</param>
        /// <param name="dataType">Column data type.</param>
        /// <param name="notNull">Not null option.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns></returns>
        IDDLTable IDDLTable.AddColumn(string columnName, SQLDataType dataType, bool notNull, string defaultValue)
        {
            return AddColumn(columnName, dataType, notNull, defaultValue);
        }


        /// <summary>Adds a primary key constraint to the table.</summary>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        IDDLTable IDDLTable.AddPrimaryKey(params string[] columnNames)
        {
            return AddPrimaryKey(columnNames);
        }


        /// <summary>Adds a primary key to the table.</summary>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        IDDLTable IDDLTable.AddNamedPrimaryKey(string constraintName, params string[] columnNames)
        {
            return AddNamedPrimaryKey(constraintName, columnNames);
        }


        /// <summary>Adds a foreign key constraint to the table.</summary>
        /// <param name="columnName">Column name.</param>
        /// <param name="referencedTableName">Referenced table name.</param>
        /// <param name="referencedColumnName">Referenced column name.</param>
        /// <param name="cascadeDelete">Delete cascade flag.</param>
        /// <returns></returns>
        IDDLTable IDDLTable.AddForeignKey(string columnName, string referencedTableName, string referencedColumnName, bool cascadeDelete)
        {
            return AddForeignKey(columnName, referencedTableName, referencedColumnName, cascadeDelete);
        }


        /// <summary>Adds a foreign key constraint to the table.</summary>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="columnName">Column name.</param>
        /// <param name="referencedTableName">Referenced table name.</param>
        /// <param name="referencedColumnName">Referenced column name.</param>
        /// <param name="cascadeDelete">Delete cascade flag.</param>
        /// <returns></returns>
        IDDLTable IDDLTable.AddNamedForeignKey(string constraintName, string columnName, string referencedTableName, string referencedColumnName, bool cascadeDelete)
        {
            return AddNamedForeignKey(constraintName, columnName, referencedTableName, referencedColumnName, cascadeDelete);
        }


        /// <summary>Adds a check constraint to the table.</summary>
        /// <param name="condition">Check condition.</param>
        /// <returns>DDL Table.</returns>
        IDDLTable IDDLTable.AddCheck(string condition)
        {
            return AddCheck(condition);
        }


        /// <summary>Adds a named check constraint to the table.</summary>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="condition">Check condition.</param>
        /// <returns>DDL Table.</returns>
        IDDLTable IDDLTable.AddNamedCheck(string constraintName, string condition)
        {
            return AddNamedCheck(constraintName, condition);
        }


        /// <summary>Adds an index to the table.</summary>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        IDDLTable IDDLTable.AddIndex(params string[] columnNames)
        {
            return AddIndex(columnNames);
        }


        /// <summary>Adds an index to the table.</summary>
        /// <param name="indexName">Index name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        IDDLTable IDDLTable.AddNamedIndex(string indexName, params string[] columnNames)
        {
            return AddNamedIndex(indexName, columnNames);
        }


        /// <summary>Adds a unique index to the table.</summary>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        IDDLTable IDDLTable.AddUniqueIndex(params string[] columnNames)
        {
            return AddUniqueIndex(columnNames);
        }


        /// <summary>Adds a named unique index to the table.</summary>
        /// <param name="indexName">Index name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL table.</returns>
        IDDLTable IDDLTable.AddNamedUniqueIndex(string indexName, params string[] columnNames)
        {
            return AddNamedUniqueIndex(indexName, columnNames);
        }
    }
}
