﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server roles folder.</summary>
    public class MsRolesFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        protected string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        internal MsRolesFolder(MsDatabase database)
        {
            _Parent = Database = database;
            _Name = "sqlfwx::tree.folder.roles".Localize("Roles");
            Owner = null;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="user">Parent user.</param>
        internal MsRolesFolder(MsDatabase database, MsUser user): this(database)
        {
            Owner = user;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="user">Parent group.</param>
        internal MsRolesFolder(MsDatabase database, MsGroup user) : this(database)
        {
            Owner = user;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; protected set; }


        /// <summary>Gets the owner.</summary>
        public DbItem Owner { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mssql::folder"; }
        }
        

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            IDbCommand cmd = null;
            IDataReader re = null;

            _Children = new List<DbItem>();

            try
            {
                cmd = Database.Provider.CreateCommand();
                if(Owner == null)
                {
                    cmd.CommandText = "SELECT NAME FROM sys.DATABASE_PRINCIPALS WHERE TYPE = 'R' ORDER BY NAME";
                }
                else if(Owner is MsUser)
                {
                    cmd.CommandText = "SELECT R.NAME FROM sys.DATABASE_PRINCIPALS R, sys.DATABASE_PRINCIPALS U, sys.DATABASE_ROLE_MEMBERS X WHERE X.ROLE_PRINCIPAL_ID = R.PRINCIPAL_ID AND X.MEMBER_PRINCIPAL_ID = U.PRINCIPAL_ID AND U.TYPE IN ('C', 'E', 'K', 'S', 'U') AND U.NAME = '" + Owner.Name + "' ORDER BY R.NAME";
                }
                else if(Owner is MsGroup)
                {
                    cmd.CommandText = "SELECT R.NAME FROM sys.DATABASE_PRINCIPALS R, sys.DATABASE_PRINCIPALS U, sys.DATABASE_ROLE_MEMBERS X WHERE X.ROLE_PRINCIPAL_ID = R.PRINCIPAL_ID AND X.MEMBER_PRINCIPAL_ID = U.PRINCIPAL_ID AND U.TYPE IN ('G', 'X') AND U.NAME = '" + Owner.Name + "' ORDER BY R.NAME";
                }
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    _Children.Add(new MsRole(Database, this, re.GetString(0)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX01213", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
