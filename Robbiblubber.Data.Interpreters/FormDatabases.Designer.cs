﻿namespace Robbiblubber.Data.Interpreters
{
    partial class FormDatabases
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDatabases));
            this._TreeConnections = new System.Windows.Forms.TreeView();
            this._CmenuConnections = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._CmenuAddFolder = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddConnection = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuMove = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuMoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuMoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAdvanced = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuExport = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuImport = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuConnect = new System.Windows.Forms.ToolStripMenuItem();
            this._IlistConnections = new System.Windows.Forms.ImageList(this.components);
            this._ButtonConnect = new System.Windows.Forms.Button();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._ButtonOK = new System.Windows.Forms.Button();
            this._ComboProvider = new Robbiblubber.Util.Controls.RichComboBox();
            this._IlistProviders = new System.Windows.Forms.ImageList(this.components);
            this._PanelSettings = new System.Windows.Forms.Panel();
            this._LabelClass = new System.Windows.Forms.Label();
            this._LabelIcon = new System.Windows.Forms.Label();
            this._CmenuConnections.SuspendLayout();
            this.SuspendLayout();
            // 
            // _TreeConnections
            // 
            this._TreeConnections.AllowDrop = true;
            this._TreeConnections.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TreeConnections.ContextMenuStrip = this._CmenuConnections;
            this._TreeConnections.HideSelection = false;
            this._TreeConnections.ImageIndex = 0;
            this._TreeConnections.ImageList = this._IlistConnections;
            this._TreeConnections.Location = new System.Drawing.Point(0, 0);
            this._TreeConnections.Name = "_TreeConnections";
            this._TreeConnections.SelectedImageIndex = 0;
            this._TreeConnections.Size = new System.Drawing.Size(301, 433);
            this._TreeConnections.TabIndex = 0;
            this._TreeConnections.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this._TreeConnections_AfterSelect);
            this._TreeConnections.DragDrop += new System.Windows.Forms.DragEventHandler(this._TreeConnections_DragDrop);
            this._TreeConnections.DragOver += new System.Windows.Forms.DragEventHandler(this._TreeConnections_DragOver);
            this._TreeConnections.DoubleClick += new System.EventHandler(this._ButtonConnect_Click);
            this._TreeConnections.KeyDown += new System.Windows.Forms.KeyEventHandler(this._TreeConnections_KeyDown);
            this._TreeConnections.MouseDown += new System.Windows.Forms.MouseEventHandler(this._TreeConnections_MouseDown);
            this._TreeConnections.MouseMove += new System.Windows.Forms.MouseEventHandler(this._TreeConnections_MouseMove);
            // 
            // _CmenuConnections
            // 
            this._CmenuConnections.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuAddFolder,
            this._CmenuAddConnection,
            this._CmenuDelete,
            this._CmenuMove,
            this._CmenuAdvanced,
            this._CmenuConnect});
            this._CmenuConnections.Name = "_CmenuConnections";
            this._CmenuConnections.Size = new System.Drawing.Size(162, 136);
            this._CmenuConnections.Opening += new System.ComponentModel.CancelEventHandler(this._CmenuConnections_Opening);
            // 
            // _CmenuAddFolder
            // 
            this._CmenuAddFolder.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuAddFolder.Image")));
            this._CmenuAddFolder.Name = "_CmenuAddFolder";
            this._CmenuAddFolder.Size = new System.Drawing.Size(161, 22);
            this._CmenuAddFolder.Text = "Add Folder";
            this._CmenuAddFolder.Click += new System.EventHandler(this._CmenuAddFolder_Click);
            // 
            // _CmenuAddConnection
            // 
            this._CmenuAddConnection.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuAddConnection.Image")));
            this._CmenuAddConnection.Name = "_CmenuAddConnection";
            this._CmenuAddConnection.Size = new System.Drawing.Size(161, 22);
            this._CmenuAddConnection.Text = "Add Connection";
            this._CmenuAddConnection.Click += new System.EventHandler(this._CmenuAddConnection_Click);
            // 
            // _CmenuDelete
            // 
            this._CmenuDelete.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuDelete.Image")));
            this._CmenuDelete.Name = "_CmenuDelete";
            this._CmenuDelete.Size = new System.Drawing.Size(161, 22);
            this._CmenuDelete.Text = "Delete";
            this._CmenuDelete.Click += new System.EventHandler(this._CmenuDelete_Click);
            // 
            // _CmenuMove
            // 
            this._CmenuMove.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuMoveUp,
            this._CmenuMoveDown});
            this._CmenuMove.Name = "_CmenuMove";
            this._CmenuMove.Size = new System.Drawing.Size(161, 22);
            this._CmenuMove.Text = "Move";
            this._CmenuMove.DropDownOpening += new System.EventHandler(this._CmenuMove_DropDownOpening);
            // 
            // _CmenuMoveUp
            // 
            this._CmenuMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuMoveUp.Image")));
            this._CmenuMoveUp.Name = "_CmenuMoveUp";
            this._CmenuMoveUp.Size = new System.Drawing.Size(180, 22);
            this._CmenuMoveUp.Text = "Move Up";
            this._CmenuMoveUp.Click += new System.EventHandler(this._CmenuMoveUp_Click);
            // 
            // _CmenuMoveDown
            // 
            this._CmenuMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuMoveDown.Image")));
            this._CmenuMoveDown.Name = "_CmenuMoveDown";
            this._CmenuMoveDown.Size = new System.Drawing.Size(180, 22);
            this._CmenuMoveDown.Text = "Move Down";
            this._CmenuMoveDown.Click += new System.EventHandler(this._CmenuMoveDown_Click);
            // 
            // _CmenuAdvanced
            // 
            this._CmenuAdvanced.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuExport,
            this._CmenuImport});
            this._CmenuAdvanced.Name = "_CmenuAdvanced";
            this._CmenuAdvanced.Size = new System.Drawing.Size(161, 22);
            this._CmenuAdvanced.Text = "Advanced";
            this._CmenuAdvanced.DropDownOpening += new System.EventHandler(this._CmenuAdvanced_DropDownOpening);
            // 
            // _CmenuExport
            // 
            this._CmenuExport.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuExport.Image")));
            this._CmenuExport.Name = "_CmenuExport";
            this._CmenuExport.Size = new System.Drawing.Size(180, 22);
            this._CmenuExport.Text = "&Export...";
            this._CmenuExport.Click += new System.EventHandler(this._CmenuExport_Click);
            // 
            // _CmenuImport
            // 
            this._CmenuImport.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuImport.Image")));
            this._CmenuImport.Name = "_CmenuImport";
            this._CmenuImport.Size = new System.Drawing.Size(180, 22);
            this._CmenuImport.Text = "&Import...";
            this._CmenuImport.Click += new System.EventHandler(this._CmenuImport_Click);
            // 
            // _CmenuConnect
            // 
            this._CmenuConnect.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._CmenuConnect.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuConnect.Image")));
            this._CmenuConnect.Name = "_CmenuConnect";
            this._CmenuConnect.Size = new System.Drawing.Size(161, 22);
            this._CmenuConnect.Text = "Connect";
            this._CmenuConnect.Click += new System.EventHandler(this._ButtonConnect_Click);
            // 
            // _IlistConnections
            // 
            this._IlistConnections.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_IlistConnections.ImageStream")));
            this._IlistConnections.TransparentColor = System.Drawing.Color.Transparent;
            this._IlistConnections.Images.SetKeyName(0, "databases");
            this._IlistConnections.Images.SetKeyName(1, "folder");
            // 
            // _ButtonConnect
            // 
            this._ButtonConnect.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonConnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonConnect.Location = new System.Drawing.Point(313, 392);
            this._ButtonConnect.Name = "_ButtonConnect";
            this._ButtonConnect.Size = new System.Drawing.Size(147, 29);
            this._ButtonConnect.TabIndex = 3;
            this._ButtonConnect.Tag = "sqlfwx::udiag.databases.button.connect";
            this._ButtonConnect.Text = "Co&nnect";
            this._ButtonConnect.UseVisualStyleBackColor = true;
            this._ButtonConnect.Click += new System.EventHandler(this._ButtonConnect_Click);
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(629, 392);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(147, 29);
            this._ButtonCancel.TabIndex = 5;
            this._ButtonCancel.Tag = "sqlfwx::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(476, 392);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(147, 29);
            this._ButtonOK.TabIndex = 4;
            this._ButtonOK.Tag = "sqlfwx::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            // 
            // _ComboProvider
            // 
            this._ComboProvider.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this._ComboProvider.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._ComboProvider.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ComboProvider.ImageList = this._IlistProviders;
            this._ComboProvider.Location = new System.Drawing.Point(313, 12);
            this._ComboProvider.Name = "_ComboProvider";
            this._ComboProvider.Size = new System.Drawing.Size(463, 26);
            this._ComboProvider.TabIndex = 1;
            this._ComboProvider.Visible = false;
            this._ComboProvider.SelectedIndexChanged += new System.EventHandler(this._ComboProvider_SelectedIndexChanged);
            // 
            // _IlistProviders
            // 
            this._IlistProviders.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this._IlistProviders.ImageSize = new System.Drawing.Size(16, 16);
            this._IlistProviders.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // _PanelSettings
            // 
            this._PanelSettings.Location = new System.Drawing.Point(313, 48);
            this._PanelSettings.Name = "_PanelSettings";
            this._PanelSettings.Size = new System.Drawing.Size(463, 338);
            this._PanelSettings.TabIndex = 2;
            this._PanelSettings.Visible = false;
            // 
            // _LabelClass
            // 
            this._LabelClass.Location = new System.Drawing.Point(351, 16);
            this._LabelClass.Name = "_LabelClass";
            this._LabelClass.Size = new System.Drawing.Size(174, 21);
            this._LabelClass.TabIndex = 7;
            this._LabelClass.Text = "Databases";
            // 
            // _LabelIcon
            // 
            this._LabelIcon.Image = ((System.Drawing.Image)(resources.GetObject("_LabelIcon.Image")));
            this._LabelIcon.Location = new System.Drawing.Point(310, 11);
            this._LabelIcon.Name = "_LabelIcon";
            this._LabelIcon.Size = new System.Drawing.Size(35, 26);
            this._LabelIcon.TabIndex = 6;
            // 
            // FormDatabases
            // 
            this.AcceptButton = this._ButtonConnect;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(788, 433);
            this.Controls.Add(this._ComboProvider);
            this.Controls.Add(this._LabelClass);
            this.Controls.Add(this._LabelIcon);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._ButtonConnect);
            this.Controls.Add(this._TreeConnections);
            this.Controls.Add(this._PanelSettings);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormDatabases";
            this.Tag = "sqlfwx::udiag.databases.caption";
            this.Text = "Databases";
            this._CmenuConnections.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView _TreeConnections;
        private System.Windows.Forms.Button _ButtonConnect;
        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Button _ButtonOK;
        private Robbiblubber.Util.Controls.RichComboBox _ComboProvider;
        private System.Windows.Forms.Panel _PanelSettings;
        private System.Windows.Forms.ImageList _IlistConnections;
        private System.Windows.Forms.Label _LabelIcon;
        private System.Windows.Forms.Label _LabelClass;
        private System.Windows.Forms.ContextMenuStrip _CmenuConnections;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddFolder;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddConnection;
        private System.Windows.Forms.ToolStripMenuItem _CmenuDelete;
        private System.Windows.Forms.ImageList _IlistProviders;
        private System.Windows.Forms.ToolStripMenuItem _CmenuConnect;
        private System.Windows.Forms.ToolStripMenuItem _CmenuMove;
        private System.Windows.Forms.ToolStripMenuItem _CmenuMoveUp;
        private System.Windows.Forms.ToolStripMenuItem _CmenuMoveDown;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAdvanced;
        private System.Windows.Forms.ToolStripMenuItem _CmenuExport;
        private System.Windows.Forms.ToolStripMenuItem _CmenuImport;
    }
}