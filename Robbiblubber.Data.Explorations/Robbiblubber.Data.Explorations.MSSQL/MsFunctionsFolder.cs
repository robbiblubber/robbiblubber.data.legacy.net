﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server functions folder.</summary>
    public class MsFunctionsFolder: MsProceduresFolder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="schema">Parent schema.</param>
        internal MsFunctionsFolder(MsDatabase database, MsSchema schema): base(database, schema)
        {
            _Name = "sqlfwx::tree.folder.functions".Localize("Functions");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] MsProceduresFolder                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            IDbCommand cmd = null;
            IDataReader re = null;

            _Children = new List<DbItem>();

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT O.NAME, O.TYPE FROM sys.OBJECTS O, sys.SCHEMAS S WHERE S.NAME = '" + Schema.Name + "' AND S.SCHEMA_ID = O.SCHEMA_ID AND O.TYPE IN ('AF', 'FN', 'FS', 'FT', 'IF', 'TF') ORDER BY O.NAME";
                re = cmd.ExecuteReader();
                
                while(re.Read())
                {
                    _Children.Add(new MsProcedure(Database, this, re.GetString(0), re.GetString(1)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX01209", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
