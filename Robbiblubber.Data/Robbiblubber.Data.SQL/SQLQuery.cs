﻿using System;
using System.Collections.Generic;
using System.Text;

using Robbiblubber.Data.Providers;



namespace Robbiblubber.Data.SQL
{
    /// <summary>This class provides a basic implementation of an SQL query builder.</summary>
    public class SQLQuery: ISQLQuery
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>SQL Parser.</summary>
        protected IParser _Parser = null;

        /// <summary>Primary table.</summary>
        protected ISQLTable _FromTable = null;

        /// <summary>Selection fields.</summary>
        protected List<ISQLColumn> _SelectedFields = new List<ISQLColumn>();

        /// <summary>Left outer join tables.</summary>
        protected List<Tuple<ISQLTable, List<string>>> _LeftOuterJoins = new List<Tuple<ISQLTable, List<string>>>();

        /// <summary>Right outer join tables.</summary>
        protected List<Tuple<ISQLTable, List<string>>> _RightOuterJoins = new List<Tuple<ISQLTable, List<string>>>();

        /// <summary>Full outer join tables.</summary>
        protected List<Tuple<ISQLTable, List<string>>> _FullOuterJoins = new List<Tuple<ISQLTable, List<string>>>();

        /// <summary>Inner join tables.</summary>
        protected List<Tuple<ISQLTable, List<string>>> _InnerJoins = new List<Tuple<ISQLTable, List<string>>>();

        /// <summary>And-linked conditions.</summary>
        protected List<string> _AndConditions = new List<string>();

        /// <summary>Or-linked conditions.</summary>
        protected List<string> _OrConditions = new List<string>();

        /// <summary>Order by fields.</summary>
        protected List<Tuple<ISQLColumn, bool>> _OrderBy = new List<Tuple<ISQLColumn, bool>>();

        /// <summary>Group by fields.</summary>
        protected List<ISQLColumn> _GroupBy = new List<ISQLColumn>();

        /// <summary>Having conditions.</summary>
        protected List<string> _HavingConditions = new List<string>();

        /// <summary>Union statements.</summary>
        protected List<Tuple<ISQLQuery, bool>> _Unions = new List<Tuple<ISQLQuery, bool>>();

        /// <summary>Distinct flag.</summary>
        protected bool _Distinct = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="p">Parser.</param>
        public SQLQuery(IParser p)
        {
            _Parser = p;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an inner join to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <param name="cond">Join conditions.</param>
        /// <returns>SQL query.</returns>
        public virtual ISQLQuery InnerJoin(ISQLTable tab, params string[] cond)
        {
            return Join(tab, cond);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Appends a condition list.</summary>
        /// <param name="s">String builder.</param>
        /// <param name="cond">Conditions.</param>
        /// <param name="op">Operator.</param>
        protected virtual void _AppendCondition(ref StringBuilder s, List<string> cond, string op)
        {
            bool first = true;
            foreach(string i in cond)
            {
                if(first) { first = false; } else { s.Append(op); }

                s.Append('(');
                s.Append(i);
                s.Append(')');
            }
        }


        /// <summary>Appends a field list to the query.</summary>
        /// <param name="s">String builder.</param>
        /// <param name="fields">Field list.</param>
        /// <param name="aliases">Determines if field aliases are allowed.</param>
        protected virtual void _AppendFieldList(ref StringBuilder s, List<ISQLColumn> fields, bool aliases)
        {
            bool first = true;

            foreach(ISQLColumn i in fields)
            {
                if(first)
                {
                    first = false;
                }
                else { s.Append(", "); }

                if(!string.IsNullOrWhiteSpace(i.TableAlias))
                {
                    s.Append(i.TableAlias);
                    s.Append('.');
                }
                s.Append(i.ColumnName);

                if(aliases && (!string.IsNullOrWhiteSpace(i.ColumnAlias)))
                {
                    s.Append(_Parser.AsOperator);
                    s.Append(i.ColumnAlias);
                }
            }
        }


        /// <summary>Appends a field list to the query.</summary>
        /// <param name="s">String builder.</param>
        /// <param name="fields">Field list.</param>
        /// <param name="aliases">Determines if field aliases are allowed.</param>
        protected virtual void _AppendFieldList(ref StringBuilder s, List<Tuple<ISQLColumn, bool>> fields, bool aliases)
        {
            bool first = true;

            foreach(Tuple<ISQLColumn, bool> i in fields)
            {
                if(first)
                {
                    first = false;
                }
                else { s.Append(", "); }

                if(!string.IsNullOrWhiteSpace(i.Item1.TableAlias))
                {
                    s.Append(i.Item1.TableAlias);
                    s.Append('.');
                }
                s.Append(i.Item1.ColumnName);

                if(aliases && (!string.IsNullOrWhiteSpace(i.Item1.ColumnAlias)))
                {
                    s.Append(_Parser.AsOperator);
                    s.Append(i.Item1.ColumnAlias);
                }

                if(i.Item2) { s.Append(" DESC"); }
            }
        }


        /// <summary>Appends the from table clause to the query.</summary>
        /// <param name="s">String builder.</param>
        protected virtual void _AppendFrom(ref StringBuilder s)
        {
            s.Append(" FROM ");
            s.Append(_FromTable.TableName);
            
            if(!string.IsNullOrWhiteSpace(_FromTable.TableAlias))
            {
                s.Append(_Parser.AsOperator);
                s.Append(_FromTable.TableAlias);
            }
        }


        /// <summary>Appends the GROUP BY clause to the query.</summary>
        /// <param name="s">String builder.</param>
        protected virtual void _AppendGroup(ref StringBuilder s)
        {
            if(_GroupBy.Count > 0)
            {
                s.Append(" GROUP BY ");
                _AppendFieldList(ref s, _GroupBy, true);
            }
        }


        /// <summary>Appends the WHERE clause to the query.</summary>
        /// <param name="s">String builder.</param>
        protected virtual void _AppendWhere(ref StringBuilder s)
        {
            if((_AndConditions.Count + _OrConditions.Count) == 0) return;

            s.Append(" WHERE ");

            if(_AndConditions.Count > 0)
            {
                _AppendCondition(ref s, _AndConditions, " AND ");

                if(_OrConditions.Count > 0) { s.Append(" OR "); }
            }
            _AppendCondition(ref s, _OrConditions, " OR ");
        }


        /// <summary>Appends the HAVING clause to the query.</summary>
        /// <param name="s">String builder.</param>
        protected virtual void _AppendHaving(ref StringBuilder s)
        {
            if(_HavingConditions.Count > 0)
            {
                s.Append(" HAVING ");
                _AppendCondition(ref s, _HavingConditions, " AND ");
            }
        }


        /// <summary>Appends the ORDER BY clause to the query.</summary>
        /// <param name="s">String builder.</param>
        protected virtual void _AppendOrderBy(ref StringBuilder s)
        {
            if(_OrderBy.Count > 0)
            {
                s.Append(" ORDER BY ");
                _AppendFieldList(ref s, _OrderBy, false);
            }
        }


        /// <summary>Appends inner join clauses to the query.</summary>
        /// <param name="s">String builder.</param>
        /// <param name="joinStatement">Join statement.</param>
        /// <param name="joins">Tables and clauses to join.</param>
        protected virtual void _AppendJoins(ref StringBuilder s, string joinStatement, List<Tuple<ISQLTable, List<string>>> joins)
        {
            foreach(Tuple<ISQLTable, List<string>> i in joins)
            {
                s.Append(joinStatement);
                s.Append(i.Item1.TableName);

                if(!string.IsNullOrWhiteSpace(i.Item1.TableAlias))
                {
                    s.Append(_Parser.AsOperator);
                    s.Append(i.Item1.TableAlias);
                }

                if(i.Item2.Count > 0)
                {
                    s.Append(" ON ");
                    _AppendCondition(ref s, i.Item2, " AND ");                    
                }
            }
        }


        /// <summary>Appends UNION and UNION ALL joined queries to the query.</summary>
        /// <param name="s">String builder.</param>
        protected virtual void _AppendUnions(ref StringBuilder s)
        {
            foreach(Tuple<ISQLQuery, bool> i in _Unions)
            {
                s.Append(i.Item2 ? " UNION ALL " : " UNION ");
                s.Append(i.Item1.Text);
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ISQLQuery                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the SQL text.</summary>
        public virtual string Text
        {
            get
            {
                StringBuilder rval = new StringBuilder();
                rval.Append("SELECT ");
                if(_Distinct) { rval.Append("DISTINCT "); }

                _AppendFieldList(ref rval, _SelectedFields, true);
                _AppendFrom(ref rval);

                _AppendJoins(ref rval, " INNER JOIN ", _InnerJoins);
                _AppendJoins(ref rval, " LEFT OUTER JOIN ", _LeftOuterJoins);
                _AppendJoins(ref rval, " RIGHT OUTER JOIN ", _RightOuterJoins);
                _AppendJoins(ref rval, " FULL OUTER JOIN ", _FullOuterJoins);

                _AppendGroup(ref rval);

                _AppendWhere(ref rval);
                _AppendHaving(ref rval);
                _AppendOrderBy(ref rval);

                _AppendUnions(ref rval);

                return rval.ToString();
            }
        }


        /// <summary>Gets or sets the parameters for the query.</summary>
        public virtual List<Tuple<string, object>> Parameters { get; set; } = new List<Tuple<string, object>>();


        /// <summary>Returns a copy of this SQL query.</summary>
        /// <returns>Cloned SQL query.</returns>
        public virtual ISQLQuery Clone()
        {
            SQLQuery rval = new SQLQuery(_Parser);

            rval._FromTable = _FromTable;
            rval._SelectedFields = _SelectedFields;
            rval._LeftOuterJoins = _LeftOuterJoins;
            rval._RightOuterJoins = _RightOuterJoins;
            rval._FullOuterJoins = _FullOuterJoins;
            rval._InnerJoins = _InnerJoins;
            rval._AndConditions = _AndConditions;
            rval._OrConditions = _OrConditions;
            rval._OrderBy = _OrderBy;
            rval._GroupBy = _GroupBy;
            rval._HavingConditions = _HavingConditions;
            rval._Distinct = _Distinct;

            rval._Unions = new List<Tuple<ISQLQuery, bool>>();
            foreach(Tuple<ISQLQuery, bool> i in _Unions) { rval._Unions.Add(new Tuple<ISQLQuery, bool>(i.Item1.Clone(), i.Item2)); }

            return rval;
    }


        /// <summary>Adds a primary query table to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <returns>SQL query.</returns>
        public virtual ISQLQuery From(ISQLTable tab)
        {
            _FromTable = tab;

            return this;
        }


        /// <summary>Adds selection fields to the query.</summary>
        /// <param name="field">Field.</param>
        /// <returns>SQL query.</returns>
        public virtual ISQLQuery Select(params ISQLColumn[] field)
        {
            _SelectedFields.AddRange(field);

            return this;
        }


        /// <summary>Makes the query distinct.</summary>
        /// <returns>SQL query.</returns>
        public virtual ISQLQuery Distinct()
        {
            _Distinct = true;

            return this;
        }


        /// <summary>Adds a left outer join to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <param name="cond">Join conditions.</param>
        /// <returns>SQL query.</returns>
        public virtual ISQLQuery LeftOuterJoin(ISQLTable tab, params string[] cond)
        {
            _LeftOuterJoins.Add(new Tuple<ISQLTable, List<string>>(tab, new List<string>(cond)));

            return this;
        }


        /// <summary>Adds a right outer join to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <param name="cond">Join conditions.</param>
        /// <returns>SQL query.</returns>
        public virtual ISQLQuery RightOuterJoin(ISQLTable tab, params string[] cond)
        {
            _RightOuterJoins.Add(new Tuple<ISQLTable, List<string>>(tab, new List<string>(cond)));

            return this;
        }


        /// <summary>Adds a full outer join to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <param name="cond">Join conditions.</param>
        /// <returns>SQL query.</returns>
        public virtual ISQLQuery FullOuterJoin(ISQLTable tab, params string[] cond)
        {
            _FullOuterJoins.Add(new Tuple<ISQLTable, List<string>>(tab, new List<string>(cond)));

            return this;
        }


        /// <summary>Adds an inner join to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <param name="cond">Join conditions.</param>
        /// <returns>SQL query.</returns>
        public virtual ISQLQuery Join(ISQLTable tab, params string[] cond)
        {
            _InnerJoins.Add(new Tuple<ISQLTable, List<string>>(tab, new List<string>(cond)));

            return this;
        }


        /// <summary>Adds a where condition to the query.</summary>
        /// <param name="cond">Where conditions.</param>
        /// <returns>SQL query.</returns>
        public virtual ISQLQuery Where(params string[] cond)
        {
            return And(cond);
        }


        /// <summary>Adds an AND where condition to the query.</summary>
        /// <param name="cond">Where conditions.</param>
        /// <returns>SQL query.</returns>
        public virtual ISQLQuery And(params string[] cond)
        {
            foreach(string i in cond)
            {
                if(!string.IsNullOrWhiteSpace(i)) { _AndConditions.Add(i); }
            }

            return this;
        }


        /// <summary>Adds an OR where condition to the query.</summary>
        /// <param name="cond">Where conditions.</param>
        /// <returns>SQL query.</returns>
        public virtual ISQLQuery Or(params string[] cond)
        {
            foreach(string i in cond)
            {
                if(!string.IsNullOrWhiteSpace(i)) { _OrConditions.Add(i); }
            }

            return this;
        }


        /// <summary>Adds ORDER BY clauses to the query.</summary>
        /// <param name="field">Fields.</param>
        /// <returns>SQL Query.</returns>
        public virtual ISQLQuery OrderBy(params ISQLColumn[] field)
        {
            for(int i = field.Length - 1; i >= 0; i--)
            {
                _OrderBy.Insert(0, new Tuple<ISQLColumn, bool>(field[i], false));
            }

            return this;
        }


        /// <summary>Adds ORDER BY DESC clauses to the query.</summary>
        /// <param name="field">Fields.</param>
        /// <returns>SQL Query.</returns>
        public virtual ISQLQuery OrderByDesc(params ISQLColumn[] field)
        {
            for(int i = field.Length - 1; i >= 0; i--)
            {
                _OrderBy.Insert(0, new Tuple<ISQLColumn, bool>(field[i], true));
            }

            return this;
        }


        /// <summary>Adds group by clauses to the query.</summary>
        /// <param name="field">Fields.</param>
        /// <returns>SQL Query.</returns>
        public virtual ISQLQuery GroupBy(params ISQLColumn[] field)
        {
            _GroupBy.AddRange(field);

            return this;
        }


        /// <summary>Adds having clauses to the query.</summary>
        /// <param name="cond">Having conditions.</param>
        /// <returns>SQL Query.</returns>
        public virtual ISQLQuery Having(params string[] cond)
        {
            _HavingConditions.AddRange(cond);

            return this;
        }


        /// <summary>Adds a parameter to the query.</summary>
        /// <param name="name">Parameter name.</param>
        /// <param name="value">Parameter value.</param>
        /// <returns>SQL query.</returns>
        public virtual ISQLQuery AddParameter(string name, object value)
        {
            Parameters.Add(new Tuple<string, object>(name, value));

            return this;
        }


        /// <summary>Adds a UNION statement to the query.</summary>
        /// <param name="query">SQL query.</param>
        /// <returns>SQL query.</returns>
        public virtual ISQLQuery Union(ISQLQuery query)
        {
            _Unions.Add(new Tuple<ISQLQuery, bool>(query, false));

            return this;
        }


        /// <summary>Adds a UNION ALL statement to the query.</summary>
        /// <param name="query">SQL query.</param>
        /// <returns>SQL query.</returns>
        public virtual ISQLQuery UnionAll(ISQLQuery query)
        {
            _Unions.Add(new Tuple<ISQLQuery, bool>(query, true));

            return this;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return Text;
        }
    }
}
