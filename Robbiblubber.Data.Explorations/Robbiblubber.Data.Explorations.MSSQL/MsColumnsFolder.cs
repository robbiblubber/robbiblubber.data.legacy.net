﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Explorations.MSSQL
{
    /// <summary>This class represents a SQL Server columns folder.</summary>
    public class MsColumnsFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="table">Parent table.</param>
        internal MsColumnsFolder(MsDatabase database, MsTable table)
        {
            Database = database;
            _Parent = Table = table;
            _Name = "sqlfwx::tree.folder.columns".Localize("Columns");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public MsDatabase Database { get; private set; }


        /// <summary>Gets the parent table.</summary>
        public MsTable Table { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "mssql::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            List<string> pk = new List<string>();
            List<string> fk = new List<string>();

            IDbCommand cmd = null;
            IDataReader re = null;

            if(!(Table is MsView))
            {
                try
                {
                    cmd = Database.Provider.CreateCommand();
                    cmd.CommandText = "SELECT U.COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS C, INFORMATION_SCHEMA.KEY_COLUMN_USAGE U WHERE C.CONSTRAINT_TYPE = 'PRIMARY KEY' AND C.CONSTRAINT_NAME = U.CONSTRAINT_NAME AND U.TABLE_SCHEMA = '" + Table.Folder.Schema.Name + "' AND U.TABLE_NAME = '" + Table.Name + "'";
                    re = cmd.ExecuteReader();

                    while(re.Read())
                    {
                        pk.Add(re.GetString(0));
                    }
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX01204", ex); }

                Disposal.Dispose(re, cmd);


                try
                {
                    cmd = Database.Provider.CreateCommand();
                    cmd.CommandText = "SELECT U.COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS C, INFORMATION_SCHEMA.KEY_COLUMN_USAGE U WHERE C.CONSTRAINT_TYPE = 'FOREIGN KEY' AND C.CONSTRAINT_NAME = U.CONSTRAINT_NAME AND U.TABLE_SCHEMA = '" + Table.Folder.Schema.Name + "' AND U.TABLE_NAME = '" + Table.Name + "'";
                    re = cmd.ExecuteReader();

                    while(re.Read())
                    {
                        fk.Add(re.GetString(0));
                    }
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX01205", ex); }

                Disposal.Dispose(re, cmd);
            }

            _Children = new List<DbItem>();

            try
            {
                cmd = Database.Provider.CreateCommand();
                cmd.CommandText = "SELECT C.NAME, C.IS_NULLABLE, Y.NAME, C.MAX_LENGTH FROM sys.ALL_COLUMNS C, sys." + ((Table is MsView) ? "ALL_VIEWS" : "TABLES") + " T, sys.SCHEMAS S, sys.TYPES Y WHERE S.NAME = '" + Table.Folder.Schema.Name + "' AND T.NAME = '" + Table.Name + "' AND S.SCHEMA_ID = T.SCHEMA_ID AND T.OBJECT_ID = C.OBJECT_ID AND Y.SYSTEM_TYPE_ID = C.SYSTEM_TYPE_ID AND Y.USER_TYPE_ID = C.USER_TYPE_ID ORDER BY C.COLUMN_ID";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    string dataType = re.GetString(2);
                    if(dataType.ToLower().EndsWith("char")) { dataType += "(" + re.GetValue(3).ToString() + ")"; }
                    _Children.Add(new MsColumn(Database, this, re.GetString(0), dataType, pk.Contains(re.GetString(0)), fk.Contains(re.GetString(0)), re.GetBoolean(1)));
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX01206", ex); }

            Disposal.Dispose(re, cmd);
        }
    }
}
