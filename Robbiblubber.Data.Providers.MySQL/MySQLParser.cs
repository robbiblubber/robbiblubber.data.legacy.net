﻿using System;
using System.Collections.Generic;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Providers.MySQL
{
    /// <summary>This class provides a MySQL-specific implementation of the SQLParser class.</summary>
    public class MySQLParser: Parser, IParser
    {
        /// <summary>Returns a SQL data type for a database data type expression.</summary>
        /// <param name="dbDataType">Database data type string.</param>
        /// <returns>SQL data type.</returns>
        public SQLDataType ToSQLDataType(string dbDataType)
        {
            switch(dbDataType.ToUpper())
            {
                case "TINYTEXT": return SQLDataType.Text(255);
                case "TEXT": return SQLDataType.Text(65535);
                case "MEDIUMTEXT": return SQLDataType.Text(16777215);
                case "LONGTEXT": return SQLDataType.TEXT;
                case "DATETIME": case "TIMESTAMP": return SQLDataType.TIMESTAMP;
                case "TINYINT": return SQLDataType.INT8;
                case "SMALLINT": return SQLDataType.INT16;
                case "MEDIUMINT": return SQLDataType.INT16;
                case "INT": case "INTEGER": return SQLDataType.INT32;
                case "BIGINT": return SQLDataType.INT64;
                case "BOOLEAN": return SQLDataType.BOOLEAN;
                case "REAL": case "DOUBLE": case "FIXED": case "DOUBLE PRECISION": return SQLDataType.DOUBLE;
            }

            if(dbDataType.ToUpper().StartsWith("FLOAT"))
            {
                return SQLDataType.FLOAT;
            }

            if(dbDataType.ToUpper().StartsWith("DOUBLE"))
            {
                return SQLDataType.DOUBLE;
            }

            if(dbDataType.ToUpper().StartsWith("DEC") || dbDataType.ToUpper().StartsWith("NUMERIC") || dbDataType.ToUpper().StartsWith("REAL") || dbDataType.ToUpper().StartsWith("FIXED"))
            {
                return SQLDataType.DECIMAL;
            }

            if(dbDataType.ToUpper().StartsWith("VARC") || dbDataType.ToUpper().StartsWith("CHAR"))
            {
                string l = dbDataType.ToUpper().Trim("ABCDEFGHIJKLMNOPQRSTUVWXYZ()".ToCharArray());
                if(string.IsNullOrWhiteSpace(l)) { return SQLDataType.STRING; }

                return SQLDataType.String(int.Parse(l));
            }

            return SQLDataType.TEXT;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLParser                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parses a source string.</summary>
        /// <param name="source">Source string.</param>
        /// <returns>Parsed SQL statements.</returns>
        public override ISQLStatement[] Parse(string source)
        {
            List<string> strings = _Parse(source);

            MySQLStatement[] rval = new MySQLStatement[strings.Count];

            for(int i = 0; i < strings.Count; i++)
            {
                rval[i] = new MySQLStatement(strings[i]);
            }

            return rval;
        }


        /// <summary>Gets a well-formatted query function call for last auto increment value.</summary>
        public override string LastValue
        {
            get { return "Last_Insert_ID()"; }
        }
    }
}
