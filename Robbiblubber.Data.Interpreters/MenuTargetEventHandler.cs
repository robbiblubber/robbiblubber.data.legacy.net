﻿using System;



namespace Robbiblubber.Data.Interpreters
{
    /// <summary>This delegate is used by menu events.</summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Event arguments.</param>
    public delegate void MenuTargetEventHandler(object sender, MenuTargetEventArgs e);
}
