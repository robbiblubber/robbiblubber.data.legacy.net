﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Data.Providers;
using Robbiblubber.Util;



namespace Robbiblubber.Data
{
    /// <summary>This class defines SQL extension methods.</summary>
    public static class SQLExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a database command.</summary>
        /// <param name="cn">Connection.</param>
        /// <param name="text">Command text.</param>
        /// <returns>Command.</returns>
        public static IDbCommand CreateCommand(this IDbConnection cn, string text)
        {
            IDbCommand cmd = cn.CreateCommand();
            cmd.CommandText = text;

            return cmd;
        }


        /// <summary>Gets the database type for a parameter value.</summary>
        /// <param name="cmd">Database command.</param>
        /// <param name="value">Parameter value.</param>
        /// <returns>Database type.</returns>
        public static DbType GetParameterType(this IDbCommand cmd, object value)
        {
            DbType rval = DbType.Object;

            if(value is string)
            {
                rval = DbType.String;
            }
            else if(value is bool)
            {
                rval = DbType.Boolean;
            }
            else if(value is byte)
            {
                rval = DbType.Byte;
            }
            else if(value is DateTime)
            {
                rval = DbType.DateTime;
            }
            else if(value is decimal)
            {
                rval = DbType.Decimal;
            }
            else if(value is double)
            {
                rval = DbType.Double;
            }
            else if(value is short)
            {
                rval = DbType.Int16;
            }
            else if(value is int)
            {
                rval = DbType.Int32;
            }
            else if(value is long)
            {
                rval = DbType.Int64;
            }
            else if(value is sbyte)
            {
                rval = DbType.SByte;
            }
            else if(value is float)
            {
                rval = DbType.Single;
            }
            else if(value is ushort)
            {
                rval = DbType.UInt16;
            }
            else if(value is uint)
            {
                rval = DbType.UInt32;
            }
            else if(value is ulong)
            {
                rval = DbType.UInt64;
            }

            return rval;
        }


        /// <summary>Adds a parameter to the the command.</summary>
        /// <param name="cmd">Database command.</param>
        /// <param name="name">Parameter name.</param>
        /// <param name="value">Parameter value.</param>
        /// <returns>Parameter object.</returns>
        public static IDataParameter AddParameter(this IDbCommand cmd, string name, object value)
        {
            return cmd.AddParameter(name, cmd.GetParameterType(value), value);
        }


        /// <summary>Adds a parameter to the the command.</summary>
        /// <param name="cmd">Database command.</param>
        /// <param name="type">Database type.</param>
        /// <param name="name">Parameter name.</param>
        /// <param name="value">Parameter value.</param>
        /// <returns>Parameter object.</returns>
        public static IDataParameter AddParameter(this IDbCommand cmd, string name, DbType type, object value)
        {
            IDataParameter rval = cmd.CreateParameter();
            rval.ParameterName = name;
            rval.Value = value;
            rval.DbType = type;

            cmd.Parameters.Add(rval);

            return rval;
        }


        /// <summary>Adds a list of parameters to the command.</summary>
        /// <param name="cmd">Database command.</param>
        /// <param name="data">Parameter list.</param>
        public static void AddParameters(this IDbCommand cmd, IEnumerable<Tuple<string, object>> data)
        {
            foreach(Tuple<string, object> i in data)
            {
                cmd.AddParameter(i.Item1, i.Item2);
            }
        }


        /// <summary>Gets if a column value is NULL.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="i">Column index.</param>
        /// <returns>Returns TRUE if the value is NULL, otherwise returns FALSE.</returns>
        public static bool IsNull(this IDataReader re, int i)
        {
            return re.IsDBNull(i);
        }


        /// <summary>Gets if a column value is NULL.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="c">Column name.</param>
        /// <returns>Returns TRUE if the value is NULL, otherwise returns FALSE.</returns>
        public static bool IsNull(this IDataReader re, string c)
        {
            return re.IsDBNull(re.GetOrdinal(c));
        }


        /// <summary>Gets the string value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="i">Column index.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static string GetStringValue(this IDataReader re, int i, string nullv = "")
        {
            return (re.IsDBNull(i) ? nullv : re.GetValue(i).ToString());
        }


        /// <summary>Gets the string value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="c">Column name.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static string GetStringValue(this IDataReader re, string c, string nullv = "")
        {
            int i = re.GetOrdinal(c);
            return (re.IsDBNull(i) ? nullv : re.GetValue(i).ToString());
        }


        /// <summary>Gets the string value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="c">Column name.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static string GetString(this IDataReader re, string c, string nullv = "")
        {
            int i = re.GetOrdinal(c);
            return (re.IsDBNull(i) ? nullv : re.GetValue(i).ToString());
        }


        /// <summary>Gets the integer value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="i">Column index.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static int GetInteger(this IDataReader re, int i, int nullv = 0)
        {
            return (re.IsDBNull(i) ? nullv : Convert.ToInt32(re.GetValue(i)));
        }


        /// <summary>Gets the integer value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="c">Column name.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static int GetInteger(this IDataReader re, string c, int nullv = 0)
        {
            int i = re.GetOrdinal(c);
            return (re.IsDBNull(i) ? nullv : Convert.ToInt32(re.GetValue(i)));
        }


        /// <summary>Gets the long integer value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="i">Column index.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static long GetLong(this IDataReader re, int i, long nullv = 0)
        {
            return (re.IsDBNull(i) ? nullv : Convert.ToInt64(re.GetValue(i)));
        }


        /// <summary>Gets the long integer value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="c">Column name.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static long GetLong(this IDataReader re, string c, long nullv = 0)
        {
            int i = re.GetOrdinal(c);
            return (re.IsDBNull(i) ? nullv : Convert.ToInt64(re.GetValue(i)));
        }


        /// <summary>Gets the short integer value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="i">Column index.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static short GetShort(this IDataReader re, int i, short nullv = 0)
        {
            return (re.IsDBNull(i) ? nullv : Convert.ToInt16(re.GetValue(i)));
        }


        /// <summary>Gets the short integer value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="c">Column name.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static short GetShort(this IDataReader re, string c, short nullv = 0)
        {
            int i = re.GetOrdinal(c);
            return (re.IsDBNull(i) ? nullv : Convert.ToInt16(re.GetValue(i)));
        }


        /// <summary>Gets the double value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="i">Column index.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static double GetDbl(this IDataReader re, int i, double nullv = 0d)
        {
            return (re.IsDBNull(i) ? nullv : Convert.ToDouble(re.GetValue(i)));
        }


        /// <summary>Gets the double value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="c">Column name.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static double GetDbl(this IDataReader re, string c, double nullv = 0d)
        {
            int i = re.GetOrdinal(c);
            return (re.IsDBNull(i) ? nullv : Convert.ToDouble(re.GetValue(i)));
        }


        /// <summary>Gets the boolean value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="i">Column index.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static bool GetBool(this IDataReader re, int i, bool nullv = false)
        {
            if(re.IsDBNull(i)) return nullv;

            object rval = re.GetValue(i);

            if(rval is bool) { return ((bool) rval); }

            long l;
            if(long.TryParse(rval.ToString(), out l)) { return l != 0; }

            return (rval.ToString().ToLower().StartsWith("t") || rval.ToString().ToLower().StartsWith("y"));
        }


        /// <summary>Gets the short integer value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="c">Column name.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static bool GetBool(this IDataReader re, string c, bool nullv = false)
        {
            return re.GetBool(re.GetOrdinal(c), nullv);
        }


        /// <summary>Gets the datetime value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="i">Column index.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static DateTime GetTimestamp(this IDataReader re, int i, DateTime nullv)
        {
            return (re.IsDBNull(i) ? nullv : re.GetDateTime(i));
        }


        /// <summary>Gets the datetime value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="i">Column index.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns DateTime.MinValue.</returns>
        public static DateTime GetTimestamp(this IDataReader re, int i)
        {
            return (re.IsDBNull(i) ? DateTime.MinValue : re.GetDateTime(i));
        }


        /// <summary>Gets the datetime value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="c">Column name.</param>
        /// <param name="nullv">Null value.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns the null value.</returns>
        public static DateTime GetTimestamp(this IDataReader re, string c, DateTime nullv)
        {
            int i = re.GetOrdinal(c);
            return (re.IsDBNull(i) ? nullv : re.GetDateTime(i));
        }


        /// <summary>Gets the datetime value of a column.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="c">Column name.</param>
        /// <returns>Returns the value of the column, if the value is NULL returns DateTime.MinValue.</returns>
        public static DateTime GetTimestamp(this IDataReader re, string c)
        {
            int i = re.GetOrdinal(c);
            string s = re.GetString(i);
            return (re.IsDBNull(i) ? DateTime.MinValue : re.GetDateTime(i));
        }


        /// <summary>Creates a data table from a query.</summary>
        /// <param name="cn">Database connection.</param>
        /// <param name="query">Query string.</param>
        /// <returns>Data table.</returns>
        public static DataTable CreateDataTable(this IDbConnection cn, string query)
        {
            IDbCommand cmd = cn.CreateCommand(query);
            IDataReader re = cmd.ExecuteReader();

            DataTable rval = new DataTable();
            for(int i = 0; i < re.FieldCount; i++) { rval.Columns.Add(re.GetName(i), re.GetFieldType(i)); }

            while(re.Read())
            {
                DataRow r = rval.NewRow();
                for(int i = 0; i < re.FieldCount; i++)
                {
                    r[i] = re.GetValue(i);
                }
                rval.Rows.Add(r);
            }
            Disposal.Dispose(re, cmd);

            return rval;
        }


        /// <summary>Creates a data table from a query.</summary>
        /// <param name="provider">Database provider.</param>
        /// <param name="query">Query string.</param>
        /// <returns>Data table.</returns>
        public static DataTable CreateDataTable(this IProvider provider, string query)
        {
            IDbCommand cmd = provider.CreateCommand(query);
            IDataReader re = cmd.ExecuteReader();

            DataTable rval = new DataTable();
            for(int i = 0; i < re.FieldCount; i++) { rval.Columns.Add(re.GetName(i), re.GetFieldType(i)); }

            while(re.Read())
            {
                DataRow r = rval.NewRow();
                for(int i = 0; i < provider.CountFields(re); i++)
                {
                    r[i] = re.GetValue(i);
                }
                rval.Rows.Add(r);
            }
            Disposal.Dispose(re, cmd);

            return rval;
        }
    }
}
