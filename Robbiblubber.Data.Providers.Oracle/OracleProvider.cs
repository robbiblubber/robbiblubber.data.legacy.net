﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using Oracle.ManagedDataAccess.Client;

using Robbiblubber.Data.DDL;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Providers.Oracle
{
    /// <summary>This class implements the Oracle database provider.</summary>
    public class OracleProvider: ProviderBase, IProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public OracleProvider(): base()
        {
            _Parser = new OracleParser();
        }

        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Provider configuration data.</param>
        public OracleProvider(string data): base(data)
        {
            _Parser = new OracleParser();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a provider for a connection string.</summary>
        /// <param name="connectionString">Connection string.</param>
        /// <returns>Oracle provider object.</returns>
        public static OracleProvider ForConnectionsString(string connectionString)
        {
            Ddp ddp = new Ddp();
            ddp.Set("enhanced", connectionString);

            return new OracleProvider(ddp.ToString());
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the data source.</summary>
        public string DataSource { get; set; }

        
        /// <summary>Gets or sets the user ID.</summary>
        public string UserID { get; set; }


        /// <summary>Gets or sets the password.</summary>
        [PasswordPropertyTextAttribute]
        public string Password { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the table DDL for a number of tables.</summary>
        /// <param name="schemaName">Schama name.</param>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table DDL.</returns>
        public virtual IDDLTable[] GetTables(string schemaName, string filter)
        {
            List<IDDLTable> rval = new List<IDDLTable>();
            foreach(string i in GetTablesNames(schemaName, filter)) { rval.Add(GetTable(i)); }

            return rval.ToArray();
        }


        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="schemaName">Schema name.</param>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        public virtual IDDLTable GetTable(string schemaName, string tableName)
        {
            return GetTable(schemaName + "." + tableName);
        }


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="schemaName">Schema name.</param>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        public virtual string[] GetTablesNames(string schemaName, string filter)
        {
            if(filter == null) { filter = "%"; }
            filter = filter.Replace("*", "%").Replace("?", "_");

            IDbCommand cmd;
            List<string> rval = new List<string>();

            if(schemaName == null)
            {
                cmd = CreateCommand("SELECT CURRENT_USER FROM DUAL");
                schemaName = (string) cmd.ExecuteScalar();
                Disposal.Dispose(cmd);
            }

            cmd = CreateCommand("SELECT TABLE_NAME FROM SYS.ALL_TABLES WHERE OWNER = :s AND TABLE_NAME LIKE :f ORDER BY TABLE_NAME");
            cmd.AddParameter(":s", schemaName);
            cmd.AddParameter(":f", filter);
            IDataReader re = cmd.ExecuteReader();

            while(re.Read())
            {
                rval.Add(schemaName + "." + re.GetString(0));
            }
            Disposal.Dispose(re, cmd);

            return rval.ToArray();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLProvider                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the provider configuration data.</summary>
        public override string Data
        {
            get
            {
                Ddp ddp = new Ddp();

                ddp["provider"].Value = ProviderName;
                ddp["tns"].Value = DataSource;
                ddp["userid"].Value = UserID;
                ddp["password"].Value = Password;
                ddp["enhanced"].Value = EnhancedString;

                return ddp.ToString();
            }
            set
            {
                Ddp ddp = new Ddp(value);

                DataSource = ddp["tns"];
                UserID = ddp["userid"];
                Password = ddp["password"];
                EnhancedString = ddp["enhanced"];
            }
        }


        /// <summary>Connects to the database.</summary>
        public override void Connect()
        {
            if(string.IsNullOrWhiteSpace(EnhancedString))
            {
                _Connection = new OracleConnection("Data Source=" + DataSource + ";User ID=" + UserID + ";Password=" + Password + ";");
            }
            else
            {
                _Connection = new OracleConnection(EnhancedString);
            }

            _Connection.Open();
        }



        /// <summary>Gets the table DDL for a table.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>Table DDL.</returns>
        public override IDDLTable GetTable(string tableName)
        {
            string[] tab = tableName.Split('.');

            DDLTable rval = new DDLBuilder(Parser).AddTable(tableName);

            IDbCommand cmd = CreateCommand("SELECT COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DATA_PRECISION, DATA_SCALE, NULLABLE FROM SYS.ALL_TAB_COLUMNS WHERE OWNER = :s AND TABLE_NAME = :t ORDER BY COLUMN_ID");
            cmd.AddParameter(":s", tab[0]);
            cmd.AddParameter(":t", tab[1]);
            IDataReader re = cmd.ExecuteReader();

            while(re.Read())
            {
                string ty = (re.GetString(1));
                if(re.IsDBNull(3))
                {
                    if(!re.IsDBNull(2)) { ty += ("(" +  re.GetInt32(2).ToString() + ")"); }
                }
                else { ty += ("(" + (re.IsDBNull(4) ? re.GetInt32(3).ToString() : re.GetInt32(3).ToString() + "," + re.GetInt32(4).ToString()) + ")"); }
                
                rval.AddColumn(re.GetString(0), ((OracleParser) Parser).ToSQLDataType(ty), re.GetString(5).ToUpper() == "NO");
            }
            Disposal.Dispose(re, cmd);

            List<string> tmp = new List<string>();
            cmd = CreateCommand("SELECT C.COLUMN_NAME FROM SYS.ALL_CONS_COLUMNS C, SYS.ALL_CONSTRAINTS X WHERE X.CONSTRAINT_TYPE = 'P' AND  C.CONSTRAINT_NAME = X.CONSTRAINT_NAME AND X.CONSTRAINT_TYPE IN ('P', 'R') AND X.OWNER = :s AND X.TABLE_NAME = :t AND C.TABLE_NAME = :t");
            cmd.AddParameter(":s", tab[0]);
            cmd.AddParameter(":t", tab[1]);
            re = cmd.ExecuteReader();

            while(re.Read())
            {
                tmp.Add(re.GetString(0));
            }
            Disposal.Dispose(re, cmd);
            if(tmp.Count > 0) rval.AddPrimaryKey(tmp.ToArray());

            cmd = CreateCommand("SELECT X.CONSTRAINT_NAME, C.COLUMN_NAME, T.OWNER, T.TABLE_NAME, T.COLUMN_NAME FROM SYS.ALL_CONS_COLUMNS T, SYS.ALL_CONS_COLUMNS C, SYS.ALL_CONSTRAINTS X WHERE C.OWNER = :s AND C.TABLE_NAME = :t AND C.CONSTRAINT_NAME = X.CONSTRAINT_NAME AND X.CONSTRAINT_TYPE = 'R' AND X.R_CONSTRAINT_NAME = T.CONSTRAINT_NAME");
            cmd.AddParameter(":s", tab[0]);
            cmd.AddParameter(":t", tab[1]);
            re = cmd.ExecuteReader();

            while(re.Read())
            {
                rval.AddNamedForeignKey(re.GetString(0), re.GetString(1), re.GetString(2) + "." + re.GetString(3), re.GetString(4));
            }
            Disposal.Dispose(re, cmd);

            cmd = CreateCommand("SELECT INDEX_NAME, UNIQUENESS FROM SYS.ALL_INDEXES WHERE TABLE_OWNER = :s AND TABLE_NAME = :t ORDER BY INDEX_NAME");
            cmd.AddParameter(":s", tab[0]);
            cmd.AddParameter(":t", tab[1]);
            re = cmd.ExecuteReader();

            List<Tuple<string, bool>> idx = new List<Tuple<string, bool>>();
            while(re.Read())
            {
                idx.Add(new Tuple<string, bool>(re.GetString(0), re.GetString(1) == "UNIQUE"));
            }
            Disposal.Dispose(re, cmd);

            foreach(Tuple<string, bool> i in idx)
            {
                cmd = CreateCommand("SELECT COLUMN_NAME FROM SYS.ALL_IND_COLUMNS WHERE TABLE_OWNER = :s AND TABLE_NAME = :t AND INDEX_NAME = :i ORDER BY COLUMN_POSITION");
                cmd.AddParameter(":s", tab[0]);
                cmd.AddParameter(":t", tab[1]);
                cmd.AddParameter(":i", i.Item1);
                re = cmd.ExecuteReader();

                List<string> cols = new List<string>();
                while(re.Read())
                {
                    cols.Add(re.GetString(0));
                }
                Disposal.Dispose(re, cmd);

                if(i.Item2) { rval.AddNamedUniqueIndex(i.Item1, cols.ToArray()); } else { rval.AddNamedIndex(i.Item1, cols.ToArray()); }
            }

            return rval;
        }


        /// <summary>Gets the table names for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table names.</returns>
        public override string[] GetTablesNames(string filter)
        {
            string[] tab = (filter.Contains(".") ? filter.Split('.') : (new string[] { null, filter }));
            return GetTablesNames(tab[0], tab[1]);
        }


        /// <summary>Gets the table DDL for a number of tables.</summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Table DDL.</returns>
        public override IDDLTable[] GetTables(string filter)
        {
            string[] tab = (filter.Contains(".") ? filter.Split('.') : (new string[] { null, filter }));
            return GetTables(tab[0], tab[1]);
        }
    }
}
