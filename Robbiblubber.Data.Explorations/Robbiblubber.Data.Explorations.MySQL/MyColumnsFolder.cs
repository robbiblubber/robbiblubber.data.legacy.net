﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MySQL
{
    /// <summary>This class represents a MySQL columns folder.</summary>
    public class MyColumnsFolder: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Folder name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="host">Host.</param>
        /// <param name="table">Parent table.</param>
        internal MyColumnsFolder(MyHost host, MyTable table)
        {
            Host = host;
            _Parent = Table = table;
            _Name = "Columns";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the host instance.</summary>
        public MyHost Host { get; private set; }


        /// <summary>Gets the parent table.</summary>
        public MyTable Table { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.folder; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "my::folder"; }
        }


        /// <summary>Loads child objects.</summary>
        protected override void _Load()
        {
            List<string> fk = new List<string>();

            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Host.Provider.CreateCommand();
                cmd.CommandText = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_SCHEMA = '" + Table.Folder.Database.Name + "' AND TABLE_NAME = '" + Table.Name + "' AND REFERENCED_TABLE_NAME IS NOT NULL";
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    fk.Add(re.GetString(0));
                }
            }
            catch(Exception) {}

            Disposal.Dispose(re, cmd);

            _Children = new List<DbItem>();

            try
            {
                cmd = Host.Provider.CreateCommand();
                cmd.CommandText = "DESCRIBE " + Table.Folder.Database.Name + "." + Table.Name;
                re = cmd.ExecuteReader();
                while(re.Read())
                {
                    _Children.Add(new MyColumn(Host, this, re.GetString(0), re.GetString(1).ToUpper(), (re.GetString(3) == "PRI"), fk.Contains(re.GetString(0)), (re.GetString(2) == "NO")));
                }
            }
            catch(Exception) {}

            Disposal.Dispose(re, cmd);
        }
    }
}
