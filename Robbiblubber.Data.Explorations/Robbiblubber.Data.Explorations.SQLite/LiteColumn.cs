﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.SQLite
{
    /// <summary>This class represents a SQLite column.</summary>
    public class LiteColumn: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Column name.</summary>
        private string _Name;


        /// <summary>Referenced table.</summary>
        private string _ReferencedTable = null;


        /// <summary>Referenced column.</summary>
        private string _ReferencedColumn = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="database">Database.</param>
        /// <param name="folder">Parent folder.</param>
        /// <param name="name">Name.</param>
        /// <param name="dataType">Data type.</param>
        /// <param name="pk">Primary key.</param>
        /// <param name="fk">Foreign key.</param>
        /// <param name="notNull">Not null.</param>
        internal LiteColumn(LiteDatabase database, LiteColumnsFolder folder, string name, string dataType, bool pk, bool fk, bool notNull)
        {
            Database = database;
            _Parent = Folder = folder;
            _Name = name;
            DataType = dataType;
            IsPrimaryKey = pk;
            IsForeignKey = fk;
            IsNotNull = notNull;

            if(fk) { _Read(); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database instance.</summary>
        public LiteDatabase Database { get; private set; }


        /// <summary>Gets the parent folder.</summary>
        public LiteColumnsFolder Folder { get; private set; }


        /// <summary>Gets the data type.</summary>
        public string DataType { get; private set; }


        /// <summary>Gets if the field is a primary key.</summary>
        public bool IsPrimaryKey { get; private set; }


        /// <summary>Gets if the field is a foreign key.</summary>
        public bool IsForeignKey { get; private set; }


        /// <summary>Gets if the field is not nullable.</summary>
        public bool IsNotNull { get; private set; }


        /// <summary>Gets the referenced table.</summary>
        public string ReferencedTable
        {
            get
            {
                return _ReferencedTable;
            }
        }


        /// <summary>Gets the referenced column.</summary>
        public string ReferencedColumn
        {
            get
            {
                return _ReferencedColumn;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Reads referenced table and column.</summary>
        private void _Read()
        {
            IDbCommand cmd = null;
            IDataReader re = null;

            try
            {
                cmd = Database.Provider.CreateCommand("PRAGMA Foreign_Key_List(" + Folder.Table.Name + ")");
                re = cmd.ExecuteReader();

                while(re.Read())
                {
                    if(re.GetString(3) == Name)
                    {
                        _ReferencedTable = re.GetString(2);
                        _ReferencedColumn = re.GetString(4);
                        break;
                    }
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX00100", ex); }
            
            Disposal.Dispose(re, cmd);

            if(string.IsNullOrWhiteSpace(DataType))
            {
                try
                {
                    cmd = Database.Provider.CreateCommand("PRAGMA Table_Info(" + _ReferencedTable + ")");
                    re = cmd.ExecuteReader();

                    while(re.Read())
                    {
                        if(re.GetString(1) == _ReferencedColumn)
                        {
                            DataType = re.GetString(2);
                            break;
                        }
                    }
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX00101", ex); }

                Disposal.Dispose(re, cmd);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get
            {
                if(Folder.Table is LiteView) { return Resources.column_view; }

                if(IsPrimaryKey) { return Resources.column_pk; }
                if(IsForeignKey) { return Resources.column_fk; }
                if(IsNotNull) { return Resources.column_nn; }

                return Resources.column;
            }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get
            {
                if(Folder.Table is LiteView) { return "lite::column/view"; }

                if(IsPrimaryKey) { return "lite::column/pk"; }
                if(IsForeignKey) { return "lite::column/fk"; }
                if(IsNotNull) { return "lite::column/nn"; }

                return "lite::column";
            }
        }


        /// <summary>Gets if the object has children.</summary>
        public override bool HasChildren
        {
            get { return false; }
        }


        /// <summary>Gets the item children.</summary>
        public override IEnumerable<DbItem> Children
        {
            get { return null; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get
            {
                string rval = Name + "   " + DataType;
                if(IsNotNull)    { rval += "   NOT NULL"; }
                if(IsPrimaryKey) { rval += "   PRIMARY KEY"; }
                if(IsForeignKey) { rval += "   REFERENCES " + ReferencedTable + "(" + ReferencedColumn + ")"; }
                rval += ";";

                return rval;
            }
        }
    }
}
