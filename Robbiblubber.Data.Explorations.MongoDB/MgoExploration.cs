﻿using System;
using System.Collections.Generic;

using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Explorations.MongoDB
{
    /// <summary>This class implements an exploration provider for MongoDB.</summary>
    public class MgoExploration: IExploration
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Explorer dictionary.</summary>
        protected Dictionary<ProviderItem, IExporer> _Explorers = new Dictionary<ProviderItem, IExporer>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MgoExploration()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbExploration                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the explorer for a provider.</summary>
        /// <param name="provider">Provider.</param>
        /// <returns>Explorer.</returns>
        public IExporer this[ProviderItem provider]
        {
            get
            {
                if(!_Explorers.ContainsKey(provider))
                {
                    if(provider.ProviderName == "Robbiblubber.Data.Providers.MongoDB.MongoProvider")
                    {
                        IExporer x = new MgoExplorer(provider);
                        _Explorers.Add(provider, x);

                        return x;
                    }
                }

                try
                {
                    return _Explorers[provider];
                }
                catch(Exception) { return null; }
            }
        }
    }
}
